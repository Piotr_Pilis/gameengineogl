#include <gli/gli.hpp>
#include "Texture/bitmap.hpp"
#include "Texture/texture.hpp"
#include "Texture/textureReader.hpp"
#include "Texture/asyncTextureUpdater.hpp"
#include "asyncTextureUpdaterTest.hpp"
#include "Log/glStatus.hpp"

namespace UnitTests
{
    namespace Test
    {
        const GLint numUncompressedPixelsToUpdate = 5;
        const GLint numPixelsToCheck = 5;
        const GLint numCompressedBlocksToUpdate = 1;
        const GLint numCompressedBlocksToCheck = 1;

        AsyncTextureUpdaterTest::AsyncTextureUpdaterTest()
        {
            // Resources
            m_imageResources = std::make_unique<Corrade::Utility::Resource>("images");

            // Bitmaps
            m_bitmapData = m_imageResources->getRaw("bitmap.dds");
            m_compressedBitmapData = m_imageResources->getRaw("compressedBitmap.dds");
            
            // Tests
            addTests({ 
                &AsyncTextureUpdaterTest::uncompressedTextureUpdateFromBeginTest,
                &AsyncTextureUpdaterTest::uncompressedTextureUpdateWithOffsetTest,
                &AsyncTextureUpdaterTest::uncompressedTextureUpdateWithVerticalOffsetTest,

                &AsyncTextureUpdaterTest::compressedTextureUpdateTest
            });
        }

        void AsyncTextureUpdaterTest::uncompressedTextureUpdateFromBeginTest()
        {
            Engine::Texture::Bitmap bitmap;
            bitmap.loadFromRaw(m_bitmapData, m_bitmapData.size());

            const GLubyte* originalData = (const GLubyte*)bitmap.getData(0, 0, 0);

            Engine::Texture::Texture texture;
            texture.initialize(bitmap);

            Engine::Texture::AsyncTextureUpdater textureUpdater(texture, 1);

            GLubyte* pixels = textureUpdater.startUpdateData(true);
            // from (0, 0)
            for (GLint pixelID = 0; pixelID < numPixelsToCheck; ++pixelID)
                for (GLint channelID = 0; channelID < 3; ++channelID)
                    pixels[pixelID * 3 + channelID] = static_cast<GLubyte>(pixelID * 3 + channelID) % 255;
            textureUpdater.endUpdateData();

            textureUpdater.setDataSizeToUpdate(numUncompressedPixelsToUpdate, 1);
            textureUpdater.updateTexture();

            // To synchronize
            textureUpdater.startUpdateData(true);
            textureUpdater.endUpdateData();

            Engine::Texture::TextureReader textureReader(texture);
            textureReader.readPixels();

            GLubyte* receivedData = (GLubyte*)textureReader.getData();
            CORRADE_VERIFY(receivedData != nullptr);

            // from (0, 0)
            for (GLint pixelID = 0; pixelID < numUncompressedPixelsToUpdate + numPixelsToCheck; ++pixelID)
                for (GLint channelID = 0; channelID < 3; ++channelID)
                {
                    if (pixelID < numUncompressedPixelsToUpdate)
                        CORRADE_COMPARE(
                            receivedData[pixelID * 3 + channelID],
                            static_cast<GLubyte>(pixelID * 3 + channelID) % 255
                        );
                    else
                        CORRADE_COMPARE(
                            receivedData[pixelID * 3 + channelID],
                            originalData[pixelID * 3 + channelID]
                        );
                }

            // to (end, 0)
            for (GLint pixelID = 0; pixelID < numUncompressedPixelsToUpdate + numPixelsToCheck; ++pixelID)
                for (GLint channelID = 0; channelID < 3; ++channelID)
                {
                    CORRADE_COMPARE(
                        receivedData[bitmap.getSize(0) - (pixelID * 3 + channelID) - 1],
                        originalData[bitmap.getSize(0) - (pixelID * 3 + channelID) - 1]
                    );
                }
            CHECK_GL_ERROR();
        }

        void AsyncTextureUpdaterTest::uncompressedTextureUpdateWithOffsetTest()
        {
            Engine::Texture::Bitmap bitmap;
            bitmap.loadFromRaw(m_bitmapData, m_bitmapData.size());

            const GLubyte* originalData = (const GLubyte*)bitmap.getData(0, 0, 0);

            Engine::Texture::Texture texture;
            texture.initialize(bitmap);

            Engine::Texture::AsyncTextureUpdater textureUpdater(texture, 1);

            GLubyte* pixels = textureUpdater.startUpdateData(true);
            // from (0, 0)
            for (GLint pixelID = 0; pixelID < numPixelsToCheck; ++pixelID)
                for (GLint channelID = 0; channelID < 3; ++channelID)
                    pixels[pixelID * 3 + channelID] = (static_cast<GLubyte>(pixelID * 3 + channelID) * 50) % 255;
            textureUpdater.endUpdateData();

            textureUpdater.setDataSizeToUpdate(numUncompressedPixelsToUpdate, 1);
            textureUpdater.setBeginTexturePtrAdress(bitmap.getExtent().x - numPixelsToCheck, bitmap.getExtent().y - 1);
            textureUpdater.updateTexture();

            // To synchronize
            textureUpdater.startUpdateData(true);
            textureUpdater.endUpdateData();

            Engine::Texture::TextureReader textureReader(texture);
            textureReader.readPixels();

            GLubyte* receivedData = (GLubyte*)textureReader.getData();
            CORRADE_VERIFY(receivedData != nullptr);

            // from (0, 0)
            for (GLint pixelID = 0; pixelID < numUncompressedPixelsToUpdate + numPixelsToCheck; ++pixelID)
                for (GLint channelID = 0; channelID < 3; ++channelID)
                {
                    CORRADE_COMPARE(
                        receivedData[pixelID * 3 + channelID],
                        originalData[pixelID * 3 + channelID]
                    );
                }

            // to (end, 0)
            for (GLint pixelID = 0; pixelID < numUncompressedPixelsToUpdate + numPixelsToCheck; ++pixelID)
                for (GLint channelID = 0; channelID < 3; ++channelID)
                {
                    if(pixelID < numUncompressedPixelsToUpdate)
                        CORRADE_COMPARE(
                            receivedData[bitmap.getSize(0) - (pixelID * 3 + channelID) - 1],
                            (static_cast<GLubyte>(3* numUncompressedPixelsToUpdate - 1 - pixelID * 3 - channelID) * 50) % 255
                        );
                    else
                        CORRADE_COMPARE(
                            receivedData[bitmap.getSize(0) - (pixelID * 3 + channelID) - 1],
                            originalData[bitmap.getSize(0) - (pixelID * 3 + channelID) - 1]
                        );
                }
            CHECK_GL_ERROR();
        }

        void AsyncTextureUpdaterTest::uncompressedTextureUpdateWithVerticalOffsetTest()
        {
            Engine::Texture::Bitmap bitmap;
            bitmap.loadFromRaw(m_bitmapData, m_bitmapData.size());
            const GLubyte* originalData = (const GLubyte*)bitmap.getData(0, 0, 0);

            Engine::Texture::Texture texture;
            texture.initialize(bitmap);

            Engine::Texture::AsyncTextureUpdater textureUpdater(texture, 1);

            GLubyte* pixels = textureUpdater.startUpdateData(true);
            // from (0, 0)
            for (GLint pixelID = 0; pixelID < numPixelsToCheck; ++pixelID)
                for (GLint channelID = 0; channelID < 3; ++channelID)
                    pixels[pixelID * 3 + channelID] = static_cast<GLubyte>(pixelID * 3 + channelID + 40) % 255;
            textureUpdater.endUpdateData();

            textureUpdater.setDataSizeToUpdate(1, numUncompressedPixelsToUpdate);
            textureUpdater.setBeginTexturePtrAdress(3, 0);
            textureUpdater.updateTexture();

            // To synchronize
            textureUpdater.startUpdateData(true);
            textureUpdater.endUpdateData();

            Engine::Texture::TextureReader textureReader(texture);
            textureReader.readPixels();

            GLubyte* receivedData = (GLubyte*)textureReader.getData();
            CORRADE_VERIFY(receivedData != nullptr);

            // from (0, 0)
            for (GLint pixelID = 0; pixelID < numUncompressedPixelsToUpdate + numPixelsToCheck; ++pixelID)
                for (GLint channelID = 0; channelID < 3; ++channelID)
                {
                    //Corrade::Utility::Debug{} << receivedData[pixelID * 3 + channelID] << "\t";
                    if (pixelID == 3)
                        CORRADE_COMPARE(
                            receivedData[pixelID * 3 + channelID],
                            static_cast<GLubyte>((pixelID - 3) * 3 + channelID + 40) % 255
                        );
                    else
                        CORRADE_COMPARE(
                            receivedData[pixelID * 3 + channelID],
                            originalData[pixelID * 3 + channelID]
                        );
                }

            // to (end, 0)
            for (GLint pixelID = 0; pixelID < numUncompressedPixelsToUpdate + numPixelsToCheck; ++pixelID)
                for (GLint channelID = 0; channelID < 3; ++channelID)
                {
                    CORRADE_COMPARE(
                        receivedData[bitmap.getSize(0) - (pixelID * 3 + channelID) - 1],
                        originalData[bitmap.getSize(0) - (pixelID * 3 + channelID) - 1]
                    );
                }
            CHECK_GL_ERROR();
        }

        void AsyncTextureUpdaterTest::compressedTextureUpdateTest()
        {
            Engine::Texture::Bitmap bitmap;
            bitmap.loadFromRaw(m_compressedBitmapData, m_compressedBitmapData.size());

            const GLubyte* originalData = (const GLubyte*)bitmap.getData(0, 0, 0);

            Engine::Texture::Texture texture;
            texture.initialize(bitmap);

            Engine::Texture::AsyncTextureUpdater textureUpdater(texture, 1);

            GLubyte* pixels = textureUpdater.startUpdateData(true);
            // from (0, 0)
            for (GLint byteID = 0; byteID < numCompressedBlocksToUpdate * 8; ++byteID)
                pixels[byteID] = static_cast<GLubyte>(byteID) % 255;
            textureUpdater.endUpdateData();

            textureUpdater.setDataSizeToUpdate(numCompressedBlocksToUpdate * 4, 1 * 4);
            textureUpdater.updateTexture();

            // To synchronize
            textureUpdater.startUpdateData(true);
            textureUpdater.endUpdateData();

            Engine::Texture::TextureReader textureReader(texture);
            textureReader.readPixels();

            GLubyte* receivedData = (GLubyte*)textureReader.getData();
            CORRADE_VERIFY(receivedData != nullptr);

            // from (0, 0)
            for (GLint byteID = 0; byteID < (numCompressedBlocksToUpdate + numPixelsToCheck) * 8; ++byteID)
            {
                if (byteID < numCompressedBlocksToUpdate * 8)
                    CORRADE_COMPARE(
                        receivedData[byteID],
                        static_cast<GLubyte>(byteID) % 255
                    );
                else
                    CORRADE_COMPARE(
                        receivedData[byteID],
                        originalData[byteID]
                    );
            }
            CHECK_GL_ERROR();
        }
    }
}