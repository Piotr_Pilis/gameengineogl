#include <gli/gli.hpp>
#include "Texture/bitmap.hpp"
#include "Texture/texture.hpp"
#include "Texture/textureReader.hpp"
#include "textureReaderTest.hpp"

namespace UnitTests
{
    namespace Test
    {
        const GLint numPixelsToCheck = 10;

        TextureReaderTest::TextureReaderTest()
        {
            // Resources
            m_imageResources = std::make_unique<Corrade::Utility::Resource>("images");

            // Bitmaps
            m_bitmapData = m_imageResources->getRaw("bitmap.dds");
            m_compressedBitmapData = m_imageResources->getRaw("compressedBitmap.dds");

            // Tests
            addTests({ 
                &TextureReaderTest::uncompressedTextureReadTest,
                &TextureReaderTest::compressedTextureReadTest
            });
        }

        void TextureReaderTest::uncompressedTextureReadTest()
        {
            Engine::Texture::Bitmap bitmap;
            bitmap.loadFromRaw(m_bitmapData, m_bitmapData.size());

            Engine::Texture::Texture texture;
            texture.initialize(bitmap);

            Engine::Texture::TextureReader textureReader(texture);
            textureReader.readPixels();

            const GLubyte* originalData = (const GLubyte*)bitmap.getData(0, 0, 0);

            GLubyte* receivedData = (GLubyte*)textureReader.getData();
            CORRADE_VERIFY(receivedData != nullptr);

            // from (0, 0)
            for (GLint pixelID = 0; pixelID < numPixelsToCheck; ++pixelID)
                for (GLint channelID = 0; channelID < 3; ++channelID)
                {
                    CORRADE_COMPARE(receivedData[pixelID * 3 + channelID], 
                        originalData[pixelID * 3 + channelID]);
                }

            // to (end, 0)
            for (GLint pixelID = 0; pixelID < numPixelsToCheck; ++pixelID)
                for (GLint channelID = 0; channelID < 3; ++channelID)
                {
                    CORRADE_COMPARE(receivedData[bitmap.getSize(0) - (pixelID * 3 + channelID) - 1], 
                        originalData[bitmap.getSize(0) - (pixelID * 3 + channelID) - 1]);
                }
        }

        void TextureReaderTest::compressedTextureReadTest()
        {
            Engine::Texture::Bitmap bitmap;
            bitmap.loadFromRaw(m_compressedBitmapData, m_compressedBitmapData.size());

            const GLubyte* originalData = (const GLubyte*)bitmap.getData(0, 0, 0);

            Engine::Texture::Texture texture;
            texture.initialize(bitmap);

            Engine::Texture::TextureReader textureReader(texture);
            textureReader.readPixels();

            GLubyte* receivedData = (GLubyte*)textureReader.getData();
            CORRADE_VERIFY(receivedData != nullptr);

            // from (0, 0)
            for (GLint pixelID = 0; pixelID < numPixelsToCheck; ++pixelID)
                for (GLint channelID = 0; channelID < 3; ++channelID)
                {
                    CORRADE_COMPARE(receivedData[pixelID * 3 + channelID], 
                        originalData[pixelID * 3 + channelID]);
                }

            // to (end, 0)
            for (GLint pixelID = 0; pixelID < numPixelsToCheck; ++pixelID)
                for (GLint channelID = 0; channelID < 3; ++channelID)
                {
                    CORRADE_COMPARE(receivedData[bitmap.getSize(0) - (pixelID * 3 + channelID) - 1], 
                        originalData[bitmap.getSize(0) - (pixelID * 3 + channelID) - 1]);
                }
        }
    }
}