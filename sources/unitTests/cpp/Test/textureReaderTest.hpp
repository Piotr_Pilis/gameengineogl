#include <Corrade/TestSuite/Tester.h>
#include <Corrade/Utility/Resource.h>
#include <memory>

namespace UnitTests
{
    namespace Test
    { 
        class TextureReaderTest : 
            public Corrade::TestSuite::Tester
        {
        public:
            explicit TextureReaderTest();

            void uncompressedTextureReadTest();
            void compressedTextureReadTest();

        private:
            std::unique_ptr<Corrade::Utility::Resource> m_imageResources;

            Corrade::Containers::ArrayView<const char> m_bitmapData;
            Corrade::Containers::ArrayView<const char> m_compressedBitmapData;

        };
    }
}