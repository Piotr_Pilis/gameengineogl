#include <Corrade/TestSuite/Tester.h>
#include <Corrade/Utility/Resource.h>
#include <memory>

namespace UnitTests
{
    namespace Test
    { 
        class AsyncTextureUpdaterTest : 
            public Corrade::TestSuite::Tester
        {
        public:
            explicit AsyncTextureUpdaterTest();

            void uncompressedTextureUpdateFromBeginTest();
            void uncompressedTextureUpdateWithOffsetTest();
            void uncompressedTextureUpdateWithVerticalOffsetTest();

            void compressedTextureUpdateTest();

        private:
            std::unique_ptr<Corrade::Utility::Resource> m_imageResources;

            Corrade::Containers::ArrayView<const char> m_bitmapData;
            Corrade::Containers::ArrayView<const char> m_compressedBitmapData;

        };
    }
}