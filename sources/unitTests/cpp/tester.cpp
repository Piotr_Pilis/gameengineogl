#include <Corrade/Utility/Endianness.h>
#include <Corrade/Utility/Resource.h>

#include <glad\glad.h>
#include <GLFW\glfw3.h>
#include <iostream>
#include "Log/criticalLog.hpp"

// Tests
#include "Test/textureReaderTest.hpp"
#include "Test/asyncTextureUpdaterTest.hpp"

void initialize()
{
    std::cout << "Initializing unitTest window..." << std::endl;
    if (!glfwInit()) {
        CRITICAL_LOG("Could not start GLFW3");
    }

    GLFWwindow* window = glfwCreateWindow(800, 600, "UnitTestWindow", nullptr, nullptr);
    if (!window)
        CRITICAL_LOG("Could not open window");

    glfwHideWindow(window);
    glfwMakeContextCurrent(window);

    if (!gladLoadGL())
        CRITICAL_LOG("Could not initialize glad");
}

template<class TESTER_OBJECT>
int doTest(int& argc, char** argv)
{
    std::cout << std::endl << "_______________________________________________________________________________________________" << std::endl;
    Corrade::TestSuite::Tester::registerArguments(argc, argv);

    TESTER_OBJECT tester;
    std::string className = typeid(tester).name();
    tester.registerTest(className + ".cpp", className);
    return tester.exec();
}

void printResult(const int& result)
{
    std::cout << std::endl;
    if (result)
        std::cout << "[1;31mSome tests failed![1;39m" << std::endl << std::endl;
    else
        std::cout << "[1;32mAll tests successed![1;39m" << std::endl << std::endl;
}

int main(int argc, char** argv)
{
    initialize();

    // Tests
    int result = 0;
    result |= doTest<UnitTests::Test::TextureReaderTest>(argc, argv);
    result |= doTest<UnitTests::Test::AsyncTextureUpdaterTest>(argc, argv);
    
    printResult(result);
    return result;
}