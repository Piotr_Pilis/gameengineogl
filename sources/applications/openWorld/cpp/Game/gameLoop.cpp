#include <boost/signals2/signal.hpp>
#include <boost/bind/bind.hpp>

#include <glad/glad.h>
#include <glm/gtc/matrix_transform.hpp>
#include <GLFW/glfw3.h>
#include <memory>

#include "Definition/buttonEventState.hpp"

#include "Transform/thirdPersonCameraController.hpp"
#include "Transform/basicCameraController.hpp"

#include "Lighting/DirectionalLight.hpp"
#include "Lighting/PointLight.hpp"

#include "Geometry/objectManager.hpp"
#include "Geometry/model.hpp"

#include "Animation/animationPlayer.hpp"
#include "Animation/animation.hpp"

#include "FileSystem/fileSystem.hpp"
#include "Log/glStatus.hpp"
#include "Map/terrain.hpp"
#include "Event/fps.hpp"
#include "engine.hpp"
#include "world.hpp"

#include "Game/Entity/player.hpp"
#include "gameLoop.hpp"

namespace Application::Game
{
    namespace
    {
        using CameraController =
#if EDIT_CAMERA
            Engine::Transform::BasicCameraController;
#else
            Engine::Transform::ThirdPersonCameraController;
#endif
    }

    GameLoop::GameLoop()
    {
    }

    GameLoop::~GameLoop()
    {
        Engine::FileSystem::deinitialize();
    }

    void GameLoop::initialize(const GLsizei& windowWidth, const GLsizei& windowHeight)
    {
        INITIALIZE_DEBUG_MODE(ASSETS_DIRECTORY);
        Engine::Log::checkCompatibility();

        // -> File system
        {
            Engine::FileSystem::initialize();
            Engine::FileSystem::permitSymbolicLinks(false);
            Engine::FileSystem::loadPath("data");
        }
        // <- File system

        // -> Initializing of engine
        {
            Engine::Engine::getInstance().initializeCanvas(windowWidth, windowHeight);
        }
        // <- Initializing of engine

        // -> Entities
        {
            m_player = std::make_shared<Entity::Player>();
        }
        // <- Entities
        
        // -> Camera controller
        {
            m_mainCameraController = CameraController::make(windowWidth, windowHeight);
            m_mainCameraController->getCamera()->setProjectionMatrix(glm::perspectiveFov(
                0.785f,
                static_cast<GLfloat>(windowWidth),
                static_cast<GLfloat>(windowHeight),
                0.1f, 6000.0f
            ));

            CameraController* mainCameraControllerAsThirdPerson =
                static_cast<CameraController*>(m_mainCameraController.get());
            mainCameraControllerAsThirdPerson->setCameraPosition(glm::vec3(0.0f, 0.0f, 40.0f));
            mainCameraControllerAsThirdPerson->setCameraRotation(-0.7f, 1.5f);

            mainCameraControllerAsThirdPerson->setCameraRotationButton(GLFW_MOUSE_BUTTON_RIGHT);

#if EDIT_CAMERA
            mainCameraControllerAsThirdPerson->setCameraMovingButtonBack(GLFW_KEY_S);
            mainCameraControllerAsThirdPerson->setCameraMovingButtonForward(GLFW_KEY_W);
            mainCameraControllerAsThirdPerson->setCameraMovingButtonLeft(GLFW_KEY_A);
            mainCameraControllerAsThirdPerson->setCameraMovingButtonRight(GLFW_KEY_D);

            mainCameraControllerAsThirdPerson->setCameraMovingButtonTop(GLFW_KEY_R);
            mainCameraControllerAsThirdPerson->setCameraMovingButtonBottom(GLFW_KEY_F);
#else
            mainCameraControllerAsThirdPerson->bindObject3D
            (
                std::dynamic_pointer_cast<Engine::Geometry::Object3D>(m_player)
            );
#endif

            // Setup engine
            Engine::Engine::getInstance().getWorld()->setActiveCameraController(m_mainCameraController);
        }
        // <- Camera controller

        // -> Terrain
        {
            m_terrain = std::make_shared<Engine::Map::Terrain>("Map000");
            Engine::Engine::getInstance().getWorld()->setActiveTerrain(m_terrain);
            //Engine::Engine::getInstance().setTerrainGridVisibility(true);
        }
        // <- Terrain

        // -> Lighting
        {
            auto& lightingManager = Engine::Engine::getInstance().getWorld()->getLightingManager();

            // Directional light
            {
                Engine::Lighting::DirectionalLight directionalLight;
                directionalLight.m_color = { 1.0, 1.0, 1.0 };
                directionalLight.m_direction = { 0.0, -1.0, 0.5 };

                lightingManager.setDirectionalLight(directionalLight);
            }

            // Point light 1
            {
                Engine::Lighting::PointLight pointLight;
                pointLight.m_color = { 1.0f, 0.0f, 0.0f };
                pointLight.m_position = { 300.0f, 200.0f, 00.0f };

                pointLight.m_constantFactor = 0.02f;
                pointLight.m_linearFactor = 0.0f;
                pointLight.m_quadraticFactor = 0.01f;

                m_pointLightId = lightingManager.pushPointLight(pointLight);
            }
        }
        // <- Lighting

        m_fps = std::make_unique<Engine::Event::FPS>();
        m_fps->changed.connect
        (
            decltype(m_fps->changed)::slot_type
            (
                &GameLoop::onFPSChanged,
                this,
                boost::placeholders::_1
            )
        );
    }

    void GameLoop::reinitialize(const GLsizei& windowWidth, const GLsizei& windowHeight)
    {
        m_mainCameraController->reinitialize(windowWidth, windowHeight);
        m_mainCameraController->getCamera()->setProjectionMatrix
        (
            glm::perspectiveFov
            (
                0.785f,
                static_cast<GLfloat>(windowWidth),
                static_cast<GLfloat>(windowHeight),
                0.1f, 6000.0f
            )
        );

        Engine::Engine::getInstance().reinitializeCanvas(windowWidth, windowHeight);
    }

    void GameLoop::onMousePositionChanged(const GLdouble& x, const GLdouble& y)
    {
        m_mainCameraController->onMousePositionChanged(static_cast<GLfloat>(x), static_cast<GLfloat>(y));
    }

    void GameLoop::onMouseButtonClicked(const GLint& mouseButtonID, const GLint& actionType)
    {
        /* Converts */
        using ButtonEventState = Engine::Definition::ButtonEventState;
        ButtonEventState buttonEventState;

        if (actionType == GLFW_PRESS)
            buttonEventState = ButtonEventState::PRESSED;
        else if (actionType == GLFW_RELEASE)
            buttonEventState = ButtonEventState::RELEASED;
        else
            buttonEventState = ButtonEventState::REPEATED;

        m_mainCameraController->onMouseButtonClicked(mouseButtonID, buttonEventState);
    }

    void GameLoop::onKeyClicked(const GLint& keyID, const GLint& actionType)
    {
        /* Converts */
        using ButtonEventState = Engine::Definition::ButtonEventState;
        ButtonEventState buttonEventState;

        if (actionType == GLFW_PRESS)
            buttonEventState = ButtonEventState::PRESSED;
        else if (actionType == GLFW_RELEASE)
            buttonEventState = ButtonEventState::RELEASED;
        else
            buttonEventState = ButtonEventState::REPEATED;

        /* Camera controller */
        m_mainCameraController->onKeyClicked(keyID, buttonEventState);

        /* Lights */
        // Tmp - light position
        if ((keyID == GLFW_KEY_Q || keyID == GLFW_KEY_E) && (buttonEventState == ButtonEventState::PRESSED || buttonEventState == ButtonEventState::REPEATED))
        {
            auto& lightingManager = Engine::Engine::getInstance().getWorld()->getLightingManager();
            auto pointLight = lightingManager.getPointLights()[0];
            pointLight.m_position += glm::vec3(1.0 * (keyID == GLFW_KEY_Q ? -1.0 : 1.0), 0.0, 0.0);
            lightingManager.updatePointLight
            (
                pointLight,
                m_pointLightId
            );
        }

        /* Player movement */
        if (keyID == GLFW_KEY_W 
            || keyID == GLFW_KEY_S
            || keyID == GLFW_KEY_A
            || keyID == GLFW_KEY_D)
        {
            if (buttonEventState == ButtonEventState::RELEASED)
            {
                m_player->stopMove();
            }
            else
            {
                // Direction
                glm::vec3 direction(0.0f, 0.0f, 0.0f);

                // Forward vector
                glm::vec2 forwardVector =  m_mainCameraController->getNormalizedForwardXYVector();

                // Set direction value
                switch (keyID)
                {
                case GLFW_KEY_W:
                    // Forward
                    direction = glm::vec3(forwardVector.x, forwardVector.y, 0.0f);
                    break;
                case GLFW_KEY_S:
                    // Backward
                    direction = -glm::vec3(forwardVector.x, forwardVector.y, 0.0f);
                    break;
                case GLFW_KEY_A:
                case GLFW_KEY_D:
                    // Right vector
                    const auto rightVector = m_mainCameraController->getNormalizedRightXYVector(forwardVector);

                    switch (keyID)
                    {
                    case GLFW_KEY_A:
                        // Left
                        direction = -glm::vec3(rightVector.x, rightVector.y, 0.0f);
                        break;

                    case GLFW_KEY_D:
                        // Right
                        direction = glm::vec3(rightVector.x, rightVector.y, 0.0f);
                        break;
                    }
                    break;
                }

                // Move player actor forward
                m_player->move(direction);
            }
        }
    }

    void GameLoop::onScrolled(const GLdouble& xOffset, const GLdouble& yOffset)
    {
        (void)xOffset;
        m_mainCameraController->onScrolled(yOffset);
    }

    void GameLoop::onFPSChanged(GLuint fps)
    {
        std::cout << "FPS: " << fps << std::endl;
    }

    void GameLoop::tick()
    {
        Engine::Engine::getInstance().tick();
        m_fps->tick();
        //m_terrain->GetTerrainUpdater()->OnCameraPositionChanged({ 1, 2, 3 });
    }
}
