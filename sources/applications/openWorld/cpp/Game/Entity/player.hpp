#pragma once
#include <string>

#include "Entity/character.hpp"
#include "Event/timer.hpp"

namespace Application::Game::Entity
{
    class Player :
        public Engine::Entity::Character
    {
    public:
        static constexpr GLfloat MAX_SPEED = 200.0f;
        static constexpr GLfloat MIN_SPEED = 1.0f;

    private:
        enum class PoseType
        {
            IDLE,
            MOVING,
            RUNNING,
            DEATH
        };

    public:
        Player();
        Player(Player&) = default;
        ~Player() = default;

        /* Actions */
        virtual void update() override;

        /* Setters */
        // Traslation
        virtual void setTranslation(const glm::vec3& translation) override;

        // Movements
        void moveTo(const glm::vec3& targetPosition);
        virtual void move(const glm::vec3& direction) override;
        void stopMove();

        void setSpeed(const GLfloat speed);

        /* Getters */
        bool isMoving() const;

    private:
        void initialize();
        void setPose(PoseType poseType);

    private:
        PoseType m_poseType;

        GLfloat m_speed = 50.0f;
        glm::vec3 m_targetPosition;
        Engine::Event::Timer m_actorMovementTimer;

    };
}