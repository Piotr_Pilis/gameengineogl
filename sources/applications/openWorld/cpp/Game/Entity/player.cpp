
#include <magic_enum.hpp>

#include "Geometry/objectManager.hpp"
#include "Geometry/model.hpp"

#include "Animation/animationPlayer.hpp"
#include "Animation/animation.hpp"

#include "Log/debugLog.hpp"
#include "Log/log.hpp"

#include "engine.hpp"
#include "player.hpp"

namespace Application::Game::Entity
{
    Player::Player()
    {
        // Initialization has to be called later - shared_ptr has to be fully initialized
        Engine::Engine::getInstance().callLater
        (
            std::bind
            (
                &Player::Player::initialize,
                this
            )
        );
    }

    void Player::initialize()
    {
        // Log
        LOG("Initializing player object...");

        // Initialze model
        auto& objectManager = Engine::Engine::getInstance().getObjectManager();
        auto playerModel = objectManager.getModel("player");

        setModel(playerModel);
        setTranslation({ 100.0f, 100.0f, 0.0f });
        setRotation({ 0.0f, 0.0f, 0.0f });
        setRenderingEnabled(true);

        // Set default pose
        setPose(PoseType::IDLE);
    }

    void Player::update()
    {
        // Is actor already moving? 
        if (isMoving())
        {
            // Get current actor position
            const auto currentPosition = getTranslation();

            // Does movement finish?
            if (m_targetPosition == currentPosition)
            {
                stopMove();
            }
            else
            {
                // Calculate direction
                const glm::vec3 direction = m_targetPosition - currentPosition;

                // Calculate distance to target position
                const GLfloat distanceToTargetPosition = glm::length(direction);

                // Calculate current position offset of movement
                GLfloat currentDistanceMovement = m_speed * m_actorMovementTimer.getElapsedSeconds();

                // Is displacement exceeds the position of the target?
                // Yes? Actor cannot move too far!
                if (distanceToTargetPosition < currentDistanceMovement)
                {
                    currentDistanceMovement = distanceToTargetPosition;
                }

                // Calculate new actor position
                const glm::vec3 displacement = glm::normalize(direction) * currentDistanceMovement;

                // Move actor
                Engine::Entity::Character::move
                (
                    displacement
                );

                // Reset movement timer till next tick
                m_actorMovementTimer.resetTimer();
            }
        }

        //
        Engine::Entity::Character::update();
    }

    void Player::setTranslation(const glm::vec3& translation)
    {
        stopMove();
        Engine::Entity::Character::setTranslation(translation);
    }

    void Player::moveTo(const glm::vec3& targetPosition)
    {
        // Set new position where actor will be moved
        m_targetPosition = targetPosition;

        // Was actor moving?
        if (!isMoving())
        {
            // Reset movement timer - new movement
            m_actorMovementTimer.resetTimer();
        }

        // Set rotation
        lookAt
        (
            targetPosition - getTranslation()
        );

        // Set pose (animation etc.)
        setPose(PoseType::MOVING);
    }

    void Player::move(const glm::vec3& direction)
    {
        // Direction should be normalized
        auto normalizedDirection = glm::normalize(direction);

        // Update target position
        moveTo(getTranslation() + normalizedDirection * MAX_SPEED);
    }

    void Player::stopMove()
    {
        // Was actor moving?
        if (!isMoving())
        {
            return;
        }

        // Set new pose value
        setPose(PoseType::IDLE);
    }

    void Player::setSpeed(const GLfloat speed)
    {
        m_speed = speed;
        if (m_speed < MIN_SPEED)
        {
            m_speed = MIN_SPEED;
        }
        if (m_speed > MAX_SPEED)
        {
            m_speed = MAX_SPEED;
        }
    }

    void Player::setPose(PoseType poseType)
    {
        // Does pose changed?
        if (m_poseType == poseType)
        {
            return;
        }

        // Debug log
#ifdef _DEBUG
        const auto currentAnimationTypeName = std::string(magic_enum::enum_name(m_poseType));
        const auto animationTypeName = std::string(magic_enum::enum_name(poseType));
        DEBUG_LOG("Changed pose of player from \"" + currentAnimationTypeName + "\" to \"" + animationTypeName +"\"");
#endif

        // Set new pose value
        m_poseType = poseType;

        /* Update animation */
        auto& objectManager = Engine::Engine::getInstance().getObjectManager();
        static auto animations = objectManager.getOrCreateAnimations("Meshes/boblampclean.md5mesh");
        auto animationPlayer = getAnimationPlayer();

        // Set animation
        animationPlayer->setAnimation(animations[0]);
        animationPlayer->play();
    }

    bool Player::isMoving() const
    {
        return m_poseType == PoseType::MOVING ||
            m_poseType == PoseType::RUNNING;
    }
}