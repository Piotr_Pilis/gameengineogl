#pragma once
#include <memory>

#define EDIT_CAMERA true

namespace Engine
{
    namespace Transform
    {
        class CameraControllerAbstract;
    }

    namespace Map
    {
        class Terrain;
    }

    namespace Event
    {
        class FPS;
    }
}

namespace Application
{
    namespace Entity
    {
        class Player;
    }

    namespace Game
    {
        class GameLoop
        {
        public:
            GameLoop();
            ~GameLoop();

            /* Methods */
            void initialize(const GLsizei& windowWidth, const GLsizei& windowHeight);
            void reinitialize(const GLsizei& windowWidth, const GLsizei& windowHeight);

            void tick();

            /* Slots */
            void onMousePositionChanged(const GLdouble& x, const GLdouble& y);
            void onMouseButtonClicked(const GLint& mouseButtonID, const GLint& actionType);
            void onKeyClicked(const GLint& keyID, const GLint& actionType);
            void onScrolled(const GLdouble& xOffset, const GLdouble& yOffset);

            void onFPSChanged(GLuint fps);

        private:
            std::unique_ptr<Engine::Event::FPS> m_fps;

            std::shared_ptr<Engine::Transform::CameraControllerAbstract> m_mainCameraController;
            std::shared_ptr<Engine::Map::Terrain> m_terrain;

            std::shared_ptr<Entity::Player> m_player;
            GLuint m_pointLightId;

        };
    }
}

