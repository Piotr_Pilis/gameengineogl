#include <glad\glad.h>
#include <GLFW\glfw3.h>
#include <iostream>

#include "Log/log.hpp"
#include "Log/warningLog.hpp"
#include "Log/criticalLog.hpp"

#include "Game/gameLoop.hpp"

#include "window.hpp"

using namespace std;

namespace Application
{
    namespace Window
    {
        namespace
        {
            const GLchar* WINDOW_TITLE = "UNDEFINED TITLE";
            const GLint DEFAULT_WINDOW_WIDTH = 1366;
            const GLint DEFAULT_WINDOW_HEIGHT = 768;

            /*
            const GLint DEFAULT_WINDOW_WIDTH = 3840;
            const GLint DEFAULT_WINDOW_HEIGHT = 2160;
            */
        }

        std::unique_ptr<Game::GameLoop> Window::m_gameLoop = std::make_unique<Game::GameLoop>();
        Window::Window()
        {
            initialize();
        }

        Window::~Window()
        {
            destroyWindow();
        }

        Window& Window::getInstance()
        {
            static Window window;
            return window;
        }

        void Window::loop()
        {
            while (!glfwWindowShouldClose(m_window))
            {
                // Actions
                m_gameLoop->tick();

                glfwSwapBuffers(m_window);
                glfwPollEvents();
            }
        }

        void Window::setVerticalSynchronization(bool enabled)
        {
            glfwSwapInterval(enabled ? 1 : 0);
        }

        const std::pair<GLint, GLint> Window::getWindowSize()
        {
            int width, height;
            // Now, this is bugged
            //glfwGetWindowSize(m_window, &width, &height);
            width = DEFAULT_WINDOW_WIDTH;
            height = DEFAULT_WINDOW_HEIGHT;

            return pair<GLint, GLint>(width, height);
        }

        void Window::initialize()
        {
            Engine::Log::Log::setProjectName(WINDOW_TITLE);
            Engine::Log::Log().clear();
            Engine::Log::WarningLog().clear();
            Engine::Log::CriticalLog().clear();

            LOG("Initializing window...");
            initWindow();

            LOG("Initializing engine...");
            m_gameLoop->initialize(DEFAULT_WINDOW_WIDTH, DEFAULT_WINDOW_HEIGHT);
            setVerticalSynchronization(false);

            initCallbacks();
        }

        void Window::initWindow()
        {
            if (!glfwInit()) {
                CRITICAL_LOG("Could not start GLFW3");
            }

            /*glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
            glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 6);
            glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);*/

            m_window = glfwCreateWindow(DEFAULT_WINDOW_WIDTH, DEFAULT_WINDOW_HEIGHT, WINDOW_TITLE, nullptr, nullptr);
            if (!m_window)
                CRITICAL_LOG("Could not open window");

            glfwMakeContextCurrent(m_window);

            if (!gladLoadGL())
                CRITICAL_LOG("Could not initialize glad");
        }

        void Window::initCallbacks()
        {
            glfwSetKeyCallback(m_window, keyCallback);
            glfwSetCursorPosCallback(m_window, passiveMouseCallback);
            glfwSetMouseButtonCallback(m_window, mouseCallback);
            glfwSetScrollCallback(m_window, scrollCallback);
        }

        void Window::destroyWindow()
        {
            glfwDestroyWindow(m_window);
            glfwTerminate();
        }

        void Window::mouseCallback(GLFWwindow* window, GLint button, GLint action, GLint modifiers)
        {
            m_gameLoop->onMouseButtonClicked(button, action);
        }

        void Window::passiveMouseCallback(GLFWwindow* window, GLdouble x, GLdouble y)
        {
            m_gameLoop->onMousePositionChanged(x, y);
        }

        void Window::keyCallback(GLFWwindow* window, GLint key, GLint scancode, GLint action, GLint modifiers)
        {
            m_gameLoop->onKeyClicked(key, action);
        }

        void Window::scrollCallback(GLFWwindow* window, GLdouble xOffset, GLdouble yOffset)
        {
            m_gameLoop->onScrolled(xOffset, yOffset);
        }
    }
}
