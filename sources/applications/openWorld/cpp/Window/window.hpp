#pragma once
#include <glad/glad.h>
#include <utility>
#include <memory>

struct GLFWwindow;
namespace Application
{
    namespace Game
    {
        class GameLoop;
    }

    namespace Window
    {
        /* Only one instance of Window class should exist */
        class Window
        {
        private:
            Window();
            ~Window();

        public:
            static Window& getInstance();

            // Methods
            void loop();

            /* Sets enabled or disabled Vertical Synchronization */
            void setVerticalSynchronization(bool enabled);

            // Getters/ Setters
            const std::pair<GLint, GLint> getWindowSize();

        private:
            void initialize();
            void initWindow();
            void initCallbacks();

            void destroyWindow();
            static void mouseCallback(GLFWwindow* window, GLint button, GLint action, GLint modifiers);
            static void passiveMouseCallback(GLFWwindow* window, GLdouble x, GLdouble y);
            static void keyCallback(GLFWwindow* window, GLint key, GLint scancode, GLint action, GLint modifiers);
            static void scrollCallback(GLFWwindow* window, GLdouble xOffset, GLdouble yOffset);

        private:
            GLFWwindow* m_window;

            /* It's static variable, because it has to be available for callbacks */
            static std::unique_ptr<Game::GameLoop> m_gameLoop;

        };
    }
}

