#include "window/window.hpp"

int main(int argc, char *argv[])
{
    auto& mainWindow = Application::Window::Window::getInstance();
    mainWindow.loop();
    return 0;
}