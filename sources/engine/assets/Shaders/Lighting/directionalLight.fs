#version 330  

#define AMBIENT_LIGHT_ENABLED 1
#include<Engine/Shaders/Lighting/baseLight.fs>
in vec2 gsTexCoord;

struct DirectionalLight
{
    vec3 color;
    vec3 direction;
};
uniform DirectionalLight directionalLight;

vec3 getFragToLight()
{
    return normalize(directionalLight.direction);
}

vec3 getLightColor()
{
    return directionalLight.color;
}

float getAttenuation(vec3 fragPosition)
{
    // No attenuation for directional light
    return 1.0;
}

void main(void)
{
    computeColor(gsTexCoord);
}