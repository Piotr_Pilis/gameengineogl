#define PI  3.14159265359
//#define AMBIENT_LIGHT_ENABLED 0

out vec4 fragColor;

uniform usampler2D shadingTypeMap;
uniform sampler2D colorShadowMap;
uniform sampler2D normalMap;
uniform sampler2D positionMap;
uniform sampler2D aoSpecularRoughnessHeightMap;

layout(std140) uniform BlockTransform
{
    mat4 vpMatrix;
    mat4 vMatrix;
    vec3 eyeWorldPosition;
};

/*
** The Fresnel equation describes the ratio of surface reflection at different surface angles.
*/
vec3 computeFresnel(float cosTheta, vec3 fresnelColor)
{
    return fresnelColor + (1.0 - fresnelColor) * pow(max(1.0 - cosTheta, 0.0), 5.0);
}  

/*
** Normal distribution function: approximates the amount the surface's microfacets are aligned to the halfway vector, 
** influenced by the roughness of the surface; this is the primary function approximating the microfacets.
*/
float computeNormalDistributionGGX(vec3 normal, vec3 halfwayVector, float roughnessFactor)
{
    float roughnessFactor2 = roughnessFactor * roughnessFactor;
    float roughnessFactor4 = roughnessFactor2 * roughnessFactor2;
    float normalDotHalfwayVector  = max(dot(normal, halfwayVector), 0.0);
	
    float denominator = (normalDotHalfwayVector * normalDotHalfwayVector * (roughnessFactor4 - 1.0) + 1.0);
    denominator = PI * denominator * denominator;
	
    return roughnessFactor4 / denominator;
}

/*
** Geometry function: describes the self-shadowing property of the microfacets. 
** When a surface is relatively rough, the surface's microfacets can overshadow other microfacets reducing 
**  the light the surface reflects.
*/
float computeGeometrySchlickGGX(float normalDotCamPosToFrag, float roughnessFactor)
{
    float r = (roughnessFactor + 1.0);
    float k = (r * r) / 8.0;

    float denominator = normalDotCamPosToFrag * (1.0 - k) + k;	
    return normalDotCamPosToFrag / denominator;
}

float computeSelfShadowingProperty(vec3 normal, vec3 camPosToFrag, vec3 fragToDirectionalLight, float roughnessFactor)
{
    float normalDotCamPosToFrag = max(dot(normal, camPosToFrag), 0.0);
    float normalDotFragToDirectionalLight = max(dot(normal, fragToDirectionalLight), 0.0);
    float ggx2 = computeGeometrySchlickGGX(normalDotCamPosToFrag, roughnessFactor);
    float ggx1 = computeGeometrySchlickGGX(normalDotFragToDirectionalLight, roughnessFactor);
	
    return ggx1 * ggx2;
}

// To define
vec3 getFragToLight();
vec3 getLightColor();
float getAttenuation(vec3 fragPosition);

void computeColor(vec2 texCoord)
{
    uint shadingType = texture(shadingTypeMap, texCoord).r;
    vec4 fragColorShadow = texture2D(colorShadowMap, texCoord);
    vec3 fragNormal = normalize(texture(normalMap, texCoord).xyz); // N
    vec3 fragPosition = texture2D(positionMap, texCoord).xyz;
    vec4 fragAoSpecularRoughnessHeight = texture2D(aoSpecularRoughnessHeightMap, texCoord);

    /*if(shadingType != 2u)
    {
        fragColor = vec4(fragColorShadow.rgb == vec3(0.0, 0.0, 0.0) ? vec3(0.3) : fragColorShadow.rgb, 1.0);
        return;
    }*/

    // V
    vec3 camPosToFrag = normalize(eyeWorldPosition - fragPosition);

    // F0
    vec3 fresnelColor = mix(vec3(0.04), fragColorShadow.rgb, fragAoSpecularRoughnessHeight.g);

    /* -> Per light */
    // L
    vec3 fragToLight = getFragToLight();

    // The halfway vector that sits halfway between the light and view vector
    vec3 halfwayVector = normalize(camPosToFrag + fragToLight);

    float attenuation = getAttenuation(fragPosition); 
    vec3 radiance = getLightColor() * attenuation; 

    /* 
    ** cook-torrance BRDF (bidirectional reflective distribution function) - 
    **  approximates how much each individual light ray contributes to the final 
    **  reflected light of an opaque surface given its material properties.
    */ 

    // NDF
    float normalDistribution = computeNormalDistributionGGX
    (
        fragNormal, 
        halfwayVector, 
        fragAoSpecularRoughnessHeight.b
    );
    float selfShadowing = computeSelfShadowingProperty
    (
        fragNormal, 
        camPosToFrag, 
        fragToLight, 
        fragAoSpecularRoughnessHeight.b
    );
    vec3 fresnel = computeFresnel
    (
        max(dot(halfwayVector, camPosToFrag), 0.0), 
        fresnelColor
    );

    // vec3 reflectionFactor = fresnel;
    vec3 refractionFactor = (vec3(1.0) - fresnel) * (1.0 - fragAoSpecularRoughnessHeight.g);

    vec3 numerator = normalDistribution * selfShadowing * fresnel * 5;
    float denominator = 4.0 * max(dot(fragNormal, camPosToFrag), 0.0) * 
                        max(dot(fragNormal, fragToLight), 0.0);
    vec3 specular = numerator / max(denominator, 0.001);  
        
    // Albedo combined with light equations
    // Lo
    vec3 outgoingRadiance = (refractionFactor * fragColorShadow.rgb / PI + specular) * 
                        radiance * max(dot(fragNormal, fragToLight), 0.0); 

    /* <- Per light */

    // This equation will be moved to separated Pass
    #if defined(AMBIENT_LIGHT_ENABLED) && (AMBIENT_LIGHT_ENABLED == 1)
        vec3 ambient = vec3(0.25) * fragColorShadow.rgb;// * fragAoSpecularRoughnessHeight.r;
        vec3 color = ambient + outgoingRadiance;
    #else
        vec3 color = outgoingRadiance;
    #endif
	
    // HDR correction
    color = color / (color + vec3(1.0));

    // Gamma correction
    color = pow(color, vec3(1.0/2.2));  
   
    // Final color
    fragColor = vec4(color, 1.0);

    //fragColor = vec4(colorShadow.rgb, 1.0);// * directionalLightFactor * vec4(directionalLight.base.color, 1.0);

    // tmp
    //float ggg = float(shadingType) / 2.0;
    //fragColor = vec4(ggg, 0.0, 0.0, 1.0);

    //fragColor = vec4(colorShadow.rgb, 1.0) * (directionalLightFactor + 1.0) * vec4(directionalLight.base.color, 1.0);
    //fragColor = fragAoSpecularRoughnessHeight.rrrr;
    //fragColor = vec4(fragNormal.rgb, 1.0);
}