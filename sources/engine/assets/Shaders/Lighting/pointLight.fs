#version 330  

#define AMBIENT_LIGHT_ENABLED 0
#include<Engine/Shaders/Lighting/baseLight.fs>

#define MAX_POINT_LIGHTS    #value<MAX_POINT_LIGHTS>

 flat in uint vsSpotId;
in vec2 vsTexCoord;

struct PointLight
{
    vec3 color;
    vec3 position;
    float constantFactor;
    float linearFactor;
    float quadraticFactor;
};
uniform PointLight pointLights[MAX_POINT_LIGHTS];

vec3 getFragToLight()
{
    return normalize(eyeWorldPosition - pointLights[vsSpotId].position);
}

vec3 getLightColor()
{
    return pointLights[vsSpotId].color;
}

float getAttenuation(vec3 fragPosition)
{
    float distance = length(pointLights[vsSpotId].position - fragPosition);
    return 1.0f / 
    (
        pointLights[vsSpotId].constantFactor + 
        pointLights[vsSpotId].linearFactor * distance + 
        pointLights[vsSpotId].quadraticFactor * distance * distance
    );
}

void main(void)
{
    vec2 texCoord = gl_FragCoord.xy / vec2(1366.0f, 768.0f);
    computeColor(texCoord);

    //fragColor = vec4(1.0, 0.0, 0.0, 1.0);
}