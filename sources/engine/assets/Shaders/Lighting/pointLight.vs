#version 430
#define MAX_POINT_LIGHTS    #value<MAX_POINT_LIGHTS>

layout( location = #value<VERTEX_BO_TYPE> ) in vec3 inVertexPosition;
flat out uint vsSpotId;

layout(std140) uniform BlockTransform
{
    mat4 vpMatrix;
    mat4 vMatrix;
    vec3 eyeWorldPosition;
};

// Describes arrays id per instance id
uniform uint spotIds[MAX_POINT_LIGHTS];
uniform mat4 modelMatrices[MAX_POINT_LIGHTS];

void main(void)
{
    vsSpotId = spotIds[gl_InstanceID];
    vec4 worldPosition = modelMatrices[vsSpotId] * vec4(inVertexPosition, 1.0f);
    gl_Position  = vpMatrix * worldPosition;
}