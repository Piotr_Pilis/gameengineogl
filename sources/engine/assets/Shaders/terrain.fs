#version 420

in vec3 vsVertexPosition;
in vec3 vsNormal;
in vec2 vsTexCoord;
in vec2 vsGlobalTexCoord;

flat in uint vsMapId;

layout (location = #value<SHADING_TYPE_MAP_ID>) out uint fragShadingType;
layout (location = #value<COLOR_SHADOW_MAP_ID>) out vec4 fragColorShadow;
layout (location = #value<NORMAL_MAP_ID>) out vec3 fragNormal;
layout (location = #value<POSITION_MAP_ID>) out vec3 fragPosition;
layout (location = #value<AO_SPECULAR_ROUGHNESS_HEIGHT_MAP_ID>) out vec4 fragAoSpecularRoughnessHeight;

layout(binding=#value<TERRAIN_PATH_MAP_ID>) uniform sampler2DArray pathMaps;

layout(binding=#value<TERRAIN_MATERIAL_DIFFUSE_MAP_ID>) uniform sampler2DArray diffuseMaps;
layout(binding=#value<TERRAIN_MATERIAL_NORMAL_MAP_ID>) uniform sampler2DArray normalMaps;

uniform int materialCount;

void main () 
{    
    // Blend materials
    fragColorShadow = vec4(0.0);
    float totalMaskValue = 0.0;
    vec4 pathMapSample = texture(pathMaps, vec3(vsGlobalTexCoord, vsMapId));
    for(int materialId = 0; materialId < materialCount - 1; materialId++)
    {
        float maskValue; 
        switch(materialId % 4)
        {
            case 0:
                maskValue = pathMapSample.r;
            break;
            case 1:
                maskValue = pathMapSample.g;
            break;
            case 2:
                maskValue = pathMapSample.b;
            break;
            default:
                maskValue = pathMapSample.a;
        }

        vec3 materialDiffuseMapSample = texture(diffuseMaps, vec3(vsTexCoord, materialId + 1)).rgb;
        fragColorShadow.rgb += maskValue * materialDiffuseMapSample;

        totalMaskValue += maskValue;
    }

    // First material
    if(totalMaskValue < 1.0)
    {
        vec3 materialDiffuseMapSample = texture(diffuseMaps, vec3(vsTexCoord, 0)).rgb;
        fragColorShadow.rgb += (1.0 - totalMaskValue) * materialDiffuseMapSample;
    }
    else
    {
        fragColorShadow.rgb /= totalMaskValue;
    }

    fragShadingType = #value<FLAT_SHADING_TYPE>;
    fragNormal = vsNormal;
    fragPosition = vsVertexPosition;

    fragAoSpecularRoughnessHeight = vec4
    (
        0.0,
        0.0,
        0.0,
        0.0
    );
    
    // Map Id to color
    /*uint ggg = 4;
    if(vsMapId % ggg == 0)
    {
        fragColorShadow = vec4(1.0, 0.0, 0.0, 1.0);
    }
    else if(vsMapId % ggg == 1)
    {
        fragColorShadow = vec4(0.0, 1.0, 0.0, 1.0);
    }
    else if(vsMapId % ggg == 2)
    {
        fragColorShadow = vec4(0.0, 0.0, 1.0, 1.0);
    }
    else if(vsMapId % ggg == 3)
    {
        fragColorShadow = vec4(1.0, 0.0, 1.0, 1.0);
    }
    else
    {
        fragColorShadow = vec4(1.0, 1.0, 1.0, 1.0);
    }*/

}
