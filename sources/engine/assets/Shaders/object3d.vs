#version 430
layout( location = 0 ) in vec3 inVertexPosition;
layout( location = 1 ) in vec3 inTexcoord;
layout( location = 2 ) in vec3 inNormal;

#section<SKELETAL_ANIMATION>
    layout( location = 3 ) in ivec3 inBoneIds;
    layout( location = 4 ) in vec3 inBoneWeights;
#endsection

layout(std140) uniform BlockTransform
{
    mat4 vpMatrix;
    mat4 vMatrix;
    vec3 eyeWorldPosition;
};

#section<SKELETAL_ANIMATION>
    const int MAX_NUM_BONES = #value<MAX_NUM_BONES>;
    uniform mat4 boneTransformations[MAX_NUM_BONES];
#endsection

uniform mat4 modelMatrix;

out vec2 vsTexCoord;
out vec3 vsNormal;
out vec3 vsVertexPosition;

void main(void)
{
    // Computes bone transformation
    #section<SKELETAL_ANIMATION>
        mat4 boneTransform = boneTransformations[inBoneIds[0]] * inBoneWeights[0];
        boneTransform += boneTransformations[inBoneIds[1]] * inBoneWeights[1];
        boneTransform += boneTransformations[inBoneIds[2]] * inBoneWeights[2];
    #endsection

    // Position
    #section<SKELETAL_ANIMATION>
        vec4 postion = boneTransform * vec4(inVertexPosition, 1.0);
        vec4 worldPosition = modelMatrix * postion;
    #else
        vec4 worldPosition = modelMatrix * vec4(inVertexPosition, 1.0);
    #endsection
    gl_Position  = vpMatrix * worldPosition;

    // Other
    mat3 normalMatrix = transpose(inverse(mat3(modelMatrix)));
    vsNormal = normalize(normalMatrix * inNormal);
    vsTexCoord = inTexcoord.xy;
    vsVertexPosition = worldPosition.xyz;
}