#version 420

in vec2 vsTexCoord;
in vec3 vsNormal;
in vec3 vsVertexPosition;

layout(binding=0) uniform sampler2D albedoMap;
layout(binding=1) uniform sampler2D normalMap;
layout(binding=2) uniform sampler2D heightMap;
layout(binding=3) uniform sampler2D ambientOcclusionMap;
layout(binding=4) uniform sampler2D specularMap;
layout(binding=5) uniform sampler2D roughnessMap;

layout (location = #value<SHADING_TYPE_MAP_ID>) out uint fragShadingType;
layout (location = #value<COLOR_SHADOW_MAP_ID>) out vec4 fragColorShadow;
layout (location = #value<NORMAL_MAP_ID>) out vec3 fragNormal;
layout (location = #value<POSITION_MAP_ID>) out vec3 fragPosition;
layout (location = #value<AO_SPECULAR_ROUGHNESS_HEIGHT_MAP_ID>) out vec4 fragAoSpecularRoughnessHeight;

void main () 
{    
    vec4 rgba = texture2D(albedoMap, vsTexCoord);
    if(rgba.a < 0.1)
        discard;

    float heightFactor = texture2D(heightMap, vsTexCoord).r;
    float ambientOcclusionFactor = texture2D(ambientOcclusionMap, vsTexCoord).r;
    float specularFactor = texture2D(specularMap, vsTexCoord).r;
    float roughnessFactor = texture2D(roughnessMap, vsTexCoord).r;

    fragShadingType = #value<PBR_SHADING_TYPE>;
    fragColorShadow = vec4
    (
        rgba.rgb,
        1.0
    );
    fragNormal = vsNormal;
    fragPosition = vsVertexPosition;
    fragAoSpecularRoughnessHeight = vec4
    (
        ambientOcclusionFactor, 
        specularFactor, 
        roughnessFactor,
        heightFactor
    );
}
