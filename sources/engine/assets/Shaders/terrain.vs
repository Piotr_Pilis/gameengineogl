#version 420
#define LOCAL_TEXCOORD_MULTIPLIER    #value<LOCAL_TEXCOORD_MULTIPLIER>
#define GLOBAL_TEXCOORD_MULTIPLIER    #value<GLOBAL_TEXCOORD_MULTIPLIER>
#define NUM_CHUNK_ROWS_TO_BE_RENDERED   #value<NUM_CHUNK_ROWS_TO_BE_RENDERED>
#define GEOMETRY_SIZE    #value<GEOMETRY_SIZE>

layout( location = 0 ) in vec2 inVertexPosition;

layout(std140) uniform BlockTransform
{
    mat4 vpMatrix;
};

out vec3 vsVertexPosition;
out vec3 vsNormal;
out vec2 vsTexCoord;
out vec2 vsGlobalTexCoord;

out uint vsMapId;

layout(binding=#value<TERRAIN_HEIGHT_MAP_ID>) uniform sampler2DArray heightMaps;
uniform uvec2 baseMapsCount;
uniform float maxHeight;

float getHeight(vec2 texCoord)
{
	return texture(heightMaps, vec3(abs(texCoord), vsMapId)).r * maxHeight;
}

void computeNormal(vec2 worldPostion)
{
    vec3 texCoordOffset = vec3(1.0, 1.0, 0.0);
	float heightLeft = getHeight((worldPostion - texCoordOffset.xz) * GLOBAL_TEXCOORD_MULTIPLIER);
	float heightRight = getHeight((worldPostion + texCoordOffset.xz) * GLOBAL_TEXCOORD_MULTIPLIER);
	float heightFar = getHeight((worldPostion + texCoordOffset.zy) * GLOBAL_TEXCOORD_MULTIPLIER);
	float heightNear = getHeight((worldPostion - texCoordOffset.zy) * GLOBAL_TEXCOORD_MULTIPLIER);

	vsNormal.x = heightLeft - heightRight;
	vsNormal.y = heightNear - heightFar;
	vsNormal.z = 2.0;

	vsNormal = normalize(vsNormal);
}

void main(void)
{    
    uvec2 chunkId = uvec2
    (
        gl_InstanceID % NUM_CHUNK_ROWS_TO_BE_RENDERED, 
        gl_InstanceID / NUM_CHUNK_ROWS_TO_BE_RENDERED
    );

    vsVertexPosition = vec3(inVertexPosition + chunkId * GEOMETRY_SIZE, 0.0f);

    vsMapId = uint(vsVertexPosition.y / GEOMETRY_SIZE) * baseMapsCount.x 
              + uint(vsVertexPosition.x / GEOMETRY_SIZE);
    vsTexCoord = vsVertexPosition.xy * LOCAL_TEXCOORD_MULTIPLIER;
    vsGlobalTexCoord = vsVertexPosition.xy * GLOBAL_TEXCOORD_MULTIPLIER;

    vsVertexPosition.z = getHeight(vsGlobalTexCoord);
    computeNormal(vsVertexPosition.xy);
    
    gl_Position = vpMatrix * vec4(vsVertexPosition.xyz, 1.0);
}