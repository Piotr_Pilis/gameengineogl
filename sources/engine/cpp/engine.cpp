#include <filesystem>

#include "RenderPass/FrameGraph.hpp"
#include "Log/criticalLog.hpp"
#include "Log/log.hpp"
#include "world.hpp"
#include "engine.hpp"

namespace Engine
{
    Engine::Engine()
    {
#ifdef _DEBUG
        namespace Engine = Engine;
        LOG(std::string("Working directory '") + std::filesystem::current_path().string() + "'");
#endif

        // -> World
        {
            m_world = std::make_unique<World>();
        }
        // <- World

        // -> Frame Graph
        {
            m_frameGraph = std::make_unique<RenderPass::FrameGraph>(*m_world);
            m_frameGraph->setDefaultFrameBufferObject(0);
        }
        // <- Frame Graph

        // -> Geometry Manager
        {
            Geometry::ObjectManager::initialize();
            m_objectManager = std::make_unique<Geometry::ObjectManager>();
        }
        // <- Geometry Manager
    }

    Engine::~Engine()
    {
    }

    Engine& Engine::getInstance()
    {
        static Engine engine;
        return engine;
    }

    void Engine::initializeCanvas(const GLsizei& windowWidth, const GLsizei& windowHeight)
    {
        m_frameGraph->initialize(windowWidth, windowHeight);
    }

    void Engine::reinitializeCanvas(const GLsizei& windowWidth, const GLsizei& windowHeight)
    {
        m_frameGraph->reinitialize(windowWidth, windowHeight);
    }

    void Engine::setTargetFrameBufferObject(const GLuint& targetFBO)
    {
        m_frameGraph->setDefaultFrameBufferObject(targetFBO);
    }

    void Engine::tick()
    {
        // Process functions that were suspended till next frame
        for (auto& function : m_nextFrames)
        {
            function();
        }
        m_nextFrames.clear();

        // Render frame
        m_frameGraph->render();
    }

    void Engine::callLater(std::function<void()>&& function)
    {
        m_nextFrames.emplace_back(std::move(function));
    }

    std::unique_ptr<World>& Engine::getWorld()
    {
        return m_world;
    }

    Geometry::ObjectManager& Engine::getObjectManager()
    {
        return *m_objectManager;
    }
}