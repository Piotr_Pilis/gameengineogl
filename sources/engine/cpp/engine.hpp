#pragma once
#include <glad/glad.h>
#include <functional>
#include <memory>
#include <string>
#include <vector>

#include "Geometry/objectManager.hpp"
#include "FileSystem/fileSystem.hpp"
#include "macros.hpp"

// Assets path in debug mode
#ifdef _DEBUG
#include "FileSystem/ifstream.hpp"

#define INITIALIZE_DEBUG_MODE(ASSET_PATH) Engine::FileSystem::setAssetDirectoryPath(ASSET_PATH);
#else
#define INITIALIZE_DEBUG_MODE(ASSET_PATH)
#endif

namespace Engine
{
    namespace RenderPass
    {
        class FrameGraph;
    }

    namespace Map
    {
        class Terrain;
    }

    namespace Transform
    {
        class CameraControllerAbstract;
    }

    class World;
    class Engine
    {
    private:
        Engine();
        ~Engine();

    public:
        static Engine& getInstance();

        ////////////////////////////// Public methods //////////////////////////////
        /* This has to be called before use */
        void initializeCanvas(const GLsizei& windowWidth, const GLsizei& windowHeight);

        /* This has to be called during change window size */
        void reinitializeCanvas(const GLsizei& windowWidth, const GLsizei& windowHeight);

        /* Sets target FBO */
        void setTargetFrameBufferObject(const GLuint& targetFBO = 0);

        /* This should be called inside infinite game loop 
        ** Generates single frame */
        void tick();

        /* Function will be called in next tick */
        void callLater(std::function<void()>&& function);

        //////////////////////////////// Getters ////////////////////////////////
        /* This class should be invisible to external solutions */
        std::unique_ptr<World>& getWorld();

        /* Gets object manager */
        Geometry::ObjectManager& getObjectManager();

    private:
        std::unique_ptr<RenderPass::FrameGraph> m_frameGraph;
        std::unique_ptr<World> m_world;

        // Object Manager is global - 
        //  The world may be different, but loaded meshes, materials etc. should not be reloaded
        std::unique_ptr<Geometry::ObjectManager> m_objectManager;

        std::vector<std::function<void()>> m_nextFrames;

    };
}