#pragma warning(disable : 4996)

#include <iostream>
#include <fstream>
#include <chrono>
#include <ctime>
#include <filesystem>

#include "log.hpp"

namespace Engine
{
    namespace Log
    {
        std::string Log::m_projectName = "undefinedProjectName";

        Log::Log(const std::string& sourcePath, const unsigned& codeLine):
            m_fileName("log"),
            m_sourcePath(sourcePath),
            m_codeLine(codeLine)
        {
        }

        Log::~Log()
        {
        }

        const std::string Log::getFullPath() const
        {
            char* pathToAppdata = getenv("APPDATA");
            std::string path = std::string(pathToAppdata) + "\\" + m_projectName;

            if (!std::filesystem::exists(path)) {
                std::filesystem::create_directory(path);
            }

            return path + "\\" + m_fileName + ".txt";
        }

        void Log::write(std::string msg)
        {
            std::ofstream fout;
            fout.open(getFullPath(), std::ios::out | std::ios::app);
            if (!fout)
            {
                fout.close();
                return;
            }

            std::time_t now = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
            fout << std::endl << std::ctime(&now);

            if (!m_prefix.empty())
            {
                msg = m_prefix + ": " + msg;
            }

            if (!m_sourcePath.empty())
            {
                std::string sourcePath = m_sourcePath + ":" + std::to_string(m_codeLine);
                if (sourcePath.size() < 40)
                    sourcePath.resize(40, ' ');
                msg = sourcePath + "\t" + msg;
            }

#ifdef _DEBUG
            std::cout << msg << std::endl;
#endif

            fout << msg << std::endl;
            fout.close();
        }

        void Log::write(std::stringstream msg)
        {
            write(msg.str());
        }

        void Log::clear()
        {
            std::ofstream fout;
            fout.open(getFullPath(), std::ios::out | std::ios::trunc);
            fout.close();
        }

        void Log::setProjectName(std::string projectName)
        {
            if(!projectName.empty())
                m_projectName = projectName;
        }

        Log& Log::instance()
        {
            static Log instance;
            return instance;
        }
    }
}