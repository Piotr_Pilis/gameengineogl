#pragma once
#include "log.hpp"

#define WARNING_LOG Engine::Log::WarningLog(__FILENAME__, __LINE__).write

namespace Engine
{
    namespace Log
    {
        class WarningLog : public Log
        {
        public:
            WarningLog(const std::string& sourcePath = "", const unsigned& codeLine = 0);
            ~WarningLog();

            virtual void write(std::string messageLog) override;

            static WarningLog& instance();

        };
    }
}