#pragma once
#include "warningLog.hpp"

#define CRITICAL_LOG Engine::Log::CriticalLog(__FILENAME__, __LINE__).write

namespace Engine
{
    namespace Log
    {
        class CriticalLog : public WarningLog
        {
        public:
            CriticalLog(const std::string& sourcePath = "", const unsigned& codeLine = 0);
            ~CriticalLog();

            virtual void write(std::string message) override;
            virtual void write(std::string messsageForUser, std::string messageForLog);
            void write(std::stringstream messsageForUser, std::stringstream messageForLog);

            static CriticalLog& instance();

        };
    }
}