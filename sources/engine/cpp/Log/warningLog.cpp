#include "warningLog.hpp"

namespace Engine
{
    namespace Log
    {
        WarningLog::WarningLog(const std::string& sourcePath, const unsigned& codeLine):
            Log(sourcePath, codeLine)
        {
            m_fileName = "errorLog";
            m_prefix = "Warning";
        }

        WarningLog::~WarningLog()
        {
        }

        void WarningLog::write(std::string messageLog)
        {
            Log::write(messageLog);
        }

        WarningLog& WarningLog::instance()
        {
            static WarningLog instance;
            return instance;
        }
    }
}