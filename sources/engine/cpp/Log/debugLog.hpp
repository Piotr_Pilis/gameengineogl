#pragma once
#include "log.hpp"

#ifdef _DEBUG
#define DEBUG_LOG(__MESSAGE__) Engine::Log::DebugLog(__FILENAME__, __LINE__).write(__MESSAGE__)
#else
#define DEBUG_LOG(__MESSAGE__)
#endif

namespace Engine
{
    namespace Log
    {
        class DebugLog : public Log
        {
        public:
            DebugLog(const std::string& sourcePath = "", const unsigned& codeLine = 0);
            ~DebugLog();

            virtual void write(std::string messageLog) override;

            static DebugLog& instance();

        };
    }
}