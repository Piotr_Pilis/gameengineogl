#include <sstream>

#include "../log/criticalLog.hpp"
#include "glStatus.hpp"

namespace Engine
{
    namespace Log
    {
        void checkGLError(const GLchar* file, GLint line)
        {
            GLuint glError;
            if ((glError = glGetError()) != GL_NO_ERROR) 
            {
                std::stringstream ss;
                ss << "OpenGL error in file " << file << " @ line " << line << ": '" << glError << "'";

                CRITICAL_LOG("OpenGL error!", ss.str());
            }
        }

        void checkFrameBufferError(const GLchar* file, GLint line)
        {
            GLuint status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
            std::stringstream out;

            switch (status) {
            case GL_FRAMEBUFFER_COMPLETE:
                break;
            case GL_FRAMEBUFFER_UNSUPPORTED:
                out << "Unsupported framebuffer format : '" << status << "'" << std::endl <<
                    "OpenGL error in file " << file << " @ line " << line;
                CRITICAL_LOG("OpenGL error!", out.str());
                break;
            default:
                out << "Invalid framebuffer format : '" << status << "'" << std::endl <<
                    "OpenGL error in file " << file << " @ line " << line;
                CRITICAL_LOG("OpenGL error!", out.str());
            }
        }

        void checkCompatibility()
        {
            GLint majorVersion, minorVersion;
            glGetIntegerv(GL_MAJOR_VERSION, &majorVersion);
            glGetIntegerv(GL_MINOR_VERSION, &minorVersion);

            std::stringstream ss;
            ss << "Renderer: " << glGetString(GL_RENDERER) << "\nVersion " << glGetString(GL_VERSION) << "\tGLSL " << glGetString(GL_SHADING_LANGUAGE_VERSION);

            if (majorVersion < 4 && minorVersion < 5)
            {
                std::stringstream errorss;
                errorss << "Your graphics hardware does not support OpenGL 4.4!\n" << ss.str();
                CRITICAL_LOG("Your graphics hardware does not support OpenGL 4.4!",
                    errorss.str()
                );
            }

            LOG(ss.str());
        }
    }
}