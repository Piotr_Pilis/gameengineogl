#pragma once
#include <string>
#include <sstream>
#include <iostream>
#include "../macros.hpp"

#define LOG Engine::Log::Log(__FILENAME__, __LINE__).write

namespace Engine
{
    namespace Log
    {
        class Log
        {
        public:
            Log(const std::string& sourcePath = "", const unsigned& codeLine = 0);
            ~Log();

            virtual void write(std::string msg);
            void write(std::stringstream msg);
            
            void clear();

            static void setProjectName(std::string projectName);
            static Log& instance();

        protected:
            const std::string getFullPath() const;

        protected:
            std::string m_fileName;
            std::string m_prefix;

        private:
            static std::string m_projectName;

            std::string m_sourcePath;
            unsigned m_codeLine;

        };
    }
}