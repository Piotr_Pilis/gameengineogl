#pragma once

#include "glad/glad.h"

#define CHECK_GL_ERROR() Engine::Log::checkGLError(__FILE__, __LINE__)
#define CHECK_FRAMEBUFFER_ERROR() Engine::Log::checkFrameBufferError(__FILE__, __LINE__)

namespace Engine
{
    namespace Log
    {
        void checkGLError(const GLchar* file, GLint line);
        void checkFrameBufferError(const GLchar* file, GLint line);
        void checkCompatibility();
    }
}