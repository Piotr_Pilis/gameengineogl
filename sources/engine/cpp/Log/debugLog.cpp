#include "debugLog.hpp"

namespace Engine
{
    namespace Log
    {
        DebugLog::DebugLog(const std::string& sourcePath, const unsigned& codeLine):
            Log(sourcePath, codeLine)
        {
            m_prefix = "Debug";
        }

        DebugLog::~DebugLog()
        {
        }

        void DebugLog::write(std::string messageLog)
        {
            Log::write(messageLog);
        }

        DebugLog& DebugLog::instance()
        {
            static DebugLog instance;
            return instance;
        }
    }
}