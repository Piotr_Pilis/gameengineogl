#include <Windows.h>
#include "criticalLog.hpp"

namespace Engine
{
    namespace Log
    {
        CriticalLog::CriticalLog(const std::string& sourcePath, const unsigned& codeLine):
            WarningLog(sourcePath, codeLine)
        {
            m_fileName = "errorLog";
            m_prefix = "CriticalError";
        }

        CriticalLog::~CriticalLog()
        {
        }

        void CriticalLog::write(std::string message)
        {
            Log::write(message);
            MessageBox(NULL, (std::string("FATAL ERROR: ") + message).c_str(), NULL, MB_ICONHAND | MB_OK);
            exit(EXIT_FAILURE);
        }

        void CriticalLog::write(std::string messsageForUser, std::string messageForLog)
        {
            Log::write(messageForLog);
            MessageBox(NULL, (std::string("FATAL ERROR: ") + messsageForUser).c_str(), NULL, MB_ICONHAND | MB_OK);
            exit(EXIT_FAILURE);
        }

        void CriticalLog::write(std::stringstream messsageForUser, std::stringstream messageForLog)
        {
            write(messsageForUser.str(), messageForLog.str());
        }

        CriticalLog& CriticalLog::instance()
        {
            static CriticalLog instance;
            return instance;
        }
    }
}