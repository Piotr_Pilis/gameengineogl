#include "../Log/log.hpp"
#include "../Log/criticalLog.hpp"
#include "../Log/warningLog.hpp"
#include "../Log/glStatus.hpp"
#include "asyncTextureUpdater.hpp"

namespace Engine
{
    namespace Texture
    {
        AsyncTextureUpdater::AsyncTextureUpdater(Texture& textureToUpdate, GLuint numPBOs,
            const GLsizei& width, const GLsizei& height) :
            m_texture(&textureToUpdate), 
            m_numPBOs(numPBOs),
            m_currentActivePBO(0),
            m_textureXOffsetToUpdate(0),
            m_textureYOffsetToUpdate(0)
        {
            checkTextureCompatibility();
            
            if (m_numPBOs < 1)
            {
                WARNING_LOG("Number of PBOs cannot be less than 1!\n\tIt has been set to 1");
                m_numPBOs = 1;
            }

            getTextureInformation();
            setDataSizeToUpdate(m_textureWidth, m_textureHeight);

            if (width != 0 || height != 0)
            {
                if (m_isTextureCompressed)
                    m_dataSize = Texture::getDataSize(
                        width != 0 ? width : m_textureWidth,
                        height != 0 ? height : m_textureHeight,
                        m_internalFormat
                    );
                else
                    m_dataSize =
                    (width != 0 ? width : m_textureWidth) *
                    (height != 0 ? height : m_textureHeight) *
                    m_texture->getChannelCount();
            }
            

            m_PBOs = new GLuint[m_numPBOs];
            createPBOs();
        }

        AsyncTextureUpdater::~AsyncTextureUpdater()
        {
            glDeleteBuffers(m_numPBOs, m_PBOs);
            delete[] m_PBOs;
        }

        // Received pointer to PBO data will indicate from this offset, so 
        //      0 will be [xOffset][yOffset], 
        //      1 will be [xOffset][yOffset+1] (yes, yOffset + 1) - Is it flipped? (idk, but it works :) )
        void AsyncTextureUpdater::setBeginPBOPtrAdress(const GLint& xOffset, const GLint& yOffset)
        {
            glPixelStorei(GL_UNPACK_ROW_LENGTH, m_textureWidth);
            glPixelStorei(GL_UNPACK_SKIP_PIXELS, xOffset);
            glPixelStorei(GL_UNPACK_SKIP_ROWS, yOffset);
        }

        void AsyncTextureUpdater::setBeginTexturePtrAdress(const GLint& xOffset, const GLint& yOffset)
        {
            m_textureXOffsetToUpdate = xOffset;
            m_textureYOffsetToUpdate = yOffset;
        }

        void AsyncTextureUpdater::setDataSizeToUpdate(const GLsizei& width, const GLsizei& height)
        {
            m_textureWidthToUpdate = width == 0 ? m_textureWidth : width;
            m_textureHeightToUpdate = height == 0 ? m_textureHeight : height;
            if (m_isTextureCompressed)
                m_dataSizeToUpdate = Texture::getDataSize(m_textureWidthToUpdate, m_textureHeightToUpdate, m_internalFormat);
        }

        void AsyncTextureUpdater::updateTexture()
        {
            m_texture->bind();
            glBindBuffer(GL_PIXEL_UNPACK_BUFFER_ARB, m_PBOs[m_currentActivePBO]);
            if (m_isTextureCompressed)
                m_texture->uploadCompressedData(
                    m_texture->m_target, m_internalFormat, m_dataSizeToUpdate, nullptr,
                    m_textureXOffsetToUpdate, m_textureYOffsetToUpdate, 0,
                    m_textureWidthToUpdate, m_textureHeightToUpdate, 0,
                    0, 0, false
                );
            else
                m_texture->uploadUncompressedData(
                    m_texture->m_target, m_textureType, GL_UNSIGNED_BYTE, nullptr,
                    m_textureXOffsetToUpdate, m_textureYOffsetToUpdate, 0,
                    m_textureWidthToUpdate, m_textureHeightToUpdate, 0,
                    0, 0, false
                );
            glBindBuffer(GL_PIXEL_UNPACK_BUFFER_ARB, 0);
        }

        GLubyte* AsyncTextureUpdater::startUpdateData(bool waitForSynchronization)
        {
            glBindBuffer(GL_PIXEL_UNPACK_BUFFER, m_PBOs[m_currentActivePBO]);
            if(!waitForSynchronization)
                glBufferData(GL_PIXEL_UNPACK_BUFFER, m_dataSize, 0, GL_STREAM_DRAW);

            GLubyte* ptr = (GLubyte*)glMapBuffer(GL_PIXEL_UNPACK_BUFFER, GL_WRITE_ONLY);
            return ptr;
        }

        void AsyncTextureUpdater::endUpdateData()
        {
            glUnmapBuffer(GL_PIXEL_UNPACK_BUFFER);
            glBindBuffer(GL_PIXEL_UNPACK_BUFFER, 0);
        }

        void AsyncTextureUpdater::createPBOs()
        {
            glGenBuffers(m_numPBOs, m_PBOs);
            for (GLuint pboID = 0; pboID < m_numPBOs; ++pboID)
            {
                glBindBuffer(GL_PIXEL_UNPACK_BUFFER, m_PBOs[pboID]);
                glBufferData(GL_PIXEL_UNPACK_BUFFER, m_dataSize, 0, GL_STREAM_DRAW);
            }
            glBindBuffer(GL_PIXEL_UNPACK_BUFFER, 0);
        }

        void AsyncTextureUpdater::checkTextureCompatibility()
        {
            if (!glIsTexture(m_texture->m_textureObject))
                CRITICAL_LOG(
                    "OpenGL error!", 
                    "Given texture is not texture object!"
                );

            if(m_texture->m_target != GL_TEXTURE_2D)
                // PBO does not support a more dimensional texture target, but the array is supported, so it can be done in future if needed 
                CRITICAL_LOG(
                    "OpenGL error!", 
                    "Target of given texture is not supported!"
                );
        }

        void AsyncTextureUpdater::getTextureInformation()
        {
            // It's needed, because this method is so slow (getting data from GPU)
            m_isTextureCompressed = m_texture->isCompressed();
            m_internalFormat = m_texture->getInternalFormat();
            m_textureWidth = m_texture->getWidth();
            m_textureHeight = m_texture->getHeight();
            m_dataSize = m_texture->getDataSize();

            switch (m_texture->getChannelCount())
            {
            case 1:
                m_textureType = GL_R;
                break;
            case 2:
                m_textureType = GL_RG;
                break;
            case 3:
                m_textureType = GL_RGB;
                break;
            default:
                m_textureType = GL_RGBA;
            }
        }

        void AsyncTextureUpdater::switchPBO()
        {
            m_currentActivePBO = (m_currentActivePBO + 1) % m_numPBOs;
        }
    }
}