#pragma once

#include <glad/glad.h>
#include "texture.hpp"

namespace Engine
{
    namespace Texture
    {
        class TextureReader
        {
        public:
            TextureReader() = delete;
            TextureReader(Texture& textureToRead);
            ~TextureReader();
            
            void readPixels(const GLint& level = 0);
            GLvoid* getData();

        private:
            void checkTextureCompatibility();
            void getTextureInformation();

        private:
            Texture* m_texture;
            GLvoid* m_data;

            bool m_isTextureCompressed;
            GLenum m_textureFormat;

            GLint m_dataSize;

        };
    }
}