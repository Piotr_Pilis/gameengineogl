#pragma once
#include <glad/glad.h>
#include <gli/gli.hpp>
#include <vector>
#include <memory>
#include "bitmap.hpp"

namespace Engine
{
    namespace Texture
    {
        class Texture
        {
        public:
            Texture();
            ~Texture();

            void remove();
            void bind() const;

            bool initialize
            (
                const Bitmap& bitmap, 
                GLint textureWrapS = GL_CLAMP_TO_EDGE, 
                GLint textureWrapT = GL_CLAMP_TO_EDGE, 
                GLint textureMagFilter = GL_LINEAR, 
                GLint textureMinFilter = GL_LINEAR_MIPMAP_LINEAR
            );

            bool initialize
            (
                const std::vector<std::shared_ptr<Bitmap>>& bitmaps,
                GLint textureWrapS = GL_CLAMP_TO_EDGE,
                GLint textureWrapT = GL_CLAMP_TO_EDGE,
                GLint textureMagFilter = GL_LINEAR,
                GLint textureMinFilter = GL_LINEAR_MIPMAP_LINEAR
            );

            GLint getWidth() const;
            GLint getHeight() const;
            GLint getInternalFormat() const;
            GLint getChannelCount() const;
            bool isCompressed() const;
            GLint getCompressedDataSize() const;
            bool isTexture() const;

            /* It doesn't matter that data is compressed or not */
            GLint getDataSize() const;

            /* Gets size of compressed data by given width and height
            ** For compressed texture data only 
            */
            static GLint getDataSize
            (
                const GLsizei& width, 
                const GLsizei& height, 
                const GLint& internalFormat
            );

        private:
            bool convertTargetToArray();

            void setTextureParameters
            (
                const gli::gl::format& glFormat, 
                const GLuint64& numLevels,
                const GLint& textureWrapS, 
                const GLint& textureWrapT, 
                const GLint& textureMagFilter, 
                const GLint& textureMinFilter
            );

            /* Specify storage requirements by using glTexStorage_D */
            bool specifyStorageRequirements
            (
                const gli::gl::format& glFormat, 
                const glm::tvec3<GLsizei>& extent,
                const GLuint64& numLayers, 
                const GLuint64& numFaces, 
                const GLuint64& numLevels
            );

            /* Fills storage by given data - all levels, faces and layers */
            void fillData
            (
                const Bitmap& bitmap, 
                const glm::tvec3<GLsizei>& extent, 
                const gli::gl::format& glFormat,
                const GLint& xOffset, 
                const GLint& yOffset, 
                const GLint& zOffset,
                const GLuint64& numLayers, 
                const GLuint64& numFaces, 
                const GLuint64& numLevels
            );

            /* Fills storage by given data using glSubImageXD - single level, face, layer 
            ** It's also used by TextureUpdater class */
            void uploadUncompressedData
            (
                GLenum glTarget, 
                const GLenum& format, 
                const GLenum& type, 
                const void* data,
                const GLint& xOffset, 
                const GLint& yOffset, 
                const GLint& zOffset,
                const GLsizei& width, 
                const GLsizei& height, 
                const GLsizei& depth,
                const GLsizei& faceID, 
                const GLsizei& levelID, 
                bool isTargetCube
            );

            void uploadCompressedData
            (
                GLenum glTarget, 
                const GLenum& format, 
                const GLsizei& dataSize, 
                const void* data,
                const GLint& xOffset, 
                const GLint& yOffset, 
                const GLint& zOffset,
                const GLsizei& width, 
                const GLsizei& height, 
                const GLsizei& depth,
                const GLsizei& faceID, 
                const GLsizei& levelID, 
                bool isTargetCube
            );

        private:
            GLuint m_textureObject;
            GLenum m_target;

            friend class TextureReader;
            friend class AsyncTextureUpdater;
        };
    }
}