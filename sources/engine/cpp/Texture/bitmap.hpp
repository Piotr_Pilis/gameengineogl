#pragma once
#define GLM_ENABLE_EXPERIMENTAL

#include <glad/glad.h>
#include <gli/texture.hpp> 
#include <string>

namespace Engine
{
    namespace Texture
    {
        class Bitmap
        {
        public:
            /* Loads DDS, KTX or KMG textures from file */
            bool loadFromFile(const std::string& fileName);
            bool loadFromRaw(const GLchar* data, const GLint64& size);
            void saveToFile(const std::string& fileName);
            void generateMipMaps();

            bool isCompressed() const;
            bool isLoaded() const;
            bool isTargetCube() const;

            gli::target getTarget() const;
            gli::format getFormat() const;
            gli::swizzles getSwizzles() const;
            GLuint64 getNumLevels() const;
            GLuint64 getNumLayers() const;
            gli::extent3d getExtent(GLsizei level = 0) const;
            GLsizei getSize(GLsizei level) const;
            GLuint64 getNumFaces() const;
            const void* getData(GLuint64 layer, GLuint64 face, GLuint64 level) const;
            const gli::texture& get() const;

        private:
            gli::texture m_bitmapData;
            
        };
    }
}