#include "Log/log.hpp"
#include "Log/warningLog.hpp"
#include "Log/criticalLog.hpp"
#include "Log/glStatus.hpp"

#include "texture.hpp"

namespace Engine::Texture
{
    Texture::Texture()
    {
        glBindTexture(GL_TEXTURE_2D, 0);
        glGenTextures(1, &m_textureObject);
    }

    Texture::~Texture()
    {
        remove();
    }

    void Texture::remove()
    {
        glDeleteTextures(1, &m_textureObject);
    }

    void Texture::bind() const
    {
        glBindTexture(m_target, m_textureObject);
    }

    bool Texture::initialize
    (
        const Bitmap& bitmap,
        GLint textureWrapS,
        GLint textureWrapT,
        GLint textureMagFilter,
        GLint textureMinFilter
    )
    {
        LOG("Initializing texture from bitmap...");

        if (!bitmap.isLoaded())
        {
            WARNING_LOG("Given bitmap is not loaded!");
            return false;
        }

        // Fetching texture data
        gli::gl glProfile(gli::gl::PROFILE_GL33);
        gli::gl::format const glFormat = glProfile.translate(bitmap.getFormat(), bitmap.getSwizzles());
        m_target = glProfile.translate(bitmap.getTarget());
        GLuint64 numLayers = bitmap.getNumLayers();
        GLuint64 numFaces = bitmap.getNumFaces();
        GLuint64 numLevels = bitmap.getNumLevels();
        glm::tvec3<GLsizei> const extent(bitmap.getExtent());

        bind();
        setTextureParameters(glFormat, numLevels, textureWrapS, textureWrapT, textureMagFilter, textureMinFilter);
        specifyStorageRequirements(glFormat, extent, numLayers, numFaces, numLevels);
        fillData
        (
            bitmap, extent, glFormat,
            0, 0, 0,
            numLayers, numFaces, numLevels
        );

        CHECK_GL_ERROR();
        return true;
    }

    bool Texture::initialize
    (
        const std::vector<std::shared_ptr<Bitmap>>& bitmaps,
        GLint textureWrapS,
        GLint textureWrapT,
        GLint textureMagFilter,
        GLint textureMinFilter
    )
    {
        LOG("Initializing texture from bitmaps...");
        if (bitmaps.size() == 0 || bitmaps[0] == nullptr)
        {
            WARNING_LOG("Array of bitmaps is empty. Minimum one is required!");
            return false;
        }

        // Fetching texture data
        gli::gl glProfile(gli::gl::PROFILE_GL33);

        // Data of first texture
        gli::gl::format glFormat = glProfile.translate(bitmaps[0]->getFormat(), bitmaps[0]->getSwizzles());
        m_target = glProfile.translate(bitmaps[0]->getTarget());
        GLuint64 numLayers = bitmaps.size();
        GLuint64 numFaces = bitmaps[0]->getNumFaces();
        GLuint64 numLevels = bitmaps[0]->getNumLevels();
        glm::tvec3<GLsizei> const extent(bitmaps[0]->getExtent());

        // Checks validation
        for (GLuint bitmapId = 1; bitmapId < bitmaps.size(); ++bitmapId)
        {
            auto& bitmap = bitmaps[bitmapId];

            gli::gl::format bitmapGlFormat = glProfile.translate(bitmaps[0]->getFormat(), bitmaps[0]->getSwizzles());
            GLenum bitmapTarget = glProfile.translate(bitmaps[0]->getTarget());
            GLuint64 bitmapNumFaces = bitmaps[0]->getNumFaces();
            GLuint64 bitmapNumLevels = bitmaps[0]->getNumLevels();
            glm::tvec3<GLsizei> const bitmapExtent(bitmaps[0]->getExtent());

            if (bitmapGlFormat.Internal != glFormat.Internal)
            {
                WARNING_LOG("Internal format of bitmaps are different and cannot be initialized as texture array!");
                return false;
            }
            if (bitmapTarget != m_target)
            {
                WARNING_LOG("Target of bitmaps are different and cannot be initialized as texture array!");
                return false;
            }
            if (bitmapNumFaces != numFaces)
            {
                WARNING_LOG("Number of bitmaps faces are different and cannot be initialized as texture array!");
                return false;
            }
            if (bitmapNumLevels != numLevels)
            {
                WARNING_LOG("Number of bitmaps levels are different and cannot be initialized as texture array!");
                return false;
            }
            if (bitmapExtent != extent)
            {
                WARNING_LOG("Size of bitmaps are different and cannot be initialized as texture array!");
                return false;
            }
        }

        // Convert to array target
        if (!convertTargetToArray())
            return false;

        bind();
        setTextureParameters(glFormat, numLevels, textureWrapS, textureWrapT, textureMagFilter, textureMinFilter);
        specifyStorageRequirements(glFormat, extent, numLayers, numFaces, numLevels);
        for (GLuint bitmapId = 0; bitmapId < bitmaps.size(); ++bitmapId)
        {
            auto& bitmap = bitmaps[bitmapId];
            if (bitmap == nullptr)
            {
                WARNING_LOG("One of the bitmaps is nullptr!");
                return false;
            }

            fillData
            (
                *bitmap,
                extent,
                glFormat,

                // Offsets
                0,
                m_target == GL_TEXTURE_1D_ARRAY ? bitmapId : 0,
                m_target != GL_TEXTURE_1D_ARRAY ? bitmapId : 0,

                1, // TODO fix GL_TEXTURE_CUBE_MAP (this value was set to numLayers, but our numLayers is layer of array), first you have to fix cube map target (below TOFIX)
                numFaces,
                numLevels
            );
        }

        CHECK_GL_ERROR();
        return true;
    }

    bool Texture::convertTargetToArray()
    {
        switch (m_target)
        {
        case GL_TEXTURE_1D:
            m_target = GL_TEXTURE_1D_ARRAY;
            return true;
            break;
        case GL_TEXTURE_2D:
            m_target = GL_TEXTURE_2D_ARRAY;
            return true;
            break;

            // Could be but is not implemented (data uploading)
            /*case GL_TEXTURE_CUBE_MAP:
                m_target = GL_TEXTURE_CUBE_MAP_ARRAY;
                return true;
                break;*/
        }
        WARNING_LOG("Target '" + std::to_string(m_target) + "' cannot be converted to target array");
        return false;
    }

    void Texture::setTextureParameters(const gli::gl::format& glFormat, const GLuint64& numLevels,
        const GLint& textureWrapS, const GLint& textureWrapT, const GLint& textureMagFilter, const GLint& textureMinFilter)
    {
        glTexParameteri(m_target, GL_TEXTURE_WRAP_S, textureWrapS);
        glTexParameteri(m_target, GL_TEXTURE_WRAP_T, textureWrapT);
        glTexParameteri(m_target, GL_TEXTURE_MAG_FILTER, textureMagFilter);
        glTexParameteri(m_target, GL_TEXTURE_MIN_FILTER, textureMinFilter);
        glTexParameteri(m_target, GL_TEXTURE_BASE_LEVEL, 0);
        glTexParameteri(m_target, GL_TEXTURE_MAX_LEVEL, static_cast<GLint>(numLevels - 1));
        glTexParameteri(m_target, GL_TEXTURE_SWIZZLE_R, glFormat.Swizzles[0]);
        glTexParameteri(m_target, GL_TEXTURE_SWIZZLE_G, glFormat.Swizzles[1]);
        glTexParameteri(m_target, GL_TEXTURE_SWIZZLE_B, glFormat.Swizzles[2]);
        glTexParameteri(m_target, GL_TEXTURE_SWIZZLE_A, glFormat.Swizzles[3]);
    }

    bool Texture::specifyStorageRequirements(
        const gli::gl::format& glFormat, const glm::tvec3<GLsizei>& extent,
        const GLuint64& numLayers, const GLuint64& numFaces, const GLuint64& numLevels)
    {
        GLsizei const faceTotal = static_cast<GLsizei>(numLayers * numFaces);

        switch (m_target)
        {
        case GL_TEXTURE_1D:
            glTexStorage1D(
                m_target, static_cast<GLint>(numLevels), glFormat.Internal, extent.x);
            break;
        case GL_TEXTURE_1D_ARRAY:
        case GL_TEXTURE_2D:
        case GL_TEXTURE_CUBE_MAP:
            glTexStorage2D(
                m_target, static_cast<GLint>(numLevels), glFormat.Internal,
                extent.x, m_target == GL_TEXTURE_2D ? extent.y : faceTotal);
            break;
        case GL_TEXTURE_2D_ARRAY:
        case GL_TEXTURE_3D:
        case GL_TEXTURE_CUBE_MAP_ARRAY:
            glTexStorage3D(
                m_target, static_cast<GLint>(numLevels), glFormat.Internal,
                extent.x, extent.y,
                m_target == GL_TEXTURE_3D ? extent.z : faceTotal);
            break;
        default:
            WARNING_LOG("Target of bitmap is wrong!");
            return false;
        }
        return true;
    }

    void Texture::fillData(
        const Bitmap& bitmap, const glm::tvec3<GLsizei>& extent, const gli::gl::format& glFormat,
        const GLint& xOffset, const GLint& yOffset, const GLint& zOffset,
        const GLuint64& numLayers, const GLuint64& numFaces, const GLuint64& numLevels)
    {
        bool isTargetCube = bitmap.isTargetCube();

        for (GLsizei layerID = 0u; layerID < numLayers; ++layerID)
            for (GLsizei faceID = 0u; faceID < numFaces; ++faceID)
                for (GLsizei levelID = 0u; levelID < numLevels; ++levelID)
                {
                    glm::tvec3<GLsizei> extent(bitmap.getExtent(levelID));

                    if (bitmap.isCompressed())
                    {
                        uploadCompressedData
                        (
                            m_target, glFormat.Internal, bitmap.getSize(levelID), bitmap.getData(layerID, faceID, levelID),
                            xOffset, yOffset, zOffset,
                            extent.x,

                            m_target == GL_TEXTURE_1D_ARRAY ? 1 : extent.y, // There was issue (depth instead of 1 was set to LayerId, depth cannot be 0, TODO fix cube map target!
                            m_target == GL_TEXTURE_3D ? extent.z : 1,   // as above
                            faceID, levelID, isTargetCube
                        );
                    }
                    else
                    {
                        uploadUncompressedData
                        (
                            m_target, glFormat.External, glFormat.Type, bitmap.getData(layerID, faceID, levelID),
                            xOffset, yOffset, zOffset,
                            extent.x,
                            m_target == GL_TEXTURE_1D_ARRAY ? 1 : extent.y, // as above
                            m_target == GL_TEXTURE_3D ? extent.z : 1,   // as above
                            faceID, levelID, isTargetCube
                        );
                    }
                }
    }

    void Texture::uploadUncompressedData(
        GLenum glTarget, const GLenum& format, const GLenum& type, const void* data,
        const GLint& xOffset, const GLint& yOffset, const GLint& zOffset,
        const GLsizei& width, const GLsizei& height, const GLsizei& depth,
        const GLsizei& faceID, const GLsizei& levelID, bool isTargetCube)
    {
        // format <=> externalFormat
        if (isTargetCube)
            glTarget = static_cast<GLenum>(GL_TEXTURE_CUBE_MAP_POSITIVE_X + faceID);

        switch (glTarget)
        {
        case GL_TEXTURE_1D:
            glTexSubImage1D(
                glTarget, static_cast<GLint>(levelID),
                xOffset,
                width,
                format, type, data
            );
            break;

        case GL_TEXTURE_1D_ARRAY:
        case GL_TEXTURE_2D:
        case GL_TEXTURE_CUBE_MAP:
            glTexSubImage2D(
                glTarget, static_cast<GLint>(levelID),
                xOffset, yOffset,
                width, height,
                format, type, data
            );
            break;

        case GL_TEXTURE_2D_ARRAY:
        case GL_TEXTURE_3D:
        case GL_TEXTURE_CUBE_MAP_ARRAY:
            glTexSubImage3D(
                glTarget, static_cast<GLint>(levelID),
                xOffset, yOffset, zOffset,
                width, height, depth,
                format, type, data
            );
            break;
        default: assert(0); break;
        }
    }

    void Texture::uploadCompressedData(
        GLenum glTarget, const GLenum& format, const GLsizei& dataSize, const void* data,
        const GLint& xOffset, const GLint& yOffset, const GLint& zOffset,
        const GLsizei& width, const GLsizei& height, const GLsizei& depth,
        const GLsizei& faceID, const GLsizei& levelID, bool isTargetCube)
    {
        // format <=> internalFormat
        if (isTargetCube)
            glTarget = static_cast<GLenum>(GL_TEXTURE_CUBE_MAP_POSITIVE_X + faceID);

        switch (glTarget)
        {
        case GL_TEXTURE_1D:
            glCompressedTexSubImage1D(
                glTarget, static_cast<GLint>(levelID),
                xOffset,
                width,
                format, dataSize, data
            );
            break;
        case GL_TEXTURE_1D_ARRAY:
        case GL_TEXTURE_2D:
        case GL_TEXTURE_CUBE_MAP:
            glCompressedTexSubImage2D(
                glTarget, static_cast<GLint>(levelID),
                xOffset, yOffset,
                width, height,
                format, dataSize, data
            );
            break;

        case GL_TEXTURE_2D_ARRAY:
        case GL_TEXTURE_3D:
        case GL_TEXTURE_CUBE_MAP_ARRAY:
            glCompressedTexSubImage3D(
                glTarget, static_cast<GLint>(levelID),
                xOffset, yOffset, zOffset,
                width, height, depth,
                format, dataSize, data
            );
            break;
        default: assert(0); break;
        }
    }

    GLint Texture::getWidth() const
    {
        GLint width;
        glGetTextureLevelParameteriv(m_textureObject, 0, GL_TEXTURE_WIDTH, &width);
        return width;
    }

    GLint Texture::getHeight() const
    {
        GLint height;
        glGetTextureLevelParameteriv(m_textureObject, 0, GL_TEXTURE_HEIGHT, &height);
        return height;
    }

    GLint Texture::getInternalFormat() const
    {
        GLint internalFormat;
        glGetTextureLevelParameteriv(m_textureObject, 0, GL_TEXTURE_INTERNAL_FORMAT, &internalFormat);
        return internalFormat;
    }

    GLint Texture::getChannelCount() const
    {
        GLint channelCount = 0;
        GLint componentSize;
        glGetTextureLevelParameteriv(m_textureObject, 0, GL_TEXTURE_RED_SIZE, &componentSize);
        if (componentSize != 0)
            ++channelCount;

        glGetTextureLevelParameteriv(m_textureObject, 0, GL_TEXTURE_GREEN_SIZE, &componentSize);
        if (componentSize != 0)
            ++channelCount;

        glGetTextureLevelParameteriv(m_textureObject, 0, GL_TEXTURE_BLUE_SIZE, &componentSize);
        if (componentSize != 0)
            ++channelCount;

        glGetTextureLevelParameteriv(m_textureObject, 0, GL_TEXTURE_ALPHA_SIZE, &componentSize);
        if (componentSize != 0)
            ++channelCount;

        glGetTextureLevelParameteriv(m_textureObject, 0, GL_TEXTURE_DEPTH_SIZE, &componentSize);
        if (componentSize != 0)
            ++channelCount;

        return channelCount;
    }

    bool Texture::isCompressed() const
    {
        GLint isCompressed;
        glGetTextureLevelParameteriv(m_textureObject, 0, GL_TEXTURE_COMPRESSED, &isCompressed);
        return isCompressed == 1;
    }

    GLint Texture::getCompressedDataSize() const
    {
        GLint dataSize;
        glGetTextureLevelParameteriv(m_textureObject, 0, GL_TEXTURE_COMPRESSED_IMAGE_SIZE, &dataSize);
        return dataSize;
    }

    bool Texture::isTexture() const
    {
        return glIsTexture(m_textureObject);
    }

    GLint Texture::getDataSize() const
    {
        if (isCompressed())
            return getCompressedDataSize();
        return getWidth() * getHeight() * getChannelCount();
    }

    GLint Texture::getDataSize(const GLsizei& width, const GLsizei& height, const GLint& internalFormat)
    {
        GLint blockSize = 0;
        switch (internalFormat)
        {
        case GL_COMPRESSED_RGBA_S3TC_DXT1_EXT:
        case GL_COMPRESSED_RED_RGTC1:
            blockSize = 8;
            break;

        case GL_COMPRESSED_RGBA_S3TC_DXT3_EXT:
        case GL_COMPRESSED_RGBA_S3TC_DXT5_EXT:
        case GL_COMPRESSED_RG_RGTC2:
            blockSize = 16;
            break;
        default:
            CRITICAL_LOG("Internal format of texture is not supported!");
        }

        return static_cast<GLint>(std::ceilf(width * 0.25f) * std::ceilf(height * 0.25f) * blockSize);
    }
}