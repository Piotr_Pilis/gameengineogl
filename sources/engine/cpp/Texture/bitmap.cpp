#include <gli/gli.hpp>
#include <gli/texture2d.hpp>
#include <gli/generate_mipmaps.hpp>
#include "../Log/log.hpp"
#include "../Log/warningLog.hpp"
#include "../FileSystem/ifstream.hpp"
#include "bitmap.hpp"

namespace Engine
{
    namespace Texture
    {
        bool Bitmap::loadFromFile(const std::string& fileName)
        {
            LOG("Loading bitmap from file '" + fileName + "'...");

            try
            {
                FileSystem::Ifstream fin(fileName);
                GLint64 fileSize = fin.size();

                GLchar* dataBuffer = new GLchar[fileSize + 1];
                dataBuffer[fileSize] = '\0';
                fin.read(dataBuffer, fileSize);

                if (!loadFromRaw(dataBuffer, fileSize))
                {
                    WARNING_LOG("Error during loading texture '" + fileName + "'");
                    delete[] dataBuffer;
                    return false;
                }
                delete[] dataBuffer;
            }
            catch (std::invalid_argument st)
            {
                WARNING_LOG("Could not open " + fileName + "'");
                return false;
            }

            return true;
        }

        bool Bitmap::loadFromRaw(const GLchar* data, const GLint64& size)
        {
            m_bitmapData = gli::load(data, size);
            if (m_bitmapData.empty())
                return false;
            return true;
        }

        void Bitmap::saveToFile(const std::string& fileName)
        {
            gli::save(m_bitmapData, fileName);
            m_bitmapData.faces();
        }

        void Bitmap::generateMipMaps()
        {
            LOG("Generating mip maps...");

            gli::texture2d textureSource(m_bitmapData);
            if (textureSource.empty())
            {
                WARNING_LOG("Error during generating mip maps");
                return;
            }

            textureSource = gli::generate_mipmaps(textureSource, gli::FILTER_LINEAR);
        }

        bool Bitmap::isCompressed() const
        {
            return gli::is_compressed(m_bitmapData.format());
        }

        bool Bitmap::isLoaded() const
        {
            return !m_bitmapData.empty();
        }

        bool Bitmap::isTargetCube() const
        {
            return gli::is_target_cube(m_bitmapData.target());
        }

        gli::target Bitmap::getTarget() const
        {
            return m_bitmapData.target();
        }

        gli::format Bitmap::getFormat() const
        {
            return m_bitmapData.format();
        }

        gli::swizzles Bitmap::getSwizzles() const
        {
            return m_bitmapData.swizzles();
        }

        GLuint64 Bitmap::getNumLevels() const
        {
            return m_bitmapData.levels();
        }

        GLuint64 Bitmap::getNumLayers() const
        {
            return m_bitmapData.layers();
        }

        GLsizei Bitmap::getSize(GLsizei level) const
        {
            return static_cast<GLsizei>(m_bitmapData.size(level));
        }

        gli::extent3d Bitmap::getExtent(GLsizei level) const
        {
            return m_bitmapData.extent(level);
        }

        GLuint64 Bitmap::getNumFaces() const
        {
            return m_bitmapData.faces();
        }

        const void* Bitmap::getData(GLuint64 layer, GLuint64 face, GLuint64 level) const
        {
            return m_bitmapData.data(layer, face, level);
        }

        const gli::texture& Bitmap::get() const
        {
            return m_bitmapData;
        }
    }
}