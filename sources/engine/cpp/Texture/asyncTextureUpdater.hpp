#pragma once

#include <glad/glad.h>
#include <gli/gli.hpp>
#include "texture.hpp"

namespace Engine
{
    namespace Texture
    {
        /* Asynchronous texture updater
        ** 1 or 2 possible number of PBOs 
        ** Usage: Data must be placed into PBO (StartUpdateData and EndUpdateData), then 
        ** You can call method to copying of data from PBO to texture object (UpdateTexture) 
        **
        ** If number of PBOs is greater than 1, You can use SwitchPBO() to work on the next PBO 
        */
        class AsyncTextureUpdater
        {
        public:
            AsyncTextureUpdater() = delete;
            AsyncTextureUpdater(Texture& textureToUpdate, GLuint numPBOs = 1, 
                const GLsizei& width = 0, const GLsizei& height = 0);
            ~AsyncTextureUpdater();

            // .............................Settings.............................
            /* Moves value of PBO pointer from(!) which data will be copied 
            ** This values will be saved */
            void setBeginPBOPtrAdress(const GLint& xOffset, const GLint& yOffset);

            /* Moves value of target texture pointer to(!) which data will be copied 
            ** This values will be saved */
            void setBeginTexturePtrAdress(const GLint& xOffset, const GLint& yOffset);

            /* Default value is maximum texture dataSize 
            ** This values will be saved 
            ** 0 means maximum of value 
            ** !! In pixel unit, not in block unit !!*/
            void setDataSizeToUpdate(const GLsizei& width, const GLsizei& height);

            // .............................Actions.............................
            /* Copys data from PBO to texture 
            ** If u want the same adress as texture You need to call SetBeginPBOPtrAdress(...) */
            void updateTexture();

            /* Updates data inside PBO */
            GLubyte* startUpdateData(bool waitForSynchronization);
            void endUpdateData();

            /* Only for number of PBOs greater or equal 2 */
            void switchPBO();

            // ............................Configures...........................
            const GLint& getMaxWidth() { return m_textureWidth; }
            const GLint& getMaxHeight() { return m_textureHeight; }

        private:
            void createPBOs();

            /* Checks that given texture can be updated */
            void checkTextureCompatibility();
            void getTextureInformation();

        private:
            GLuint* m_PBOs;
            Texture* m_texture;

            GLuint m_numPBOs;
            GLuint m_currentActivePBO;

            bool m_isTextureCompressed;
            GLint m_internalFormat;
            GLint m_textureWidth;
            GLint m_textureHeight;
            GLenum m_textureType;

            GLint m_dataSize;

            GLint m_textureXOffsetToUpdate;
            GLint m_textureYOffsetToUpdate;
            GLint m_dataSizeToUpdate;
            GLsizei m_textureWidthToUpdate;
            GLsizei m_textureHeightToUpdate;

        };
    }
}