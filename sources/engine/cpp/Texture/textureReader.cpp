#include "../Log/criticalLog.hpp"
#include "../Log/warningLog.hpp"
#include "textureReader.hpp"

namespace Engine
{
    namespace Texture
    {
        TextureReader::TextureReader(Texture& textureToUpdate) :
            m_texture(&textureToUpdate),
            m_data(nullptr)
        {
            checkTextureCompatibility();
            getTextureInformation();

            if(m_dataSize)
                m_data = new GLubyte[m_dataSize];
        }

        TextureReader::~TextureReader()
        {
            if (m_dataSize)
                delete[] m_data;
        }

        void TextureReader::readPixels(const GLint& level)
        {
            if(m_isTextureCompressed)
                glGetCompressedTextureImage(m_texture->m_textureObject, level, m_dataSize, m_data);
            else
                glGetTextureImage(m_texture->m_textureObject, level, m_textureFormat, GL_UNSIGNED_BYTE, m_dataSize, m_data);
        }

        GLvoid* TextureReader::getData()
        {
            return m_data;
        }

        void TextureReader::checkTextureCompatibility()
        {
            if (!glIsTexture(m_texture->m_textureObject))
                CRITICAL_LOG(
                    "OpenGL error!", 
                    "Given texture is not texture object!"
                );
        }

        void TextureReader::getTextureInformation()
        {
            // It's needed, because this method is so slow (getting data from GPU)
            m_isTextureCompressed = m_texture->isCompressed();
            m_dataSize = m_texture->getDataSize();
            
            switch (m_texture->getChannelCount())
            {
            case 1:
                m_textureFormat = GL_R;
                break;
            case 2:
                m_textureFormat = GL_RG;
                break;
            case 3:
                m_textureFormat = GL_RGB;
                break;
            default:
                m_textureFormat = GL_RGBA;
            }
        }
    }
}