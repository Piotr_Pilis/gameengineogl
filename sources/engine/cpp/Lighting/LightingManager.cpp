#include "../Log/warningLog.hpp"
#include "../macros.hpp"

#include "lightingManager.hpp"

namespace Engine::Lighting
{
    void LightingManager::setDirectionalLight(const DirectionalLight& directionalLight)
    {
        if (!m_directionalLight.has_value())
        {
            m_directionalLight = std::make_optional<DirectionalLight>();
        }

        bool changed = false;
        if (m_directionalLight->m_color != directionalLight.m_color)
        {
            m_directionalLight->m_color = directionalLight.m_color;
            changed = true;
        }
        if (m_directionalLight->m_direction != directionalLight.m_direction)
        {
            m_directionalLight->m_direction = directionalLight.m_direction;
            changed = true;
        }
        
        if (changed)
        {
            directionalLightChanged(m_directionalLight);
        }
    }

    void LightingManager::removeDirectionalLight()
    {
        if (m_directionalLight.has_value())
        {
            m_directionalLight = std::nullopt;
            directionalLightChanged(m_directionalLight);
        }
    }

    void LightingManager::updatePointLight(const PointLight& pointLight, const GLuint lightId)
    {
        if (m_pointLights.size() <= lightId)
        {
            WARNING_LOG("Light with identifier [" + std::to_string(lightId) + "] doesn't exist");
            return;
        }

        auto& currentPointLight = m_pointLights[lightId];

        bool changed = false;
        if (currentPointLight.m_color != pointLight.m_color)
        {
            currentPointLight.m_color = pointLight.m_color;
            changed = true;
        }
        if (currentPointLight.m_position != pointLight.m_position)
        {
            currentPointLight.m_position = pointLight.m_position;
            changed = true;
        }
        if (currentPointLight.m_constantFactor != pointLight.m_constantFactor)
        {
            currentPointLight.m_constantFactor = pointLight.m_constantFactor;
            changed = true;
        }
        if (currentPointLight.m_linearFactor != pointLight.m_linearFactor)
        {
            currentPointLight.m_linearFactor = pointLight.m_linearFactor;
            changed = true;
        }
        if (currentPointLight.m_quadraticFactor != pointLight.m_quadraticFactor)
        {
            currentPointLight.m_quadraticFactor = pointLight.m_quadraticFactor;
            changed = true;
        }

        if (changed)
        {
            pointLightChanged(lightId);
        }
    }

    const GLuint LightingManager::pushPointLight(const PointLight& pointLight)
    {
        m_pointLights.push_back(pointLight);
        const GLuint lightId = m_pointLights.size() - 1;
        pointLightAdded(lightId);
        return lightId;
    }

    void LightingManager::removePointLight(const GLuint lightId)
    {
        if (m_pointLights.size() <= lightId)
        {
            WARNING_LOG("Light with identifier [" + std::to_string(lightId) + "] doesn't exist");
            return;
        }

        m_pointLights.erase(m_pointLights.begin() + lightId);
        pointLightRemoved(lightId);
    }
}