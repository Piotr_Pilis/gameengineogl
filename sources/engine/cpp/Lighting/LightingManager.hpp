#pragma once
#include <glad/glad.h>

#include <boost/signals2/signal.hpp>
#include <optional>
#include <vector>

#include "DirectionalLight.hpp"
#include "PointLight.hpp"

namespace Engine
{
    namespace Lighting
    {
        class LightingManager
        {
        public:
            LightingManager() = default;
            ~LightingManager() = default;

            void setDirectionalLight(const DirectionalLight& directionalLight);
            void removeDirectionalLight();
            const std::optional<DirectionalLight>& getDirectionalLight() const { return m_directionalLight; }

            void updatePointLight
            (
                const PointLight& pointLight, 
                const GLuint lightId
            );
            const GLuint pushPointLight
            (
                const PointLight& pointLight
            );
            void removePointLight
            (
                const GLuint lightId
            );
            const std::vector<PointLight>& getPointLights() const 
            {
                return m_pointLights; 
            }

            /* Signals */
            // Directional light
            boost::signals2::signal<void(const std::optional<DirectionalLight>&)> 
                directionalLightChanged;

            // Point light
            boost::signals2::signal<void(const GLuint)>
                pointLightChanged;
            boost::signals2::signal<void(const GLuint)>
                pointLightAdded;
            boost::signals2::signal<void(const GLuint)>
                pointLightRemoved;

        private:
            std::optional<DirectionalLight> m_directionalLight;
            std::vector<PointLight> m_pointLights;
           
        };
    }
}