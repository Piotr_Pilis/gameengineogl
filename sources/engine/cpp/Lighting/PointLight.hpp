#pragma once
#define GLM_FORCE_CUDA
#include <glm/vec3.hpp>

namespace Engine
{
    namespace Lighting
    {
        struct PointLight
        {
            glm::vec3 m_color;
            glm::vec3 m_position;

            GLfloat m_constantFactor;
            GLfloat m_linearFactor;
            GLfloat m_quadraticFactor;
        };
    }
}