#pragma once
#define GLM_FORCE_CUDA
#include <glm/vec3.hpp>

namespace Engine
{
    namespace Lighting
    {
        struct DirectionalLight
        {
            glm::vec3 m_color;
            glm::vec3 m_direction;
        };
    }
}