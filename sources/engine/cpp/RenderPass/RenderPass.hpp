#pragma once

namespace Engine
{
    namespace RenderPass
    {
        class RenderPass
        {
        public:
            RenderPass() = default;
            ~RenderPass() = default;

            /* Does render pass */
            virtual void execute() = 0;

        protected:
            virtual void beginRenderPass() = 0;

        };
    }
}