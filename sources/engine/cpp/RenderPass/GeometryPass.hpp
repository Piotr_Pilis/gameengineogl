#pragma once
#include <glad/glad.h>
#include <memory>
#include "RenderPass.hpp"

namespace Engine
{
    class World;

    namespace Geometry
    {
        class Canvas;
    }

    namespace RenderPass
    {
        class GeometryPass : 
            public RenderPass
        {
        public:
            GeometryPass() = delete;
            GeometryPass
            (
                Geometry::Canvas& canvas, 
                World& world
            );
            ~GeometryPass();

            /* Binds program(s) of render pass and calls render methods */
            void execute() override;

        private:
            void beginRenderPass() override;

        private:
            Geometry::Canvas& m_canvas;
            World& m_world;

        };
    }
}