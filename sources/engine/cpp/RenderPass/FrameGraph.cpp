#include "../Geometry/canvas.hpp"
#include "../world.hpp"

#include "GeometryPass.hpp"
#include "LightingPass.hpp"
#include "FrameGraph.hpp"

namespace Engine::RenderPass
{
    FrameGraph::FrameGraph(World& world) :
        m_world(world)
    {
        m_canvas = std::make_unique<Geometry::Canvas>();
        m_geometryPass = std::make_unique<GeometryPass>(*m_canvas, m_world);
        m_lightingPass = std::make_unique<LightingPass>(*m_canvas, m_world, m_defaultFBO);
    }

    FrameGraph::~FrameGraph()
    {
    }

    void FrameGraph::initialize(const GLsizei& windowWidth, const GLsizei& windowHeight)
    {
        glEnable(GL_TEXTURE_2D);
        reinitialize(windowWidth, windowHeight);
    }

    void FrameGraph::reinitialize(const GLsizei& windowWidth, const GLsizei& windowHeight)
    {
        m_canvas->initialize(windowWidth, windowHeight);
    }

    void FrameGraph::setDefaultFrameBufferObject(const GLuint& defaultFBO)
    {
        m_defaultFBO = defaultFBO;
    }

    void FrameGraph::render()
    {
        m_geometryPass->execute();
        m_lightingPass->execute();
    }
}