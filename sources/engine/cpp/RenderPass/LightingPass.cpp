#include "Node/DirectionalLightPassNode.hpp"
#include "Node/PointLightPassNode.hpp"

#include "Log/criticalLog.hpp"
#include "Geometry/canvas.hpp"
#include "world.hpp"

#include "LightingPass.hpp"

namespace Engine::RenderPass
{
    LightingPass::LightingPass
    (
        Geometry::Canvas& canvas, 
        World& world, 
        const GLuint& defaultFBO
    ): m_canvas(canvas),
        m_world(world),
        m_defaultFBO(defaultFBO)
    {
        m_pointLightPassNode = std::make_unique<Node::PointLightPassNode>(m_world);
        m_directionalLightPassNode = std::make_unique<Node::DirectionalLightPassNode>(m_world);
    }

    LightingPass::~LightingPass()
    {
    }

    void LightingPass::beginRenderPass()
    {
        m_canvas.blitDepthBuffer(m_defaultFBO);
        
        glEnable(GL_BLEND);
        glBlendEquation(GL_FUNC_ADD);
        glBlendFunc(GL_ONE, GL_ONE);

        glBindFramebuffer(GL_DRAW_FRAMEBUFFER, m_defaultFBO);
        glClear(GL_COLOR_BUFFER_BIT);
        m_canvas.bindForReading();
    }

    void LightingPass::execute()
    {
        beginRenderPass();

        m_pointLightPassNode->execute();

        glDisable(GL_DEPTH_TEST);
        m_directionalLightPassNode->execute();
        glEnable(GL_DEPTH_TEST);
    }
}