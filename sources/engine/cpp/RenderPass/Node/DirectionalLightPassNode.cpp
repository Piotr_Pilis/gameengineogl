#include <boost/signals2/signal.hpp>
#include <boost/bind/bind.hpp>

#include "Lighting/LightingManager.hpp"

#include "Pipeline/globalUniformBlockBinding.hpp"
#include "Pipeline/program.hpp"
#include "Pipeline/shader.hpp"

#include "Geometry/canvas.hpp"
#include "Log/log.hpp"
#include "world.hpp"

#include "DirectionalLightPassNode.hpp"

using namespace Engine::Geometry;

namespace Engine::RenderPass::Node
{
    DirectionalLightPassNode::DirectionalLightPassNode(World& world)
    {
        initializeProgram();

        // Makes connections
        auto& lightingManager = world.getLightingManager();
        lightingManager.directionalLightChanged.connect
        (
            decltype(lightingManager.directionalLightChanged)::slot_type
            (
                &DirectionalLightPassNode::onDirectionalLightChanged,
                this,
                boost::placeholders::_1
            )
        );
    }

    void DirectionalLightPassNode::initializeProgram()
    {
        LOG("Initializing directional light pass shaders...");

        /* Initializes program */
        m_program = std::make_unique<Pipeline::Program>
        (
            Pipeline::Shader
            (
                "Engine/Shaders/dummy.vs", 
                Pipeline::Shader::ShaderType::VertexShader
            ),
            Pipeline::Shader
            (
                "Engine/Shaders/Lighting/directionalLight.gs",
                Pipeline::Shader::ShaderType::GeometryShader
            ),
            Pipeline::Shader
            (
                "Engine/Shaders/Lighting/directionalLight.fs",
                Pipeline::Shader::ShaderType::FragmentShader
            )
        );

        /* Sets texture uniforms */
        Canvas::initializeProgramAsReader(**m_program);

        /* Gets uniform locations */
        m_directionalLightUniformLocations.color = m_program->getUniformLocation
        (
            "directionalLight.color"
        );
        m_directionalLightUniformLocations.direction = m_program->getUniformLocation
        (
            "directionalLight.direction"
        );

        // Binds uniform block which contains information about camera transform
        Pipeline::Program::bindUniformBlock
        (
            **m_program,
            "BlockTransform",
            static_cast<GLuint>(Pipeline::GlobalUniformBlockBinding::BLOCK_TRANSFORM)
        );
    }

    void DirectionalLightPassNode::execute()
    {
        if (!m_enabled)
            return;
        glUseProgram(**m_program);
        glDrawArrays(GL_POINTS, 0, 1);
    }

    void DirectionalLightPassNode::onDirectionalLightChanged
    (
        const std::optional<Lighting::DirectionalLight>& directionalLight
    )
    {
        m_enabled = directionalLight.has_value();
        if (directionalLight.has_value())
        {
            glUseProgram(**m_program);
            glUniform3f
            (
                m_directionalLightUniformLocations.color,
                directionalLight->m_color.r,
                directionalLight->m_color.g,
                directionalLight->m_color.b
            );
            glUniform3f
            (
                m_directionalLightUniformLocations.direction,
                directionalLight->m_direction.x,
                directionalLight->m_direction.y,
                directionalLight->m_direction.z
            );
        }
    }
}