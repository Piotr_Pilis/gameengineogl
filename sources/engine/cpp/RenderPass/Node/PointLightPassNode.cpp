#include <boost/signals2/signal.hpp>
#include <boost/bind/bind.hpp>

#include <glad/glad.h>
#include <glm/vec3.hpp>
#include <algorithm>

#include "Lighting/LightingManager.hpp"

#include "Pipeline/globalUniformBlockBinding.hpp"
#include "Pipeline/program.hpp"
#include "Pipeline/shader.hpp"

#include "Geometry/Creator/meshCreator.hpp"
#include "Geometry/canvas.hpp"
#include "Geometry/mesh.hpp"

#include "Log/glStatus.hpp"
#include "Log/warningLog.hpp"
#include "Log/log.hpp"

#include "world.hpp"

#include "PointLightPassNode.hpp"

using namespace Engine::Geometry;

namespace Engine::RenderPass::Node
{
    PointLightPassNode::PointLightPassNode(World& world) :
        m_world(world)
    {
        // Fills m_freePointLightIds
        for (GLint pointLightId = Config::LightingConfig::MAX_NUM_POINT_LIGHTS - 1; pointLightId >= 0; --pointLightId)
        {
            m_freeVramSpotIds.push(static_cast<GLuint>(pointLightId));
        }
         
        // General initialization
        initializeProgram();
        initializeVAO();

        // Makes connections
        auto& lightingManager = world.getLightingManager();
        lightingManager.pointLightChanged.connect
        (
            decltype(lightingManager.pointLightChanged)::slot_type
            (
                &PointLightPassNode::onPointLightChanged,
                this,
                boost::placeholders::_1
            )
        );
        lightingManager.pointLightAdded.connect
        (
            decltype(lightingManager.pointLightAdded)::slot_type
            (
                &PointLightPassNode::onPointLightAdded,
                this,
                boost::placeholders::_1
            )
        );
        lightingManager.pointLightRemoved.connect
        (
            decltype(lightingManager.pointLightRemoved)::slot_type
            (
                &PointLightPassNode::onPointLightRemoved,
                this,
                boost::placeholders::_1
            )
        );
    }

    PointLightPassNode::~PointLightPassNode()
    {
        if (glIsVertexArray(m_sphereVao))
            glDeleteVertexArrays(1, &m_sphereVao);

        if (glIsBuffer(m_sphereVbo))
            glDeleteBuffers(1, &m_sphereVbo);

        if (glIsBuffer(m_sphereIbo))
            glDeleteBuffers(1, &m_sphereIbo);
    }

    void PointLightPassNode::initializeProgram()
    {
        LOG("Initializing point light pass shaders...");

        /* Initializes program */
        m_program = std::make_unique<Pipeline::Program>
        (
            Pipeline::Shader
            (
                "Engine/Shaders/Lighting/pointLight.vs", 
                Pipeline::Shader::ShaderType::VertexShader,
                {
                    {
                        "VERTEX_BO_TYPE",
                        std::to_string(VERTEX_BO)
                    },
                    {
                        "MAX_POINT_LIGHTS",
                        std::to_string(Config::LightingConfig::MAX_NUM_POINT_LIGHTS)
                    }
                }
            ),
            Pipeline::Shader
            (
                "Engine/Shaders/Lighting/pointLight.fs", 
                Pipeline::Shader::ShaderType::FragmentShader,
                {
                    {
                        "MAX_POINT_LIGHTS",
                        std::to_string(Config::LightingConfig::MAX_NUM_POINT_LIGHTS)
                    }
                }
            )
        );

        /* Sets texture uniforms */
        Canvas::initializeProgramAsReader(**m_program);

        /* Gets uniform locations */
        for (GLuint pointLightUniformLocationId = 0; pointLightUniformLocationId < m_pointLightUniformLocations.size(); ++pointLightUniformLocationId)
        {
            auto& pointLightUniformLocation = m_pointLightUniformLocations[pointLightUniformLocationId];
            pointLightUniformLocation.color = m_program->getUniformLocation
            (
                "pointLights[" + std::to_string(pointLightUniformLocationId) + "].color"
            );
            pointLightUniformLocation.position = m_program->getUniformLocation
            (
                "pointLights[" + std::to_string(pointLightUniformLocationId) + "].position"
            );

            pointLightUniformLocation.constantFactor = m_program->getUniformLocation
            (
                "pointLights[" + std::to_string(pointLightUniformLocationId) + "].constantFactor"
            );
            pointLightUniformLocation.linearFactor = m_program->getUniformLocation
            (
                "pointLights[" + std::to_string(pointLightUniformLocationId) + "].linearFactor"
            );
            pointLightUniformLocation.quadraticFactor = m_program->getUniformLocation
            (
                "pointLights[" + std::to_string(pointLightUniformLocationId) + "].quadraticFactor"
            );
        }

        m_spotIdUniformLocation = m_program->getUniformLocation
        (
            "spotIds"
        );

        // Initializes model matrices 
        for (GLuint modelTransformId = 0; modelTransformId < m_pointLightModelTransforms.size(); ++modelTransformId)
        {
            auto& modelTransform = m_pointLightModelTransforms[modelTransformId];
            modelTransform.setUnifromLocation
            (
                m_program->getUniformLocation
                (
                    "modelMatrices[" + std::to_string(modelTransformId) + "]"
                )
            );
        }

        // Binds uniform block which contains information about camera transform
        Pipeline::Program::bindUniformBlock
        (
            **m_program,
            "BlockTransform",
            static_cast<GLuint>(Pipeline::GlobalUniformBlockBinding::BLOCK_TRANSFORM)
        );

        CHECK_GL_ERROR();
    }

    void PointLightPassNode::initializeVAO()
    {
        // Generates spehere mesh
        Mesh sphereMesh;
        Creator::MeshCreator::createSphere
        (
            sphereMesh, 
            1.0f, 
            10u,
            10u
        );

        // Exctracts vertices position
        auto& vertices = sphereMesh.getVertices();

        std::vector<glm::vec3> vertexPositions;
        vertexPositions.reserve(vertices.size());

        for (auto& vertex : vertices)
        {
            vertexPositions.emplace_back
            (
                vertex.m_position
            );
        }

        // Initializes vertex buffer object
        glGenVertexArrays(1, &m_sphereVao);
        glBindVertexArray(m_sphereVao);
        glGenBuffers(1, &m_sphereVbo);
        glGenBuffers(1, &m_sphereIbo);

        // Vbo
        glBindBuffer
        (
            GL_ARRAY_BUFFER, 
            m_sphereVbo
        );
        glBufferData
        (
            GL_ARRAY_BUFFER, 
            sizeof(vertexPositions[0]) * vertexPositions.size(),
            vertexPositions.data(),
            GL_STATIC_DRAW
        );

        // Ibo
        auto& indices = sphereMesh.getIndices();
        m_numSphereInidces = indices.size();
        glBindBuffer
        (
            GL_ELEMENT_ARRAY_BUFFER, 
            m_sphereIbo
        );
        glBufferData
        (
            GL_ELEMENT_ARRAY_BUFFER,
            sizeof(indices[0]) * m_numSphereInidces,
            indices.data(),
            GL_STATIC_DRAW
        );

        GLint stride = sizeof(vertexPositions[0]);
        GLuint offset = 0;
        glEnableVertexAttribArray(VERTEX_BO);
        glVertexAttribPointer
        (
            VERTEX_BO, 
            3, 
            GL_FLOAT, 
            GL_FALSE, 
            stride,
            reinterpret_cast<const GLvoid*>(offset)
        );

        glBindVertexArray(0);
        CHECK_GL_ERROR();
    }

    void PointLightPassNode::execute()
    {
        if (getNumActivePointLightSpots() <= 0)
        {
            return;
        }

        glUseProgram(**m_program);
        glBindVertexArray(m_sphereVao);
        glDrawElementsInstanced
        (
            GL_TRIANGLES,
            m_numSphereInidces,
            GL_UNSIGNED_INT,
            NULL,
            getNumActivePointLightSpots()
        );
    }

    const GLuint PointLightPassNode::getNumActivePointLightSpots() const
    {
        return m_vramSpotIds.size();
    }

    void PointLightPassNode::updateVram
    (
        const GLuint vramSpotId,
        const GLuint pointLightId
    )
    {
        // Gets point light data
        auto& lightingManager = m_world.getLightingManager();
        auto& pointLight = lightingManager.getPointLights()[pointLightId];

        // Gets transformation of current point light spot
        auto& pointLightModelTransform = 
            m_pointLightModelTransforms
            [
                vramSpotId
            ];

        // Updates VRAM
        // TODO - what was changed?
        glUseProgram(**m_program);

        glUniform3f
        (
            m_pointLightUniformLocations[vramSpotId].color,
            pointLight.m_color.r,
            pointLight.m_color.g,
            pointLight.m_color.b
        );
        glUniform3f
        (
            m_pointLightUniformLocations[vramSpotId].position,
            pointLight.m_position.r,
            pointLight.m_position.g,
            pointLight.m_position.b
        );

        glUniform1f
        (
            m_pointLightUniformLocations[vramSpotId].constantFactor,
            pointLight.m_constantFactor
        );
        glUniform1f
        (
            m_pointLightUniformLocations[vramSpotId].linearFactor,
            pointLight.m_linearFactor
        );
        glUniform1f
        (
            m_pointLightUniformLocations[vramSpotId].quadraticFactor,
            pointLight.m_quadraticFactor
        );

        // Computes radius
        float maxLightColorComponent = std::fmaxf
        (
            std::fmaxf
            (
                pointLight.m_color.r, 
                pointLight.m_color.g
            ), 
            pointLight.m_color.b
        );
        GLfloat pointLightRadius = 
        (
            -pointLight.m_linearFactor + std::sqrtf
            (
                pointLight.m_linearFactor * pointLight.m_linearFactor - 
                4 * pointLight.m_quadraticFactor * 
                (
                    pointLight.m_constantFactor - (256.0f / 5.0f) * maxLightColorComponent
                )
            )
        ) / (2 * pointLight.m_quadraticFactor);

        // Updates model matrix
        pointLightModelTransform.setTranslation
        (
            pointLight.m_position
        );

        pointLightModelTransform.setScale
        (
            glm::vec3(pointLightRadius, pointLightRadius, pointLightRadius)
        );
        pointLightModelTransform.bindMatrix();
        CHECK_GL_ERROR();
    }

    void PointLightPassNode::onPointLightChanged
    (
        const GLuint lightId
    )
    {
        // Gets spot data
        auto pointLightId = std::find_if
        (
            m_pointLightIds.begin(),
            m_pointLightIds.end(),
            [lightId](const GLuint activePointLightId)
            {
                return activePointLightId == lightId;
            }
        );

        // Light is not currently active
        if (pointLightId == m_pointLightIds.end())
        {
            return;
        }

        // Updates vram
        updateVram
        (
            m_vramSpotIds
            [
                pointLightId - m_pointLightIds.begin()
            ],
            *pointLightId
        );
    }

    void PointLightPassNode::onPointLightAdded
    (
        const GLuint lightId
    )
    {
        //
        if (m_freeVramSpotIds.size() <= 0)
        {
            WARNING_LOG("Cannot add more point lights!");
            return;
        }

        // Is this point light currently active?
        auto pointLightId = std::find_if
        (
            m_pointLightIds.begin(),
            m_pointLightIds.end(),
            [lightId](const GLuint activePointLightId)
            {
                return activePointLightId == lightId;
            }
        );

        // Light is currently active
        if (pointLightId != m_pointLightIds.end())
        {
            WARNING_LOG("Point light[" + std::to_string(lightId) + "] is already active");
            return;
        }

        // Creates new point light spot
        GLuint vramSpotId = m_freeVramSpotIds.top();
        m_freeVramSpotIds.pop();

        m_vramSpotIds.emplace_back
        (
            vramSpotId
        );
        m_pointLightIds.emplace_back
        (
            lightId
        );

        // Updates vram
        glUseProgram(**m_program);
        glUniform1uiv
        (
            m_spotIdUniformLocation,
            static_cast<GLuint>(m_vramSpotIds.size()),
            m_vramSpotIds.data()
        );
        updateVram
        (
            m_vramSpotIds[m_vramSpotIds.size() - 1],
            m_pointLightIds[m_pointLightIds.size() - 1]
        );
    }

    void PointLightPassNode::onPointLightRemoved
    (
        const GLuint lightId
    )
    {

    }
}