#pragma once
#include <glad/glad.h>

#include <optional>
#include <memory>

#include "Lighting/DirectionalLight.hpp"
#include "macros.hpp"

namespace Engine
{
    class World;
}

namespace Engine::Pipeline
{
    class Program;
}

namespace Engine::RenderPass::Node
{
    class DirectionalLightPassNode
    {
    public:
        DirectionalLightPassNode() = delete;
        DirectionalLightPassNode(World& world);
        ~DirectionalLightPassNode() = default;

        /* Binds programs and calls render method */
        void execute();

        /* Slots */
        // VRAM managment
        void onDirectionalLightChanged
        (
            const std::optional<Lighting::DirectionalLight>& directionalLight
        );

    private:
        void initializeProgram();

    private:
        struct
        {
            GLuint color = INVALID_UNIFORM_LOCATION;
            GLuint direction = INVALID_UNIFORM_LOCATION;
        } m_directionalLightUniformLocations;

        std::unique_ptr<Pipeline::Program> m_program;
        bool m_enabled = false;

    };
}