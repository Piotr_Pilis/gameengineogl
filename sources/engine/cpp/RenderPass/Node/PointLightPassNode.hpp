#pragma once
#include <glad/glad.h>
#include <glm/mat4x4.hpp>

#include <memory>
#include <vector>
#include <array>
#include <stack>

#include "Config/LightingConfig.hpp"
#include "Lighting/PointLight.hpp"
#include "Pipeline/transform.hpp"
#include "macros.hpp"

namespace Engine
{
    class World;
}

namespace Engine::Pipeline
{
    class Program;
}

namespace Engine::RenderPass::Node
{
    class PointLightPassNode
    {
    private:
        struct PointLightUniformLocations
        {
            GLuint color = INVALID_UNIFORM_LOCATION;
            GLuint position = INVALID_UNIFORM_LOCATION;

            GLuint constantFactor = INVALID_UNIFORM_LOCATION;
            GLuint linearFactor = INVALID_UNIFORM_LOCATION;
            GLuint quadraticFactor = INVALID_UNIFORM_LOCATION;
        };

        enum BufferObjectType
        {
            VERTEX_BO,
            INDICES_BO
        };

    public:
        PointLightPassNode() = delete;
        PointLightPassNode(World& world);
        ~PointLightPassNode();

        /* Binds programs and calls render method */
        void execute();

    private:
        void initializeProgram();
        void initializeVAO();

        const GLuint getNumActivePointLightSpots() const;

        void updateVram
        (
            const GLuint vramSpotId,
            const GLuint pointLightId
        );

        /* Slots */
        // VRAM managment
        void onPointLightChanged
        (
            const GLuint lightId
        );

        void onPointLightAdded
        (
            const GLuint lightId
        );

        void onPointLightRemoved
        (
            const GLuint lightId
        );

    private:
        World& m_world;

        std::unique_ptr<Pipeline::Program> m_program;

        GLuint m_sphereVao;
        GLuint m_sphereVbo;
        GLuint m_sphereIbo;

        GLuint m_numSphereInidces;

        std::array
        <
            PointLightUniformLocations,
            Config::LightingConfig::MAX_NUM_POINT_LIGHTS
        > m_pointLightUniformLocations;
        GLuint m_spotIdUniformLocation = INVALID_UNIFORM_LOCATION;
        std::array
        <
            Pipeline::Transform,
            Config::LightingConfig::MAX_NUM_POINT_LIGHTS
        > m_pointLightModelTransforms;

        // Lift of point light identifiers that are inactive
        std::stack<GLuint> m_freeVramSpotIds;

        // List of point light indentifiers that are active
        // Number of m_vramSpotIds and m_pointLightIds should be equal
        std::vector<GLuint> m_vramSpotIds;
        std::vector<GLuint> m_pointLightIds;

    };
}