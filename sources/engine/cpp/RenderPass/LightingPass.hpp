#pragma once
#include <glad/glad.h>
#include <memory>

#include "RenderPass.hpp"

namespace Engine
{
    class World;

    namespace Geometry
    {
        class Canvas;
    }

    namespace RenderPass
    {
        namespace Node
        {
            class PointLightPassNode;
            class DirectionalLightPassNode;
        }

        class LightingPass:
            public RenderPass
        {
        public:
            LightingPass() = delete;
            LightingPass
            (
                Geometry::Canvas& canvas, 
                World& world,
                const GLuint& defaultFBO
            );
            ~LightingPass();

            /* Binds programs of render pass nodes and calls render methods */
            void execute() override;

        private:
            void beginRenderPass() override;

        private:
            Geometry::Canvas& m_canvas;
            World& m_world;
            const GLuint& m_defaultFBO;

            std::unique_ptr<Node::PointLightPassNode> m_pointLightPassNode;
            std::unique_ptr<Node::DirectionalLightPassNode> m_directionalLightPassNode;

        };
    }
}