#include "../Log/criticalLog.hpp"
#include "../Geometry/canvas.hpp"
#include "../world.hpp"
#include "GeometryPass.hpp"

namespace Engine::RenderPass
{
    GeometryPass::GeometryPass
    (
        Geometry::Canvas& canvas, 
        World& world
    ) : m_canvas(canvas),
        m_world(world)
    {
    }

    GeometryPass::~GeometryPass()
    {
    }

    void GeometryPass::beginRenderPass()
    {
        m_canvas.bindForWriting();
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glDepthMask(GL_TRUE);
        glEnable(GL_DEPTH_TEST);
        glDisable(GL_BLEND);
    }

    void GeometryPass::execute()
    {
        beginRenderPass();
        m_world.update();
        m_world.render();
    }
}