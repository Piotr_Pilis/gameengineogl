#pragma once
#include <glad/glad.h>
#include <memory>

#include "GeometryPass.hpp"
#include "LightingPass.hpp"

namespace Engine
{
    class World;
    namespace Geometry
    {
        class Canvas;
    }

    namespace RenderPass
    {
        class FrameGraph
        {
        public:
            FrameGraph() = delete;
            FrameGraph(World& world);
            ~FrameGraph();

            /* Calls all render passes */
            void render();

            /* This has to be called before use */
            void initialize(const GLsizei& windowWidth, const GLsizei& windowHeight);

            /* This has to be called during change window size */
            void reinitialize(const GLsizei& windowWidth, const GLsizei& windowHeight);

            /* Defines where frame should be rendered */
            void setDefaultFrameBufferObject(const GLuint& defaultFBO = 0);

            /* Getters */
            /*GeometryPass& getGeometryPass() { return *m_geometryPass; }
            LightingPass& getLightPass() { return *m_lightingPass; }*/

        private:
            std::unique_ptr<Geometry::Canvas> m_canvas;

            std::unique_ptr<GeometryPass> m_geometryPass = nullptr;
            std::unique_ptr<LightingPass> m_lightingPass = nullptr;

            World& m_world;
            GLuint m_defaultFBO;
        };
    }
}