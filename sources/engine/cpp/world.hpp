#pragma once
#include <boost/signals2/signal.hpp>

#include <glad/glad.h>
#include <memory>

#include "Lighting/LightingManager.hpp"
#include "Entity/renderingManager.hpp"

namespace Engine
{
    namespace Map
    {
        class Terrain;
    }

    namespace Transform
    {
        class CameraControllerAbstract;
    }

    class World
    {
    public:
        World();
        ~World();

        /* Sets current active camera */
        void setActiveCameraController(
            const std::shared_ptr<Transform::CameraControllerAbstract>& cameraController);
        const std::shared_ptr<Transform::CameraControllerAbstract>& 
            getActiveCameraController() const { return m_activeCameraController; }

        /* Sets/ Gets current active terrain */
        void setActiveTerrain(const std::shared_ptr<Map::Terrain>& terrain = nullptr);
        const std::shared_ptr<Map::Terrain>& getActiveTerrain() const { return m_activeTerrain; }

        /* Updates geometry data */
        void update();

        /* Calls render methods */
        void render();

        /* Gets object manager */
        Entity::RenderingManager& getRenderingManager();

        /* Gets lighting manager */
        Lighting::LightingManager& getLightingManager();

    private:
        /* Initializes world */
        void initialize();

    public:
        /* Signals */
        boost::signals2::signal<void(std::shared_ptr<Transform::CameraControllerAbstract>&)>
            activeCameraControllerChanged;

    private:
        // Different world has different lighitng and rendering queue
        std::unique_ptr<Entity::RenderingManager> m_renderingManager;
        std::unique_ptr<Lighting::LightingManager> m_lightingManager;

        std::shared_ptr<Transform::CameraControllerAbstract> 
            m_activeCameraController;

        std::shared_ptr<Map::Terrain> m_activeTerrain;
        bool m_showTerrainGrid = false;
    };
}