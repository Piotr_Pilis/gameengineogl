#include <unordered_map>
#include <cmath>

#include "Entity/renderingManager.hpp"
#include "Log/criticalLog.hpp"

#include "animation.hpp"
#include "skeleton.hpp"
#include "engine.hpp"
#include "world.hpp"

#include "animationPlayer.hpp"

namespace Engine
{
    namespace Animation
    {
        // Category names
        namespace
        {
            constexpr const char* const ANIMATION_NODE_LINKED_TO_SKELETON_CATEGORY_NAME = "AnimationPlayer_LinksToNode";
            constexpr const char* const ANIMATION_RESULT_CATEGORY_NAME = "AnimationPlayer_Result";
        }

        // Constants
        namespace 
        {
            constexpr const GLfloat DEFAULT_TICKS_PER_SECOND = 25.0f;
        }

        AnimationPlayer::AnimationPlayer(const SkeletonPtr& skeleton) :
            m_skeleton(skeleton)
        {
            initialize();
        }

        AnimationPlayer::~AnimationPlayer()
        {
        }

        void AnimationPlayer::initialize()
        {
            // Validation
            if (m_skeleton.get() == nullptr)
            {
                namespace Engine = Engine;
                CRITICAL_LOG("Internal error", "Skeleton is nullptr");
            }

            // Binds signals
            m_skeleton->m_boneMapChanged.connect
            (
                std::bind
                (
                    &AnimationPlayer::resetResult,
                    this
                )
            );
            resetResult();
        }

        void AnimationPlayer::play()
        {
            if (m_state == State::PLAYING)
            {
                return;
            }
            if (m_state == State::STOPPED)
            {
                m_animationTimer.resetTimer();
            }
            setUpdatingEnabled(true);
            m_state = State::PLAYING;
            m_stateChanged.callSlots();
        }

        void AnimationPlayer::pause()
        {
            if (m_state == State::PAUSED)
            {
                return;
            }
            setUpdatingEnabled(false);
            m_state = State::PAUSED;
            m_stateChanged.callSlots();
        }

        void AnimationPlayer::stop()
        {
            if (m_state == State::STOPPED)
            {
                return;
            }
            m_animationTimer.resetTimer();
            setUpdatingEnabled(false);
            m_state = State::STOPPED;
            m_stateChanged.callSlots();
        }

        void AnimationPlayer::update()
        {
            if (m_animation.get() == nullptr)
                return;

            GLfloat ticksPerSecond = (m_animation->getTicksPerSecond() == 0.0 ?
                DEFAULT_TICKS_PER_SECOND : static_cast<GLfloat>(m_animation->getTicksPerSecond()));
            GLfloat animationTime = fmod
            (
                m_animationTimer.getElapsedSeconds() * ticksPerSecond, 
                static_cast<GLfloat>(m_animation->getDuration())
            );

            updateNodeHierarchy
            (
                animationTime,
                m_skeleton->getRootNode(),
                glm::mat4x4(1.0f)
            );
        }

        // This method is not checking m_animation!
        void AnimationPlayer::updateNodeHierarchy
        (
            const GLfloat& animationTime,
            const BoneNode& boneNode,
            const glm::mat4x4& parentTransformation
        )
        {
            auto animationNode = m_animation->getNodeByName(boneNode.getName());
            glm::mat4x4 animationTransformation;

            // Animation keys for this node exists
            if (animationNode != nullptr)
            {
                animationNode->calculateTransformationMatrix
                (
                    animationTransformation,
                    animationTime
                );
            }
            else
            {
                animationTransformation = boneNode.getDefaultTransformationMatrix();
            }

            glm::mat4x4 globalTransformation = parentTransformation * animationTransformation;
            if (boneNode.hasBoneData())
            {
                auto& boneData = boneNode.getBoneData();
                auto& boneOffsetMatrix = boneData->getOffsetMatrix();
                m_boneTransformations[boneData->getId()] = m_skeleton->getGlobalInverseTransform() * globalTransformation * boneOffsetMatrix;
            }

            // Updates childrens
            auto& boneNodeChildren = boneNode.getChildren();
            for (auto& boneNodeChild : boneNodeChildren)
            {
                updateNodeHierarchy
                (
                    animationTime,
                    *boneNodeChild,
                    globalTransformation
                );
            }
        }

        void AnimationPlayer::resetResult()
        {
            auto& boneMap = m_skeleton->getBoneMap();
            m_boneTransformations.resize(boneMap.size());

            for (auto& boneTransformation : m_boneTransformations)
            {
                boneTransformation = glm::mat4x4(1.0);
            }
        }

        void AnimationPlayer::setAnimation(const AnimationPtr& animation)
        {
            if (m_animation.get() == animation.get())
                return;

            if (animation->getDuration() == 0.0)
            {
                m_animation = nullptr;
                return;
            }

            m_animation = animation;
            m_animationTimer.resetTimer();
            linkAnimationNodes();
        }

        TODO(Linking animation nodes to skeleton node);
        void AnimationPlayer::linkAnimationNodes()
        {
            //ANIMATION_NODE_LINKED_TO_SKELETON_CATEGORY_NAME
        }

        const AnimationPlayer::BoneTransformations& AnimationPlayer::getResult() const
        {
            return m_boneTransformations;
        }

        const AnimationPlayer::State AnimationPlayer::getState() const
        {
            return m_state;
        }

        bool AnimationPlayer::uses(const Skeleton& skeleton) const
        {
            return m_skeleton.get() == &skeleton;
        }
    }
}
