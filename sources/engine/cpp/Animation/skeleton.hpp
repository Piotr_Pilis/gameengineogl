#pragma once
#include <glad/glad.h>
#include <glm/mat4x4.hpp>
#include <string>
#include <vector>
#include <memory>

#include "Event/connection.hpp"
#include "boneNode.hpp"

struct aiNode;
namespace Engine
{
    namespace Animation
    {
        namespace Creator
        {
            class SkeletonCreator;
        }

        /*
        Contains information about bone hierarchy.
        */
        class Skeleton
        {
            friend class Creator::SkeletonCreator;

        public:
            typedef std::vector<std::shared_ptr<BoneNode>> BoneMap;

        private:
            Skeleton();

        public:
            ~Skeleton();

            const BoneNode* getNodeByName(const std::string& nodeName) const;
            const BoneNode& getRootNode() const;

            // One node (root node) means that there is not bone hierarchy
            bool containsBoneHierarchy() const;
            bool containsNode(const BoneNode& node) const;

            /* Bone node mapping */
            ////////////////////////////////////////////
            const BoneMap& getBoneMap() const;

            // Creates bone data from node or compares boneOffset that should be equal 
            bool createBoneFromNode(const BoneNode& node, const glm::mat4x4& boneOffsetMatrix);

            // Bone id will be stored in m_boneMap variable and Node.Bone.m_id
            bool getBoneId(const BoneNode& node, GLuint& boneId);

            ////////////////////////////////////////////
            const glm::mat4x4& getGlobalInverseTransform() const;

        private:
            // Removes node binding to VRAM
            // Animation result matrices won't be sent to VRAM
            // Be aware that bone weight data may contain incorrect bone identifiers after 
            //  using this method
            void clearBoneMapping();
            void reset();

            // Only Creator can use this method
            bool loadSkeleton(aiNode* rootNode);

            //
            bool extractNode(aiNode* aiNode, BoneNode& node);
            const BoneNode* getNodeByName
            (
                const std::string& nodeName,
                const BoneNode& currentNode
            ) const;
            std::shared_ptr<BoneNode> getNodeAsSharedPtr
            (
                const BoneNode& node,
                const BoneNode& currentNode
            ) const;
            bool containsNode
            (
                const BoneNode& node,
                const BoneNode& currentNode
            ) const;

        private:
            // Order of bone representation in VRAM (array indexes in VRAM)
            BoneMap m_boneMap;

            // All skeletons contain at least the rood node that is unique
            // One node means that there is not bone hierarchy
            std::shared_ptr<BoneNode> m_rootNode;

            // In fact, it is just the inverse offset matrix of the root node
            glm::mat4x4 m_globalInverseTransform;

        public:
            // Signals
            Event::Connection::Signal<> m_boneMapChanged;

        };
    }
}
