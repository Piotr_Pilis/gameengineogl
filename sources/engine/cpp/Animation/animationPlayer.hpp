#pragma once
#include <glad/glad.h>
#include <glm/mat4x4.hpp>
#include <vector>
#include <memory>

#include "Entity/updatable.hpp"
#include "Event/connection.hpp"
#include "Event/timer.hpp"

namespace Engine
{
    namespace Animation
    {
        class Animation;
        class Skeleton;
        class BoneNode;

        /*
            Animates skeletal and contains result of this action
        */
        class AnimationPlayer :
            public Entity::Updatable
        { 
        public:
            typedef std::shared_ptr<Animation> AnimationPtr;
            typedef std::shared_ptr<Skeleton> SkeletonPtr;

            typedef std::vector<glm::mat4x4> BoneTransformations;

        public:
            enum class State
            {
                STOPPED,
                PAUSED,
                PLAYING
            };

        public:
            AnimationPlayer() = delete;
            AnimationPlayer(const SkeletonPtr& skeleton);
            ~AnimationPlayer();

            // States
            void play();
            void pause();
            void stop();

            // Setters
            // Resets time of animation
            void setAnimation(const AnimationPtr& animation);

            // Action
            // Used by rendering manager
            void update() override;

            // Returns last computed animation result
            const BoneTransformations& getResult() const;

            // Returns state of animation player
            const State getState() const;

            // Does it use given skeleton?
            bool uses(const Skeleton& skeleton) const;
            
        private:
            // Binds signals etc.
            void initialize();

            // Sets size of m_boneTransformations and resets values
            void resetResult();

            // Links (or gets previously linked) animation nodes to skeleton.
            //  Creates categories of AnimationNodes
            void linkAnimationNodes();

            // Recalculates the animation result of the indicated node
            // Animation time is not real time in seconds! This is realtime * ticksPerSeconds
            void updateNodeHierarchy
            (
                const GLfloat& animationTime,
                const BoneNode& boneNode,
                const glm::mat4x4& parentTransformation
            );

        private:
            // External properties
            AnimationPtr m_animation;
            SkeletonPtr m_skeleton;

            // Internal properties
            State m_state = State::STOPPED;
            Event::Timer m_animationTimer;

            // Size of this property depends on used bone nodes.
            BoneTransformations m_boneTransformations;

        public:
            // Signals
            Event::Connection::Signal<> m_stateChanged;

        };
    }
}
