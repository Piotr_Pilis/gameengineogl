#include "boneNode.hpp"

namespace Engine
{
    namespace Animation
    {
        BoneNode::BoneNode()
        {
        }

        BoneNode::~BoneNode()
        {
        }

        void BoneNode::setName(const std::string& name)
        {
            m_name = name; 
        }

        const std::string& BoneNode::getName() const 
        {
            return m_name; 
        }

        void BoneNode::setDefaultTransformationMatrix(const glm::mat4x4& defaultTransformationMatrix)
        {
            m_defaultTransformationMatrix = defaultTransformationMatrix; 
        }

        const glm::mat4x4& BoneNode::getDefaultTransformationMatrix() const
        {
            return m_defaultTransformationMatrix; 
        }

        void BoneNode::pushChild(const std::shared_ptr<BoneNode>& boneNode)
        {
            m_children.push_back(boneNode);
        }

        const std::vector<std::shared_ptr<BoneNode>>& BoneNode::getChildren() const
        {
            return m_children; 
        }

        bool BoneNode::hasBoneData() const
        {
            return m_boneData.get() != nullptr;
        }

        void BoneNode::makeBoneData(const BoneData& boneData)
        {
            m_boneData = std::make_unique<BoneData>(boneData);
        }

        const std::unique_ptr<BoneNode::BoneData>& BoneNode::getBoneData() const
        {
            return m_boneData;
        }

        bool BoneNode::containsNode(const BoneNode& boneNode) const
        {
            for (auto& child : m_children)
            {
                if (child.get() == &boneNode)
                {
                    return true;
                }
            }
            return false;
        }

        /* BoneData */
        void BoneNode::BoneData::setId(const GLuint& id)
        {
            m_id = id;
        }

        const GLuint& BoneNode::BoneData::getId() const
        {
            return m_id; 
        }

        void BoneNode::BoneData::setOffsetMatrix(const glm::mat4x4& offsetMatrix)
        {
            m_offsetMatrix = offsetMatrix; 
        }

        const glm::mat4x4& BoneNode::BoneData::getOffsetMatrix() const
        {
            return m_offsetMatrix;
        }
    }
}
