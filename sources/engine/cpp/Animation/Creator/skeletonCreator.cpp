#include <assimp/Importer.hpp>
#include <assimp/scene.h>

#include "FileSystem/ifstream.hpp"
#include "Log/warningLog.hpp"

#include "skeletonCreator.hpp"

namespace Engine
{
    namespace Animation
    {
        namespace Creator
        {
            bool SkeletonCreator::loadSkeleton(const std::string& path, std::shared_ptr<Skeleton>& skeleton)
            {
                Assimp::Importer aiImporter;
                try
                {
                    FileSystem::Ifstream fin(path);
                    std::stringstream bufferStream;
                    bufferStream << fin.rdbuf();
                    auto buffer = bufferStream.str();

                    auto aiScene = aiImporter.ReadFileFromMemory
                    (
                        buffer.c_str(),
                        buffer.size(),
                        0
                    );
                    if (!aiScene)
                    {
                        WARNING_LOG("Skeleton file '" + path + "' cannot be parsed, reason: " +
                            aiImporter.GetErrorString());
                        return false;
                    }

                    return SkeletonCreator::loadSkeleton(aiScene->mRootNode, skeleton);
                }
                catch (std::invalid_argument invalidArgument)
                {
                    WARNING_LOG("Could not open '" + path + "'");
                    return false;
                }
                return false;
            }

            bool SkeletonCreator::loadSkeleton(aiNode* rootNode, std::shared_ptr<Skeleton>& skeleton)
            {
                skeleton = std::shared_ptr<Skeleton>(new Skeleton());
                return skeleton->loadSkeleton(rootNode);
            }
        }
    }
}
