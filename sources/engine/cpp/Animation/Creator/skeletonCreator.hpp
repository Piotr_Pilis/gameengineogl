#pragma once
#include <glad/glad.h>
#include <string>
#include <memory>
#include "Animation/skeleton.hpp"

struct aiNode;
namespace Engine
{
    namespace Geometry
    {
        namespace Creator
        {
            class MeshCreator;
        }
    }

    namespace Animation
    {
        namespace Creator
        {
            /*
            Access for specified classes only
            */
            class SkeletonCreator
            {
                // Only Mesh can load and manipulate this data 
                friend class Geometry::Creator::MeshCreator;

            private:
                SkeletonCreator() = default;

                // Full path to the skeleton file
                static bool loadSkeleton
                (
                    const std::string& path, 
                    std::shared_ptr<Skeleton>& skeleton
                );
                static bool loadSkeleton
                (
                    aiNode* rootNode, 
                    std::shared_ptr<Skeleton>& skeleton
                );

            };
        }
    }
}
