#pragma once
#include <glad/glad.h>
#include <vector>
#include <string>
#include <memory>
#include "Animation/animation.hpp"

namespace Engine
{
    namespace Geometry
    {
        class ObjectManager;
    }

    namespace Animation
    {
        namespace Creator
        {
            /*
            Access for specified classes only
            */
            class AnimationCreator
            {
                // Only ObjectManager can load and manipulate this data 
                friend class Geometry::ObjectManager;

            private:
                AnimationCreator() = default;

                // Full path to the animations file
                static bool loadAnimations
                (
                    const std::string& path,
                    std::vector<std::shared_ptr<Animation>>& animations
                );

            };
        }
    }
}
