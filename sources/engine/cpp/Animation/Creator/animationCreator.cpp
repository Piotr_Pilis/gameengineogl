#include <assimp/Importer.hpp>
#include <assimp/scene.h>

#include "FileSystem/assimpIOSystem.hpp"
#include "Log/warningLog.hpp"

#include "AnimationCreator.hpp"

namespace Engine
{
    namespace Animation
    {
        namespace Creator
        {
            bool AnimationCreator::loadAnimations
            (
                const std::string& path,
                std::vector<std::shared_ptr<Animation>>& animations
            )
            {
                Assimp::Importer aiImporter;
                try
                {
                    aiImporter.SetIOHandler(new FileSystem::AssimpIOSystem());
                    auto aiScene = aiImporter.ReadFile
                    (
                        path,
                        0
                    );
                    if (!aiScene)
                    {
                        WARNING_LOG("Animations file '" + path + "' cannot be parsed, reason: " +
                            aiImporter.GetErrorString());
                        return false;
                    }

                    auto numAnimations = aiScene->mNumAnimations;
                    if (numAnimations > 0)
                        animations.resize(numAnimations);
                    for (GLuint animationId = 0; animationId < numAnimations; ++animationId)
                    {
                        auto aiAnimation = aiScene->mAnimations[animationId];
                        animations[animationId] = std::shared_ptr<Animation>(new Animation());
                        if (!animations[animationId]->loadAnimation(aiAnimation))
                        {
                            animations.clear();
                            return false;
                        }
                    }
                    return true;
                }
                catch (std::invalid_argument invalidArgument)
                {
                    WARNING_LOG("Could not open '" + path + "'");
                    return false;
                }
                return false;
            }
        }
    }
}
