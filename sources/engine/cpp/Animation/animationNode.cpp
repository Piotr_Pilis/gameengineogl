#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/transform.hpp>
#include <assimp/scene.h>
#include <algorithm>
#include "../Log/criticalLog.hpp"
#include "animationNode.hpp"

namespace Engine
{
    namespace Animation
    {
        bool AnimationNode::loadAnimationNode(aiNodeAnim* aiNodeAnim)
        {
            if (!aiNodeAnim)
                return false;

            m_nodeName = std::string(aiNodeAnim->mNodeName.data);
            auto numPositionKeys = aiNodeAnim->mNumPositionKeys;
            auto numScalingKeys = aiNodeAnim->mNumScalingKeys;
            auto numRotationKeys = aiNodeAnim->mNumRotationKeys;

            if (numPositionKeys > 0)
                m_positionKeys.reserve(numPositionKeys);
            if (numScalingKeys > 0)
                m_scalingKeys.reserve(numScalingKeys);
            if (numRotationKeys > 0)
                m_rotationKeys.reserve(numRotationKeys);

            for (GLuint positionKeyId = 0; positionKeyId < numPositionKeys; ++positionKeyId)
            {
                auto& aiPositionKey = aiNodeAnim->mPositionKeys[positionKeyId];
                m_positionKeys.push_back
                (
                    PositionKey
                    (
                        static_cast<GLfloat>(aiPositionKey.mTime),
                        glm::vec3
                        {
                            aiPositionKey.mValue.x,
                            aiPositionKey.mValue.y,
                            aiPositionKey.mValue.z
                        }
                    )
                );
            }
            for (GLuint scalingKeyId = 0; scalingKeyId < numScalingKeys; ++scalingKeyId)
            {
                auto& aiScalingKey = aiNodeAnim->mScalingKeys[scalingKeyId];
                m_scalingKeys.push_back
                (
                    ScalingKey
                    (
                        static_cast<GLfloat>(aiScalingKey.mTime),
                        glm::vec3
                        {
                            aiScalingKey.mValue.x,
                            aiScalingKey.mValue.y,
                            aiScalingKey.mValue.z
                        }
                    )
                );
            }
            for (GLuint rotationKeyId = 0; rotationKeyId < numRotationKeys; ++rotationKeyId)
            {
                auto& aiRotationKey = aiNodeAnim->mRotationKeys[rotationKeyId];
                m_rotationKeys.push_back
                (
                    RotationKey
                    (
                        static_cast<GLfloat>(aiRotationKey.mTime),
                        glm::quat
                        {
                            aiRotationKey.mValue.w,
                            aiRotationKey.mValue.x,
                            aiRotationKey.mValue.y,
                            aiRotationKey.mValue.z
                        }
                    )
                );
            }

            std::sort(m_positionKeys.begin(), m_positionKeys.end(),
                [](const PositionKey& a, const PositionKey& b)
            {
                return a.getTime() < b.getTime();
            });
            std::sort(m_scalingKeys.begin(), m_scalingKeys.end(),
                [](const ScalingKey& a, const ScalingKey& b)
            {
                return a.getTime() < b.getTime();
            });
            std::sort(m_rotationKeys.begin(), m_rotationKeys.end(),
                [](const RotationKey& a, const RotationKey& b)
            {
                return a.getTime() < b.getTime();
            });

            return true;
        }

        void AnimationNode::calculateTransformationMatrix
        (
            glm::mat4x4& resultMatrix,
            const GLfloat& animationTime
        ) const
        {
            glm::vec3 interpolatedPositionValue;
            glm::vec3 interpolatedScalingValue;
            glm::quat interpolatedRotationValue;
            getInterpolatedPositionValue
            (
                interpolatedPositionValue,
                animationTime
            );
            getInterpolatedScalingValue
            (
                interpolatedScalingValue,
                animationTime
            );
            getInterpolatedRotationValue
            (
                interpolatedRotationValue,
                animationTime
            );

            resultMatrix =
                glm::translate(interpolatedPositionValue) *
                glm::toMat4(interpolatedRotationValue) *
                glm::scale(interpolatedScalingValue);
        }

        void AnimationNode::getInterpolatedPositionValue
        (
            glm::vec3& result,
            const GLfloat& animationTime
        ) const
        {
            if (m_positionKeys.size() == 1)
            {
                result = m_positionKeys[0].getValue();
                return;
            }

            // Last key will be never reached, because last key is for last animationTime - we use modulo from this value
            auto animationKeyId = getAnimationKeyId(m_positionKeys, animationTime);
            auto nextAnimationKeyId = animationKeyId + 1;

            GLfloat deltaTime =
                m_positionKeys[nextAnimationKeyId].getTime() -
                m_positionKeys[animationKeyId].getTime();
            if (deltaTime == 0)
                deltaTime = 0.0000001f;

            GLfloat interpolationFactor =
                (animationTime - m_positionKeys[animationKeyId].getTime()) / deltaTime;
            auto deltaValue =
                m_positionKeys[nextAnimationKeyId].getValue() -
                m_positionKeys[animationKeyId].getValue();
            result = m_positionKeys[animationKeyId].getValue() + deltaValue * interpolationFactor;
        }

        void AnimationNode::getInterpolatedScalingValue
        (
            glm::vec3& result,
            const GLfloat& animationTime
        ) const
        {
            if (m_scalingKeys.size() == 1)
            {
                result = m_scalingKeys[0].getValue();
                return;
            }

            // Last key will be never reached, because last key is for last animationTime - we use modulo from this value
            auto animationKeyId = getAnimationKeyId(m_scalingKeys, animationTime);
            auto nextAnimationKeyId = animationKeyId + 1;

            GLfloat deltaTime =
                m_scalingKeys[nextAnimationKeyId].getTime() -
                m_scalingKeys[animationKeyId].getTime();
            if (deltaTime == 0)
                deltaTime = 0.0000001f;

            GLfloat interpolationFactor =
                (animationTime - m_scalingKeys[animationKeyId].getTime()) / deltaTime;
            auto deltaValue =
                m_scalingKeys[nextAnimationKeyId].getValue() -
                m_scalingKeys[animationKeyId].getValue();
            result = m_scalingKeys[animationKeyId].getValue() + deltaValue * interpolationFactor;
        }

        void AnimationNode::getInterpolatedRotationValue
        (
            glm::quat& result,
            const GLfloat& animationTime
        ) const
        {
            if (m_rotationKeys.size() == 1)
            {
                result = m_rotationKeys[0].getValue();
                return;
            }

            // Last key will be never reached, because last key is for last animationTime - we use modulo from this value
            auto animationKeyId = getAnimationKeyId(m_rotationKeys, animationTime);
            auto nextAnimationKeyId = animationKeyId + 1; 

            GLfloat deltaTime =
                m_rotationKeys[nextAnimationKeyId].getTime() -
                m_rotationKeys[animationKeyId].getTime();
            if (deltaTime == 0)
                deltaTime = 0.0000001f;

            GLfloat interpolationFactor =
                (animationTime - m_rotationKeys[animationKeyId].getTime()) / deltaTime;
            result = glm::mix
            (
                m_rotationKeys[animationKeyId].getValue(), 
                m_rotationKeys[nextAnimationKeyId].getValue(),
                interpolationFactor
            );
        }

        template<class AnimationKey>
        GLuint AnimationNode::getAnimationKeyId
        (
            const std::vector<AnimationKey>& animationKeys,
            const GLfloat& animationTime
        ) const
        {
            for (GLuint keyId = 0; keyId < animationKeys.size() - 1; ++keyId)
            {
                // Last key will be never reached, because last key is for last animationTime - we use modulo from this value
                if (animationKeys[keyId + 1].getTime() > animationTime)
                    return keyId;
            }

            CRITICAL_LOG
            (
                "Skeletal animation is corrupted", 
                "Skeletal animation doesn't contain key with given time"
            );
            assert(0);
            return 0;
        }
    }
}
