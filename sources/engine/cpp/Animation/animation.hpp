#pragma once
#include <glad/glad.h>
#include <vector>
#include <string>

#include "animationNode.hpp"

struct aiAnimation;
namespace Engine
{
    namespace Animation
    {
        namespace Creator
        {
            class AnimationCreator;
        }

        /*
        
        */
        class Animation
        {
            friend class Creator::AnimationCreator;

        private:
            Animation();

        public:
            ~Animation();

            // Getters
            const std::string& getName() const;
            const GLdouble& getDuration() const;
            const GLdouble& getTicksPerSecond() const;

            const std::vector<AnimationNode>& getNodes() const;
            const AnimationNode* const getNodeByName(const std::string& nodeName) const;

        private:
            // Only Creator can use this method
            bool loadAnimation
            (
                aiAnimation* aiAnimation
            );

        private:
            std::string m_name = "";
            GLdouble m_duration = 0.0;
            GLdouble m_ticksPerSecond = 0.0;
            std::vector<AnimationNode> m_animationNodes;

        };
    }
}
