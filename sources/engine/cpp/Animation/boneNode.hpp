#pragma once
#include <glad/glad.h>
#include <glm/mat4x4.hpp>
#include <string>
#include <vector>
#include <memory>

namespace Engine
{
    namespace Animation
    {
        /*
        
        */
        class BoneNode
        {
        public:
            class BoneData
            {
            public:
                void setId(const GLuint& id);
                const GLuint& getId() const;

                void setOffsetMatrix(const glm::mat4x4& offsetMatrix);
                const glm::mat4x4& getOffsetMatrix() const;

            private:
                GLuint m_id;
                glm::mat4x4 m_offsetMatrix; // Used to transform from node space to geometry space

            };

        public:
            BoneNode();
            ~BoneNode();

            void setName(const std::string& name);
            const std::string& getName() const;

            void setDefaultTransformationMatrix(const glm::mat4x4& defaultTransformationMatrix);
            const glm::mat4x4& getDefaultTransformationMatrix() const;

            void pushChild(const std::shared_ptr<BoneNode>& boneNode);
            const std::vector<std::shared_ptr<BoneNode>>& getChildren() const;

            bool hasBoneData() const;
            void makeBoneData(const BoneData& boneData);
            const std::unique_ptr<BoneData>& getBoneData() const;

            // This function is not recursive
            bool containsNode(const BoneNode& boneNode) const;

        private:
            std::string m_name;
            glm::mat4x4 m_defaultTransformationMatrix;
            std::vector<std::shared_ptr<BoneNode>> m_children;

            std::unique_ptr<BoneData> m_boneData = nullptr;

        };
    }
}
