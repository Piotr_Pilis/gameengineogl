#pragma once
#define GLM_ENABLE_EXPERIMENTAL
#include <glad/glad.h>
#include <glm/vec3.hpp>
#include <glm/gtx/quaternion.hpp>

namespace Engine
{
    namespace Animation
    {
        /*
        
        */
        template<class VALUE_TYPE>
        class AnimationKey
        {
        public:
            AnimationKey() = delete;
            AnimationKey(const GLfloat& time, const VALUE_TYPE& value) :
                m_time(time),
                m_value(value)
            {}
            ~AnimationKey() = default;

            const GLfloat& getTime() const { return m_time; }
            const VALUE_TYPE& getValue() const { return m_value; }

        private:
            GLfloat m_time;
            VALUE_TYPE m_value;

        };

        typedef AnimationKey<glm::vec3> PositionKey;
        typedef AnimationKey<glm::vec3> ScalingKey;
        typedef AnimationKey<glm::quat> RotationKey;
    }
}
