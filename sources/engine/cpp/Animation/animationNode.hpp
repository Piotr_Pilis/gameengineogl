#pragma once
#include <glad/glad.h>
#include <vector>
#include <string>
#include <glm/mat4x4.hpp>

#include "Feature/categorizable.hpp"
#include "animationKey.hpp"

struct aiNodeAnim;
namespace Engine
{
    namespace Animation
    {
        /*
        This class contains all animation keys of single bone node
        */
        class AnimationNode :
            public Feature::Categorizable
        {
        public:
            AnimationNode() = default;
            ~AnimationNode() = default;

            bool loadAnimationNode(aiNodeAnim* aiNodeAnim);

            void setNodeName(const std::string& nodeName) { m_nodeName = nodeName; }
            const std::string& getNodeName() const { return m_nodeName; }

            void calculateTransformationMatrix
            (
                glm::mat4x4& resultMatrix, 
                const GLfloat& animationTime
            ) const;

        private:
            void getInterpolatedPositionValue
            (
                glm::vec3& result,
                const GLfloat& animationTime
            ) const;
            void getInterpolatedScalingValue
            (
                glm::vec3& result,
                const GLfloat& animationTime
            ) const;
            void getInterpolatedRotationValue
            (
                glm::quat& result,
                const GLfloat& animationTime
            ) const;

            template<class AnimationKey>
            GLuint getAnimationKeyId
            (
                const std::vector<AnimationKey>& animationKeys,
                const GLfloat& animationTime
            ) const;

        private:
            std::string m_nodeName;
            std::vector<PositionKey> m_positionKeys;
            std::vector<ScalingKey> m_scalingKeys;
            std::vector<RotationKey> m_rotationKeys;

        };
    }
}
