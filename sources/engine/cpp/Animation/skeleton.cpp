#include <assimp/Importer.hpp>
#include <assimp/scene.h>

#include "../FileSystem/ifstream.hpp"
#include "../Log/warningLog.hpp"
#include "skeleton.hpp"

namespace Engine
{
    namespace Animation
    {
        Skeleton::Skeleton()
        {
            reset();
        }

        Skeleton::~Skeleton()
        {
        }

        void Skeleton::reset()
        {
            // Root bone
            m_rootNode = std::make_shared<BoneNode>();
            clearBoneMapping();
        }

        bool Skeleton::loadSkeleton(aiNode* rootNode)
        {
            reset();
            bool result = extractNode(rootNode, *m_rootNode);
            if (result)
            {
                m_globalInverseTransform = glm::inverse(m_rootNode->getDefaultTransformationMatrix());
            }
            return result;
        }

        bool Skeleton::extractNode(aiNode* aiNode, BoneNode& node)
        {
            if (!aiNode)
                return false;
            glm::mat4x4 defaultTransformationMatrix;
            defaultTransformationMatrix[0][0] = aiNode->mTransformation.a1;
            defaultTransformationMatrix[0][1] = aiNode->mTransformation.b1;
            defaultTransformationMatrix[0][2] = aiNode->mTransformation.c1;
            defaultTransformationMatrix[0][3] = aiNode->mTransformation.d1;

            defaultTransformationMatrix[1][0] = aiNode->mTransformation.a2;
            defaultTransformationMatrix[1][1] = aiNode->mTransformation.b2;
            defaultTransformationMatrix[1][2] = aiNode->mTransformation.c2;
            defaultTransformationMatrix[1][3] = aiNode->mTransformation.d2;

            defaultTransformationMatrix[2][0] = aiNode->mTransformation.a3;
            defaultTransformationMatrix[2][1] = aiNode->mTransformation.b3;
            defaultTransformationMatrix[2][2] = aiNode->mTransformation.c3;
            defaultTransformationMatrix[2][3] = aiNode->mTransformation.d3;

            defaultTransformationMatrix[3][0] = aiNode->mTransformation.a4;
            defaultTransformationMatrix[3][1] = aiNode->mTransformation.b4;
            defaultTransformationMatrix[3][2] = aiNode->mTransformation.c4;
            defaultTransformationMatrix[3][3] = aiNode->mTransformation.d4;

            node.setName(std::string(aiNode->mName.data));
            node.setDefaultTransformationMatrix(defaultTransformationMatrix);

            // Extracts children nodes
            for (GLuint childrenId = 0; childrenId < aiNode->mNumChildren; ++childrenId)
            {
                auto aiChildrenNode = aiNode->mChildren[childrenId];
                std::shared_ptr<BoneNode> childrenNode = std::make_shared<BoneNode>();
                if (!extractNode(aiChildrenNode, *childrenNode))
                    return false;
                node.pushChild(childrenNode);
            }
            return true;
        }

        const BoneNode* Skeleton::getNodeByName
        (
            const std::string& nodeName
        ) const
        {
            return getNodeByName
            (
                nodeName,
                *m_rootNode
            );
        }

        const BoneNode* Skeleton::getNodeByName
        (
            const std::string& nodeName,
            const BoneNode& currentNode
        ) const
        {
            if (currentNode.getName() == nodeName)
                return &currentNode;

            auto& childrens = currentNode.getChildren();
            for (GLuint childrenId = 0; childrenId < childrens.size(); ++childrenId)
            {
                auto result = getNodeByName
                (
                    nodeName,
                    *childrens[childrenId]
                );
                if (result != nullptr)
                    return result;
            }
            return nullptr;
        }

        const BoneNode& Skeleton::getRootNode() const
        {
            return *m_rootNode;
        }

        std::shared_ptr<BoneNode> Skeleton::getNodeAsSharedPtr
        (
            const BoneNode& node,
            const BoneNode& currentNode
        ) const
        {
            for (auto& child : currentNode.getChildren())
            {
                if (child.get() == &node)
                {
                    return child;
                }
                auto bineAsSharedPtr = getNodeAsSharedPtr(node, *child);
                if (bineAsSharedPtr.get() != nullptr)
                    return bineAsSharedPtr;
            }
            return nullptr;
        }

        bool Skeleton::containsBoneHierarchy() const
        {
            return m_rootNode->getChildren().size() != 0;
        }

        bool Skeleton::containsNode(const BoneNode& node) const
        {
            return containsNode(node, *m_rootNode);
        }

        bool Skeleton::containsNode
        (
            const BoneNode& node,
            const BoneNode& currentNode
        ) const
        {
            for (auto& child : currentNode.getChildren())
            {
                if (child.get() == &node)
                {
                    return true;
                }
                if (containsNode(node, *child))
                    return true;
            }
            return false;
        }

        /* Bane mapping */
        void Skeleton::clearBoneMapping()
        {
            m_boneMap.clear();
        }

        const Skeleton::BoneMap& Skeleton::getBoneMap() const
        {
            return m_boneMap;
        }

        bool Skeleton::createBoneFromNode(const BoneNode& node, const glm::mat4x4& boneOffsetMatrix)
        {
            // Casts node to shared pointer
            std::shared_ptr<BoneNode> boneAsSharedPtr;
            if (m_rootNode.get() == &node)
                boneAsSharedPtr = m_rootNode;
            else
                boneAsSharedPtr = getNodeAsSharedPtr(node, *m_rootNode);

            if (boneAsSharedPtr.get() == nullptr)
            {
                WARNING_LOG("Given node is not part of this skeleton");
                return false;
            }
            
            // Bone has not been added yet
            if (!boneAsSharedPtr->hasBoneData())
            {
                // Creates bone id
                auto boneId = static_cast<GLuint>(m_boneMap.size());
                m_boneMap.push_back(boneAsSharedPtr);

                // Creates bone data
                BoneNode::BoneData boneData;
                boneData.setId(boneId);
                boneData.setOffsetMatrix(boneOffsetMatrix);
                boneAsSharedPtr->makeBoneData(boneData);

                // Emits signal
                m_boneMapChanged.callSlots();
                return true;
            }

            // Bone has already been added - checks only validation
            return boneAsSharedPtr->getBoneData()->getOffsetMatrix() == boneOffsetMatrix;
        }

        bool Skeleton::getBoneId(const BoneNode& node, GLuint& boneId)
        {
            // Finds bone id by node
            for (GLuint currentBoneId = 0; currentBoneId < m_boneMap.size(); ++currentBoneId)
            {
                auto& currentBone = m_boneMap[currentBoneId];
                if (currentBone.get() == &node)
                { 
                    boneId = currentBoneId;
                    return true;
                }
            }
            return false;
        }

        const glm::mat4x4& Skeleton::getGlobalInverseTransform() const
        {
            return m_globalInverseTransform;
        }
    }
}
