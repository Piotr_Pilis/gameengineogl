#include <assimp/anim.h>
#include <algorithm>

#include "animation.hpp"

namespace Engine
{
    namespace Animation
    {
        Animation::Animation()
        {
        }

        Animation::~Animation()
        {
        }

        bool Animation::loadAnimation
        (
            aiAnimation* aiAnimation
        )
        {
            m_animationNodes.clear();
            if (!aiAnimation)
                return false;

            m_name = std::string(aiAnimation->mName.C_Str(), aiAnimation->mName.length);
            m_duration = aiAnimation->mDuration;
            m_ticksPerSecond = aiAnimation->mTicksPerSecond;

            auto& numChannels = aiAnimation->mNumChannels;
            if (numChannels > 0)
                m_animationNodes.resize(numChannels);
            for (GLuint channelId = 0; channelId < numChannels; ++channelId)
            {
                auto aiChannel = aiAnimation->mChannels[channelId];
                if (!m_animationNodes[channelId].loadAnimationNode(aiChannel))
                    return false;
            }
            return true;
        }

        const std::string& Animation::getName() const
        {
            return m_name;
        }

        const GLdouble& Animation::getDuration() const
        {
            return m_duration;
        }

        const GLdouble& Animation::getTicksPerSecond() const
        {
            return m_ticksPerSecond;
        }

        const std::vector<AnimationNode>& Animation::getNodes() const
        {
            return m_animationNodes;
        }

        const AnimationNode* const Animation::getNodeByName(const std::string& nodeName) const
        {
            const auto animationNodeIt = std::find_if
            (
                m_animationNodes.cbegin(),
                m_animationNodes.cend(),
                [nodeName](const auto& animationNode)
            {
                return animationNode.getNodeName() == nodeName;
            });
            if (animationNodeIt == m_animationNodes.cend())
                return nullptr;
            return &(*animationNodeIt);
        }
    }
}
