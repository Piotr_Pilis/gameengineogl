#pragma once
#include <string>
#include <vector>
#include "../../Geometry/object3d.hpp"

namespace Engine
{
    namespace Exporter
    {
        namespace Object
        {
            class ObjectsExporterAbstract
            {
            public:
                ObjectsExporterAbstract() = default;
                ~ObjectsExporterAbstract() = default;

                // Exports informations about transformations of objects to file
                virtual bool export(const std::vector<Geometry::Object3D>& objects, const std::string& outputFilename) = 0;

            };
        }
    }
}
