#include "timer.hpp"
#include "fps.hpp"

namespace Engine
{
    namespace Event
    {
        FPS::FPS() :
            m_fps(0)
        {
            m_timer = std::make_unique<Timer>();
        }

        FPS::~FPS()
        {
        }

        void FPS::tick()
        {
            if (m_timer->getElapsedSeconds() >= 1.0f)
            {
                changed(static_cast<GLuint>(m_fps));
                m_timer->resetTimer();
                m_fps = 0.0f;
            }
            else
                ++m_fps;
        }
    }
}