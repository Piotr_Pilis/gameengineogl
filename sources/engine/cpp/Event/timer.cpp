#include <time.h>
#include "timer.hpp"

namespace Engine
{
    namespace Event
    {
        Timer::Timer() 
        { 
            m_previousTime = static_cast <GLfloat>(clock()) / CLOCKS_PER_SEC;
        };

        Timer::~Timer()
        {
        }

        GLfloat Timer::getElapsedSeconds() 
        {
            GLfloat currentseconds = static_cast <GLfloat>(clock()) / CLOCKS_PER_SEC;
            GLfloat elapsedseconds = currentseconds - m_previousTime;
            return elapsedseconds;
        }

        void Timer::resetTimer() 
        { 
            m_previousTime = static_cast <GLfloat>(clock()) / CLOCKS_PER_SEC; 
        };
    }
}