#pragma once
#include <glad/glad.h>

namespace Engine
{
    namespace Event
    {
        class Timer
        {
        public:
            Timer();
            ~Timer();

            GLfloat getElapsedSeconds();
            void resetTimer();

        protected:
            GLfloat m_previousTime;
        };
    }
}