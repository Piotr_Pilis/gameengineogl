#pragma once
#include <boost/signals2/signal.hpp>

#include <glad/glad.h>
#include <memory>

namespace Engine
{
    namespace Event
    {
        class Timer;
        class FPS
        {
        public:
            FPS();
            ~FPS();

            void tick();

            /* Signals */
            boost::signals2::signal<void(GLuint)>
                changed;

        private:
            std::unique_ptr<Event::Timer> m_timer;
            GLfloat m_fps;
            
        };
    }
}