// This class should be replaced by boost signals2

#pragma once
#include <functional>
#include <vector>
#include <mutex>
#include <memory>

namespace Engine
{
    namespace Event
    {
        namespace Connection
        {
            template<class ...ArgumentTypes>
            using Callback = std::function<void(ArgumentTypes ...arguments)>;

            template<class ...ArgumentTypes>
            using Slot = std::weak_ptr<Callback<ArgumentTypes...>>;

            template<class ...ArgumentTypes>
            class Signal
            {
            public:
                void callSlots(ArgumentTypes ...arguments)
                {
                    std::lock_guard<std::mutex> lockGuard(m_slotsMutex);
                    for (auto& slot : m_slots)
                    {
                        (*slot)(arguments...);
                    }
                }

                Slot<ArgumentTypes...> connect(Callback<ArgumentTypes...>&& slot)
                {
                    std::lock_guard<std::mutex> lockGuard(m_slotsMutex);
                    m_slots.emplace_back
                    (
                        std::make_shared<Callback<ArgumentTypes...>>(std::move(slot))
                    );
                    return m_slots[m_slots.size() - 1];
                }

                void disconnect(const Slot<ArgumentTypes...>& slot)
                {
                    std::lock_guard<std::mutex> lockGuard(m_slotsMutex);
                    auto lockedSlot = slot.lock();
                    if (lockedSlot.get() == nullptr)
                        return;
                    m_slots.erase
                    (
                        std::remove_if
                        (
                            m_slots.begin(),
                            m_slots.end(),
                            [&lockedSlot](const auto& currentSlot)
                    {
                        return lockedSlot == currentSlot;
                    }
                        ),
                        m_slots.end()
                        );
                }

            private:
                std::vector<std::shared_ptr<Callback<ArgumentTypes...>>> m_slots;
                std::mutex m_slotsMutex;

            };
        }
    }
}