#pragma once
#include <string>
#include <memory>
#include <ios>

#include "assimp/IOStream.hpp"

namespace Engine
{
    namespace FileSystem
    {
        class AssimpIOSystem;
        class AssimpIOStream :
            public Assimp::IOStream
        {
            friend class AssimpIOSystem;

        protected:
            AssimpIOStream() = delete;
            AssimpIOStream
            (
                const std::string& filename,
                bool outputMode
            );

        public:
            ~AssimpIOStream(void) override;

            size_t Read(void* buffer, size_t size, size_t count) override;
            size_t Write(const void* buffer, size_t size, size_t count) override;
            aiReturn Seek(size_t offset, aiOrigin origin) override;
            size_t Tell() const override;
            size_t FileSize() const override;
            void Flush() override;

        private:
            bool isOpened() const;

        private:
            std::unique_ptr<std::ios> m_ioStream;

        };
    }
}