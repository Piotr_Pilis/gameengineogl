#pragma once
#include <string>

#ifndef _DEBUG
#include <istream>
#include "Private/baseFstream.hpp"
#else
#include <fstream>
#include <glad/glad.h>
#endif

namespace Engine
{
    namespace FileSystem
    {
        class Ifstream : 
#ifndef _DEBUG
            public Private::BaseFstream, public std::istream
#else
            public std::ifstream
#endif
        {
        public:
            Ifstream(std::string const& filename);
            virtual ~Ifstream();

            GLint64 size();

        };
    }
}