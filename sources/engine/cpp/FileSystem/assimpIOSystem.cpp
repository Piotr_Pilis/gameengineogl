#include "fileSystem.hpp"

#include "assimpIOStream.hpp"
#include "assimpIOSystem.hpp"

namespace Engine
{
    namespace FileSystem
    {
        AssimpIOSystem::AssimpIOSystem()
        {
        }

        bool AssimpIOSystem::Exists(const char* filename) const
        {
            return exists(std::string(filename));
        }

        char AssimpIOSystem::getOsSeparator() const
        {
            return '/';
        }

        Assimp::IOStream* AssimpIOSystem::Open(const char* filename, const char* streamMode)
        {
            auto streamModeAsString = std::string(streamMode);
            bool outputMode = streamModeAsString.find('w') != std::string::npos;
            try
            {
                return new AssimpIOStream
                (
                    std::string(filename),
                    outputMode
                );
            }
            catch (std::invalid_argument exception)
            {
                throw exception;
            }
        }

        void AssimpIOSystem::Close(Assimp::IOStream* ioStream)
        {
            delete ioStream;
        }
    }
}