#include <physfs.h>
#include <filesystem>
#include "../Log/log.hpp"
#include "fileSystem.hpp"

namespace Engine
{
    namespace FileSystem
    {
        namespace
        {
#ifdef _DEBUG
            static std::string g_assetDirectoryPath = "";
#endif
        }

#ifdef _DEBUG
        void setAssetDirectoryPath(const std::string& assetDirectoryPath)
        {
            FileSystem::g_assetDirectoryPath = assetDirectoryPath;
        }

        const std::string& getAssetDirectoryPath()
        {
            return FileSystem::g_assetDirectoryPath;
        }
#endif

        void initialize()
        {
            LOG("Initializing File System...");
            PHYSFS_init(nullptr);
        }

        void deinitialize()
        {
            LOG("Deinitializing File System...");
            PHYSFS_deinit();
        }

        void permitSymbolicLinks(bool allow)
        {
            PHYSFS_permitSymbolicLinks(allow);
        }

        std::vector<std::string> getCDRomDirs()
        {
            std::vector<std::string> directories;
            GLchar** dirBegin = PHYSFS_getCdRomDirs();
            for (GLchar** dir = dirBegin; *dir != NULL; ++dir) 
            {
                directories.push_back(*dir);
            }
            PHYSFS_freeList(dirBegin);
            return directories;
        }

        std::string getBaseDir()
        {
            return PHYSFS_getBaseDir();
        }

        std::string getUserDir()
        {
            return PHYSFS_getUserDir();
        }

        std::string getWriteDir()
        {
            return PHYSFS_getWriteDir();
        }

        void makeDir(const std::string& dirName) 
        {
#ifdef _DEBUG
            std::filesystem::create_directory
            (
                getAssetDirectoryPath() + "/" + dirName
            );
#else
            PHYSFS_mkdir(dirName.c_str());
#endif
        }

        void deleteFile(const std::string& filename) 
        {
#ifdef _DEBUG
            std::filesystem::remove
            (
                getAssetDirectoryPath() + "/" + filename
            );
#else
            PHYSFS_delete(filename.c_str());
#endif
        }

        std::string getRealDir(const std::string& filename)
        {
            return PHYSFS_getRealDir(filename.c_str());
        }

        std::vector<std::string> enumerateEntries(const std::string& directoryPath)
        {
            std::vector<std::string> files;

#ifdef _DEBUG
            if (!exists(directoryPath) || !isDirectory(directoryPath))
            {
                return {};
            }

            const auto fullDirectoryPath = getAssetDirectoryPath() + "/" + directoryPath;
            for (const auto& entry : std::filesystem::directory_iterator(fullDirectoryPath))
            {
                const auto relativeEntryPath = std::filesystem::relative(entry.path(), getAssetDirectoryPath());
                std::string entryPath = std::move(relativeEntryPath.string());
                files.emplace_back(std::move(entryPath));
            }
#else
            GLchar** listBegin = PHYSFS_enumerateFiles(directoryPath.c_str());
            for (GLchar** file = listBegin; *file != NULL; ++file) 
            {
                files.push_back(*file);
            }

            PHYSFS_freeList(listBegin);
#endif

            return files;
        }

        bool exists(const std::string& filename)
        {
#ifdef _DEBUG
            return std::filesystem::exists(getAssetDirectoryPath() + "/" + filename);

#else
            return PHYSFS_exists(filename.c_str()) != 0;
#endif
        }

        bool isDirectory(const std::string& filename)
        {
#ifdef _DEBUG
            return std::filesystem::is_directory
            (
                getAssetDirectoryPath() + "/" + filename
            );
#else
            return PHYSFS_isDirectory(filename.c_str()) != 0;
#endif
        }

        bool isSymbolicLink(const std::string& filename)
        {
#ifdef _DEBUG
            return std::filesystem::is_symlink
            (
                std::filesystem::status
                (
                    getAssetDirectoryPath() + "/" + filename
                )
            );
            
#else
           return PHYSFS_isSymbolicLink(filename.c_str()) != 0;
#endif
        }

        bool isInit() 
        {
            return PHYSFS_isInit() != 0;
        }

        void loadPath(std::string const& path)
        {
            LOG("Loading directory '" + path + "'...");
            if (!std::filesystem::exists(path))
            {
                return;
            }

            for (auto& filename : std::filesystem::directory_iterator(path))
            {
                if (filename.path().extension() == ".pak")
                {
                    LOG("\tLoading file '" + std::filesystem::canonical(filename).string() + "'...");
                    PHYSFS_addToSearchPath(std::filesystem::canonical(filename).string().c_str(), 1);
                }
            }
        }

        void removeFromSearchPath(std::string const& file)
        {
            PHYSFS_removeFromSearchPath(file.c_str());
        }
    }
}