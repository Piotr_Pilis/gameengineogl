#include "ofstream.hpp"
#include "ifstream.hpp"
#include "assimpIOStream.hpp"

namespace Engine
{
    namespace FileSystem
    {
        AssimpIOStream::AssimpIOStream
        (
            const std::string& filename, 
            bool outputMode
        )
        {
            try
            {
                if (outputMode)
                {
                    m_ioStream = std::make_unique<Ofstream>(filename);
                }
                else
                {
                    m_ioStream = std::make_unique<Ifstream>(filename);
                }
            }
            catch (...)
            {
                // No exception - file is just not opened
                //throw std::invalid_argument("Cannot open file stream");
            }
        }

        AssimpIOStream::~AssimpIOStream(void)
        {
        }

        size_t AssimpIOStream::Read(void* buffer, size_t size, size_t count)
        {
            auto ifstream = dynamic_cast<Ifstream*>(m_ioStream.get());
            if (ifstream == nullptr)
                return 0;

            auto fullSize = size * count;
            if (fullSize <= 0)
                return 0;
            ifstream->read(reinterpret_cast<char*>(buffer), fullSize);
            return ifstream->gcount();
        }

        size_t AssimpIOStream::Write(const void* buffer, size_t size, size_t count)
        {
            auto ofstream = dynamic_cast<Ofstream*>(m_ioStream.get());
            if (ofstream == nullptr)
                return 0;

            auto fullSize = size * count;
            if (fullSize <= 0)
                return 0;

            size_t sizeBefore = ofstream->tellp();
            ofstream->write(reinterpret_cast<const char*>(buffer), fullSize);
            size_t numberOfBytesWritten = static_cast<size_t>(ofstream->tellp()) - sizeBefore;
            return numberOfBytesWritten;
        }

        aiReturn AssimpIOStream::Seek(size_t pOffset, aiOrigin pOrigin)
        {
            int way;
            switch (pOrigin)
            {
            case aiOrigin_END:
                way = static_cast<GLint>(std::ios_base::end);
                break;
            case aiOrigin_CUR:
                way = static_cast<GLint>(std::ios_base::cur);
                break;
            case aiOrigin_SET:
            default:
                way = static_cast<GLint>(std::ios_base::beg);
            }

            auto ifstream = dynamic_cast<Ifstream*>(m_ioStream.get());
            if (ifstream != nullptr)
            {
                ifstream->seekg(pOffset, way);
                return aiReturn::aiReturn_SUCCESS;
            }

            auto ofstream = dynamic_cast<Ofstream*>(m_ioStream.get());
            if (ofstream != nullptr)
            {
                ofstream->seekp(pOffset, way);
                return aiReturn::aiReturn_SUCCESS;
            }

            return aiReturn::aiReturn_FAILURE;
        }

        size_t AssimpIOStream::Tell() const
        {
            auto ifstream = dynamic_cast<Ifstream*>(m_ioStream.get());
            if (ifstream != nullptr)
            {
                return ifstream->tellg();
            }

            auto ofstream = dynamic_cast<Ofstream*>(m_ioStream.get());
            if (ofstream != nullptr)
            {
                return ofstream->tellp();
            }

            return 0;
        }

        size_t AssimpIOStream::FileSize() const
        {
            if (!isOpened())
            {
                return 0;
            }

            size_t fileSize = 0;
            auto ifstream = dynamic_cast<Ifstream*>(m_ioStream.get());
            if (ifstream != nullptr)
            {
                auto currentPosition = ifstream->tellg();
                ifstream->seekg(0, ifstream->end);
                fileSize = ifstream->tellg();
                ifstream->seekg(currentPosition, ifstream->beg);
            }
            else
            {
                auto ofstream = dynamic_cast<Ofstream*>(m_ioStream.get());
                if (ofstream != nullptr)
                {
                    auto currentPosition = ofstream->tellp();
                    ofstream->seekp(0, ofstream->end);
                    fileSize = ofstream->tellp();
                    ofstream->seekp(currentPosition, ofstream->beg);
                }
            }

            return fileSize;
        }

        void AssimpIOStream::Flush()
        {
            auto ofstream = dynamic_cast<Ofstream*>(m_ioStream.get());
            if (ofstream != nullptr)
            {
                ofstream->flush();
            }
        }

        bool AssimpIOStream::isOpened() const
        {
            auto ifstream = dynamic_cast<Ifstream*>(m_ioStream.get());
            if (ifstream != nullptr)
            {
                return ifstream->is_open();
            }

            auto ofstream = dynamic_cast<Ofstream*>(m_ioStream.get());
            if (ofstream != nullptr)
            {
                return true;
            }
            return false;
        }
    }
}