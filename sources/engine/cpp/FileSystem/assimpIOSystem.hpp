#pragma once
#include <assimp/IOSystem.hpp>
#include <assimp/IOStream.hpp>

namespace Engine
{
    namespace FileSystem
    {
        class AssimpIOSystem :
            public Assimp::IOSystem
        {
        public:
            AssimpIOSystem();
            ~AssimpIOSystem() override = default;

            // -------------------------------------------------------------------
            /** Tests for the existence of a file at the given path. */
            bool Exists(const char* filename) const override;

            // -------------------------------------------------------------------
            /** Returns the directory separator. */
            char getOsSeparator() const override;

            // -------------------------------------------------------------------
            /** Open a new file with a given path. */
            Assimp::IOStream* Open(const char* filename, const char* streamMode = "rb") override;

            // -------------------------------------------------------------------
            /** Closes the given file and releases all resources associated with it. */
            void Close(Assimp::IOStream* ioStream) override;

        };
    }
}