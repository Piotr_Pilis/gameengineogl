#pragma once

#include <ostream>
#include <string>
#include "Private/baseFstream.hpp"

namespace Engine
{
    namespace FileSystem
    {
        // Only Release is supported right now (to be implemented like Ifstream if will be needed)
        class Ofstream : public Private::BaseFstream, public std::ostream
        {
        public:
            enum class MODE
            {
                OVERWRITE,
                APPEND
            };

        public:
            Ofstream(std::string const& filename, MODE mode = MODE::OVERWRITE);
            virtual ~Ofstream();

        };
    }
}