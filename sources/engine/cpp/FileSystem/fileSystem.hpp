#pragma once

#include <istream>
#include <string>
#include <vector>
#include "Private/baseFstream.hpp"

#ifdef DeleteFile
#undef DeleteFile
#endif

namespace Engine
{
    // All given methods works on virtual files and directories!
    namespace FileSystem
    {
#ifdef _DEBUG
        void setAssetDirectoryPath(const std::string& assetDirectoryPath);
        const std::string& getAssetDirectoryPath();
#endif

        void initialize();
        void deinitialize();

        void permitSymbolicLinks(bool allow);

        /* This is used to retrieve a list of paths representing 
        ** CD-ROM or DVD-ROM drives on the system. 
        ** In the context of game development, this function can be useful when 
        ** you want to access files or resources from physical media like CDs or DVDs. 
        ** It allows you to determine the paths to these drives, so you can read files from them.
        */
        std::vector<std::string> getCDRomDirs();
        std::string getBaseDir();
        std::string getUserDir();
        std::string getWriteDir();
        void makeDir(const std::string& dirName);
        void deleteFile(const std::string& filename);
        std::string getRealDir(const std::string& filename);
        std::vector<std::string> enumerateEntries(const std::string& directory);
        bool exists(const std::string& filename);
        bool isDirectory(const std::string& filename);
        bool isSymbolicLink(const std::string& filename);
        bool isInit();

        // Load all .PAK files from given directory
        void loadPath(std::string const& path);
        void removeFromSearchPath(std::string const& file);
    }
}