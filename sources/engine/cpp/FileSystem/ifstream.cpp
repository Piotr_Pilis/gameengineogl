#include <physfs.h>

#include "Private/fileBuffer.hpp"
#include "fileSystem.hpp"
#include "ifstream.hpp"

namespace Engine
{
    namespace FileSystem
    {
        Ifstream::Ifstream(const std::string& filename) :
#ifndef _DEBUG
            Private::BaseFstream(PHYSFS_openRead(filename.c_str())), std::istream(new Private::FileBuffer(m_file))
#else
            std::ifstream(FileSystem::getAssetDirectoryPath() + "/" + filename, std::ios::binary | std::ios::in)
#endif
        {
#ifndef _DEBUG
            if (m_file == nullptr)
#else
            if (!is_open())
#endif
            {
                throw std::invalid_argument("Cannot open file: " + std::string(filename));
            }
        }

        Ifstream::~Ifstream()
        {
#ifndef _DEBUG
            delete rdbuf();
#endif
        }

        GLint64 Ifstream::size()
        {
#ifndef _DEBUG
            return PHYSFS_fileLength(m_file);
#else
            seekg(0, std::ios::end);
            std::streampos fileSize = tellg();
            seekg(0, std::ios::beg);
            return static_cast<GLint64>(fileSize);
#endif
        }
    }
}