#include <physfs.h>
#include "Private/fileBuffer.hpp"
#include "ofstream.hpp"

namespace Engine
{
    namespace FileSystem
    {
        Ofstream::Ofstream(const std::string& filename, MODE mode)
            : Private::BaseFstream(mode == MODE::OVERWRITE ? 
                PHYSFS_openWrite(filename.c_str()) : PHYSFS_openAppend(filename.c_str())),
            std::ostream(new Private::FileBuffer(m_file))
        {
            if (m_file == nullptr) 
            {
                throw std::invalid_argument("Cannot open file: " + std::string(filename));
            }
        }

        Ofstream::~Ofstream()
        {
            delete rdbuf();
        }
    }
}