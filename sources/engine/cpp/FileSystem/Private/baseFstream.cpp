#include <physfs.h>
#include "baseFstream.hpp"

namespace Engine
{
    namespace FileSystem
    {
        namespace Private
        {
            BaseFstream::BaseFstream(PHYSFS_File* file) :
                m_file(file)
            {
            }

            BaseFstream::~BaseFstream()
            {
                PHYSFS_close(m_file);
            }
        }
    }
}