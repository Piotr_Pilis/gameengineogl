#pragma once

#include <glad/glad.h>
#include <streambuf>
#include <string>

struct PHYSFS_File;

namespace Engine
{
    namespace FileSystem
    {
        namespace Private
        {
            class FileBuffer : public std::streambuf
            {
            private:
                FileBuffer(const FileBuffer&) = delete;
                FileBuffer& operator=(const FileBuffer&) = delete;

                GLint underflow();
                GLint overflow(GLint c = traits_type::eof());

                std::streampos seekoff(std::streamoff pos, std::ios_base::seekdir dir, std::ios_base::openmode mode);
                std::streampos seekpos(std::streampos pos, std::ios_base::openmode mode);

                GLint sync();

            protected:
                PHYSFS_File* const m_file;

            private:
                GLchar* m_buffer;
                GLint64 const m_bufferSize;
            
            public:
                FileBuffer(PHYSFS_File* file, GLint64 bufferSize = 2048);
                ~FileBuffer();

            };
        }
    }
}