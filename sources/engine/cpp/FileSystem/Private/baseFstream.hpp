#pragma once
#include <glad/glad.h>

struct PHYSFS_File;

namespace Engine
{
    namespace FileSystem
    {
        namespace Private
        {
            class BaseFstream 
            {
            protected:
                BaseFstream() = delete;
                BaseFstream(PHYSFS_File* file);
                virtual ~BaseFstream();

            protected:
                PHYSFS_File* const m_file;

            };
        }
    }
}