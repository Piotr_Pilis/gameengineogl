#include <physfs.h>
#include "fileBuffer.hpp"

namespace Engine
{
    namespace FileSystem
    {
        namespace Private
        {
            FileBuffer::FileBuffer(PHYSFS_File* file, GLint64 bufferSize) :
                m_file(file),
                m_bufferSize(bufferSize)
            {
                m_buffer = new char[bufferSize];
                char * end = m_buffer + bufferSize;
                setg(end, end, end);
                setp(m_buffer, end);
            }

            FileBuffer::~FileBuffer()
            {
                sync();
                delete[] m_buffer;
            }

            GLint FileBuffer::underflow()
            {
                if (PHYSFS_eof(m_file))
                    return traits_type::eof();

                GLint64 bytesRead = PHYSFS_read(m_file, m_buffer, 1, static_cast<GLint>(m_bufferSize));
                if (bytesRead < 1)
                    return traits_type::eof();

                setg(m_buffer, m_buffer, m_buffer + bytesRead);
                return (GLubyte)*gptr();
            }

            GLint FileBuffer::overflow(GLint c)
            {
                if (pptr() == pbase() && c == traits_type::eof())
                    return 0;

                if (PHYSFS_write(m_file, pbase(), static_cast<GLint>(pptr() - pbase()), 1) < 1)
                    return traits_type::eof();

                if (c != traits_type::eof())
                    if (PHYSFS_write(m_file, &c, 1, 1) < 1)
                        return traits_type::eof();

                return 0;
            }

            std::streampos FileBuffer::seekoff(std::streamoff pos, std::ios_base::seekdir dir, std::ios_base::openmode mode)
            {
                switch (dir) 
                {
                case std::ios_base::beg:
                    PHYSFS_seek(m_file, pos);
                    break;

                case std::ios_base::cur:
                    PHYSFS_seek(m_file, (PHYSFS_tell(m_file) + pos) - (egptr() - gptr()));
                    break;

                case std::ios_base::end:
                    PHYSFS_seek(m_file, PHYSFS_fileLength(m_file) + pos);
                    break;
                }

                if (mode & std::ios_base::in) {
                    setg(egptr(), egptr(), egptr());
                }
                if (mode & std::ios_base::out) {
                    setp(m_buffer, m_buffer);
                }
                return PHYSFS_tell(m_file);
            }

            std::streampos FileBuffer::seekpos(std::streampos pos, std::ios_base::openmode mode)
            {
                PHYSFS_seek(m_file, pos);
                if (mode & std::ios_base::in) 
                {
                    setg(egptr(), egptr(), egptr());
                }

                if (mode & std::ios_base::out) 
                {
                    setp(m_buffer, m_buffer);
                }

                return PHYSFS_tell(m_file);
            }

            GLint FileBuffer::sync()
            {
                return overflow();
            }
        }
    }
}