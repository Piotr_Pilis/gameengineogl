#pragma once
#include <vector>
#include <memory>
#include "renderable.hpp"
#include "updatable.hpp"

namespace Engine
{
    namespace Entity
    {
        class RenderingManager
        {
        public:
            typedef std::shared_ptr<Renderable> RenderableEntity;
            typedef std::vector<RenderableEntity> RenderableEntities;

            typedef std::shared_ptr<Updatable> UpdatableObject;

        public:
            RenderingManager() = default;
            ~RenderingManager() = default;
           
            // Calls every render function of renderable entities
            void update();
            void render();

            /*// ... Without textures
            void renderForShadow(const GLuint& worldMatrixLocation);
            void renderInstancedForShadow(const GLuint& worldMatrixLocation);*/

            // Queue
            void pushRenderableToQueue(const RenderableEntity& entity);
            void removeRenderableFromQueue(const RenderableEntity& entity);

            void pushUpdatableToQueue(const UpdatableObject& object);
            void removeUpdatableFromQueue(const UpdatableObject& object);

        private:
            // Sorted entities by program
            std::vector<RenderableEntities> m_sortedRenderableEntities;
            std::vector<UpdatableObject> m_updatableObjects;

        };
    }
}