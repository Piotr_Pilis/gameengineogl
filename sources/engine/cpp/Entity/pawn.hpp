#pragma once
#include "Geometry/object3d.hpp"
#include "actor.hpp"

namespace Engine
{
    namespace Entity
    {
        // Static entity
        class Pawn :
            public Geometry::Object3D
        {
        public:
            Pawn();
            ~Pawn() = default;

        };
    }
}