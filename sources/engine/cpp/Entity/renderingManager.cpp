#include <algorithm>

#include "Pipeline/program.hpp"
#include "Log/warningLog.hpp"

#include "renderingManager.hpp"

namespace Engine
{
    namespace Entity
    {
        void RenderingManager::update()
        {
            for (auto& updatableObject : m_updatableObjects)
            {
                updatableObject->update();
            }
        }

        void RenderingManager::render()
        {
            for (auto& renderableEntities : m_sortedRenderableEntities)
            {
                if (renderableEntities.size() > 0)
                    glUseProgram(*renderableEntities[0]->getProgram());

                for (auto& renderableEntity : renderableEntities)
                {
                    renderableEntity->render();
                }
            }
        }

        /*void RenderingManager::RenderForShadow(const GLuint& worldMatrixLocation)
        {
            for (auto& entity : m_entity)
            {
                entity->renderForShadow(worldMatrixLocation);
            }
        }

        void RenderingManager::RenderInstancedForShadow(const GLuint& worldMatrixLocation)
        {
            for (auto& entity : m_instancedEntity)
            {
                entity->renderForShadow(worldMatrixLocation);
            }
        }*/

        void RenderingManager::pushRenderableToQueue(const RenderableEntity& entity)
        {
            if (entity.get() == nullptr)
            {
                WARNING_LOG("Entity is not initialized");
                return;
            }

            // Is already added?
            bool alreadyAdded = false;
            for (auto& renderableEntities : m_sortedRenderableEntities)
            {
                if (std::find
                (
                    renderableEntities.cbegin(), 
                    renderableEntities.cend(), 
                    entity
                ) != renderableEntities.cend())
                {
                    alreadyAdded = true;
                    break;
                }
            }

            // Pushes entity to the rendering queue
            if (!alreadyAdded)
            {
                // Finds the suitable vector with renderable entity (by program)
                const auto& program = entity->getProgram();
                for (auto& renderableEntities : m_sortedRenderableEntities)
                {
                    if (renderableEntities.size() > 0)
                    {
                        const auto& programOfGroup = renderableEntities[0]->getProgram();
                        if (*program == *programOfGroup)
                        {
                            renderableEntities.emplace_back(entity);
                            return;
                        }
                    }
                }

                // Group is not added yet
                m_sortedRenderableEntities.push_back
                (
                    {
                        entity
                    }
                );
            }
        }

        void RenderingManager::removeRenderableFromQueue(const RenderableEntity& entity)
        {
            for (auto& renderableEntities : m_sortedRenderableEntities)
            {
                for (auto renderableEntity = renderableEntities.begin(); 
                    renderableEntity != renderableEntities.end(); 
                    ++renderableEntity)
                {
                    renderableEntities.erase(renderableEntity);
                    return;
                }
            }
        }

        void RenderingManager::pushUpdatableToQueue(const UpdatableObject& object)
        {
            if (object.get() == nullptr)
            {
                WARNING_LOG("Object is not initialized");
                return;
            }

            // Is already added?
            bool alreadyAdded = false;
            if (std::find
            (
                m_updatableObjects.cbegin(),
                m_updatableObjects.cend(),
                object
            ) != m_updatableObjects.cend())
            {
                alreadyAdded = true;
            }

            // Pushes object to the queue
            if (!alreadyAdded)
            {
                m_updatableObjects.emplace_back(object);
            }
        }

        void RenderingManager::removeUpdatableFromQueue(const UpdatableObject& object)
        {
            for (auto updatableObject = m_updatableObjects.begin(); 
                updatableObject != m_updatableObjects.end(); 
                ++updatableObject)
            {
                m_updatableObjects.erase(updatableObject);
                return;
            }
        }
    }
}