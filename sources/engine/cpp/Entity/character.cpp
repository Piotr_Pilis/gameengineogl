#include <algorithm>

#include "Component/Object3D/skeletalAnimationComponent.hpp"
#include "Geometry/meshInstance.hpp"
#include "Geometry/model.hpp"

#include "Log/warningLog.hpp"
#include "character.hpp"

namespace Engine
{
    namespace Entity
    {
        Character::Character() :
            Geometry::Object3D({ Geometry::Object3D::ComponentFlag::SkeletalAnimation })
        {
            m_modelChanged.connect
            (
                std::bind
                (
                    &Character::onModelChanged,
                    this
                )
            );
        }

        void Character::createAnimationPlayer()
        {
            if (m_model.get() == nullptr)
            {
                m_animationPlayer = nullptr;
            }
            else
            {
                // Gets skeleton
                auto& skeleton = m_model->getMeshInstance()->getSkeleton();

                // // Creates new AnimationPlayer
                m_animationPlayer = std::make_shared<Animation::AnimationPlayer>(skeleton);
            }

            // Binds m_animationPlayer with SkeletalAnimationComponent
            bindAnimationPlayer();
        }

        bool Character::setSharedAnimationPlayer(const AnimationPlayerPtr& animationPlayer)
        {
            if (animationPlayer.get() == nullptr)
            {
                WARNING_LOG("Given animation player doesn't exist");
                return false;
            }

            if (m_model.get() == nullptr)
            {
                WARNING_LOG("Model is not set");
                return false;
            }

            // Checks compatibility
            auto& skeleton = m_model->getMeshInstance()->getSkeleton();
            if (!animationPlayer->uses(*skeleton))
            {
                WARNING_LOG("Animation player uses different skeleton");
                return false;
            }

            // Sets shared m_animationPlayer
            m_animationPlayer = animationPlayer;

            // Binds m_animationPlayer with SkeletalAnimationComponent
            bindAnimationPlayer();
            return true;
        }

        void Character::bindAnimationPlayer()
        {
            auto& object3DComponent = getSkeletalAnimationComponent();
            auto skeletalAnimationComponent = dynamic_cast<Component::Object3D::SkeletalAnimationComponent*>(object3DComponent.get());
            skeletalAnimationComponent->setAnimationPlayer(m_animationPlayer);
        }

        void Character::pushAnimations(const std::vector<AnimationPtr>& animations)
        {
            // Merges data
            m_animations.insert(m_animations.end(), animations.begin(), animations.end());

            // Removes duplicated data
            m_animations.erase(unique(m_animations.begin(), m_animations.end()), m_animations.end());
        }

        const Character::AnimationPtr& Character::getAnimation(const std::string& name) const
        {
            for (auto& animation : m_animations)
            {
                if (animation->getName() == name)
                    return animation;
            }
            return nullptr;
        }

        const Character::AnimationPtr& Character::getAnimation(const unsigned& id) const
        {
            if (m_animations.size() > id)
                return m_animations[id];
            return nullptr;
        }

        const Character::AnimationPlayerPtr& Character::getAnimationPlayer() const
        {
            return m_animationPlayer;
        }

        // Slots
        void Character::onModelChanged()
        {
            createAnimationPlayer();
        }
    }
}