#pragma once
#include "Pipeline/program.hpp"
#include "actor.hpp"

namespace Engine
{
    namespace Entity
    {
        /*
        Interface that descriptes renderable actor
        */
        class Renderable :
            public std::enable_shared_from_this<Renderable>
        {
        public:
            virtual void render() = 0;
            virtual void update() = 0;

            // By default, rendering is disabled
            // Object has to be shared_ptr
            bool setRenderingEnabled(bool enabled);

            virtual Pipeline::Program& getProgram() = 0;

        };
    }
}
