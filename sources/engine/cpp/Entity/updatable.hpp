#pragma once
#include <memory>

namespace Engine
{
    namespace Entity
    {
        /*
        Interface that descriptes updatable object
        */
        class Updatable :
            public std::enable_shared_from_this<Updatable>
        {
        public:
            virtual void update() = 0;

        protected:
            // By default, updating is disabled
            // Object has to be shared_ptr
            bool setUpdatingEnabled(bool enabled);

        };
    }
}
