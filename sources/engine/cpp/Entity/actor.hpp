#pragma once

namespace Engine
{
    namespace Entity
    {
        // Something... Pawn, special effect, sound, ...
        class Actor
        {
        public:
            Actor() = default;
            ~Actor() = default;

        };
    }
}