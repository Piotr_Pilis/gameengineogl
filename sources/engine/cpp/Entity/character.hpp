#pragma once
#include <vector>
#include "Animation/animationPlayer.hpp"
#include "Animation/animation.hpp"
#include "Geometry/object3d.hpp"
#include "actor.hpp"

namespace Engine
{
    namespace Animation
    {
        class AnimationPlayer;
        class Animation;
    }

    namespace Entity
    {
        // Entity with skeleton and animations
        class Character :
            public Actor,
            public Geometry::Object3D
        {
        public:
            typedef std::shared_ptr<Animation::Animation> AnimationPtr;
            typedef std::shared_ptr<Animation::AnimationPlayer> AnimationPlayerPtr;

        public:
            // Creates new animation player
            Character();
            ~Character() = default;

            // Use existed animation player (shared animation result)
            bool setSharedAnimationPlayer(const AnimationPlayerPtr& animationPlayer);

            // Animations can be store in separated file
            void pushAnimations(const std::vector<AnimationPtr>& animations);

            // Returns nullptr if doesn't contain
            const AnimationPtr& getAnimation(const std::string& name) const;
            const AnimationPtr& getAnimation(const unsigned& id) const;

            const AnimationPlayerPtr& getAnimationPlayer() const;

        private:
            // Creates unique AnimationPlayer
            void createAnimationPlayer();

            // Binds m_animationPlayer with SkeletalAnimationComponent
            void bindAnimationPlayer();

        private:
            // Slots
            void onModelChanged();

        private:
            std::vector<AnimationPtr> m_animations;
            AnimationPlayerPtr m_animationPlayer;

        };
    }
}