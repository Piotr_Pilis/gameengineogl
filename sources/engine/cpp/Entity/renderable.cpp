#include "renderingManager.hpp"
#include "Log/warningLog.hpp"
#include "engine.hpp"
#include "world.hpp"

#include "renderable.hpp"

namespace Engine
{
    namespace Entity
    {
        bool Renderable::setRenderingEnabled(bool enabled)
        {
            try
            {
                auto& renderingManager = Engine::getInstance().getWorld()->getRenderingManager();
                if(enabled)
                    renderingManager.pushRenderableToQueue(shared_from_this());
                else
                    renderingManager.removeRenderableFromQueue(shared_from_this());
                return true;
            }
            catch (std::bad_weak_ptr& exception)
            {
                // Exception: shared_from_this is called without having std::shared_ptr owning the caller 
                namespace Engine = Engine;
                WARNING_LOG(exception.what());
                return false;
            }
        }
    }
}
