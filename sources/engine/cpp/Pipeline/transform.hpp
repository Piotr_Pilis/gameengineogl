#pragma once
#define GLM_FORCE_CUDA
#include <glad/glad.h>
#include <glm/vec3.hpp>
#include <glm/mat4x4.hpp>

namespace Engine::Pipeline
{
    class Transform
    {
    public:
        Transform() = default;
        Transform(const GLint uniformLocation);

        void setUnifromLocation(const GLint uniformLocation);

        void setTranslation(const glm::vec3& translation);
        void setRotation(const glm::vec3& radianAngles);
        void setScale(const glm::vec3& scale);
        void lookAt(const glm::vec3& direction);

        const glm::vec3& getTranslation() const
        {
            return m_translation;
        }
        const glm::vec3& getRotation() const
        {
            return m_rotation;
        } // In degrees
        const glm::vec3& getScale() const
        {
            return m_scale;
        }

        // Bind to assigned uniform location or ...
        void bindMatrix();

        // ... to given uniform location
        void bindMatrix(const GLint uniformLocation);
        static void bindIdentityMatrix(const GLint uniformLocation);

    private:
        inline void updateMatrix();

    private:
        glm::mat4x4 m_worldMatrix;

        glm::vec3 m_translation;
        glm::vec3 m_rotation;
        glm::vec3 m_scale;

        GLint m_uniformLocation;
        bool m_transformIsUptodate;
    };
}