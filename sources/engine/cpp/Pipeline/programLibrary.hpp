#pragma once
#include <optional>
#include <vector>
#include <map>

#include "shaderVariant.hpp"
#include "shader.hpp"
#include "program.hpp"

namespace Engine
{
    namespace Pipeline
    {
        /*
        ProgramLibrary manages programs - creates or stores (You can use it or not - it's just a helper).
        Programs are identified by Shaders identifiers
        */
        class ProgramLibrary
        {
        private:
            typedef std::vector<std::shared_ptr<Shader>> ShadersOfProgram;

        private:
            ProgramLibrary() = default;
            ~ProgramLibrary() = default;

        public:
            static ProgramLibrary& instance();

            const std::shared_ptr<Shader>& getOrCreateShaderVariant
            (
                const std::string shaderPath,
                const Shader::ShaderType& shaderType,
                const ShaderVariant& shaderVariant
            );

            const std::shared_ptr<Program>& getOrCreateProgram
            (
                bool& newObject,
                const std::shared_ptr<Shader>& vertexShader,
                std::optional<std::shared_ptr<Shader>> tessellationControlShader = std::nullopt,
                std::optional<std::shared_ptr<Shader>> tessellationEvaluationShader = std::nullopt,
                std::optional<std::shared_ptr<Shader>> fragmentShader = std::nullopt,
                std::optional<std::shared_ptr<Shader>> geometryShader = std::nullopt
            );

        private:
            // <shader path, <variant(options), shader>>
            std::map<std::string,
                std::map<ShaderVariant, std::shared_ptr<Shader>>
            > m_shaders;

            // <ShadersOfProgram, <Program>>
            std::map<ShadersOfProgram,
                std::shared_ptr<Program>
            > m_programs;

        };
    }
}