#pragma once
#include <glad/glad.h>
#include "shader.hpp"

namespace Engine
{
    namespace Pipeline
    {
        class Program
        {
        public:
            Program();
            Program(const Shader& SH1);
            Program(const Shader& SH1, const Shader& SH2);
            Program(const Shader& SH1, const Shader& SH2, const Shader& SH3);
            Program(const Shader& SH1, const Shader& SH2, const Shader& SH3, const Shader& SH4);
            Program(const Shader& SH1, const Shader& SH2, const Shader& SH3, const Shader& SH4, const Shader& SH5);
            ~Program();

            void initialize(const Shader& SH1);
            void initialize(const Shader& SH1, const Shader& SH2);
            void initialize(const Shader& SH1, const Shader& SH2, const Shader& SH3);
            void initialize(const Shader& SH1, const Shader& SH2, const Shader& SH3, const Shader& SH4);
            void initialize(const Shader& SH1, const Shader& SH2, const Shader& SH3, const Shader& SH4, const Shader& SH5);

            const GLuint& operator*() const;

            static void bindUniformBlock(const GLuint& program, const GLchar* blockName, const GLuint& blockIndex);
            static void bindBuffer(const GLuint& location, const GLuint& blockIndex);
            GLuint getUniformLocation(const std::string& uniformName);

        private:
            void checkShader(const Shader& shader);

            GLuint m_objProgram;
        };
    }
}