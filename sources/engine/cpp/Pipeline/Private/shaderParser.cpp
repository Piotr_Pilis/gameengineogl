#pragma warning( disable : 4996 )
#include <algorithm>
#include <iostream>
#include <limits>

#include "FileSystem/ifstream.hpp"
#include "Log/criticalLog.hpp"
#include "macros.hpp"

#include "shaderParser.hpp"

namespace Engine
{
    namespace Pipeline
    {
        namespace Private
        {
            ShaderParser::ShaderParser(GLchar*& source) :
                m_source(source)
            {
                if (source == nullptr)
                    CRITICAL_LOG("Data of shader is empty!");
            }

            ShaderParser::~ShaderParser()
            {
            }

            void ShaderParser::parse
            (
                const std::map<std::string, std::string>& values,
                const std::vector<std::string>& sections
            )
            {
                std::string shaderData(m_source);
                m_values = values;
                m_sections = sections;

                GLuint64 statementPosition = shaderData.find("#");
                while (statementPosition != std::string::npos)
                {
                    GLuint64 statementEndPostion;
                    StatementType pragmaType = extractPreprocessorStatementType
                    (
                        shaderData,
                        statementPosition + 1,
                        statementEndPostion
                    );

                    switch (pragmaType)
                    {
                    case StatementType::valueStatement:
                        parseValueStatement
                        (
                            shaderData,
                            statementPosition,
                            statementEndPostion
                        );
                        break;
                    case StatementType::includeStatement:
                        parseIncludeStatement
                        (
                            shaderData,
                            statementPosition,
                            statementEndPostion
                        );
                        break;
                    case StatementType::sectionStatement:
                        parseSectionStatement
                        (
                            shaderData,
                            statementPosition,
                            statementEndPostion
                        );
                        break;
                    }

                    statementPosition = shaderData.find("#", statementPosition + 1);
                }

#if defined(_DEBUG) && PRINT_SHADER_SOURCES
                LOG(shaderData);
#endif

                delete[] m_source;
                m_source = new GLchar[shaderData.length() + 1];
                m_source[shaderData.copy(m_source, -1)] = '\0';
            }

            void ShaderParser::parseValueStatement
            (
                std::string& shaderData,
                const GLuint64& statementPosition,
                const GLuint64& statementEndPostion
            )
            {
                GLuint64 statementValueEndPostion;
                std::string statementValue = extractPreprocessorStatementValue
                (
                    shaderData,
                    statementEndPostion,
                    statementValueEndPostion
                );
                if (m_values.count(statementValue) == 0)
                {
                    CRITICAL_LOG("Key '" + statementValue + "' is not provided!");
                }

                std::string value = m_values[statementValue];
                replacePragmaWithData
                (
                    value,
                    statementPosition,
                    statementValueEndPostion + 1,
                    shaderData
                );
            }

            void ShaderParser::parseIncludeStatement
            (
                std::string& shaderData,
                const GLuint64& statementPosition,
                const GLuint64& statementEndPostion
            )
            {
                GLuint64 statementValueEndPostion;
                std::string statementValue = extractPreprocessorStatementValue
                (
                    shaderData,
                    statementEndPostion,
                    statementValueEndPostion
                );

                try
                {
                    FileSystem::Ifstream fin(statementValue);
                    GLint64 fileSize = fin.size();

                    GLchar* databuff = new GLchar[fileSize + 1];
                    databuff[fileSize] = '\0';
                    fin.read(databuff, fileSize);
                    std::string includeShaderData(databuff);

                    replacePragmaWithData
                    (
                        includeShaderData,
                        statementPosition,
                        statementValueEndPostion + 1,
                        shaderData
                    );
                    delete[] databuff;
                }
                catch (std::invalid_argument st)
                {
                    CRITICAL_LOG("Could not open " + statementValue);
                }
            }

            void ShaderParser::parseSectionStatement
            (
                std::string& shaderData,
                const GLuint64& statementPosition,
                const GLuint64& statementEndPostion
            )
            {
                GLuint64 statementValueEndPostion;
                std::string statementValue = extractPreprocessorStatementValue
                (
                    shaderData,
                    statementEndPostion,
                    statementValueEndPostion
                );
                bool isSectionEnabled = std::find
                (
                    m_sections.begin(), 
                    m_sections.end(), 
                    statementValue
                ) != m_sections.end() && m_sections.size() != 0;

                GLuint64 elseStatementBeginPosition;
                GLuint64 elseStatementEndPosition;
                GLuint64 endSectionStatementBeginPosition;
                GLuint64 endSectionStatementEndPosition;
                findPositionsOfSectionBlock
                (
                    shaderData,
                    statementValueEndPostion + 1,
                    elseStatementBeginPosition,
                    elseStatementEndPosition,
                    endSectionStatementBeginPosition,
                    endSectionStatementEndPosition
                );
                bool hasElseBlock = elseStatementEndPosition != std::numeric_limits<GLuint64>::max();
                
                std::string sectionContent = "";
                if (isSectionEnabled)
                {
                    sectionContent = shaderData.substr
                    (
                        statementValueEndPostion + 1,
                        (hasElseBlock ? elseStatementBeginPosition - 1 : endSectionStatementBeginPosition - 1) -
                        statementValueEndPostion
                    );
                }
                else if(hasElseBlock)
                {
                    sectionContent = shaderData.substr
                    (
                        elseStatementEndPosition + 1,
                        endSectionStatementBeginPosition - elseStatementEndPosition - 1
                    );
                }

                replacePragmaWithData
                (
                    sectionContent,
                    statementPosition,
                    endSectionStatementEndPosition,
                    shaderData
                );
            }

            ShaderParser::StatementType ShaderParser::extractPreprocessorStatementType
            (
                const std::string& shaderData,
                const GLuint64& statementBeginPosition,
                GLuint64& statementEndPostion
            )
            {
                static std::vector<GLchar> possibleEndOfPreprocessorStatement = 
                {
                    '<',
                    ' ',
                    '\r',   // Carriage Return - 'In Windows, a new line is denoted using "\r\n"'
                    '\n'    // New Line
                };

                statementEndPostion = std::numeric_limits<GLuint64>::max();
                for (auto& endCharacterOfPreprocessorStatement: possibleEndOfPreprocessorStatement)
                {
                    GLuint64 newStatementEndPostion = shaderData.find_first_of
                    (
                        endCharacterOfPreprocessorStatement, 
                        statementBeginPosition
                    );
                    if (statementEndPostion == std::string::npos ||
                        statementEndPostion > newStatementEndPostion)
                    {
                        statementEndPostion = newStatementEndPostion;
                    }
                }
                if (statementEndPostion == std::string::npos)
                {
                    CRITICAL_LOG
                    (
                        "Shader is corrupted", 
                        "Preprocessor statement of shader is not valid"
                    );
                    return StatementType::unsupportedStatement;
                }
                std::string statementName = shaderData.substr
                (
                    statementBeginPosition, 
                    statementEndPostion - statementBeginPosition
                );

                if (statementName == "value")
                    return ShaderParser::StatementType::valueStatement;
                else if (statementName == "include")
                    return ShaderParser::StatementType::includeStatement;
                else if (statementName == "section")
                    return ShaderParser::StatementType::sectionStatement;
                else if (statementName == "else")
                    return ShaderParser::StatementType::elseStatement;
                else if (statementName == "endsection")
                    return ShaderParser::StatementType::endSectionStatement;

                return ShaderParser::StatementType::unsupportedStatement;
            }

            std::string ShaderParser::extractPreprocessorStatementValue
            (
                const std::string& shaderData,
                const GLuint64& statementEndPostion,
                GLuint64& statementValueEndPostion
            )
            {
                if (shaderData.size() <= statementEndPostion)
                {
                    CRITICAL_LOG
                    (
                        "Shader is corrupted",
                        "Preprocessor statement of shader is not valid"
                    );
                    return "";
                }
                if (shaderData[statementEndPostion] != '<')
                    return "";
                statementValueEndPostion = shaderData.find_first_of(">", statementEndPostion + 1);
                std::string pragmaValue = shaderData.substr
                (
                    statementEndPostion + 1, 
                    statementValueEndPostion - statementEndPostion - 1
                );
                return pragmaValue;
            }

            void ShaderParser::replacePragmaWithData
            (
                const std::string& data,
                const GLuint64& pragmaFrom,
                const GLuint64& pragmaTo,
                std::string& shaderData
            )
            {
                shaderData.replace(pragmaFrom, pragmaTo - pragmaFrom, data);
            }

            void ShaderParser::findPositionsOfSectionBlock
            (
                const std::string& shaderData,
                const GLuint64& statementEndPostion,
                GLuint64& elseStatementBeginPosition,
                GLuint64& elseStatementEndPosition,
                GLuint64& endSectionStatementBeginPosition,
                GLuint64& endSectionStatementEndPosition
            )
            {
                GLuint blockDepth = 0;
                elseStatementBeginPosition = std::numeric_limits<GLuint64>::max();
                elseStatementEndPosition = elseStatementBeginPosition;
                endSectionStatementBeginPosition = elseStatementBeginPosition;
                endSectionStatementEndPosition = elseStatementBeginPosition;

                GLuint64 statementPosition = shaderData.find("#", statementEndPostion);
                while (statementPosition != std::string::npos)
                {
                    GLuint64 statementEndPostion;
                    StatementType pragmaType = extractPreprocessorStatementType
                    (
                        shaderData,
                        statementPosition + 1,
                        statementEndPostion
                    );
                    switch (pragmaType)
                    {
                    case StatementType::sectionStatement:
                        ++blockDepth;
                        break;
                    case StatementType::elseStatement:
                        if (blockDepth == 0)
                        {
                            elseStatementBeginPosition = statementPosition;
                            elseStatementEndPosition = statementEndPostion;
                        }
                        break;
                    case StatementType::endSectionStatement:
                        if (blockDepth == 0)
                        {
                            endSectionStatementBeginPosition = statementPosition;
                            endSectionStatementEndPosition = statementEndPostion;
                            return;
                        }
                        else
                        {
                            --blockDepth;
                        }
                        break;
                    }
                    statementPosition = shaderData.find("#", statementPosition + 1);
                }
                CRITICAL_LOG
                (
                    "Shader is corrupted",
                    "Block of preprocessor statement is not valid"
                );
            }
        }
    }
}