#pragma once
#include <glad/glad.h>
#include <string>
#include <vector>
#include <map>

namespace Engine
{
    namespace Pipeline
    {
        namespace Private
        {
            class ShaderParser
            {
            private:
                enum class StatementType : GLint
                {
                    unsupportedStatement = -1,
                    valueStatement = 0,
                    includeStatement,
                    sectionStatement,
                    elseStatement,
                    endSectionStatement
                };

            public:
                ShaderParser(GLchar*& source);
                ~ShaderParser();

                /* Shader statements:
                **  #value<key>
                **  #include<key>
                **  #section<key> #else #endsection
                */
                void parse
                (
                    const std::map<std::string, std::string>& values = {},
                    const std::vector<std::string>& sections = {}
                );

            private:
                void parseValueStatement
                (
                    std::string& shaderData,
                    const GLuint64& statementPosition,
                    const GLuint64& statementEndPostion
                );

                void parseIncludeStatement
                (
                    std::string& shaderData,
                    const GLuint64& statementPosition,
                    const GLuint64& statementEndPostion
                );

                void parseSectionStatement
                (
                    std::string& shaderData,
                    const GLuint64& statementPosition,
                    const GLuint64& statementEndPostion
                );

            private:
                StatementType extractPreprocessorStatementType
                (
                    const std::string& shaderData,
                    const GLuint64& statementBeginPosition,
                    GLuint64& statementEndPostion
                );

                std::string extractPreprocessorStatementValue
                (
                    const std::string& shaderData,
                    const GLuint64& statementEndPostion,
                    GLuint64& statementValueEndPostion
                );

                void replacePragmaWithData
                (
                    const std::string& data, 
                    const GLuint64& pragmaFrom, 
                    const GLuint64& pragmaTo,
                    std::string& shaderData
                );

                void findPositionsOfSectionBlock
                (
                    const std::string& shaderData,
                    const GLuint64& statementEndPostion,
                    GLuint64& elseStatementBeginPosition,
                    GLuint64& elseStatementEndPosition,
                    GLuint64& endSectionStatementBeginPosition,
                    GLuint64& endSectionStatementEndPosition
                );

            private:
                GLchar*& m_source;

                std::map<std::string, std::string> m_values;
                std::vector<std::string> m_sections;

            };
        }
    }
}