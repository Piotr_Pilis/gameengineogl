#include "../Log/criticalLog.hpp"
#include "../Log/warningLog.hpp"
#include "../macros.hpp"
#include "program.hpp"

namespace Engine
{
    namespace Pipeline
    {
        Program::Program() : 
            m_objProgram(glCreateProgram()) 
        {
        }

        Program::Program(const Shader& SH1) :
            m_objProgram(glCreateProgram())
        {
            initialize(SH1);
        }

        Program::Program(const Shader& SH1, const Shader& SH2) : 
            m_objProgram(glCreateProgram())
        {
            initialize(SH1, SH2);
        }

        Program::Program(const Shader& SH1, const Shader& SH2, const Shader& SH3) : 
            m_objProgram(glCreateProgram())
        {
            initialize(SH1, SH2, SH3);
        }

        Program::Program(const Shader& SH1, const Shader& SH2, const Shader& SH3, const Shader& SH4) : 
            m_objProgram(glCreateProgram())
        {
            initialize(SH1, SH2, SH3, SH4);
        }

        Program::Program(const Shader& SH1, const Shader& SH2, const Shader& SH3, const Shader& SH4, const Shader& SH5) : 
            m_objProgram(glCreateProgram())
        {
            initialize(SH1, SH2, SH3, SH4, SH5);
        }

        Program::Program::~Program() 
        { 
            glDeleteProgram(m_objProgram); 
        }

        void Program::initialize(const Shader& SH1)
        {
            checkShader(SH1);
            glAttachShader(m_objProgram, *SH1);

            glLinkProgram(m_objProgram);
            GLint status;
            glGetProgramiv(m_objProgram, GL_LINK_STATUS, &status);
            if (status != GL_TRUE) {
                GLchar log[10000];
                glGetProgramInfoLog(m_objProgram, 10000, NULL, log);
                CRITICAL_LOG(
                    "Failed to link shaders", 
                    "Failed to link shaders: " + std::string(log));
            }
        }

        void Program::initialize(const Shader& SH1, const Shader& SH2)
        {
            checkShader(SH1);
            checkShader(SH2);
            glAttachShader(m_objProgram, *SH1);
            glAttachShader(m_objProgram, *SH2);

            glLinkProgram(m_objProgram);
            int status;
            glGetProgramiv(m_objProgram, GL_LINK_STATUS, &status);
            if (status != GL_TRUE) {
                GLchar log[10000];
                glGetProgramInfoLog(m_objProgram, 10000, NULL, log);
                CRITICAL_LOG(
                    "Failed to link shaders", 
                    "Failed to link shaders: " + std::string(log));
            }
        }

        void Program::initialize(const Shader& SH1, const Shader& SH2, const Shader& SH3)
        {
            checkShader(SH1);
            checkShader(SH2);
            checkShader(SH3);
            glAttachShader(m_objProgram, *SH1);
            glAttachShader(m_objProgram, *SH2);
            glAttachShader(m_objProgram, *SH3);

            glLinkProgram(m_objProgram);
            GLint status;
            glGetProgramiv(m_objProgram, GL_LINK_STATUS, &status);
            if (status != GL_TRUE) {
                char log[10000];
                glGetProgramInfoLog(m_objProgram, 10000, NULL, log);
                CRITICAL_LOG(
                    "Failed to link shaders", 
                    "Failed to link shaders: " + std::string(log));
            }
        }

        void Program::initialize(const Shader& SH1, const Shader& SH2, const Shader& SH3, const Shader& SH4)
        {
            checkShader(SH1);
            checkShader(SH2);
            checkShader(SH3);
            checkShader(SH4);
            glAttachShader(m_objProgram, *SH1);
            glAttachShader(m_objProgram, *SH2);
            glAttachShader(m_objProgram, *SH3);
            glAttachShader(m_objProgram, *SH4);

            glLinkProgram(m_objProgram);
            int status;
            glGetProgramiv(m_objProgram, GL_LINK_STATUS, &status);
            if (status != GL_TRUE) {
                char log[10000];
                glGetProgramInfoLog(m_objProgram, 10000, NULL, log);
                CRITICAL_LOG(
                    "Failed to link shaders", 
                    "Failed to link shaders: " + std::string(log));
            }
        }

        void Program::initialize(const Shader& SH1, const Shader& SH2, const Shader& SH3, const Shader& SH4, const Shader& SH5)
        {
            checkShader(SH1);
            checkShader(SH2);
            checkShader(SH3);
            checkShader(SH4);
            checkShader(SH5);
            glAttachShader(m_objProgram, *SH1);
            glAttachShader(m_objProgram, *SH2);
            glAttachShader(m_objProgram, *SH3);
            glAttachShader(m_objProgram, *SH4);
            glAttachShader(m_objProgram, *SH5);

            glLinkProgram(m_objProgram);
            int status;
            glGetProgramiv(m_objProgram, GL_LINK_STATUS, &status);
            if (status != GL_TRUE) {
                char log[10000];
                glGetProgramInfoLog(m_objProgram, 10000, NULL, log);
                CRITICAL_LOG(
                    "Failed to link shaders", 
                    "Failed to link shaders: " + std::string(log));
            }
        }

        void Program::checkShader(const Shader& shader)
        {
            if (&shader == NULL)
            {
                CRITICAL_LOG
                (
                    "Failed to attach shader", 
                    "Failed to attach shader"
                );
            }
        }

        const GLuint& Program::operator*() const
        {
            return m_objProgram;
        }

        void Program::bindUniformBlock(const GLuint& program, const GLchar* blockName, const GLuint& blockIndex)
        {
            auto uniformBlockIndex = glGetUniformBlockIndex(program, blockName);
            if (uniformBlockIndex < 0)
            {
                CRITICAL_LOG
                (
                    "Cannot find uniform",
                    "Cannot find uniform with name '" + std::string(blockName) + "'"
                );
                return;
            }
            glUniformBlockBinding(program, uniformBlockIndex, blockIndex);
        }

        void Program::bindBuffer(const GLuint& location, const GLuint& blockIndex)
        {
            glBindBufferBase(GL_UNIFORM_BUFFER, blockIndex, location);
        }

        GLuint Program::getUniformLocation(const std::string& uniformName)
        {
            glUseProgram(**this);
            GLuint uniformLocation = glGetUniformLocation(**this, uniformName.c_str());
            if (uniformLocation == INVALID_UNIFORM_LOCATION) 
            {
                WARNING_LOG("Unable to get the location of uniform '" + uniformName + "'");
            }
            return uniformLocation;
        }
    }
}