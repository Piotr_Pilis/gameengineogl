#define GLM_ENABLE_EXPERIMENTAL
#define _USE_MATH_DEFINES
#include <glm/gtx/transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <cmath>

#include "transform.hpp"

namespace Engine::Pipeline
{
    Transform::Transform(const GLint uniformLocation) :
        m_translation(glm::vec3(0.0f, 0.0f, 0.0f)),
        m_rotation(glm::vec3(0.0f, 0.0f, 0.0f)),
        m_scale(glm::vec3(1.0f, 1.0f, 1.0f)),
        m_uniformLocation(uniformLocation),
        m_transformIsUptodate(true)
    {
    }

    void Transform::setUnifromLocation(const GLint uniformLocation)
    {
        m_uniformLocation = uniformLocation;
    }

    inline void Transform::updateMatrix()
    {
        if (m_transformIsUptodate)
        {
            glm::mat4 rotationMatrix =
                glm::rotate(m_rotation.z, glm::vec3(0.0, 0.0, 1.0)) *
                glm::rotate(m_rotation.y, glm::vec3(0.0, 1.0, 0.0)) *
                glm::rotate(m_rotation.x, glm::vec3(1.0, 0.0, 0.0));

            m_worldMatrix =
                glm::translate(m_translation) *
                rotationMatrix *
                glm::scale(m_scale);
            m_transformIsUptodate = false;
        }
    }

    void Transform::bindMatrix()
    {
        updateMatrix();
        glUniformMatrix4fv
        (
            m_uniformLocation,
            1,
            GL_FALSE,
            glm::value_ptr(m_worldMatrix)
        );
    }

    void Transform::bindMatrix(const GLint uniformLocation)
    {
        updateMatrix();
        glUniformMatrix4fv
        (
            uniformLocation,
            1, 
            GL_FALSE, 
            glm::value_ptr(m_worldMatrix)
        );
    }

    void Transform::bindIdentityMatrix(const GLint uniformLocation)
    {
        static glm::mat4 identityMatrix(1.0);
        glUniformMatrix4fv
        (
            uniformLocation,
            1, 
            GL_FALSE, 
            glm::value_ptr(identityMatrix)
        );
    }

    void Transform::setTranslation(const glm::vec3& translation)
    {
        m_translation = translation;
        m_transformIsUptodate = true;
    }

    void Transform::setRotation(const glm::vec3& radianAngles)
    {
        m_rotation = radianAngles;
        m_transformIsUptodate = true;
    }

    void Transform::setScale(const glm::vec3& scale)
    {
        m_scale = scale;
        m_transformIsUptodate = true;
    }

    void Transform::lookAt(const glm::vec3& direction)
    {
        GLfloat angleX = std::fmod(-std::atan2f(direction.z, direction.y), M_PI_2) ;
        setRotation
        (
            glm::vec3
            (
                angleX,
                0.0f, 
                std::atan2f(direction.y, direction.x) + M_PI_2
            )
        );
    }
}