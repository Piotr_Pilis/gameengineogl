#include <algorithm>
#include <tuple>
#include "Log/warningLog.hpp"
#include "shaderVariant.hpp"

namespace Engine
{
    namespace Pipeline
    {

        bool ShaderVariant::operator==(const ShaderVariant& other) const
        {
            return  m_values == other.m_values &&
                m_sections == other.m_sections;
        }

        bool ShaderVariant::operator<(const ShaderVariant& other) const
        {
            /* Tuple fixed the app crashes, read more:
            ** https://stackoverflow.com/questions/45966807/c-invalid-comparator-assert */
            return std::tie(m_values, m_sections) < std::tie(other.m_values, other.m_sections);
        }

        ShaderVariant& ShaderVariant::operator+=(const ShaderVariant& other)
        {
            for (auto& section : other.m_sections)
            {
                pushSection(section);
            }
            for (auto& value : other.m_values)
            {
                pushValue(value);
            }
            return *this;
        }

        const std::map<std::string, std::string>& ShaderVariant::getValues() const
        {
            return m_values;
        }

        const std::vector<std::string>& ShaderVariant::getSections() const
        {
            return m_sections;
        }

        void ShaderVariant::pushValue(const std::pair<std::string, std::string>& value)
        {
            if (m_values.find(value.first) == m_values.end())
                m_values[value.first] = value.second;
            else
            {
                if (m_values[value.first] != value.second)
                {
                    WARNING_LOG("Shader variants have different values");
                }
            }
        }

        void ShaderVariant::pushSection(const std::string& section)
        {
            if (std::find
            (
                m_sections.begin(),
                m_sections.end(),
                section
            ) == m_sections.end())
            {
                m_sections.push_back(section);
            }
        }
    }
}
