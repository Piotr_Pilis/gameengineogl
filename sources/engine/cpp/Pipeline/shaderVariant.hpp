#pragma once
#include <string>
#include <vector>
#include <utility>
#include <map>

namespace Engine
{
    namespace Pipeline
    {
        /*
        Descriptes variant of shader e.g. additional sections or values
        */
        class ShaderVariant
        {
        public:
            ShaderVariant() = default;
            ~ShaderVariant() = default;

            bool operator==(const ShaderVariant& other) const;
            bool operator<(const ShaderVariant& other) const;

            ShaderVariant& operator+=(const ShaderVariant& other);

            const std::map<std::string, std::string>& getValues() const;
            const std::vector<std::string>& getSections() const;

            void pushValue(const std::pair<std::string, std::string>& value);
            void pushSection(const std::string& section);

        protected:
            std::map<std::string, std::string> m_values;
            std::vector<std::string> m_sections;

        };
    }
}
