#pragma once
#include <glad/glad.h>

namespace Engine::Pipeline
{
    /* You should use these properties if u want to use some global UBOs */
    enum class GlobalUniformBlockBinding : GLuint
    {
        BLOCK_TRANSFORM = 0u,   // View matrices etc.
        TIME_BLOCK             // Elapsed seconds etc.
    };
}