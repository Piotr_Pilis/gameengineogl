#pragma once
#include <glad/glad.h>
#include <string>
#include <memory>
#include <vector>
#include <map>

#include "Pipeline/shaderVariant.hpp"

namespace Engine
{
    namespace Pipeline
    {
        class Shader
        {
        public:
            enum ShaderType
            {
                VertexShader,
                TessellationControlShader,
                TessellationEvaluationShader,
                FragmentShader,
                GeometryShader
            };

        public:
            Shader() = delete;
            Shader
            (
                const std::string filename,
                ShaderType type,
                const std::map<std::string, std::string>& valuesToParse = {},
                const std::vector<std::string>& sections = {}
            );
            Shader
            (
                const std::string filename,
                ShaderType type,
                const Pipeline::ShaderVariant& shaderVariant
            );
            ~Shader();

            const GLuint& operator*() const;
            const ShaderType& getType() const;

        private:
            void setup();
            GLchar* loadShaderSource(const std::string filename);

        private:
            std::string m_shaderName;
            ShaderType m_type;
            GLchar* m_sourceData = nullptr;
            GLuint m_objShader;
        };
    }
}