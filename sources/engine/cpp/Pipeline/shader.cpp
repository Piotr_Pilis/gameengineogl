#ifdef _DEBUG
#include <iostream>
#include <iomanip>
#include <sstream>
#endif

#include "Log/criticalLog.hpp"
#include "FileSystem/ifstream.hpp"
#include "Private/shaderParser.hpp"
#include "macros.hpp"

#include "shader.hpp"

namespace Engine
{
    namespace Pipeline
    {
        Shader::Shader
        (
            const std::string filename, 
            ShaderType type,
            const std::map<std::string, std::string>& valuesToParse,
            const std::vector<std::string>& sections
        ) :
            m_shaderName(filename),
            m_type(type)
        {
            switch (type)
            {
            case ShaderType::VertexShader:
                m_objShader = glCreateShader(GL_VERTEX_SHADER);
                break;
            case ShaderType::FragmentShader:
                m_objShader = glCreateShader(GL_FRAGMENT_SHADER);
                break;
            case ShaderType::TessellationControlShader:
                m_objShader = glCreateShader(GL_TESS_CONTROL_SHADER);
                break;
            case ShaderType::TessellationEvaluationShader:
                m_objShader = glCreateShader(GL_TESS_EVALUATION_SHADER);
                break;
            case ShaderType::GeometryShader:
                m_objShader = glCreateShader(GL_GEOMETRY_SHADER);
                break;
            default:
                break;
            }
            
            m_sourceData = loadShaderSource(filename);
            Private::ShaderParser shaderParser(m_sourceData);
            shaderParser.parse(valuesToParse, sections);
            setup();
        }

        Shader::Shader
        (
            const std::string filename,
            ShaderType type,
            const Pipeline::ShaderVariant& shaderVariant
        ) : Shader(filename, type, shaderVariant.getValues(), shaderVariant.getSections())
        {

        }

        Shader::~Shader()
        {
            glDeleteShader(m_objShader);
            if(m_sourceData)
                delete[] m_sourceData;
        }

        void Shader::setup()
        {
            if (m_sourceData == nullptr)
                CRITICAL_LOG("Shader data is empty!");
        
            glShaderSource(m_objShader, 1, &m_sourceData, nullptr);
            glCompileShader(m_objShader);

            GLint status;
            glGetShaderiv(m_objShader, GL_COMPILE_STATUS, &status);
            if (status != GL_TRUE) {
                GLchar log[10000];
                glGetShaderInfoLog(m_objShader, 10000, NULL, log);
#ifdef _DEBUG
                std::istringstream sourceStream(m_sourceData);
                std::string sourceLine;
                GLint lineID = 0;
                std::cerr << std::endl << "Shader data: " << std::endl << "______________________________________________" << std::endl;
                while (!sourceStream.eof())
                {
                    std::getline(sourceStream, sourceLine);
                    std::cerr << std::setw(3) << ++lineID << ":\t" << sourceLine << std::endl;
                }
                std::cerr << std::endl << "______________________________________________" << std::endl << std::endl;
#endif
                CRITICAL_LOG(
                    "Failed to compile shader: " + std::string(m_shaderName),
                    "Failed to compile shader '" + std::string(m_shaderName) + std::string("': ") + std::string(log));
            }

            delete[] m_sourceData;
            m_sourceData = nullptr;
        }

        GLchar* Shader::loadShaderSource(const std::string filename)
        {
            try
            {
                FileSystem::Ifstream fin(filename);
                GLint64 fileSize = fin.size();

                char* databuff = new char[fileSize + 1];
                databuff[fileSize] = '\0';
                fin.read(databuff, fileSize);

                return databuff;
            }
            catch (std::invalid_argument st)
            {
                CRITICAL_LOG("Could not open " + std::string(filename));
            }

            return nullptr;
        }

        const GLuint& Shader::operator*() const { return m_objShader; }
        const Shader::ShaderType& Shader::getType() const { return m_type; }
    }
}