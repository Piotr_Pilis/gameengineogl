#include "Log/criticalLog.hpp"
#include "programLibrary.hpp"

namespace Engine
{
    namespace Pipeline
    {
        ProgramLibrary& ProgramLibrary::instance()
        {
            static ProgramLibrary instance;
            return instance;
        }

        const std::shared_ptr<Shader>& ProgramLibrary::getOrCreateShaderVariant
        (
            const std::string shaderPath,
            const Shader::ShaderType& shaderType,
            const ShaderVariant& shaderVariant
        )
        {
            // Has a shader variant?
            if (m_shaders.count(shaderPath) != 0)
            {
                for (auto& shaderByVariant : m_shaders[shaderPath])
                {
                    if (shaderVariant == shaderByVariant.first)
                    {
                        return shaderByVariant.second;
                    }
                }
            }

            // ... Not?
            m_shaders[shaderPath][shaderVariant] = std::make_shared<Shader>
            (
                shaderPath,
                shaderType,
                shaderVariant.getValues(),
                shaderVariant.getSections()
            );
            return m_shaders[shaderPath][shaderVariant];
        }

        const std::shared_ptr<Program>& ProgramLibrary::getOrCreateProgram
        (
            bool& newObject,
            const std::shared_ptr<Shader>& vertexShader,
            std::optional<std::shared_ptr<Shader>> tessellationControlShader,
            std::optional<std::shared_ptr<Shader>> tessellationEvaluationShader,
            std::optional<std::shared_ptr<Shader>> fragmentShader,
            std::optional<std::shared_ptr<Shader>> geometryShader
        )
        {
            ShadersOfProgram shadersOfProgram{ vertexShader };
            if (tessellationControlShader.has_value())
                shadersOfProgram.push_back(*tessellationControlShader);
            if (tessellationEvaluationShader.has_value())
                shadersOfProgram.push_back(*tessellationEvaluationShader);
            if (fragmentShader.has_value())
                shadersOfProgram.push_back(*fragmentShader);
            if (geometryShader.has_value())
                shadersOfProgram.push_back(*geometryShader);


            // Has a program with given shaders?
            for (const auto& programByShaders : m_programs)
            {
                // Shaders count of program is exactly the same
                if (programByShaders.first.size() != shadersOfProgram.size())
                    continue;

                // So check shaders
                bool isEqual = true;
                for (const auto& shader : programByShaders.first)
                {
                    if (**shader == **vertexShader)
                    {
                        continue;
                    }
                    if (tessellationControlShader.has_value())
                    {
                        if (**shader == ***tessellationControlShader)
                        {
                            continue;
                        }
                    }
                    if (tessellationEvaluationShader.has_value())
                    {
                        if (**shader == ***tessellationEvaluationShader)
                        {
                            continue;
                        }
                    }
                    if (fragmentShader.has_value())
                    {
                        if (**shader == ***fragmentShader)
                        {
                            continue;
                        }
                    }
                    if (geometryShader.has_value())
                    {
                        if (**shader == ***geometryShader)
                        {
                            continue;
                        }
                    }
                    isEqual = false;
                    break;
                }
                if (isEqual)
                {
                    newObject = false;
                    return programByShaders.second;
                }
            }

            // ... Not?
            m_programs[shadersOfProgram] = std::make_shared<Program>();
            switch (shadersOfProgram.size())
            {
            case 1:
                m_programs[shadersOfProgram]->initialize
                (
                    *shadersOfProgram[0]
                );
                break;
            case 2:
                m_programs[shadersOfProgram]->initialize
                (
                    *shadersOfProgram[0],
                    *shadersOfProgram[1]
                );
                break;
            case 3:
                m_programs[shadersOfProgram]->initialize
                (
                    *shadersOfProgram[0],
                    *shadersOfProgram[1],
                    *shadersOfProgram[2]
                );
                break;
            case 4:
                m_programs[shadersOfProgram]->initialize
                (
                    *shadersOfProgram[0],
                    *shadersOfProgram[1],
                    *shadersOfProgram[2],
                    *shadersOfProgram[3]
                );
                break;
            case 5:
                m_programs[shadersOfProgram]->initialize
                (
                    *shadersOfProgram[0],
                    *shadersOfProgram[1],
                    *shadersOfProgram[2],
                    *shadersOfProgram[3],
                    *shadersOfProgram[4]
                );
                break;
            default:
                CRITICAL_LOG("Wrong number of shaders");
            }

            newObject = true;
            return m_programs[shadersOfProgram];
        }
    }
}