#pragma once
#include <glad/glad.h>

namespace Engine::Config
{
    namespace LightingConfig
    {
        constexpr const GLuint MAX_NUM_POINT_LIGHTS = 32;
    }
}