#pragma once
#include <glad/glad.h>

namespace Engine
{
    namespace Config
    {
        namespace TerrainConfig
        {
            constexpr const GLdouble GEOMETRY_SIZE = 2048.0;
            constexpr const GLdouble MIN_TILE_SIZE = 16.0;
            constexpr const GLuint NUM_CHUNK_ROWS_TO_BE_RENDERED = 3;
        }
    }
}