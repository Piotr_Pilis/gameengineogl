#pragma once
#include <unordered_map>
#include <algorithm>
#include <typeinfo>
#include <string>
#include <any>

#include "Log/warningLog.hpp"

namespace Engine
{
    namespace Feature
    {
        // Additional space of memory in object
        class Categorizable
        {
        public:
            bool hasCategory(const std::string& categoryName)
            {
                return m_categories.count(categoryName) != 0;
            }

            template<class Type>
            bool getOrCreateCategory(const std::string& categoryName, Type& value)
            {
                if (!hasCategory(categoryName))
                {
                    m_categories.emplace(categoryName, std::make_any<Type>());
                }

                try
                {
                    value = std::any_cast<Type&>(m_categories[categoryName]);
                }
                catch (const std::bad_any_cast& exception)
                {
                    WARNING_LOG("Category '" + categoryName + "' doesn't contain '" + typeid(value).name() + "' type");
                    return false;
                }

                return true;
            }

            void clearCategories()
            {
                m_categories.clear();
            }

        private:
            std::unordered_map<std::string, std::any> m_categories;

        };
    }
}