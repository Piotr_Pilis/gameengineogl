#pragma once
#include <functional>

namespace Engine::Utils
{
    class ScopeExit
    {
    public:
        ScopeExit() = delete;
        ScopeExit(std::function<void(void)> exitFunction) :
            m_exitFunction(exitFunction)
        {}
        ~ScopeExit()
        {
            m_exitFunction();
        }

    private:
        std::function<void(void)> m_exitFunction;

    };
}