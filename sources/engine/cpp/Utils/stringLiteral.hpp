#pragma once
#include <cstddef>
#include <utility>

#define STRING_LITERAL(stringLiteral)                   \
    Engine::Utils::StringLiteral::StringLiteral  \
    <                                                   \
        decltype(stringLiteral),                        \
        &stringLiteral                                  \
    >

namespace Engine::Utils::StringLiteral
{
    /* String literal */
    namespace
    {
        template <class T>
        struct StringLiteralLength;

        template <std::size_t numCharacters>
        struct StringLiteralLength<char const [numCharacters]>
        {
            static constexpr std::size_t value = numCharacters;
        };
    }

    struct AbstractStringLiteral
    {
    };

    template <class StringType, StringType*, class = std::make_index_sequence<StringLiteralLength<StringType>::value>>
    struct StringLiteral;

    template <class StringType, StringType* string, std::size_t... sequenceIds>
    struct StringLiteral<StringType, string, std::index_sequence<sequenceIds...>> :
        public AbstractStringLiteral
    {
        static constexpr std::size_t size = StringLiteralLength<StringType>::value;
        static constexpr char value[StringLiteralLength<StringType>::value] =
        {
            (*string)[sequenceIds]...
        };
    };

    /* Converters */
    // Uppercase
    template <class StringLiteralType, class = std::make_index_sequence<StringLiteralType::size>>
    struct Uppercase;

    template <class StringLiteralType, std::size_t... sequenceIds>
    struct Uppercase<StringLiteralType, std::index_sequence<sequenceIds...>>
    {
        static constexpr char value[StringLiteralType::size] =
        {
            (
                (StringLiteralType::value[sequenceIds] >= 'a' && StringLiteralType::value[sequenceIds] <= 'z') ?
                (static_cast<char>(StringLiteralType::value[sequenceIds] - 'a' + 'A')) :
                (StringLiteralType::value[sequenceIds])
            )...
        };
    };

    // Lowercase
    template <class StringLiteralType, class = std::make_index_sequence<StringLiteralType::size>>
    struct Lowercase;

    template <class StringLiteralType, std::size_t... sequenceIds>
    struct Lowercase<StringLiteralType, std::index_sequence<sequenceIds...>>
    {
        static constexpr char value[StringLiteralType::size] =
        {
            (
                (StringLiteralType::value[sequenceIds] >= 'A' && StringLiteralType::value[sequenceIds] <= 'Z') ?
                (static_cast<char>(StringLiteralType::value[sequenceIds] - 'A' + 'a')) :
                (StringLiteralType::value[sequenceIds])
            )...
        };
    };

    // Capitalize
    template <class StringLiteralType, class = std::make_index_sequence<StringLiteralType::size>>
    struct Capitalize;

    template <class StringLiteralType, std::size_t sequenceId, std::size_t... sequenceIds>
    struct Capitalize<StringLiteralType, std::index_sequence<sequenceId, sequenceIds...>>
    {
        static constexpr char value[StringLiteralType::size] =
        {
            (
                (StringLiteralType::value[sequenceId] >= 'a' && StringLiteralType::value[sequenceId] <= 'z') ?
                (static_cast<char>(StringLiteralType::value[sequenceId] - 'a' + 'A')) :
                (StringLiteralType::value[sequenceId])
            ),

            (
                (StringLiteralType::value[sequenceIds] >= 'A' && StringLiteralType::value[sequenceIds] <= 'Z') ?
                (static_cast<char>(StringLiteralType::value[sequenceIds] - 'A' + 'a')) :
                (StringLiteralType::value[sequenceIds])
            )...
        };
    };
}
