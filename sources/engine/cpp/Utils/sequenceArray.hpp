#pragma once
#include <utility>
#include <array>

namespace Engine::Utils::SequenceNumber
{
    namespace
    {
        template<typename ElementType, std::size_t numElements, typename GeneratorFunctor, std::size_t... indices>
        constexpr auto _makeArray(GeneratorFunctor&& generator, std::index_sequence<indices...>)
        {
            return std::array<ElementType, numElements>{ { static_cast<ElementType>(generator(indices))... } };
        }
    }

    /// <summary>
    /// Generates sequence array from generator
    /// </summary>
    /// <example>
    /// auto example = Utils::SequenceNumber::makeArray<int, 4>([](size_t i) 
    /// {
    ///     return 2 * i;
    /// });
    /// </example>
    /// <typeparam name="ElementType"></typeparam>
    /// <typeparam name="GeneratorFunctor"></typeparam>
    /// <param name="generator"></param>
    /// <returns></returns>
    template<typename ElementType, std::size_t numElements, typename GeneratorFunctor>
    constexpr auto makeArray(GeneratorFunctor&& generator)
    {
        return _makeArray<ElementType, numElements>(std::forward<GeneratorFunctor>(generator), std::make_index_sequence<numElements>{});
    }
}