/*
** Author: Piotr Pilis
** Copyright (C) 2021
*/

#pragma once
#include <type_traits>
#include <memory>

namespace Engine::Utils
{
    class EnumeratorFactory
    {
    private:
        EnumeratorFactory() = delete;
        struct AbstractVariant
        {
        };

    public:
        // Variant class with its identifier
        template<auto identifier, class ObjectVariant>
        struct Variant : AbstractVariant
        {
            Variant() = delete;
            using EnumerationType = decltype(identifier);
            static_assert(std::is_enum_v<EnumerationType>,
                "Enumeration must be enum type.");

            using ObjectType = ObjectVariant;
            static constexpr EnumerationType _identifier = identifier;
        };

        template<class EnumerationType, class VariantBase, class ...Variants>
        class Enumerator
        {
            static_assert(std::is_enum_v<EnumerationType>,
                "EnumerationType must be enum type.");

        public:
            /* Dynamic creation
            ** Example usage: auto action = ActionFactory::Object(actionType);
            */
            template<EnumerationType ...variantTypes>
            class Object
            {
            public:
                Object() = delete;
                Object(EnumerationType variantType)
                {
                    m_object = nullptr;
                    (
                        (
                            Variants::_identifier == variantType && 
                            (
                                void(m_object = std::make_unique<typename Variants::ObjectType>()), 
                                1
                            )
                        ) 
                    || ...) /* || (throw whatever, 1) */;
                }

            public:
                VariantBase* operator->()
                {
                    return m_object.get();
                }

                bool isValid() const
                {
                    return m_object.get() != nullptr;
                }

            private:
                std::unique_ptr<VariantBase> m_object;

            };

            /* Static creation
            ** Example usage: ActionFactory::Object<ActionTypes::VERSION>{}->func();
            */
            template<EnumerationType variantType>
            class Object<variantType>
            {
            private:
                // Set class - set of variants
                // This class is used to pick ObjectType
                template<class ..._Variants>
                class Set;

                template<class _Variant>
                class Set<_Variant>
                {
                    static_assert(std::is_base_of<VariantBase, typename _Variant::ObjectType>::value,
                        "ObjectVariant is not base of VariantBase");
                    static_assert(std::is_same<EnumerationType, typename _Variant::EnumerationType>::value,
                        "The porvided enumeration is invalid");

                public:
                    using ObjectType = typename std::conditional<_Variant::_identifier == variantType,
                        typename _Variant::ObjectType, void>::type;

                };

                template<class _Variant, class ..._Variants>
                class Set<_Variant, _Variants...>
                {
                    static_assert(std::is_base_of<VariantBase, typename _Variant::ObjectType>::value,
                        "ObjectVariant is not base of VariantBase");
                    static_assert(std::is_same<EnumerationType, typename _Variant::EnumerationType>::value,
                        "The porvided enumeration is invalid");

                public:
                    using ObjectType = typename std::conditional<_Variant::_identifier == variantType,
                        typename _Variant::ObjectType, typename Set<_Variants...>::ObjectType>::type;
                };

            private:
                using ObjectType = typename Set<Variants...>::ObjectType;

                static_assert(!std::is_same<void, ObjectType>::value,
                    "Variant with given identifier doesn't exist");

            public:
                ObjectType* operator->()
                {
                    return &m_object;
                }

            private:
                ObjectType m_object;

            };
        };       
    };
}