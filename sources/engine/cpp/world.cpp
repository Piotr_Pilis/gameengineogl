#include "Transform/cameraControllerAbstract.hpp"
#include "Map/terrain.hpp"
#include "Transform/camera.hpp"
#include "engine.hpp"
#include "world.hpp"

namespace Engine
{
    World::World()
    {
        initialize();
    }

    World::~World()
    {
    }

    void World::initialize()
    {
        glEnable(GL_TEXTURE_2D);
        glEnable(GL_DEPTH_TEST);
        glEnable(GL_CULL_FACE);
        glCullFace(GL_BACK);

        // -> Rendering Manager
        {
            m_renderingManager = std::make_unique<Entity::RenderingManager>();
        }
        // <- Rendering Manager

        // -> Lighting Manager
        {
            m_lightingManager = std::make_unique<Lighting::LightingManager>();
        }
        // <- Lighting Manager
    }

    void World::setActiveCameraController(
        const std::shared_ptr<Transform::CameraControllerAbstract>& cameraController)
    {
        if (cameraController != nullptr)
        {
            m_activeCameraController = cameraController;

            // Uniform block identifier 0 reserved by Transform
            cameraController->getCamera()->setActive();

            // Emit signal
            activeCameraControllerChanged(m_activeCameraController);
        }
    }

    void World::setActiveTerrain(const std::shared_ptr<Map::Terrain>& terrain)
    {
        m_activeTerrain = terrain;

        auto& objectManager = Engine::getInstance().getObjectManager();
        objectManager.loadStaticObjectsFromMap(m_activeTerrain);
    }

    void World::update()
    {
        m_renderingManager->update();
        m_activeCameraController->update();
    }

    void World::render()
    {
        if (m_activeTerrain != nullptr)
        {
            if (m_showTerrainGrid)
                glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

            m_activeTerrain->render();

            if (m_showTerrainGrid)
                glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        }

        m_renderingManager->render();
    }

    Entity::RenderingManager& World::getRenderingManager()
    {
        return *m_renderingManager;
    }


    Lighting::LightingManager& World::getLightingManager()
    {
        return *m_lightingManager;
    }
}