#pragma once
#include <memory>
#include "cameraControllerAbstract.hpp"

namespace Engine
{
    namespace Event
    {
        class Timer;
    }

    namespace Transform
    {
        class BasicCameraController : 
            public CameraControllerAbstract
        {
        private:
            BasicCameraController() = delete;
            BasicCameraController
            (
                const GLint& windowWidth, 
                const GLint& windowHeight
            );

        public:
            ~BasicCameraController();
            static std::shared_ptr<BasicCameraController> make
            (
                const GLint& windowWidth,
                const GLint& windowHeight
            );

            // When give button identifier is clicked, camera can be rotated
            void setCameraRotationButton(GLint mouseButtonID);

            void setCameraMovingButtonForward(GLint mouseButtonID);
            void setCameraMovingButtonBack(GLint mouseButtonID);
            void setCameraMovingButtonLeft(GLint mouseButtonID);
            void setCameraMovingButtonRight(GLint mouseButtonID);

            void setCameraMovingButtonTop(GLint mouseButtonID);
            void setCameraMovingButtonBottom(GLint mouseButtonID);

            void setMouseRotationSensitivity(GLfloat sensitivity = 3.0f);
            void setCameraSpeed(GLfloat cameraSpeed = 150.0f);

            // Slots
            void onMousePositionChanged(const GLfloat& x, const GLfloat& y) override;
            void onMouseButtonClicked(const GLint& mouseButtonID, ButtonEventState actionType) override;
            void onKeyClicked(const GLint& keyID, ButtonEventState actionType) override;

            void update() override;

        protected:
            void updateViewMatrix() override;

        private:
            // Transform actions
            void rotateCamera(const GLfloat& additionalYawAngle, const GLfloat& additionalTiltAngle);
            void moveCamera(const glm::vec3& vec) override;

            // Response for clicked movement buttons
            void updateCameraPosition();
            void updateMovementVectors();

            void calculateMovementVectors();

        private:
            GLint m_cameraRotationButton;

            GLint m_cameraMovingButtonForward;
            GLint m_cameraMovingButtonBack;
            GLint m_cameraMovingButtonLeft;
            GLint m_cameraMovingButtonRight;

            GLint m_cameraMovingButtonTop;
            GLint m_cameraMovingButtonBottom;

            glm::vec2 m_movementForwardVector;
            glm::vec2 m_movementRightVector;
            bool m_needUpdateMovementVectors;

            GLfloat m_mouseRotationSensitivity;
            GLfloat m_cameraSpeed;

            std::unique_ptr<Event::Timer> m_cameraMovementTimer;

            bool m_needUpdateMousePosition;
            bool m_cameraRotationButtonIsClicked;

            bool m_cameraMovingButtonForwardIsClicked;
            bool m_cameraMovingButtonBackIsClicked;
            bool m_cameraMovingButtonLeftIsClicked;
            bool m_cameraMovingButtonRightIsClicked;

            bool m_cameraMovingButtonTopIsClicked;
            bool m_cameraMovingButtonBottomIsClicked;
            
            
        };
    }
}