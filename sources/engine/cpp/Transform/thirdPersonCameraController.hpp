#pragma once
#include <boost/signals2/signal.hpp>
#include <glm/vec3.hpp>
#include <memory>

#include "cameraControllerAbstract.hpp"

namespace Engine::Geometry
{
    class Object3D;
}

namespace Engine::Transform
{
    class ThirdPersonCameraController :
        public CameraControllerAbstract,
        public std::enable_shared_from_this<ThirdPersonCameraController>
    {
    private:
        static constexpr auto MAX_ZOOM = 2000.0f;
        static constexpr auto MIN_ZOOM = 20.0f;

    protected:
        ThirdPersonCameraController() = delete;
        ThirdPersonCameraController
        (
            const GLint& windowWidth, 
            const GLint& windowHeight
        );

    public:
        ~ThirdPersonCameraController();
        static std::shared_ptr<ThirdPersonCameraController> make
        (
            const GLint& windowWidth, 
            const GLint& windowHeight
        );

        void bindObject3D(std::shared_ptr<Geometry::Object3D>& object3D);

        // When given button identifier is clicked, camera can be rotated
        void setCameraRotationButton(GLint mouseButtonID);

        void setMouseRotationSensitivity(const GLfloat sensitivity);
        void setZoomSensitivity(const GLfloat sensitivity);

    protected:
        void updateViewMatrix() override;

    private:
        // Transform actions
        void rotateCamera(const GLfloat& additionalYawAngle, const GLfloat& additionalTiltAngle);

        /* Slots */
    public:
        void onMousePositionChanged(const GLfloat& x, const GLfloat& y) override;
        void onMouseButtonClicked(const GLint& mouseButtonID, ButtonEventState actionType) override;
        void onKeyClicked(const GLint& keyID, ButtonEventState actionType) override;
        void onScrolled(const GLdouble& offset) override;

    private:
        void onObject3DPositionChanged(const std::weak_ptr<Geometry::Object3D> object3DWeakPtr);

    private:
        // Connection to signal when position will be changed of object3D
        boost::signals2::connection m_positionObject3DConnection;

        /* Rotation controls */
        GLint m_cameraRotationButton = -1;

        GLfloat m_mouseRotationSensitivity = 3.0f;

        bool m_needUpdateMousePosition = false;
        bool m_cameraRotationButtonIsClicked = false;

        /* Zoom */
        GLfloat m_zoom = 150.0f;
        GLfloat m_zoomSensitivity = 3.0f;

    };
}