#include <glm/gtc/matrix_transform.hpp>
#include "../Log/warningLog.hpp"
#include "../Event/timer.hpp"
#include "basicCameraController.hpp"

namespace Engine
{
    namespace Transform
    {
        BasicCameraController::BasicCameraController(const GLint& windowWidth, const GLint& windowHeight) :
            CameraControllerAbstract(windowWidth, windowHeight),
            m_mouseRotationSensitivity(3.0f),
            m_cameraSpeed(550.0f),
            m_needUpdateMovementVectors(true),

            m_cameraRotationButtonIsClicked(false),

            m_cameraMovingButtonForwardIsClicked(false),
            m_cameraMovingButtonBackIsClicked(false),
            m_cameraMovingButtonLeftIsClicked(false),
            m_cameraMovingButtonRightIsClicked(false),

            m_cameraMovingButtonTopIsClicked(false),
            m_cameraMovingButtonBottomIsClicked(false)
        {
            m_cameraMovementTimer = std::make_unique<Event::Timer>();
        }

        BasicCameraController::~BasicCameraController()
        {
        }

        std::shared_ptr<BasicCameraController> BasicCameraController::make
        (
            const GLint& windowWidth,
            const GLint& windowHeight
        )
        {
            struct MakeSharedPtrEnabler :
                public BasicCameraController
            {
                MakeSharedPtrEnabler(const GLint& windowWidth, const GLint& windowHeight) :
                    BasicCameraController(windowWidth, windowHeight)
                {}
            };

            return std::move(std::make_shared<MakeSharedPtrEnabler>(windowWidth, windowHeight));
        }

        void BasicCameraController::update()
        {
            updateCameraPosition();
            CameraControllerAbstract::update();
        }

        void BasicCameraController::updateViewMatrix()
        {
            // Yaw
            m_transformBlock.m_viewMatrix = glm::rotate(
                glm::mat4(1.0f),
                m_yawAngle,
                glm::vec3(0.0f, 0.0f, 1.0f)
            );

            // Tilt
            m_transformBlock.m_viewMatrix = glm::rotate(
                m_transformBlock.m_viewMatrix,
                m_tiltAngle,
                glm::vec3(1.0f, 0.0f, 0.0f)
            );

            // Normal matrix
            m_transformBlock.m_normalMatrix = glm::transpose(m_transformBlock.m_viewMatrix);

            // Rotation matrix
            m_transformBlock.m_viewMatrix = glm::inverse(m_transformBlock.m_viewMatrix);

            // And translation matrix
            m_transformBlock.m_viewMatrix = glm::translate(m_transformBlock.m_viewMatrix, -m_transformBlock.m_cameraPosition);

            m_needUpdateMovementVectors = true;
        }

        void BasicCameraController::onMousePositionChanged(const GLfloat& x, const GLfloat& y)
        {
            // Do we need update first mouse position before right action?
            if (m_needUpdateMousePosition)
            {
                m_lastMousePositionX = x;
                m_lastMousePositionY = y;
                m_needUpdateMousePosition = false;
            }
            else if (m_cameraRotationButtonIsClicked)
            {
                rotateCamera(
                    getDeltaMousePositionX(x) * m_mouseRotationSensitivity, 
                    getDeltaMousePositionY(y) * m_mouseRotationSensitivity
                );

                m_lastMousePositionX = x;
                m_lastMousePositionY = y;
            }
        }

        void BasicCameraController::onMouseButtonClicked(const GLint& mouseButtonID, ButtonEventState actionType)
        {
            if (mouseButtonID == m_cameraRotationButton)
            {
                m_cameraRotationButtonIsClicked = actionType == ButtonEventState::RELEASED ? false : true;

                // Do we need update first mouse position before right action?
                m_needUpdateMousePosition = m_cameraRotationButtonIsClicked;
            }
        }

        void BasicCameraController::onKeyClicked(const GLint& keyID, ButtonEventState actionType)
        {
            if (keyID == m_cameraMovingButtonForward)
            {
                m_cameraMovingButtonForwardIsClicked = actionType == ButtonEventState::RELEASED ? false : true;
            }
            else if (keyID == m_cameraMovingButtonBack)
            {
                m_cameraMovingButtonBackIsClicked = actionType == ButtonEventState::RELEASED ? false : true;
            }
            else if (keyID == m_cameraMovingButtonLeft)
            {
                m_cameraMovingButtonLeftIsClicked = actionType == ButtonEventState::RELEASED ? false : true;
            }
            else if (keyID == m_cameraMovingButtonRight)
            {
                m_cameraMovingButtonRightIsClicked = actionType == ButtonEventState::RELEASED ? false : true;
            }

            else if (keyID == m_cameraMovingButtonTop)
            {
                m_cameraMovingButtonTopIsClicked = actionType == ButtonEventState::RELEASED ? false : true;
            }
            else if (keyID == m_cameraMovingButtonBottom)
            {
                m_cameraMovingButtonBottomIsClicked = actionType == ButtonEventState::RELEASED ? false : true;
            }
        }

        void BasicCameraController::rotateCamera(const GLfloat& additionalYawAngle, const GLfloat& additionalTiltAngle)
        {
            m_yawAngle += additionalYawAngle;
            m_tiltAngle += additionalTiltAngle;
            m_rotationChanged = true;
            m_viewIsUpToDate = true;
        }

        void BasicCameraController::moveCamera(const glm::vec3& vec)
        {
            setCameraPosition(getCameraPosition() + vec);
        }

        void BasicCameraController::updateCameraPosition()
        {
            // Forward/ Back
            if (m_cameraMovingButtonForwardIsClicked)
            {
                updateMovementVectors();
                moveCamera(glm::vec3(m_movementForwardVector.x, m_movementForwardVector.y, 0.0f) * m_cameraMovementTimer->getElapsedSeconds() * m_cameraSpeed);
            }
            else if (m_cameraMovingButtonBackIsClicked)
            {
                updateMovementVectors();
                moveCamera(glm::vec3(m_movementForwardVector.x, m_movementForwardVector.y, 0.0f) * m_cameraMovementTimer->getElapsedSeconds() * -m_cameraSpeed);
            }

            // Right/ Left
            if (m_cameraMovingButtonLeftIsClicked)
            {
                updateMovementVectors();
                moveCamera(glm::vec3(m_movementRightVector.x, m_movementRightVector.y, 0.0f) * m_cameraMovementTimer->getElapsedSeconds() * -m_cameraSpeed);
            }
            else if (m_cameraMovingButtonRightIsClicked)
            {
                updateMovementVectors();
                moveCamera(glm::vec3(m_movementRightVector.x, m_movementRightVector.y, 0.0f) * m_cameraMovementTimer->getElapsedSeconds() * m_cameraSpeed);
            }

            // Top/ Bottom
            if (m_cameraMovingButtonTopIsClicked)
            {
                moveCamera(glm::vec3(0.0f, 0.0f, m_cameraMovementTimer->getElapsedSeconds() * m_cameraSpeed));
            }
            else if (m_cameraMovingButtonBottomIsClicked)
            {
                moveCamera(glm::vec3(0.0f, 0.0f, m_cameraMovementTimer->getElapsedSeconds() * -m_cameraSpeed));
            }

            m_cameraMovementTimer->resetTimer();
        }

        void BasicCameraController::updateMovementVectors()
        {
            if (m_needUpdateMovementVectors)
            {
                calculateMovementVectors();
                m_needUpdateMovementVectors = false;
            }
        }

        void BasicCameraController::calculateMovementVectors()
        {
            m_movementForwardVector = getNormalizedForwardXYVector();
            m_movementRightVector = getNormalizedRightXYVector(m_movementForwardVector);
        }

        void BasicCameraController::setCameraRotationButton(GLint mouseButtonID)
        {
            m_cameraRotationButton = mouseButtonID;
        }

        void BasicCameraController::setCameraMovingButtonForward(GLint mouseButtonID)
        {
            m_cameraMovingButtonForward = mouseButtonID;
        }

        void BasicCameraController::setCameraMovingButtonBack(GLint mouseButtonID)
        {
            m_cameraMovingButtonBack = mouseButtonID;
        }

        void BasicCameraController::setCameraMovingButtonLeft(GLint mouseButtonID)
        {
            m_cameraMovingButtonLeft = mouseButtonID;
        }

        void BasicCameraController::setCameraMovingButtonRight(GLint mouseButtonID)
        {
            m_cameraMovingButtonRight = mouseButtonID;
        }

        void BasicCameraController::setCameraMovingButtonTop(GLint mouseButtonID)
        {
            m_cameraMovingButtonTop = mouseButtonID;
        }

        void BasicCameraController::setCameraMovingButtonBottom(GLint mouseButtonID)
        {
            m_cameraMovingButtonBottom = mouseButtonID;
        }

        void BasicCameraController::setMouseRotationSensitivity(GLfloat sensitivity)
        {
            if (sensitivity == 0.0f)
            {
                WARNING_LOG("Sensitivity of mouse rotation cannot be 0!");
                return;
            }

            m_mouseRotationSensitivity = sensitivity;
        }

        void BasicCameraController::setCameraSpeed(GLfloat cameraSpeed)
        {
            m_cameraSpeed = cameraSpeed;
        }
    }
}