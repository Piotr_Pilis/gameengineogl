#include "../Log/log.hpp"
#include "cameraControllerAbstract.hpp"

namespace Engine
{
    namespace Transform
    {
        CameraControllerAbstract::CameraControllerAbstract(const GLint& windowWidth, const GLint& windowHeight) :
            m_yawAngle(0.0f),
            m_tiltAngle(0.0f),
            m_pitchAngle(0.0f),
            m_viewIsUpToDate(false),
            m_positionChanged(false),
            m_rotationChanged(false)
        {
            m_camera = std::make_unique<Camera>(m_transformBlock, m_viewIsUpToDate);
            reinitialize(windowWidth, windowHeight);
        }

        void CameraControllerAbstract::reinitialize(const GLint& windowWidth, const GLint& windowHeight)
        {
            m_windowWidth = windowWidth <= 0 ? 1 : windowWidth;
            m_windowHeight = windowHeight <= 0 ? 1 : windowHeight;
        }

        GLfloat CameraControllerAbstract::getDeltaMousePositionX(const GLfloat& currentMousePositionX)
        {
            return (currentMousePositionX - m_lastMousePositionX) / m_windowWidth;
        }

        GLfloat CameraControllerAbstract::getDeltaMousePositionY(const GLfloat& currentMousePositionY)
        {
            return (currentMousePositionY - m_lastMousePositionY) / m_windowHeight;
        }

        Camera* CameraControllerAbstract::getCamera()
        {
            return m_camera.get();
        }

        void CameraControllerAbstract::setCameraPosition(const glm::vec3& position)
        {
            m_transformBlock.m_cameraPosition = position;
            m_positionChanged = true;
            m_viewIsUpToDate = true;
        }

        void CameraControllerAbstract::moveCamera(const glm::vec3& vec)
        {
            setCameraPosition(getCameraPosition() + vec);
        }

        const glm::vec3& CameraControllerAbstract::getCameraPosition() const
        {
            return m_transformBlock.m_cameraPosition;
        }

        void CameraControllerAbstract::setCameraRotation(const GLfloat& yawAngle, const GLfloat& tiltAngle, const GLfloat& pitchAngle)
        {
            m_yawAngle = yawAngle;
            m_tiltAngle = tiltAngle;
            m_pitchAngle = pitchAngle;
            m_rotationChanged = true;
            m_viewIsUpToDate = true;
        }

        const glm::vec3 CameraControllerAbstract::getCameraRotation() const
        {
            return glm::vec3(m_yawAngle, m_tiltAngle, m_pitchAngle);
        }

        const glm::vec3 CameraControllerAbstract::getForwardVector() const
        {
            return -glm::vec3(m_transformBlock.m_viewMatrix[0][2], m_transformBlock.m_viewMatrix[1][2], m_transformBlock.m_viewMatrix[2][2]);
        }

        const glm::vec2 CameraControllerAbstract::getNormalizedForwardXYVector() const
        {
            // Forward vector
            glm::vec2 forwardVector = glm::vec2
            (
                getForwardVector().x,
                getForwardVector().y
            );

            // Normalize vector
            if (forwardVector.x != 0.0f || forwardVector.y != 0.0f)
                forwardVector = glm::normalize(forwardVector);

            // Result
            return forwardVector;
        }

        const glm::vec2 CameraControllerAbstract::getNormalizedRightXYVector(const glm::vec2& forwardDirection)
        {
            return glm::vec2(forwardDirection.y, -forwardDirection.x);
        }

        const glm::vec2 CameraControllerAbstract::getNormalizedRightXYVector() const
        {
            const auto forwardDirection = getNormalizedForwardXYVector();
            return getNormalizedRightXYVector(forwardDirection);
        }

        void CameraControllerAbstract::onMousePositionChanged(const GLfloat& x, const GLfloat& y)
        {
            m_lastMousePositionX = x;
            m_lastMousePositionY = y;
        }

        void CameraControllerAbstract::update()
        {
            if (m_viewIsUpToDate)
            {
                if (m_positionChanged)
                {
                    cameraPositionChanged(getCameraPosition());
                    m_positionChanged = false;
                }

                if (m_rotationChanged)
                {
                    cameraRotationChanged(getCameraRotation());
                    m_rotationChanged = false;
                }

                updateViewMatrix();
                updateViewProjectionMatrix();
                m_camera->updateUBOContent(m_transformBlock);

                m_viewIsUpToDate = false;
            }
        }

        void CameraControllerAbstract::updateViewProjectionMatrix()
        {
            m_transformBlock.m_viewProjectionMatrix = m_camera->getProjectionMatrix() * m_transformBlock.m_viewMatrix;
        }
    }
}