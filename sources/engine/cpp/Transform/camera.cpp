#include "Pipeline/globalUniformBlockBinding.hpp"
#include "Pipeline/program.hpp"
#include "camera.hpp"

namespace Engine
{
    namespace Transform
    {
        Camera::Camera(TransformBlock& transformBlock, bool& viewIsUpToDate) :
            m_projectionMatrix(1.0f),
            m_viewIsUpToDate(&viewIsUpToDate)
        {
            glGenBuffers(1, &m_ubo);
            glBindBuffer(GL_UNIFORM_BUFFER, m_ubo);
            glBufferData(GL_UNIFORM_BUFFER, sizeof(transformBlock), &transformBlock, GL_DYNAMIC_DRAW);
        }

        Camera::~Camera()
        {
            glDeleteBuffers(1, &m_ubo);
        }

        void Camera::updateUBOContent(TransformBlock& transformBlock)
        {
            glBindBuffer(GL_UNIFORM_BUFFER, m_ubo);
            glBufferSubData(GL_UNIFORM_BUFFER, 0, sizeof(transformBlock), &transformBlock);
        }

        void Camera::setProjectionMatrix(const glm::mat4x4& projectionMatrix)
        {
            m_projectionMatrix = projectionMatrix;
            *m_viewIsUpToDate = true;
        }

        const glm::mat4x4& Camera::getProjectionMatrix() const
        {
            return m_projectionMatrix;
        }

        void Camera::setActive()
        {
            Pipeline::Program::bindBuffer
            (
                m_ubo, 
                static_cast<GLuint>(Pipeline::GlobalUniformBlockBinding::BLOCK_TRANSFORM)
            );
        }
    }
}