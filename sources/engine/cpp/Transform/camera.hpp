#pragma once

#include <glad/glad.h>
#include <glm/mat4x4.hpp>

namespace Engine
{
    namespace Transform
    {
        class Camera
        {
        public:
            struct TransformBlock
            {
                TransformBlock()
                {
                    m_viewProjectionMatrix = glm::mat4x4(1.0f);
                    m_viewMatrix = glm::mat4x4(1.0f);
                    m_cameraPosition = glm::vec3(0.0f);
                    m_normalMatrix = glm::mat4x4(1.0f);
                }

                glm::mat4x4 m_viewProjectionMatrix;
                glm::mat4x4 m_viewMatrix;
                glm::vec3 m_cameraPosition; float dummy;    // 4th component not used
                glm::mat4x4 m_normalMatrix;
            };

        public:
            /* Object of camera class should be created by camera controller */
            Camera() = delete;
            Camera(TransformBlock& transformBlock, bool& viewIsUpToDate);
            ~Camera();

            /* Warning: All data block will be updated - also viewMatrix */
            void setProjectionMatrix(const glm::mat4x4& projectionMatrix);
            const glm::mat4x4& getProjectionMatrix() const;

            /* Binds uniform block to global uniform location index */
            void setActive();

            /* Sends updated data to VRAM 
            ** This method should be called only by camera controller */
            void updateUBOContent(TransformBlock& transformBlock);

        private:
            glm::mat4x4 m_projectionMatrix;

            /* OpenGL variables */
            GLuint m_ubo;

            /* Variable used to signal status of matrices 
            ** This is only reference to right variable - 
            ** instance should be placed inside camera controller */
            bool* m_viewIsUpToDate;
        };
    }
}