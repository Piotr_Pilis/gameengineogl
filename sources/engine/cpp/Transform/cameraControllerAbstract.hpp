#pragma once
#include <boost/signals2/signal.hpp>
#include <glm/vec3.hpp>
#include <glm/vec2.hpp>
#include <glad/glad.h>
#include <memory>

#include "Definition/buttonEventState.hpp"
#include "camera.hpp"

namespace Engine::Transform
{
    class CameraControllerAbstract
    {
    public:
        using ButtonEventState = Definition::ButtonEventState;

    public:
        CameraControllerAbstract() = delete;
        CameraControllerAbstract(const GLint& windowWidth, const GLint& windowHeight);

        void reinitialize(const GLint& windowWidth, const GLint& windowHeight);

        // Getters/ Setters
        GLfloat getDeltaMousePositionX(const GLfloat& currentMousePositionX);
        GLfloat getDeltaMousePositionY(const GLfloat& currentMousePositionY);

        Camera* getCamera();

        void setCameraPosition(const glm::vec3& position);
        virtual void moveCamera(const glm::vec3& vec);
        const glm::vec3& getCameraPosition() const;

        void setCameraRotation(const GLfloat& yawAngle, const GLfloat& tiltAngle, const GLfloat& pitchAngle = 0.0f);
        const glm::vec3 getCameraRotation() const;

        /* Direction vectors */
        const glm::vec3 getForwardVector() const;
        const glm::vec2 getNormalizedForwardXYVector() const;
        static const glm::vec2 getNormalizedRightXYVector(const glm::vec2& forwardDirection);
        const glm::vec2 getNormalizedRightXYVector() const;

        // Signals
        boost::signals2::signal<void(const glm::vec3&)>
            cameraPositionChanged;
        boost::signals2::signal<void(const glm::vec3&)>
            cameraRotationChanged;

        // Slots

        /* Base definition of this method saves previous value of last mouse position */
        virtual void onMousePositionChanged(const GLfloat& x, const GLfloat& y);
        virtual void onMouseButtonClicked(const GLint& mouseButtonID, ButtonEventState actionType) = 0;
        virtual void onKeyClicked(const GLint& keyID, ButtonEventState actionType) = 0;
        virtual void onScrolled(const GLdouble& offset) {}

        /* This method should be called in every tick */
        virtual void update();

    protected:
        /* Creates new view matrix */
        virtual void updateViewMatrix() = 0;

    private:
        /* Creates new viewProjection matrix */
        void updateViewProjectionMatrix();

    protected:
        std::unique_ptr<Camera> m_camera;
        GLint m_windowWidth;
        GLint m_windowHeight;

        GLfloat m_lastMousePositionX;
        GLfloat m_lastMousePositionY;

        Camera::TransformBlock m_transformBlock;

        /* Rotation factors of view matrix */
        GLfloat m_yawAngle;
        GLfloat m_tiltAngle;
        GLfloat m_pitchAngle;

        /* Variable used to signal status of matrices */
        bool m_viewIsUpToDate;
        bool m_positionChanged;
        bool m_rotationChanged;
    };
}