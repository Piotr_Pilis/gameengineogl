#include <glm/gtc/matrix_transform.hpp>

#include "Geometry/object3d.hpp"
#include "Log/warningLog.hpp"

#include "thirdPersonCameraController.hpp"

namespace Engine::Transform
{
    ThirdPersonCameraController::ThirdPersonCameraController(const GLint& windowWidth, const GLint& windowHeight) :
        CameraControllerAbstract(windowWidth, windowHeight)
    {
    }

    ThirdPersonCameraController::~ThirdPersonCameraController()
    {
        // Disconnect connections
        if (m_positionObject3DConnection.connected())
        {
            m_positionObject3DConnection.disconnect();
        }
    }

    std::shared_ptr<ThirdPersonCameraController> ThirdPersonCameraController::make
    (
        const GLint& windowWidth,
        const GLint& windowHeight
    )
    {
        struct MakeSharedPtrEnabler :
            public ThirdPersonCameraController
        {
            MakeSharedPtrEnabler(const GLint& windowWidth, const GLint& windowHeight) : 
                ThirdPersonCameraController(windowWidth, windowHeight) 
            {}
        };

        return std::move(std::make_shared<MakeSharedPtrEnabler>(windowWidth, windowHeight));
    }

    void ThirdPersonCameraController::updateViewMatrix()
    {
        // Yaw
        glm::mat4x4 rotationMatrix = glm::rotate(
            glm::mat4(1.0f),
            m_yawAngle,
            glm::vec3(0.0f, 0.0f, 1.0f)
        );

        // Tilt
        rotationMatrix = glm::rotate(
            rotationMatrix,
            m_tiltAngle,
            glm::vec3(1.0f, 0.0f, 0.0f)
        );

        // Normal matrix
        m_transformBlock.m_normalMatrix = glm::transpose(rotationMatrix);

        // Set zoom position
        m_transformBlock.m_viewMatrix =  glm::translate(rotationMatrix, glm::vec3(0.0f, 0.0f, m_zoom));

        // Rotation matrix
        m_transformBlock.m_viewMatrix = glm::inverse(m_transformBlock.m_viewMatrix);

        // And translation matrix
        m_transformBlock.m_viewMatrix = glm::translate(m_transformBlock.m_viewMatrix, -m_transformBlock.m_cameraPosition);
    }

    void ThirdPersonCameraController::rotateCamera(const GLfloat& additionalYawAngle, const GLfloat& additionalTiltAngle)
    {
        m_yawAngle += additionalYawAngle;
        m_tiltAngle += additionalTiltAngle;
        m_rotationChanged = true;
        m_viewIsUpToDate = true;
    }

    void ThirdPersonCameraController::setCameraRotationButton(GLint mouseButtonID)
    {
        m_cameraRotationButton = mouseButtonID;
    }

    void ThirdPersonCameraController::setMouseRotationSensitivity(const GLfloat sensitivity)
    {
        if (sensitivity < 0.01f)
        {
            WARNING_LOG("Sensitivity of mouse rotation is too low!");
            return;
        }

        m_mouseRotationSensitivity = sensitivity;
    }

    void ThirdPersonCameraController::setZoomSensitivity(const GLfloat sensitivity)
    {
        if (sensitivity < 0.01f)
        {
            WARNING_LOG("Sensitivity of zoom is too low!");
            return;
        }

        m_zoomSensitivity = sensitivity;
    }

    void ThirdPersonCameraController::bindObject3D(std::shared_ptr<Geometry::Object3D>& object3D)
    {
        // Validation
        if (object3D.get() == nullptr)
        {
            WARNING_LOG("Cannot bind position of null to camera controller!");
            return;
        }

        // This will be always std::shared_ptr...
        auto thisSmartPtr = shared_from_this();
        
        // Connect to signal of object 3d
        m_positionObject3DConnection = object3D->positionChanged.connect
        (
            decltype(object3D->positionChanged)::slot_type
            (
                &ThirdPersonCameraController::onObject3DPositionChanged,
                thisSmartPtr.get(),
                object3D
            ).track_foreign(thisSmartPtr)
        );

        // Set current values
        setCameraPosition
        (
            object3D->getTranslation() +
            glm::vec3(0.0f, 0.0f, object3D->getBoundBox().z * 0.5f)
        );
    }

    void ThirdPersonCameraController::onMousePositionChanged(const GLfloat& x, const GLfloat& y)
    {
        // Do we need update first mouse position before right action?
        if (m_needUpdateMousePosition)
        {
            m_lastMousePositionX = x;
            m_lastMousePositionY = y;
            m_needUpdateMousePosition = false;
        }
        else if (m_cameraRotationButtonIsClicked)
        {
            rotateCamera(
                getDeltaMousePositionX(x) * m_mouseRotationSensitivity,
                getDeltaMousePositionY(y) * m_mouseRotationSensitivity
            );

            m_lastMousePositionX = x;
            m_lastMousePositionY = y;
        }
    }

    void ThirdPersonCameraController::onMouseButtonClicked(const GLint& mouseButtonID, ButtonEventState actionType)
    {
        if (mouseButtonID == m_cameraRotationButton)
        {
            m_cameraRotationButtonIsClicked = actionType == ButtonEventState::RELEASED ? false : true;

            // Do we need update first mouse position before right action?
            m_needUpdateMousePosition = m_cameraRotationButtonIsClicked;
        }
    }

    void ThirdPersonCameraController::onKeyClicked(const GLint& keyID, ButtonEventState actionType)
    {
    }

    void ThirdPersonCameraController::onScrolled(const GLdouble& offset)
    {
        m_zoom += m_zoomSensitivity * -offset;
        if (m_zoom > MAX_ZOOM)
        {
            m_zoom = MAX_ZOOM;
        }
        if (m_zoom < MIN_ZOOM)
        {
            m_zoom = MIN_ZOOM;
        }
        m_viewIsUpToDate = true;
    }

    void ThirdPersonCameraController::onObject3DPositionChanged(const std::weak_ptr<Geometry::Object3D> object3DWeakPtr)
    {
        auto object3DPtr = object3DWeakPtr.lock();
        if (object3DPtr.get() == nullptr)
        {
            return;
        }
        setCameraPosition
        (
            object3DPtr->getTranslation() +
            glm::vec3(0.0f, 0.0f, object3DPtr->getBoundBox().z * 0.5f)
        );
    }
}