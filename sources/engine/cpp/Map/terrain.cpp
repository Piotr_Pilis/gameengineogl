#include <sstream>
#include <iomanip>
#include <locale>
#include <codecvt>
#include <fmt/format.h>

#include "Log/criticalLog.hpp"

#include "Material/materialInstance.hpp"
#include "Material/materialInfo.hpp"
#include "Material/material.hpp"

#include "FileSystem/ifstream.hpp"
#include "FileSystem/fileSystem.hpp"

#include "Private/terrainBase.hpp"
#include "Data/json.h"

#include "terrain.hpp"

namespace Engine::Map
{
    namespace
    {
        Private::TerrainBase& getTerrainBase()
        {
            static Private::TerrainBase terrainBase;
            return terrainBase;
        }

        Types::Status readConfigurationFile(
            const std::string& fullFilePath,
            GLfloat& maxHeight,
            std::vector<Material::MaterialInfo>& materialInfos)
        {
            try
            {
                FileSystem::Ifstream fin(fullFilePath);
                std::stringstream buffer;
                buffer << fin.rdbuf();

                auto jsonDocument = std::shared_ptr<JSONValue>(JSON::parse(buffer.str().c_str()));
                auto jsonRootObject = jsonDocument->asObject();

                using convert_typeX = std::codecvt_utf8<wchar_t>;
                std::wstring_convert<convert_typeX, wchar_t> converterX;

                // Error logs
                auto emitInvalidStructureFileError = [&fullFilePath]()
                {
                    return Types::Error
                    (
                        Types::Error::Code::CORRUPTED_ASSETS,
                        fmt::format
                        (
                            R"(Invalid structure of configuration file "{}")",
                            fullFilePath
                        )
                    );
                };

                // Header
                if (!jsonDocument->hasChild(L"configName") ||
                    jsonRootObject[L"configName"]->asString() != L"MapConfigurationFile")
                {
                    emitInvalidStructureFileError();
                }

                // MaxHeight
                if (!jsonDocument->hasChild(L"maxHeight"))
                {
                    emitInvalidStructureFileError();
                }
                maxHeight = static_cast<GLfloat>(jsonRootObject[L"maxHeight"]->asNumber());

                // Materials
                if (!jsonDocument->hasChild(L"materials"))
                {
                    emitInvalidStructureFileError();
                }
                auto materialsJsonData = jsonRootObject[L"materials"];

                if (!materialsJsonData->isArray())
                {
                    emitInvalidStructureFileError();
                }
                auto& materialsJsonArray = materialsJsonData->asArray();
                if (materialsJsonArray.size() != 0)
                {
                    materialInfos.reserve(materialsJsonArray.size());
                    for (auto& materialJsonData : materialsJsonArray)
                    {
                        if (!materialJsonData->isObject())
                        {
                            emitInvalidStructureFileError();
                        }
                        auto materialJsonObject = materialJsonData->asObject();

                        materialInfos.push_back
                        (
                            Material::MaterialInfo
                            {
                                materialJsonData->hasChild(L"diffuseMapPath") ? converterX.to_bytes(materialJsonObject[L"diffuseMapPath"]->asString()) : "",
                                materialJsonData->hasChild(L"normalMapPath") ? converterX.to_bytes(materialJsonObject[L"normalMapPath"]->asString()) : "",
                                materialJsonData->hasChild(L"heightBitmapPath") ? converterX.to_bytes(materialJsonObject[L"heightBitmapPath"]->asString()) : ""
                            }
                        );
                    }
                }
            }
            catch (std::invalid_argument invalidArgument)
            {
                return Types::Error
                (
                    Types::Error::Code::CORRUPTED_ASSETS,
                    fmt::format
                    (
                        R"(Could not open "{}")",
                        fullFilePath
                    )
                );
            }

            // Validation
            if (maxHeight > 100000.0f || maxHeight <= 0.0f)
            {
                return Types::Error
                (
                    Types::Error::Code::CORRUPTED_ASSETS,
                    fmt::format
                    (
                        R"(Configuration file "{}" is damaged)",
                        fullFilePath
                    )
                );
            }   

            return Types::Ok{};
        }

        auto getChunksCount
        (
            const std::string& mapPath
        )-> Types::Result<std::pair<GLuint, GLuint>>
        {
            auto getChunkColumnCount = [&mapPath](const GLuint& chunkRowId)
            {
                for (GLuint chunkColumnId = 0; true; ++chunkColumnId)
                {
                    if (!FileSystem::isDirectory
                    (
                        fmt::format
                        (
                            "{}{}x{}", 
                            mapPath, 
                            chunkColumnId,
                            chunkRowId
                        )
                    ))
                    {
                        return chunkColumnId;
                    }
                }
                return 0u;
            };

            std::pair<GLuint, GLuint> chunksCount{ getChunkColumnCount(0), 0 };
            if (chunksCount.first == 0)
            {
                return Types::Error
                (
                    Types::Error::Code::CORRUPTED_ASSETS, 
                    "Not found chunk data"
                );
            }
            for (GLuint chunkRowId = 1; true; ++chunkRowId)
            {
                const auto chunkColumnCount = getChunkColumnCount(chunkRowId);
                if (chunkColumnCount == 0)
                {
                    chunksCount.second = chunkRowId;
                    break;
                }
                else if (chunkColumnCount != chunksCount.first)
                {
                    return Types::Error
                    (
                        Types::Error::Code::CORRUPTED_ASSETS,
                        fmt::format
                        (
                            "Number of chunk columns are not equal for 1 row and {} row",
                            chunkColumnCount + 1
                        )
                    );
                }
            }
            return chunksCount;
        }

        struct ChunkRawData
        {
            std::shared_ptr<Texture::Bitmap> m_heightMap;
            std::shared_ptr<Texture::Bitmap> m_pathMap;
        };

        Types::Status loadChunkRawData
        (
            const std::string& terrainDirectoryPath,
            const GLuint chunkColumns,
            const GLuint& chunkRows,
            std::vector<std::vector<ChunkRawData>>& chunkRawData
        )
        {
            static constexpr auto heightMapFileName = "heightMap.dds";
            static constexpr auto pathMapFileName = "pathMap.dds";

            // Reserve memory
            if (chunkRows <= 0 || chunkColumns <= 0)
            {
                return Types::Ok{};
            }
            chunkRawData.resize(chunkColumns);
            for (GLuint chunkCol = 0; chunkCol < chunkColumns; ++chunkCol)
            {
                chunkRawData[chunkCol].resize(chunkRows);
            }

            // Load chunk bitmaps
            for (GLuint chunkCol = 0; chunkCol < chunkColumns; ++chunkCol)
            {
                for (GLuint chunkRow = 0; chunkRow < chunkRows; ++chunkRow)
                {
                    // Validate chunk directory
                    const std::string directoryPath = terrainDirectoryPath + fmt::format("{}x{}", chunkCol, chunkRow);
                    if (!FileSystem::isDirectory(directoryPath))
                    {
                        return Types::Error
                        (
                            Types::Error::Code::CORRUPTED_ASSETS,
                            fmt::format
                            (
                                "Data of terrain chunk [{}][{}] was not found, expected directory: \"{}\"",
                                chunkCol,
                                chunkRow,
                                directoryPath
                            )
                        );
                    }

                    // Load bitmaps
                    auto& chunkRawItem = chunkRawData[chunkCol][chunkRow];

                    const std::string heightMapPath = fmt::format
                    (
                        "{}/{}",
                        directoryPath,
                        heightMapFileName
                    );
                    chunkRawItem.m_heightMap = std::make_shared<Texture::Bitmap>();
                    chunkRawItem.m_heightMap->loadFromFile(heightMapPath);
                    if (chunkRawItem.m_heightMap->getExtent(0)[0] != chunkRawItem.m_heightMap->getExtent(0)[1])
                    {
                        return Types::Error
                        (
                            Types::Error::Code::CORRUPTED_ASSETS,
                            "Width and height of height map are not equal"
                        );
                    }

                    const std::string pathMapPath = fmt::format
                    (
                        "{}/{}",
                        directoryPath,
                        pathMapFileName
                    );
                    chunkRawItem.m_pathMap = std::make_shared<Texture::Bitmap>();
                    chunkRawItem.m_pathMap->loadFromFile(pathMapPath);
                    if (chunkRawItem.m_pathMap->getExtent(0)[0] != chunkRawItem.m_pathMap->getExtent(0)[1])
                    {
                        return Types::Error
                        (
                            Types::Error::Code::CORRUPTED_ASSETS,
                            "Width and height of path map are not equal"
                        );
                    }
                }
            }
            return Types::Ok{};
        }

        void loadMaterials
        (
            const std::vector<Material::MaterialInfo>& materialInfos,
            std::unique_ptr<Material::MaterialInstance>& materialsInstance
        )
        {
            LOG("Loading terrain materials");
            if (materialInfos.empty())
            {
                return;
            }
            std::vector<std::shared_ptr<Material::Material>> materials;
            materials.reserve(materialInfos.size());
            for (auto& materialInfo : materialInfos)
            {
                materials.emplace_back(std::make_shared<Material::Material>(materialInfo));
            }

            LOG("Initializing terrain material instance");
            materialsInstance = std::make_unique<Material::MaterialInstance>(
                Material::MaterialInstance::MaterialType::FLAT_MATERIAL,
                materials
            );
        }

        void initializeBaseTextures
        (
            const std::vector<std::vector<ChunkRawData>>& chunkRawData,
            std::vector<std::shared_ptr<Texture::Bitmap>>& heightBitmaps,
            std::array<Texture::Texture, magic_enum::enum_count<Terrain::MapType>()>& baseTextures
        )
        {
            // Extract number of columns and rows
            const auto chunkColumns = chunkRawData.size();
            if (chunkColumns == 0)
            {
                return;
            }
            const auto chunkRows = chunkRawData[0].size();
            if (chunkRows == 0)
            {
                return;
            }

            // Fill bitmaps
            std::decay_t<decltype(heightBitmaps)> pathBitmaps;

            heightBitmaps.reserve(chunkColumns * chunkRows);
            pathBitmaps.reserve(chunkColumns * chunkRows);

            for (GLuint chunkColumn = 0; chunkColumn < chunkColumns; ++chunkColumn)
            {
                for (GLuint chunkRow = 0; chunkRow < chunkRows; ++chunkRow)
                {
                    heightBitmaps.emplace_back
                    (
                        chunkRawData[chunkColumn][chunkRow].m_heightMap
                    );
                    pathBitmaps.emplace_back
                    (
                        chunkRawData[chunkColumn][chunkRow].m_pathMap
                    );
                }
            }

            // Initalizes textures
            baseTextures[static_cast<GLuint>(Terrain::MapType::HEIGHT_MAP)].initialize
            (
                heightBitmaps,
                GL_REPEAT,
                GL_REPEAT,
                GL_LINEAR,
                GL_LINEAR
            );
            baseTextures[static_cast<GLuint>(Terrain::MapType::PATH_MAP)].initialize
            (
                pathBitmaps,
                GL_REPEAT,
                GL_REPEAT,
                GL_LINEAR,
                GL_LINEAR
            );
        }
    }

    Terrain::Terrain(const std::string& terrainName)
    {
        glUseProgram(**getTerrainBase().m_program);
        ENSURE_CRITICAL
        (
            load(terrainName),
            R"(Cannot load terrain "{}", reason: {})",
            error.getMessage(),
            m_mapPath
        );
    }

    Terrain::~Terrain()
    {
    }

    void Terrain::render()
    {
        glUseProgram(**getTerrainBase().m_program);

        // Base textures
        for (GLuint baseMapId = 0; baseMapId < m_baseTextures.size(); ++baseMapId)
        {
            glActiveTexture(GL_TEXTURE0 + baseMapId);
            m_baseTextures[baseMapId].bind();
        }

        const GLuint textureIdOffset = static_cast<GLuint>(m_baseTextures.size());
        if (m_materialsInstance->getAlbedoMap() != nullptr)
        {
            glActiveTexture
            (
                GL_TEXTURE0 + 
                static_cast<GLuint>(MaterialType::DIFFUSE_MAP) + 
                textureIdOffset
            );
            m_materialsInstance->getAlbedoMap()->bind();
        }
        if (m_materialsInstance->getNormalMap() != nullptr)
        {
            glActiveTexture
            (
                GL_TEXTURE0 + 
                static_cast<GLuint>(MaterialType::NORMAL_MAP) + 
                textureIdOffset
            );
            m_materialsInstance->getNormalMap()->bind();
        }

        // Geometry
        getTerrainBase().renderGeometry();
    }

    Types::Status Terrain::load(const std::string& terrainName)
    {
        m_mapPath = "Maps/" + terrainName + "/";
        std::vector<Material::MaterialInfo> materialInfos;

        ENSURE(readConfigurationFile
        (
            m_mapPath + "config.ini", 
            m_maxHeight, 
            materialInfos
        ));

        // TODO: dynamic reload
        getTerrainBase().setMaxHeight(m_maxHeight);

        std::tie(m_chunkColumns, m_chunkRows) = UNWRAP(getChunksCount(m_mapPath));
        if (m_chunkColumns == 0 || m_chunkRows == 0)
        {
            return Types::Ok{};
        }
        
        // Load materials
        loadMaterials(materialInfos, m_materialsInstance);
        getTerrainBase().setMaterialCount(materialInfos.size());

        // Load chunk data
        std::vector<std::vector<ChunkRawData>> chunkRawData;
        ENSURE(loadChunkRawData
        (
            m_mapPath,
            m_chunkColumns,
            m_chunkRows,
            chunkRawData
        ));

        //
        getTerrainBase().setBaseMapsCountLocation
        (
            m_chunkColumns,
            m_chunkRows
        );

        // Initialize base texture like height maps, path maps etc.
        initializeBaseTextures(chunkRawData, m_heightBitmaps, m_baseTextures);
        return Types::Ok{};
    }
}