#pragma once
#include <glad/glad.h>
#include <memory>
#include "Pipeline/program.hpp"

namespace Engine::Map
{
    class Terrain;
}

namespace Engine::Map::Private
{
    /* One instance for every Terrain class
    ** Contains: geometry, program definition
    ** We don't need more than one instance of geometry or program, so this class contains these information here */
    class TerrainBase
    {
    private:
        friend class Engine::Map::Terrain;

        enum BufferObjectType
        {
            VERTEX = 0,
            INDEX,
            NUM_BOs
        };

    public:
        TerrainBase();
        ~TerrainBase();

    private:
        // These methods have to be called before use only once
        void initializeGeometry();
        void initializeProgram();

        void setMaxHeight(const GLfloat& maxHeight);
        void setMaterialCount(const GLint& materialCount);
        void setBaseMapsCountLocation(GLuint columns, GLuint rows);

        void renderGeometry();

    private:
        GLuint m_vao;
        GLuint m_vbo[BufferObjectType::NUM_BOs];

        GLuint m_numIndices;

        std::unique_ptr<Pipeline::Program> m_program;
        GLuint m_maxHeightLocation;
        GLuint m_materialCountLocation;
        GLuint m_baseMapsCountLocation;

    };
}