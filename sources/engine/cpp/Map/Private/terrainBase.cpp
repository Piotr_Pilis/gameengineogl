#include <magic_enum.hpp>
#include <glm/vec2.hpp>
#include <string_view>
#include <algorithm>
#include <cctype>

#include "Pipeline/globalUniformBlockBinding.hpp"
#include "Pipeline/shaderVariant.hpp"
#include "Pipeline/program.hpp"

#include "Config/terrainConfig.hpp"
#include "Geometry/canvas.hpp"
#include "Log/glStatus.hpp"
#include "Map/terrain.hpp"
#include "Log/log.hpp"

#include "terrainBase.hpp"

using namespace std::literals;
using namespace Engine::Config::TerrainConfig;
namespace Engine::Map::Private
{
    namespace
    {
        // Must be an odd number
        constexpr const GLuint NUM_CHUNKS_TO_BE_RENDERED = NUM_CHUNK_ROWS_TO_BE_RENDERED * NUM_CHUNK_ROWS_TO_BE_RENDERED;

        template<class EnumType>
        std::string getShaderVariantVarName(EnumType enumValue)
        {
            auto varNamePrefix = "TERRAIN_"s;
            if constexpr (std::is_same_v<EnumType, Terrain::MaterialType>)
            {
                varNamePrefix += "MATERIAL_";
            }
            return varNamePrefix + std::string(magic_enum::enum_name(enumValue)) + "_ID";
        }
    }

    TerrainBase::TerrainBase()
    {
        initializeGeometry();
        initializeProgram();
    }

    TerrainBase::~TerrainBase()
    {
        if (glIsVertexArray(m_vao))
            glDeleteVertexArrays(1, &m_vao);

        if (glIsBuffer(m_vbo[0]))
            glDeleteBuffers(BufferObjectType::NUM_BOs, m_vbo);
    }

    void TerrainBase::initializeGeometry()
    {
        LOG("Initializing terrain geometry...");

        glGenVertexArrays(1, &m_vao);
        glBindVertexArray(m_vao);

        glGenBuffers(BufferObjectType::NUM_BOs, m_vbo);

        // NumTilesPerChunk * NumTilesPerChunk * NumPrimitveVertices (quads)
        GLuint numQuadsPerRow = static_cast<GLuint>(
            std::ceil(GEOMETRY_SIZE / MIN_TILE_SIZE));
        m_numIndices = numQuadsPerRow * numQuadsPerRow * 4;

        // Fills index buffer object
        {
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_vbo[BufferObjectType::INDEX]);
            glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLuint) * m_numIndices, NULL, GL_STATIC_DRAW);

            GLuint* indices = reinterpret_cast<GLuint*>(glMapBufferRange(GL_ELEMENT_ARRAY_BUFFER, 0, sizeof(GLuint) * m_numIndices,
                GL_MAP_WRITE_BIT | GL_MAP_INVALIDATE_BUFFER_BIT | GL_MAP_UNSYNCHRONIZED_BIT));

            for (GLuint x = 0; x < numQuadsPerRow; ++x)
            {
                for (GLuint y = 0; y < numQuadsPerRow; ++y)
                {
                    indices[(x + y * numQuadsPerRow) * 4] = x + y * (numQuadsPerRow + 1);                     // x -> y
                    indices[(x + y * numQuadsPerRow) * 4 + 1] = x + 1 + y * (numQuadsPerRow + 1);             // x + 1 -> y
                    indices[(x + y * numQuadsPerRow) * 4 + 2] = x + 1 + (y + 1) * (numQuadsPerRow + 1);       // x + 1-> y + 1
                    indices[(x + y * numQuadsPerRow) * 4 + 3] = x + (y + 1) * (numQuadsPerRow + 1);       // x + 1 -> y + 1
                }
            }

            glUnmapBuffer(GL_ELEMENT_ARRAY_BUFFER);
        }

        // Fills vertex buffer object
        {
            glBindBuffer(GL_ARRAY_BUFFER, m_vbo[BufferObjectType::VERTEX]);
            glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec2) * (numQuadsPerRow + 1) * (numQuadsPerRow + 1), NULL, GL_STATIC_DRAW);

            glm::vec2* vertexAddr = reinterpret_cast<glm::vec2*>(glMapBufferRange(
                GL_ARRAY_BUFFER,
                0,
                sizeof(glm::vec2) * (numQuadsPerRow + 1) * (numQuadsPerRow + 1),
                GL_MAP_WRITE_BIT | GL_MAP_INVALIDATE_BUFFER_BIT | GL_MAP_UNSYNCHRONIZED_BIT)
                );

            for (GLuint x = 0; x <= numQuadsPerRow; ++x)
            {
                for (GLuint y = 0; y <= numQuadsPerRow; ++y)
                {
                    vertexAddr[x + y * (numQuadsPerRow + 1)] = glm::vec2(
                        x * MIN_TILE_SIZE,
                        y * MIN_TILE_SIZE
                    );
                }
            }

            glUnmapBuffer(GL_ARRAY_BUFFER);
        }

        glEnableVertexAttribArray(BufferObjectType::VERTEX);
        glVertexAttribPointer(BufferObjectType::VERTEX, 2, GL_FLOAT, GL_FALSE, sizeof(glm::vec2), 0);

        glBindBuffer(GL_ARRAY_BUFFER, 0);
        CHECK_GL_ERROR();
    }

    void TerrainBase::initializeProgram()
    {
        LOG("Initializing terrain shaders...");

        Pipeline::ShaderVariant shaderVariant = Geometry::Canvas::getShaderData();
        shaderVariant.pushValue
        (
            {
                "LOCAL_TEXCOORD_MULTIPLIER",
                std::to_string(2.0 / GEOMETRY_SIZE)
            }
        );
        shaderVariant.pushValue
        (
            {
                "GLOBAL_TEXCOORD_MULTIPLIER",
                std::to_string(1.0 / GEOMETRY_SIZE)
            }
        );
        shaderVariant.pushValue
        (
            {
                "NUM_CHUNK_ROWS_TO_BE_RENDERED",
                std::to_string(NUM_CHUNK_ROWS_TO_BE_RENDERED)
            }
        );
        shaderVariant.pushValue
        (
            {
                "GEOMETRY_SIZE",
                std::to_string(GEOMETRY_SIZE)
            }
        );

        for (const auto baseMapType : magic_enum::enum_entries<Terrain::MapType>())
        {
            shaderVariant.pushValue
            (
                {
                    getShaderVariantVarName(baseMapType.first) ,
                    std::to_string(static_cast<GLuint>(baseMapType.first))
                }
            );
        }

        GLint materialMapIdOffset = magic_enum::enum_count<Terrain::MapType>();
        for (const auto materialType : magic_enum::enum_entries<Terrain::MaterialType>())
        {
            shaderVariant.pushValue
            (
                {
                    getShaderVariantVarName(materialType.first) ,
                    std::to_string(static_cast<GLuint>(materialType.first) + materialMapIdOffset)
                }
            );
        }

        m_program = std::make_unique<Pipeline::Program>
        (
            Pipeline::Shader
            (
                "Engine/Shaders/terrain.vs",
                Pipeline::Shader::ShaderType::VertexShader,
                shaderVariant
            ),
            Pipeline::Shader
            (
                "Engine/Shaders/terrain.fs",
                Pipeline::Shader::ShaderType::FragmentShader,
                shaderVariant
            )
        );

        glUseProgram(**m_program);
        Pipeline::Program::bindUniformBlock
        (
            **m_program,
            "BlockTransform",
            static_cast<GLuint>(Pipeline::GlobalUniformBlockBinding::BLOCK_TRANSFORM)
        );

        // Uniforms
        m_maxHeightLocation = glGetUniformLocation(**m_program, "maxHeight");
        m_materialCountLocation = glGetUniformLocation(**m_program, "materialCount");
        m_baseMapsCountLocation = glGetUniformLocation(**m_program, "baseMapsCount");
    }

    void TerrainBase::setMaxHeight(const GLfloat& maxHeight)
    {
        glUniform1f(m_maxHeightLocation, maxHeight);
    }

    void TerrainBase::setMaterialCount(const GLint& materialCount)
    {
        glUniform1i(m_materialCountLocation, materialCount);
    }

    void TerrainBase::setBaseMapsCountLocation
    (
        GLuint columns, 
        GLuint rows
    )
    {
        glUniform2ui(m_baseMapsCountLocation, columns, rows);
    }

    void TerrainBase::renderGeometry()
    {
        glBindVertexArray(m_vao);
        glDrawElementsInstanced
        (
            GL_QUADS,
            m_numIndices,
            GL_UNSIGNED_INT,
            NULL,
            NUM_CHUNKS_TO_BE_RENDERED
        );
    }
}