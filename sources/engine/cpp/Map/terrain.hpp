#pragma once

#include <glad/glad.h>
#include <magic_enum.hpp>

#include <string>
#include <memory>
#include <vector>
#include <array>

#include "Types/result.hpp"
#include "Texture/bitmap.hpp"
#include "Texture/texture.hpp"

namespace Engine::Pipeline
{
    class Program;
}

namespace Engine::Texture
{
    class Texture;
}

namespace Engine::Material
{
    class MaterialInstance;
}

namespace Engine::Map::Private
{
    class TerrainBase;
}

namespace Engine::Map
{
    // To make one terrain for more than one render context you have to use copy constructor
    class Terrain
    {
    public:
        enum class MapType
        {
            HEIGHT_MAP = 0,
            PATH_MAP
        };

        enum class MaterialType
        {
            DIFFUSE_MAP = 0,
            NORMAL_MAP
        };

    public:
        Terrain() = delete;
        Terrain(const std::string& terrainName);
        Terrain(Terrain& other) = delete;
        Terrain& operator=(const Terrain& other) = delete;
        ~Terrain();

        // Renders terrain
        void render();

        const std::string& getMapPath() const { return m_mapPath; }

    private:
        /* Loads terrain from given name
        ** like materials, height maps and other information */
        Types::Status load(const std::string& terrainName);

    private:
        std::array<Texture::Texture, magic_enum::enum_count<MapType>()> m_baseTextures;
        std::unique_ptr<Material::MaterialInstance> m_materialsInstance;

        std::vector<std::shared_ptr<Texture::Bitmap>> m_heightBitmaps;

        GLfloat m_maxHeight = 100.0f;
        GLuint m_chunkColumns = 0;
        GLuint m_chunkRows = 0;
        std::string m_mapPath;

    };
}