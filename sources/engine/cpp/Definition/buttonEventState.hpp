#pragma once

namespace Engine::Definition
{
    enum class ButtonEventState
    {
        PRESSED,
        RELEASED,
        REPEATED
    };
}