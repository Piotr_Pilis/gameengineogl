#include <algorithm>

#include "materialInstance.hpp"
#include "../Log/criticalLog.hpp"
#include "../Log/warningLog.hpp"

namespace Engine
{
    namespace Material
    {
        MaterialInstance::MaterialInstance()
        {
        }

        MaterialInstance::MaterialInstance
        (
            const MaterialType materialType,
            const std::shared_ptr<Material>& material,
            bool asArray
        ) : m_materialType(materialType),
            m_asArray(asArray)
        {
            setMaterials({ material });
        }

        MaterialInstance::MaterialInstance
        (
            const MaterialType materialType,
            const std::vector<std::shared_ptr<Material>>& materials
        ) : m_materialType(materialType),
            m_asArray(true)
        {
            setMaterials(materials);
        }

        void MaterialInstance::setMaterials(const std::vector<std::shared_ptr<Material>>& materials)
        {
            m_materials = materials; 
            initialize();
        }

        void MaterialInstance::initialize()
        {
            std::vector<std::shared_ptr<Texture::Bitmap>> albedoBitmaps;
            std::vector<std::shared_ptr<Texture::Bitmap>> normalBitmaps;
            std::vector<std::shared_ptr<Texture::Bitmap>> displacementBitmaps;
            std::vector<std::shared_ptr<Texture::Bitmap>> ambientOcclusionBitmaps;
            std::vector<std::shared_ptr<Texture::Bitmap>> specularBitmaps;
            std::vector<std::shared_ptr<Texture::Bitmap>> roughnessBitmaps;

            for (auto& material : m_materials)
            {
                if (material->getAlbedoBitmap() != nullptr)
                {
                    albedoBitmaps.push_back(material->getAlbedoBitmap());
                }
                if (material->getNormalBitmap() != nullptr)
                {
                    normalBitmaps.push_back(material->getNormalBitmap());
                }
                if (material->getHeightBitmap() != nullptr)
                {
                    displacementBitmaps.push_back(material->getHeightBitmap());
                }
                if (material->getAmbientOcclusionBitmap() != nullptr)
                {
                    ambientOcclusionBitmaps.push_back(material->getAmbientOcclusionBitmap());
                }
                if (material->getSpecularBitmap() != nullptr)
                {
                    specularBitmaps.push_back(material->getSpecularBitmap());
                }
                if (material->getRoughnessBitmap() != nullptr)
                {
                    roughnessBitmaps.push_back(material->getRoughnessBitmap());
                }
            }

            GLuint expectedNumBitmaps = 0;
            auto intializeTexture = [&]
            (
                const std::vector<std::shared_ptr<Texture::Bitmap>>& bitmaps,
                std::shared_ptr<Texture::Texture>& texture,
                GLint textureWrapS,
                GLint textureWrapT,
                GLint textureMagFilter,
                GLint textureMinFilter
            )
            {
                // Check validation - size of bitmaps should be exactly the same...
                if (bitmaps.size() != 0)
                {
                    if (expectedNumBitmaps != 0)
                    {
                        if (expectedNumBitmaps != bitmaps.size())
                        {
                            WARNING_LOG("Number of bitmaps are not equal, the same types of bitmaps have to be defined for every mesh nodes!");
                            return false;
                        }
                    }
                    else
                    {
                        expectedNumBitmaps = bitmaps.size();
                    }
                }

                // Initialize VRAM
                if (bitmaps.size() != 0)
                {
                    texture = std::make_shared<Texture::Texture>();

                    bool initializationStatus = false;
                    if (m_asArray)
                    {
                        initializationStatus = texture->initialize
                        (
                            bitmaps,
                            textureWrapS,
                            textureWrapT,
                            textureMagFilter,
                            textureMinFilter
                        );
                    }
                    else
                    {
                        initializationStatus = texture->initialize
                        (
                            *bitmaps[0],
                            textureWrapS,
                            textureWrapT,
                            textureMagFilter,
                            textureMinFilter
                        );
                    }
                    if (!initializationStatus)
                    {
                        CRITICAL_LOG("Cannot initialize map");
                    }
                    return initializationStatus;
                }
                else
                {
                    texture = nullptr;
                }
                return true;
            };

            if (!intializeTexture
            (
                albedoBitmaps,
                m_albedoMap,
                GL_REPEAT,
                GL_REPEAT,
                GL_LINEAR,
                GL_LINEAR_MIPMAP_LINEAR
            ))
            {
                release();
            }
            
            if (!intializeTexture
            (
                normalBitmaps,
                m_normalMap,
                GL_REPEAT,
                GL_REPEAT,
                GL_LINEAR,
                GL_LINEAR_MIPMAP_LINEAR
            ))
            {
                release();
            }

            if (!intializeTexture
            (
                displacementBitmaps,
                m_displacementMap,
                GL_REPEAT,
                GL_REPEAT,
                GL_LINEAR,
                GL_LINEAR_MIPMAP_LINEAR
            ))
            {
                release();
            }

            if (!intializeTexture
            (
                ambientOcclusionBitmaps,
                m_ambientOcclusionMap,
                GL_REPEAT,
                GL_REPEAT,
                GL_LINEAR,
                GL_LINEAR_MIPMAP_LINEAR
            ))
            {
                release();
            }

            if (!intializeTexture
            (
                specularBitmaps,
                m_specularMap,
                GL_REPEAT,
                GL_REPEAT,
                GL_LINEAR,
                GL_LINEAR_MIPMAP_LINEAR
            ))
            {
                release();
            }

            if (!intializeTexture
            (
                roughnessBitmaps,
                m_roughnessMap,
                GL_REPEAT,
                GL_REPEAT,
                GL_LINEAR,
                GL_LINEAR_MIPMAP_LINEAR
            ))
            {
                release();
            }
        }

        void MaterialInstance::release()
        {
            m_materials.clear();

            m_albedoMap = nullptr;
            m_normalMap = nullptr;
            m_ambientOcclusionMap = nullptr;
            m_specularMap = nullptr;
            m_roughnessMap = nullptr;
        }
    }
}
