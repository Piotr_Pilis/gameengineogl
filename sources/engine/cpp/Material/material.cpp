#include "../Log/warningLog.hpp"
#include "../Log/log.hpp"

#include "material.hpp"

namespace Engine
{
    namespace Material
    {
        Material::Material(const MaterialInfo& materialInfo)
        {
            loadBitmap(materialInfo.m_albedoMapSrc, m_albedoBitmap);
            loadBitmap(materialInfo.m_normalMapSrc, m_normalBitmap);
            loadBitmap(materialInfo.m_displacementMapSrc, m_displacementBitmap);
            loadBitmap(materialInfo.m_ambientOcclusionMapSrc, m_ambientOcclusionBitmap);
            loadBitmap(materialInfo.m_specularMapSrc, m_specularBitmap);
            loadBitmap(materialInfo.m_roughnessMapSrc, m_roughnessBitmap);
        }

        Material::~Material()
        {
        }

        void Material::loadBitmap(const std::string& bitmapSrc, std::shared_ptr<Texture::Bitmap>& bitmap)
        {
            if (bitmapSrc.empty())
                return;
            bitmap = std::make_shared<Texture::Bitmap>();
            if (!bitmap->loadFromFile(bitmapSrc))
            {
                WARNING_LOG("Cannot open bitmap file '" + bitmapSrc + "'");
                bitmap = nullptr;
            }
        }

        TODO(Currently this impementation has been moved to creator - refactor creators...)
        /*bool Material::loadMaterial(const std::string& path, std::vector<std::unique_ptr<Material>>& materials)
        {
            std::string filePath = path + ".mat";
            const std::string texturePath = "Textures/";
            std::ifstream fileInput(filePath, std::ios::in);
            if (!fileInput)
                CRITICAL_LOG("Cannot open material file '" + path + "'");

            GLchar str[128];
            fileInput.getline(str, 14);
            if (!Utils::FileUtils::CheckStructure(std::string(str), "MATERIAL INFO", "Material", "WRONG_STRUCTURE_OF_MATERIAL_FILE: '" + filePath + "'"))
            {
                fileInput.close();
                return false;
            }

            fileInput >> str;
            if (!Utils::FileUtils::CheckStructure(std::string(str), "NumMaterials:", "Material", "WRONG_STRUCTURE_OF_MATERIAL_FILE: '" + filePath + "'"))
            {
                fileInput.close();
                return false;
            }

            GLuint numMaterials = 0;
            fileInput >> numMaterials;
            if (!fileInput || numMaterials < 0 || numMaterials > 256)
            {
                CRITICAL_LOG("Structure of material file '" + path + "' is wrong");
                fileInput.close();
                return false;
            }

            if (numMaterials > 0)
            {
                std::stringstream stream;
                std::vector<std::string> diffuseMaps;
                std::vector<std::string> normalMaps;
                std::vector<std::string> opacityMaps;
                GLuint materialID = 0;

                fileInput >> str;
                stream << "MATERIAL" << materialID;

                if (!Utils::FileUtils::CheckStructure(std::string(str), stream.str(), "Material", "WRONG_STRUCTURE_OF_MATERIAL_FILE: '" + filePath + "'"))
                {
                    fileInput.close();
                    return false;
                }

                stream.str("");
                ++materialID;
                stream << "MATERIAL" << materialID;

                auto LoadUrls = [&fileInput, &str, filePath](std::vector<std::string>& textureUrls)
                {
                    fileInput >> str;
                    if (std::string(str) != "NumTextures:")
                    {
                        CRITICAL_LOG("Structure of material file '" + filePath + "' is wrong");
                        fileInput.close();
                        return false;
                    }
                    GLuint numTextures = 0;
                    fileInput >> numTextures;
                    if (!fileInput || numTextures < 0)
                    {
                        CRITICAL_LOG("Structure of material file '" + filePath + "' is wrong");
                        fileInput.close();
                        return false;
                    }

                    for (GLuint textureId = 0; textureId < numTextures; ++textureId)
                    {
                        if (textureUrls.size() > Config::ModelConfig::Instance().MAX_TEXTURES_PER_OBJECT3D)
                            break;
                        fileInput >> str;
                        textureUrls.push_back(str);
                    }
                    return true;
                };

                while (!fileInput.eof())
                {
                    fileInput >> str;
                    if (std::string(str) == "DIFFUSE_MAPs")
                    {
                        LoadUrls(diffuseMaps);
                    }
                    else if (std::string(str) == "NORMAL_MAPs")
                    {
                        LoadUrls(normalMaps);
                    }
                    else if (std::string(str) == "OPACITY_MAPs")
                    {
                        LoadUrls(opacityMaps);
                    }
                    else if (std::string(str) == stream.str())
                    {
                        materials.push_back(std::make_unique<Material>(diffuseMaps.size() > 0 ? texturePath + diffuseMaps[0] : "",
                            normalMaps.size() > 0 ? texturePath + normalMaps[0] : "",
                            opacityMaps.size() > 0 ? texturePath + opacityMaps[0] : ""));

                        diffuseMaps.clear();
                        normalMaps.clear();
                        opacityMaps.clear();

                        if (!fileInput.eof())
                        {
                            if (!Utils::FileUtils::CheckStructure(std::string(str), stream.str(), "ObjectManager", "WRONG_STRUCTURE_OF_MATERIAL_FILE: '" + filePath + "'"))
                            {
                                fileInput.close();
                                return false;
                            }

                            stream.str("");
                            ++materialID;
                            stream << "MATERIAL" << materialID;
                        }
                    }
                    else
                    {
                        std::vector<std::string> vec;
                        if (!LoadUrls(vec))
                            return false;
                    }
                    if (fileInput.eof())
                    {
                        materials.push_back(std::make_unique<Material>(diffuseMaps.size() > 0 ? texturePath + diffuseMaps[0] : "",
                            normalMaps.size() > 0 ? texturePath + normalMaps[0] : "",
                            opacityMaps.size() > 0 ? texturePath + opacityMaps[0] : ""));
                    }
                }
            }

            LOG("Material '" + filePath + "' has been loaded");

            fileInput.close();
            return true;
        }*/
    }
}
