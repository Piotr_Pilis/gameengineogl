#pragma once
#include <string>
#include <memory>
#include "Material/materialInfo.hpp"
#include "Material/material.hpp"

namespace Engine
{
    namespace Geometry
    {
        class ObjectManager;
    }

    namespace Material::Creator
    {
        class MaterialCreator
        {
            // Only ObjectManager can load and manipulate this data 
            friend class Geometry::ObjectManager;

        private:
            static bool loadMaterialData
            (
                const std::string& materialInfoSrc,
                std::vector<Engine::Material::MaterialInfo>& materialInfo
            );

            static bool create
            (
                const std::vector<Engine::Material::MaterialInfo>& materialInfos,
                std::vector<std::shared_ptr<Engine::Material::Material>>& materials
            );

        };
    }
}
