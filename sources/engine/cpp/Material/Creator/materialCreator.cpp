#include <sstream>
#include <codecvt>

#include "FileSystem/ifstream.hpp"
#include "Log/warningLog.hpp"
#include "Data/json.h"

#include "materialCreator.hpp"

namespace Engine::Material::Creator
{
    bool MaterialCreator::loadMaterialData
    (
        const std::string& materialInfoSrc,
        std::vector<Engine::Material::MaterialInfo>& materialInfos
    )
    {
        const auto invalidConfigurationFileStructureErrorMessage = "Wrong structure of material file '" + materialInfoSrc + "'";

        try
        {
            FileSystem::Ifstream fin(materialInfoSrc);
            std::stringstream buffer;
            buffer << fin.rdbuf();

            using convert_typeX = std::codecvt_utf8<wchar_t>;
            std::wstring_convert<convert_typeX, wchar_t> converterX;

            auto jsonDocument = std::shared_ptr<JSONValue>(JSON::parse(buffer.str().c_str()));
            if (!jsonDocument->isArray())
            {
                WARNING_LOG(invalidConfigurationFileStructureErrorMessage);
                return false;
            }
            auto jsonRootArray = jsonDocument->asArray();

            for (auto& jsonMaterialInfoValue : jsonRootArray)
            {
                if (!jsonMaterialInfoValue->isObject())
                {
                    WARNING_LOG(invalidConfigurationFileStructureErrorMessage);
                    return false;
                }
                auto jsonMaterialInfoObject = jsonMaterialInfoValue->asObject();

                // Header
                if (!jsonMaterialInfoValue->hasChild(L"configName") ||
                    !jsonMaterialInfoObject[L"configName"]->isString() ||
                    jsonMaterialInfoObject[L"configName"]->asString() != L"Material")
                {
                    WARNING_LOG(invalidConfigurationFileStructureErrorMessage);
                    return false;
                }

                // Load map src function
                auto loadMapSrc = [&](const std::wstring& propertyName, std::string& mapSrc)
                {
                    if (jsonMaterialInfoValue->hasChild(propertyName.data()) ||
                        jsonMaterialInfoObject[propertyName.data()]->isString())
                    {
                        mapSrc = converterX.to_bytes(jsonMaterialInfoObject[propertyName.data()]->asString());
                    }
                };

                // Loads information about map sources
                Engine::Material::MaterialInfo materialInfo;
                loadMapSrc(L"albedoMapSrc", materialInfo.m_albedoMapSrc);
                loadMapSrc(L"normalMapSrc", materialInfo.m_normalMapSrc);
                loadMapSrc(L"displacementMapSrc", materialInfo.m_displacementMapSrc);
                loadMapSrc(L"ambientOcclusionMapSrc", materialInfo.m_ambientOcclusionMapSrc);
                loadMapSrc(L"specularMapSrc", materialInfo.m_specularMapSrc);
                loadMapSrc(L"roughnessMapSrc", materialInfo.m_roughnessMapSrc);

                // Push result
                materialInfos.emplace_back(std::move(materialInfo));
            }
        }
        catch (std::invalid_argument invalidArgument)
        {
            WARNING_LOG("Could not open '" + materialInfoSrc + "'");
            return false;
        }
        return true;
    }

    bool MaterialCreator::create
    (
        const std::vector<Engine::Material::MaterialInfo>& materialInfos,
        std::vector<std::shared_ptr<Engine::Material::Material>>& materials
    )
    {
        for (auto& materialInfo : materialInfos)
        {
            materials.emplace_back
            (
                std::move
                (
                    std::make_shared<Engine::Material::Material>(materialInfo)
                )
            );
        }
        return true;
    }
}