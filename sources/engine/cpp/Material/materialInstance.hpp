#pragma once
#include <glad/glad.h>
#include <memory>
#include <vector>

#include "Texture/texture.hpp"
#include "material.hpp"

namespace Engine
{
    namespace Material
    {
        /*
        Parent: *
        Inherits: -
        Description: Descriptes material.
        Contains: Information about material like material type, position of each textures in array etc.
        */
        class MaterialInstance
        {
        public:
            enum class MaterialType
            {
                FLAT_MATERIAL/*,
                PARALLAX_MATERIAL,
                WATER_MATERIAL*/
            };

        public:
            MaterialInstance();
            MaterialInstance
            (
                const MaterialType materialType,
                const std::shared_ptr<Material>& material,
                bool asArray = false
            );
            MaterialInstance
            (
                const MaterialType materialInfo,
                const std::vector<std::shared_ptr<Material>>& materials
            );

            const MaterialType& getMaterialType() const { return m_materialType; }
            const Material& getMaterial(const GLuint& index = 0) const { return *m_materials[index]; }
            const GLuint getMaterialCount() const { return static_cast<GLuint>(m_materials.size()); }

            bool isArray() const { return m_asArray; }

            // Warning! Pointer can be nullptr if map is not set.
            const std::shared_ptr<Texture::Texture>& getAlbedoMap() const { return m_albedoMap; }
            const std::shared_ptr<Texture::Texture>& getNormalMap() const { return m_normalMap; }
            const std::shared_ptr<Texture::Texture>& getDisplacementMap() const { return m_displacementMap; }
            const std::shared_ptr<Texture::Texture>& getAmbientOcclusionMap() const { return m_ambientOcclusionMap; }
            const std::shared_ptr<Texture::Texture>& getSpecularMap() const { return m_specularMap; }
            const std::shared_ptr<Texture::Texture>& getRoughnessMap() const { return m_roughnessMap; }

        private:
            void setMaterials(const std::vector<std::shared_ptr<Material>>& materials);

            void initialize();
            void release();

        private:
            MaterialType m_materialType;
            std::vector<std::shared_ptr<Material>> m_materials;

            // MaterialInstance doesn't have to be an array if number is equal 1
            bool m_asArray;

            std::shared_ptr<Texture::Texture> m_albedoMap;
            std::shared_ptr<Texture::Texture> m_normalMap;
            std::shared_ptr<Texture::Texture> m_displacementMap;
            std::shared_ptr<Texture::Texture> m_ambientOcclusionMap;
            std::shared_ptr<Texture::Texture> m_specularMap;
            std::shared_ptr<Texture::Texture> m_roughnessMap;
        };
    }
}
