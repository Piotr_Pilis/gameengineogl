#pragma once
#include <string>

namespace Engine::Material
{
    struct MaterialInfo
    {
        std::string m_albedoMapSrc = "";
        std::string m_normalMapSrc = "";
        std::string m_displacementMapSrc = "";
        std::string m_ambientOcclusionMapSrc = "";
        std::string m_specularMapSrc = "";
        std::string m_roughnessMapSrc = "";
    };
}
