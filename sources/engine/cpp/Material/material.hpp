#pragma once
#include <memory>
#include <vector>
#include <string>

#include "Texture/bitmap.hpp"
#include "materialInfo.hpp"

namespace Engine
{
    namespace Material
    {
        /*
        Parent: *
        Inherits: -
        Description: Container of bitmaps that descriptes material
        Contains: Material bitmaps like diffuseMap, specularMap, normalMap
        */
        class Material
        {
        public:
            Material() = delete;
            Material(const MaterialInfo& materialInfo);
            ~Material();

            // path: <path>.mat
            /*static bool LoadMaterial(const std::string& path,
                std::vector<std::unique_ptr<Material>>& materials);*/

            // Warning! Pointer can be nullptr if bitmap is not set.
            const std::shared_ptr<Texture::Bitmap>& getAlbedoBitmap() const { return m_albedoBitmap; }
            const std::shared_ptr<Texture::Bitmap>& getNormalBitmap() const { return m_normalBitmap; }
            const std::shared_ptr<Texture::Bitmap>& getHeightBitmap() const { return m_displacementBitmap; }
            const std::shared_ptr<Texture::Bitmap>& getAmbientOcclusionBitmap() const { return m_ambientOcclusionBitmap; }
            const std::shared_ptr<Texture::Bitmap>& getSpecularBitmap() const { return m_specularBitmap; }
            const std::shared_ptr<Texture::Bitmap>& getRoughnessBitmap() const { return m_roughnessBitmap; }

        private:
            void loadBitmap
            (
                const std::string& bitmapSrc,
                std::shared_ptr<Texture::Bitmap>& bitmap
            );

        private:
            // If u want to change number of maps remember to change definition of materialInstance.cpp:SortMaterials!
            std::shared_ptr<Texture::Bitmap> m_albedoBitmap;
            std::shared_ptr<Texture::Bitmap> m_normalBitmap;
            std::shared_ptr<Texture::Bitmap> m_displacementBitmap;
            std::shared_ptr<Texture::Bitmap> m_ambientOcclusionBitmap;
            std::shared_ptr<Texture::Bitmap> m_specularBitmap;
            std::shared_ptr<Texture::Bitmap> m_roughnessBitmap;

        };
    }
}
