#include <assimp/Importer.hpp>
#include <assimp/postprocess.h>
#include <assimp/scene.h>

#include "Animation/Creator/skeletonCreator.hpp"
#include "Animation/skeleton.hpp"

#include "FileSystem/ifstream.hpp"
#include "Log/warningLog.hpp"
#include "Mesh.hpp"

namespace Engine
{
    namespace Geometry
    {
        Mesh::Mesh()
        {
        }

        Mesh::~Mesh()
        {
        }

        bool Mesh::loadBones
        (
            Mesh& mesh,
            const std::shared_ptr<Animation::Skeleton>& skeleton,
            const aiMesh& aiMesh
        )
        {
            auto numBonesData = aiMesh.mNumBones;

            for (GLuint boneDataId = 0; boneDataId < numBonesData; ++boneDataId)
            {
                auto aiBone = aiMesh.mBones[boneDataId];

                std::string boneName(aiBone->mName.data);
                auto boneNode = skeleton->getNodeByName(boneName);
                if (boneNode == nullptr)
                {
                    WARNING_LOG("Skeleton doesn't contain bone with name '" + boneName + "'");
                    continue;
                }

                // Extracts offset matrix of bone
                glm::mat4x4 offsetMatrix;
                offsetMatrix[0][0] = aiBone->mOffsetMatrix.a1;
                offsetMatrix[0][1] = aiBone->mOffsetMatrix.b1;
                offsetMatrix[0][2] = aiBone->mOffsetMatrix.c1;
                offsetMatrix[0][3] = aiBone->mOffsetMatrix.d1;

                offsetMatrix[1][0] = aiBone->mOffsetMatrix.a2;
                offsetMatrix[1][1] = aiBone->mOffsetMatrix.b2;
                offsetMatrix[1][2] = aiBone->mOffsetMatrix.c2;
                offsetMatrix[1][3] = aiBone->mOffsetMatrix.d2;

                offsetMatrix[2][0] = aiBone->mOffsetMatrix.a3;
                offsetMatrix[2][1] = aiBone->mOffsetMatrix.b3;
                offsetMatrix[2][2] = aiBone->mOffsetMatrix.c3;
                offsetMatrix[2][3] = aiBone->mOffsetMatrix.d3;

                offsetMatrix[3][0] = aiBone->mOffsetMatrix.a4;
                offsetMatrix[3][1] = aiBone->mOffsetMatrix.b4;
                offsetMatrix[3][2] = aiBone->mOffsetMatrix.c4;
                offsetMatrix[3][3] = aiBone->mOffsetMatrix.d4;

                // Creates or checks validation of bone data from node
                if (!skeleton->createBoneFromNode(*boneNode, offsetMatrix))
                {
                    WARNING_LOG("Cannot create bone '" + boneName + "' from node");
                    return false;
                }

                // Gets bone id
                GLuint boneId;
                if (!skeleton->getBoneId(*boneNode, boneId))
                {
                    WARNING_LOG("Cannot get identifier of bone '" + boneName + "'");
                    return false;
                }

                for (GLuint weightId = 0; weightId < aiBone->mNumWeights; ++weightId)
                {
                    auto aiVertexBoneWeight = aiBone->mWeights[weightId];
                    auto vertexId = aiVertexBoneWeight.mVertexId;
                    if (mesh.m_vertices.size() <= aiVertexBoneWeight.mVertexId)
                    {
                        WARNING_LOG("The bone '" + boneName + "' indicates the wrong vertex");
                        return false;
                    }
                    mesh.m_vertices[vertexId].m_boneWeights.addBoneData
                    (
                        boneId,
                        aiVertexBoneWeight.mWeight
                    );
                }
            }
            return true;
        }
    }
}
