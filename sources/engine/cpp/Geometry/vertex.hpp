#pragma once
#include <glad/glad.h>
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>

namespace Engine
{
    namespace Geometry
    {
        class Vertex
        {
        public:
            static constexpr const GLuint NUM_BONES_PER_VERTEX = 3;

            struct BoneData
            {
                BoneData();
                ~BoneData() = default;
                bool addBoneData(const GLuint& boneId, const GLfloat& weight);

                GLuint boneIds[NUM_BONES_PER_VERTEX];
                GLfloat weights[NUM_BONES_PER_VERTEX];
            };

        public:
            glm::vec3 m_position;
            glm::vec2 m_texCoord;
            glm::vec3 m_normal;
            BoneData m_boneWeights;

        };
    }
}