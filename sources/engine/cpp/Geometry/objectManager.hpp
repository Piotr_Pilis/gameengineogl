#pragma once
#include <glad/glad.h>
#include <glm/vec3.hpp>
#include <vector>
#include <string>
#include <memory>
#include <map>

#include "Importer/Object/ObjectsImporterAbstract.hpp"
#include "object3d.hpp"

namespace Engine
{
    namespace Map
    {
        class Terrain;
    }

    namespace Material
    {
        class MaterialInstance;
    }

    namespace Animation
    {
        class Animation;
        class Skeleton;
    }

    namespace Geometry
    {
        class Mesh;
        class MeshInstance;
        class Model;
        class Object3D;
        class Object3DInstanced;

        /*
        Manages all related with Object3D. It can load mesh, initialize model and Object3D.
        Contains Vector of Meshes, Models, Materials, Object3Ds*
        */
        class ObjectManager
        {
        public:
            typedef std::shared_ptr<Map::Terrain> TerrainPtr;

            typedef std::shared_ptr<MeshInstance> MeshInstancePtr;
            typedef std::shared_ptr<Model> ModelPtr;

            typedef std::shared_ptr<Material::MaterialInstance> MaterialInstancePtr;

            typedef std::shared_ptr<Animation::Skeleton> SkeletonPtr;
            typedef std::shared_ptr<Animation::Animation> AnimationPtr;

        public:
            // For default constructor uou need to call Initialize method
            ObjectManager() = default;
            ~ObjectManager();

            // Initializes programs of objects etc.
            // Should be called only once
            static void initialize();

            // Loads objects data from file - initializing or reinitializing
            bool loadStaticObjectsFromMap(const TerrainPtr& terrain);

            // Gets or loads model by name (not full path!)
            // src: Models/<name>.ini
            const ModelPtr& getModel(const std::string& name);

            // Gets or loads material instance by src
            const std::vector<MaterialInstancePtr>& getMaterialInstance(const std::string& src);

            // Loads animations from file (if not loaded)
            const std::vector<AnimationPtr>& getOrCreateAnimations
            (
                const std::string& animationsFileName
            );

        private:
            // Creates new entity from content of data file
            bool pushEntity
            (
                const std::string& modelFileName,
                const Importer::Object::ObjectsImporterAbstract::ObjectTransformationInfo& transformationInfo
            );

            /*// Gets or loads material by name (not full path!)
            // src: Materials/<name>.ini
            std::vector<std::unique_ptr<Material::Material>>*
                getMaterial(const std::string& src);*/

            // Gets or loads mesh instance
            // e.g. src: Meshes/<fileName>.<extension>
            const MeshInstancePtr& getMeshInstance(const std::string& src);

            bool loadMeshInstance(const std::string& src);
            bool loadModel(const std::string& name);
            bool loadMaterialInstance(const std::string& src);

            void removeMesh(const std::string& src);
            void removeModel(const std::string& name);
            void removeMaterialInstance(const std::string& src);

        private:
            std::map<std::string, MeshInstancePtr> m_meshInstances;
            std::map<std::string, ModelPtr> m_models;
            std::map<std::string, std::vector<MaterialInstancePtr>> m_materialInstances;
            std::map<std::string, SkeletonPtr> m_skeletons;
            std::map<std::string, std::vector<AnimationPtr>> m_animations;

        };
    }
}
