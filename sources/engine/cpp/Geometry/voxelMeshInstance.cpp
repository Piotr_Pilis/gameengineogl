#include <vector>
#include "mesh.hpp"
#include "voxelMeshInstance.hpp"

namespace Engine::Geometry
{
    VoxelMeshInstance::VoxelMeshInstance()
    {
        initialize();
    }

    VoxelMeshInstance::~VoxelMeshInstance()
    {
    }

    VoxelMeshInstance& VoxelMeshInstance::getInstance()
    {
        static VoxelMeshInstance instance;
        return instance;
    }

    void VoxelMeshInstance::initialize()
    {
        std::vector<Mesh> meshes(1);
        Mesh& mesh = meshes[0];
        mesh.setVertices
        (
            {
                Vertex{ glm::vec3(0.0, 1.0, 0.0), glm::vec3(1.0, 1.0, 1.0), glm::vec3(1.0, 1.0, 1.0) },
                Vertex{ glm::vec3(0.0, 1.0, 1.0), glm::vec3(1.0, 1.0, 1.0), glm::vec3(1.0, 1.0, 1.0) },
                Vertex{ glm::vec3(1.0, 1.0, 0.0), glm::vec3(1.0, 1.0, 1.0), glm::vec3(1.0, 1.0, 1.0) },
                Vertex{ glm::vec3(1.0, 1.0, 1.0), glm::vec3(1.0, 1.0, 1.0), glm::vec3(1.0, 1.0, 1.0) },

                Vertex{ glm::vec3(0.0, 0.0, 0.0), glm::vec3(1.0, 1.0, 1.0), glm::vec3(1.0, 1.0, 1.0) },
                Vertex{ glm::vec3(0.0, 0.0, 1.0), glm::vec3(1.0, 1.0, 1.0), glm::vec3(1.0, 1.0, 1.0) },
                Vertex{ glm::vec3(1.0, 0.0, 0.0), glm::vec3(1.0, 1.0, 1.0), glm::vec3(1.0, 1.0, 1.0) },
                Vertex{ glm::vec3(1.0, 0.0, 1.0), glm::vec3(1.0, 1.0, 1.0), glm::vec3(1.0, 1.0, 1.0) }
            }
        );
        mesh.setIndices
        (
            {
                // Top
                0, 1, 2,
                2, 1, 3,

                //Left
                4, 0, 1,
                5, 4, 1,

                // Front
                4, 0, 2,
                6, 4, 2,

                //Right
                6, 2, 3,
                7, 6, 3,

                // Back
                7, 5, 3,
                5, 1, 3,

                // Bottom
                6, 4, 5,
                5, 7, 6
            }
        );

        setupGeometry(meshes);
    }
}