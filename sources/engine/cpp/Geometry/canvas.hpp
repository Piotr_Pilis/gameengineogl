#pragma once
#include <glad/glad.h>
#include <magic_enum.hpp>

#include <string>
#include <memory>
#include <array>
#include <map>

#include "Pipeline/shaderVariant.hpp"

namespace Engine::Geometry
{
    class Canvas
    {
    private:
        enum class MapType
        {
            SHADING_TYPE_MAP,
            COLOR_SHADOW_MAP,
            NORMAL_MAP,
            POSITION_MAP,
            AO_SPECULAR_ROUGHNESS_HEIGHT_MAP
        };

        enum class ShadingType
        {
            NONE = 0,
            FLAT,
            PBR
        };

    public:
        Canvas();
        ~Canvas();

        /* Binds frame buffer for rendering the objects to GBuffers */
        void bindForWriting();

        /* Binds frame buffer for reading the objects from GBuffers */
        void bindForReading();

        /* Copies depth buffer from canvas buffer to target buffer */
        void blitDepthBuffer(const GLuint targetFbo);

        /* Initializes frame buffer with maps
        ** It can be used many times to reinitialize */
        void initialize
        (
            const GLsizei windowWidth,
            const GLsizei windowHeight
        );

        /* Sets texture uniforms */
        static void initializeProgramAsReader(const GLuint& program);

        /* Shader values e.g. map identifiers, shading type values */
        static const Pipeline::ShaderVariant& getShaderData();

    private:
        bool clear();

    private:
        GLuint m_frameBuffer;
        std::array<GLuint, magic_enum::enum_count<MapType>()> m_maps;
        GLuint m_depthMap;

        GLsizei m_windowWidth;
        GLsizei m_windowHeight;

    };
}