#pragma once
#include <glm/vec3.hpp>
#include <vector>
#include "../Pipeline/program.hpp"
#include "object3d.hpp"
#include "../macros.hpp"

namespace Engine
{
    namespace Geometry
    {
        /*
        Descriptes instanced model 3d in world. It can move, rotate and scale object.
        */
        /*class Object3DInstanced :
            public Object3D
        {
        public:
            Object3DInstanced() = delete;
            Object3DInstanced(Model& model, const std::vector<glm::vec3>& positions);
            ~Object3DInstanced();

            static Pipeline::Program& getProgram();

            static void beforeRendering();
            virtual void render();
            virtual void renderForShadow(const GLuint& worldMatrixLocation);

            TODO(Dynamic maximum number of instances);
            const GLuint& getMaxCount() const { return m_maxCount; }

        private:
            void initializePositionArray(const std::vector<glm::vec3>& positions);

        private:
            GLuint m_positionBO;
            GLuint m_maxCount;

        };*/
    }
}
