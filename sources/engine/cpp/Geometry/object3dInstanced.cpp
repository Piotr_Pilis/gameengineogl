#include "../Pipeline/transform.hpp"
#include "../Log/log.hpp"
#include "meshInstance.hpp"
#include "model.hpp"
#include "object3dInstanced.hpp"

/*namespace Engine
{
    namespace Geometry
    {
        Object3DInstanced::Object3DInstanced(Model& model, 
            const std::vector<glm::vec3>& positions)
        {
            /*Object3D::SetModel(model);
            m_maxCount = static_cast<GLuint>(positions.size());

            m_model->GetMeshInstance()->BindVAO();

            glGenBuffers(1, &m_positionBO);
            glBindBuffer(GL_ARRAY_BUFFER, m_positionBO);
            glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3) * m_maxCount, NULL, GL_DYNAMIC_DRAW);

            // Last buffer object index
            auto positionBoIndex = MeshInstance::BufferObjectType::INDICES_BO;
            glEnableVertexAttribArray(positionBoIndex);
            glVertexAttribPointer(positionBoIndex, 3, GL_FLOAT,
                GL_FALSE, sizeof(glm::vec3), 0
            );
            glVertexAttribDivisor(positionBoIndex, 1);
            InitializePositionArray(positions);*
        }

        Object3DInstanced::~Object3DInstanced()
        {
            if (glIsBuffer(m_positionBO))
                glDeleteBuffers(1, &m_positionBO);
        }

        Pipeline::Program& Object3DInstanced::GetProgram()
        {
            static Pipeline::Program program = Pipeline::Program(
                Pipeline::Shader("Data/Shaders/object3dInstanced.vs", 
                    Pipeline::Shader::ShaderType::VertexShader),
                Pipeline::Shader("Data/Shaders/object3d.fs", 
                    Pipeline::Shader::ShaderType::FragmentShader));
            return program;
        }

        void Object3DInstanced::BeforeRendering()
        {
            glUseProgram(*GetProgram());
        }

        void Object3DInstanced::Render()
        {
            m_modelMatrix->BindMatrix();
            //m_model.RenderInstanced(m_maxCount);
        }

        void Object3DInstanced::RenderForShadow(const GLuint& worldMatrixLocation)
        {
            m_modelMatrix->BindMatrix(worldMatrixLocation);
            //m_model.RenderInstancedForShadow(m_maxCount);
        }

        void Object3DInstanced::InitializePositionArray(
            const std::vector<glm::vec3>& positions)
        {
            // Updates positionBO
            glBindBuffer(GL_ARRAY_BUFFER, m_positionBO);

            glm::vec3* address = (glm::vec3*)glMapBufferRange(
                GL_ARRAY_BUFFER, 0, sizeof(glm::vec3) * m_maxCount,
                GL_MAP_WRITE_BIT | GL_MAP_INVALIDATE_BUFFER_BIT | GL_MAP_UNSYNCHRONIZED_BIT);

            GLuint currentIndex = 0;
            for (auto& position : positions)
            {
                address[currentIndex] = position;
                ++currentIndex;
            }
            glUnmapBuffer(GL_ARRAY_BUFFER);

        }
    }
}*/