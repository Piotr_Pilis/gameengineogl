#pragma once
#include <boost/signals2/signal.hpp>
#include <glm/vec3.hpp>
#include <memory>
#include <vector>

#include "Entity/renderable.hpp"
#include "Pipeline/program.hpp"
#include "Event/connection.hpp"

namespace Engine::Pipeline
{
    class Transform;
}

namespace Engine::Component
{
    class AbstractObject3DComponent;
}

namespace Engine::Geometry
{
    class Model;

    /*
    Virtual class which descriptes static model 3D in world.
                 It can be moved, rotated and scaled.
    */
    class Object3D :
        public Entity::Renderable
    {
    public:
        typedef std::unique_ptr<Component::AbstractObject3DComponent> Object3DComponentPtr;

        enum class ComponentFlag
        {
            SkeletalAnimation
        };

    public:
        Object3D() = delete;
        Object3D(const std::vector<ComponentFlag>& componentFlags);
        ~Object3D();

        Object3D(const Object3D& other);
        Object3D& operator=(const Object3D& other);

        // Model
        virtual void setModel(const std::shared_ptr<Model>& model);
        const glm::vec3 getBoundBox() const;

        // Transformation
        virtual void setTranslation(const glm::vec3& translation);
        virtual void setRotation(const glm::vec3& radianAngles);
        virtual void setScale(const glm::vec3& scale);
        virtual void lookAt(const glm::vec3& direction);

        virtual void move(const glm::vec3& vector);
        virtual void rotate(const glm::vec3& radianAngles);
        virtual void scale(const glm::vec3& scale);

        const glm::vec3& getTranslation() const;
        const glm::vec3& getRotation() const;
        const glm::vec3& getScale() const;

        // Program
        virtual Pipeline::Program& getProgram() override;

        virtual void update() override;

        // For example we have many Object3Ds with the same mesh and 
        //  we want to optimize the rendering process
        void setMeshBindingDuringRendering(bool enabled);
        virtual void render() override;
        //virtual void RenderForShadow(const GLuint& worldMatrixLocation, bool bindMesh = true);

    protected:
        // Component
        bool hasSkeletalAnimationComponent();
        const Object3DComponentPtr& getSkeletalAnimationComponent() const;

    private:
        void insertSkeletalAnimationComponent();

    private:
        void initializeProgram();

    protected:
        std::shared_ptr<Pipeline::Program> m_program;
        GLint m_modelMatrixUniformLocation;

        std::shared_ptr<Model> m_model;
        std::unique_ptr<Pipeline::Transform> m_modelMatrix;

        std::vector<Object3DComponentPtr> m_components;

        bool m_meshBindingDuringRendering = true;

    public:
        // Signals
        Event::Connection::Signal<> m_modelChanged;

        boost::signals2::signal<void()> positionChanged;
        boost::signals2::signal<void()> rotationChanged;
        boost::signals2::signal<void()> scaleChanged;

    };
}
