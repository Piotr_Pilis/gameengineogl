#pragma once
#include <glad/glad.h>

namespace Engine
{
    namespace Geometry
    {
        namespace Private
        {
            struct MeshNodeDescriptor
            {
                GLuint m_vertexOffset;
                GLuint m_indexOffset;
                GLuint m_numIndices;
            };
        }
    }
}
