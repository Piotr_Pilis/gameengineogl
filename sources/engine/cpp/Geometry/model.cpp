#include <sstream>
#include <codecvt>

#include "Material/materialInstance.hpp"

#include "FileSystem/ifstream.hpp"
#include "Data/json.h"
#include "model.hpp"

#include "Log/criticalLog.hpp"
#include "Log/warningLog.hpp"

#include "meshInstance.hpp"


namespace Engine
{
    namespace Geometry
    {
        Model::Model
        (
            const std::shared_ptr<MeshInstance>& meshInstance,
            const std::vector<std::shared_ptr<Material::MaterialInstance>>& materialInstances
        ) : m_meshInstance(meshInstance),
            m_materialInstances(materialInstances)
        {
            if (m_meshInstance.get() == nullptr)
            {
                CRITICAL_LOG("Internal error", "Mesh instance is nullptr");
            }

            if (m_meshInstance->getNumNodes() > materialInstances.size())
            {
                WARNING_LOG("Not every mesh nodes have defined a materials");
            }
        }

        Model::~Model()
        {
        }

        void Model::bindMaterial(const GLuint materialId)
        {
            if (m_materialInstances.size() <= materialId)
                return;

            auto& materialInstance = m_materialInstances[materialId];
            if (materialInstance.get() == nullptr)
                return;

            if (materialInstance->getAlbedoMap() != nullptr)
            {
                glActiveTexture(GL_TEXTURE0);
                materialInstance->getAlbedoMap()->bind();
            }
            if (materialInstance->getNormalMap() != nullptr)
            {
                glActiveTexture(GL_TEXTURE1);
                materialInstance->getNormalMap()->bind();
            }
            if (materialInstance->getDisplacementMap() != nullptr)
            {
                glActiveTexture(GL_TEXTURE2);
                materialInstance->getDisplacementMap()->bind();
            }
            if (materialInstance->getAmbientOcclusionMap() != nullptr)
            {
                glActiveTexture(GL_TEXTURE3);
                materialInstance->getAmbientOcclusionMap()->bind();
            }
            if (materialInstance->getSpecularMap() != nullptr)
            {
                glActiveTexture(GL_TEXTURE4);
                materialInstance->getSpecularMap()->bind();
            }
            if (materialInstance->getRoughnessMap() != nullptr)
            {
                glActiveTexture(GL_TEXTURE5);
                materialInstance->getRoughnessMap()->bind();
            }
        }

        void Model::bindMesh()
        {
            /*if (!m_meshInstance.get())
            {
                WARNING_LOG("Empty mesh cannot be binded");
                return;
            }*/
            m_meshInstance->bindVAO();
        }

        void Model::render()
        {
            // Render mesh
            for (unsigned meshNodeId = 0; meshNodeId < m_meshInstance->getNumNodes(); ++meshNodeId)
            {
                // Bind material
                bindMaterial(meshNodeId);

                // Render mesh node
                m_meshInstance->render(meshNodeId);
            }
        }

        void Model::renderInstanced(const GLuint& primCount)
        {
            // Mesh node support
            // Bind material
            m_meshInstance->renderInstanced(primCount, 0);
        }
    }
}