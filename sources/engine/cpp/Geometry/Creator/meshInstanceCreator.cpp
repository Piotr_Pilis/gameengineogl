#include "meshInstanceCreator.hpp"

namespace Engine
{
    namespace Geometry
    {
        namespace Creator
        {
            bool MeshInstanceCreator::create
            (
                const std::vector<Mesh>& meshes,
                const std::shared_ptr<Animation::Skeleton>& skeleton,
                std::shared_ptr<MeshInstance>& meshInstance
            )
            {
                if (skeleton.get() == nullptr)
                    return false;
                meshInstance = std::shared_ptr<MeshInstance>(new MeshInstance
                (
                    meshes,
                    skeleton
                ));
                return true;
            }
        }
    }
}