#pragma once
#include <glad/glad.h>
#include <vector>
#include <memory>
#include <string>
#include "Geometry/mesh.hpp"

namespace Engine::Animation
{
    class Skeleton;
}

namespace Engine::Geometry
{
    class ObjectManager;
}

namespace Engine::Geometry::Creator
{
    /*
    Access for specified classes only
    */
    class MeshCreator
    {
        // Only ObjectManager can load and manipulate this data 
        friend class Geometry::ObjectManager;

    private:
        // Full path to mesh file
        // One skeleton for many meshes (one file)
        // Skeleton will always contain at least the root node
        // One node means that there is not bone hierarchy

        // Provide skeleton object if you want to use existed skeleton
        static bool loadMeshes
        (
            const std::string& path,
            std::vector<Mesh>& meshes,
            std::shared_ptr<Animation::Skeleton>& skeleton
        );

    public:
        // Spherical triangle
        static void createSphere
        (
            Mesh& mesh,
            const GLfloat radius,
            const GLuint stackCount,
            const GLuint sectorCount
        );

    };
}
