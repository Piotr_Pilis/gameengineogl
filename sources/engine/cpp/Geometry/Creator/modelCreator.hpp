#pragma once
#include <string>
#include <memory>
#include "Geometry/model.hpp"

namespace Engine
{
    namespace Geometry
    {
        class ObjectManager;
        class MeshInstance;
        namespace Creator
        {
            class ModelCreator
            {
                // Only ObjectManager can load and manipulate this data 
                friend class Geometry::ObjectManager;

            private:
                // Models/<name>.ini
                static bool loadModelData
                (
                    const std::string& name,
                    std::string& meshFilePath,
                    std::string& materialFilePath
                );
            };
        }
    }
}
