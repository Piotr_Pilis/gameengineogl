#include <assimp/Importer.hpp>
#include <assimp/postprocess.h>
#include <assimp/scene.h>

#include <glm/gtc/constants.hpp>
#include <glm/glm.hpp>

#include "Animation/Creator/skeletonCreator.hpp"
#include "FileSystem/assimpIOSystem.hpp"
#include "Log/warningLog.hpp"

#include "meshCreator.hpp"

namespace Engine::Geometry::Creator
{
    bool MeshCreator::loadMeshes
    (
        const std::string& path,
        std::vector<Mesh>& meshes,
        std::shared_ptr<Animation::Skeleton>& skeleton
    )
    {
        Assimp::Importer meshImporter;

        try
        {
            meshImporter.SetIOHandler(new FileSystem::AssimpIOSystem());
            auto aiScene = meshImporter.ReadFile
            (
                path,
                aiProcess_Triangulate |
                aiProcess_GenSmoothNormals |
                aiProcess_FlipUVs |
                aiProcess_JoinIdenticalVertices
            );
            if (!aiScene)
            {
                WARNING_LOG("Mesh file '" + path + "' cannot be parsed, reason: " +
                    meshImporter.GetErrorString());
                return false;
            }

            // Loads skeleton (bone hierarchy)
            if (skeleton.get() == nullptr)
            {
                if (!Animation::Creator::SkeletonCreator::loadSkeleton(aiScene->mRootNode, skeleton))
                {
                    WARNING_LOG("Cannot extract skeleton data from file '" + path + "'");
                    return false;
                }
            }

            // Loads meshes
            meshes.clear();
            if (!aiScene->mNumMeshes)
            {
                WARNING_LOG("File '" + path + "' doesn't contain data about meshes");
                return false;
            }
            meshes.resize(aiScene->mNumMeshes);
            for (GLuint meshId = 0; meshId < aiScene->mNumMeshes; ++meshId)
            {
                auto& mesh = meshes[meshId];
                auto aiMesh = aiScene->mMeshes[meshId];

                if (!aiMesh->mNumVertices)
                    continue;
                if (!aiMesh->mNumFaces)
                    continue;

                mesh.m_vertices.resize(aiMesh->mNumVertices);
                mesh.m_indices.resize(aiMesh->mNumFaces * 3);

                // Vertices
                for (GLuint vertexId = 0; vertexId < aiMesh->mNumVertices; ++vertexId)
                {
                    auto& vertexData = aiMesh->mVertices[vertexId];
                    mesh.m_vertices[vertexId].m_position.x = vertexData.x;
                    mesh.m_vertices[vertexId].m_position.y = vertexData.y;
                    mesh.m_vertices[vertexId].m_position.z = vertexData.z;

                    auto& normalData = aiMesh->mNormals[vertexId];
                    mesh.m_vertices[vertexId].m_normal.x = normalData.x;
                    mesh.m_vertices[vertexId].m_normal.y = normalData.y;
                    mesh.m_vertices[vertexId].m_normal.z = normalData.z;

                    if (aiMesh->HasTextureCoords(0))
                    {
                        auto& texCoordData = aiMesh->mTextureCoords[0][vertexId];
                        mesh.m_vertices[vertexId].m_texCoord.x = texCoordData.x;
                        mesh.m_vertices[vertexId].m_texCoord.y = texCoordData.y;
                    }
                }

                // Indices
                for (GLuint faceId = 0; faceId < aiMesh->mNumFaces; ++faceId)
                {
                    auto& faceData = aiMesh->mFaces[faceId];
                    mesh.m_indices[faceId * 3] = faceData.mIndices[0];
                    mesh.m_indices[faceId * 3 + 1] = faceData.mIndices[1];
                    mesh.m_indices[faceId * 3 + 2] = faceData.mIndices[2];
                }

                // Bone data
                Mesh::loadBones
                (
                    mesh,
                    skeleton,
                    *aiMesh
                );
            }
        }
        catch (std::invalid_argument invalidArgument)
        {
            WARNING_LOG("Could not open '" + path + "'");
            return false;
        }
        return true;
    }

    void MeshCreator::createSphere
    (
        Mesh& mesh,
        const GLfloat radius,
        const GLuint stackCount,
        const GLuint sectorCount
    )
    {
        // Validation
        if (stackCount == 0)
        {
            WARNING_LOG("Invalid stackCount is set (" + std::to_string(stackCount) + ")");
            return;
        }

        if (sectorCount == 0)
        {
            WARNING_LOG("Invalid sectorCount is set (" + std::to_string(sectorCount) + ")");
            return;
        }

        if (radius <= 0)
        {
            WARNING_LOG("Invalid radius is set (" + std::to_string(radius) + ")");
            return;
        }

        // Computes vertices and indices count
        const GLuint numVertices = (sectorCount + 1) * (stackCount + 1);
        const GLuint numIndices = 3 * 2 * sectorCount * (stackCount - 1);

        mesh.m_vertices.resize(numVertices);
        mesh.m_indices.resize(numIndices);

        const GLfloat pi = glm::pi<GLfloat>();
        const GLfloat lengthInv = 1.0f / radius;
        const GLfloat sectorStep = 2 * pi / sectorCount;
        const GLfloat stackStep = pi / stackCount;

        GLuint vertexId = 0;
        for (GLuint stackId = 0; stackId <= stackCount; ++stackId)
        {
            const GLfloat stackAngle = pi / 2.0f - stackId * stackStep; // starting from pi/2 to -pi/2
            const GLfloat xy = radius * glm::cos(stackAngle); // r * cos(u)
            const GLfloat z = radius * glm::sin(stackAngle);  // r * sin(u)

            // add (sectorCount+1) vertices per stack
            // the first and last vertices have same position and normal, but different tex coords
            for (GLuint sectorId = 0; sectorId <= sectorCount; ++sectorId)
            {
                const GLfloat sectorAngle = sectorId * sectorStep; // starting from 0 to 2pi

                // vertex position
                const GLfloat x = xy * glm::cos(sectorAngle); // r * cos(u) * cos(v)
                const GLfloat y = xy * glm::sin(sectorAngle); // r * cos(u) * sin(v)
                mesh.m_vertices[vertexId].m_position.x = x;
                mesh.m_vertices[vertexId].m_position.y = y;
                mesh.m_vertices[vertexId].m_position.z = z;

                // normalized vertex normal
                const GLfloat nx = x * lengthInv;
                const GLfloat ny = y * lengthInv;
                const GLfloat nz = z * lengthInv;
                mesh.m_vertices[vertexId].m_normal.x = nx;
                mesh.m_vertices[vertexId].m_normal.y = ny;
                mesh.m_vertices[vertexId].m_normal.z = nz;

                // vertex tex coord between [0, 1]
                const GLfloat s = sectorId / sectorCount;
                const GLfloat t = stackId / stackCount;
                mesh.m_vertices[vertexId].m_texCoord.x = s;
                mesh.m_vertices[vertexId].m_texCoord.y = t;

                // next
                ++vertexId;
            }
        }

        // indices
        //  index1--index1+1
        //  |      / |
        //  |    /  |
        //  |   /   |
        //  | /     |
        //  index2--index2+1
        GLuint faceId = 0;
        for (GLuint stackId = 0; stackId < stackCount; ++stackId)
        {
            GLuint index1 = stackId * (sectorCount + 1); // beginning of current stack
            GLuint index2 = index1 + sectorCount + 1;        // beginning of next stack

            for (GLuint sectorId = 0; sectorId < sectorCount; ++sectorId, ++index1, ++index2)
            {
                // 2 triangles per sector excluding 1st and last stacks
                if (stackId != 0)
                {
                    // index1---index2---index1+1
                    mesh.m_indices[faceId * 3] = index1;
                    mesh.m_indices[faceId * 3 + 1] = index2;
                    mesh.m_indices[faceId * 3 + 2] = index1 + 1;
                    ++faceId;
                }

                if (stackId != (stackCount - 1))
                {
                    // index1+1---index2---index2+1
                    mesh.m_indices[faceId * 3] = index1 + 1;
                    mesh.m_indices[faceId * 3 + 1] = index2;
                    mesh.m_indices[faceId * 3 + 2] = index2 + 1;
                    ++faceId;
                }
            }
        }
    }
}
