#include <sstream>
#include <codecvt>

#include "FileSystem/ifstream.hpp"
#include "Log/warningLog.hpp"
#include "Data/json.h"

#include "modelCreator.hpp"


namespace Engine
{
    namespace Geometry
    {
        namespace Creator
        {
            bool ModelCreator::loadModelData
            (
                const std::string& name,
                std::string& meshFilePath,
                std::string& materialFilePath
            )
            {
                std::string inputPath = "Models/" + name + ".ini";
                const std::string wrongConfigurationFileStructureErrorMessage = "Wrong structure of model file '" + inputPath + "'";
                try
                {
                    FileSystem::Ifstream fin(inputPath);
                    std::stringstream buffer;
                    buffer << fin.rdbuf();

                    using convert_typeX = std::codecvt_utf8<wchar_t>;
                    std::wstring_convert<convert_typeX, wchar_t> converterX;

                    auto jsonDocument = std::shared_ptr<JSONValue>(JSON::parse(buffer.str().c_str()));
                    auto jsonRootObject = jsonDocument->asObject();

                    // Header
                    if (!jsonDocument->hasChild(L"configName") ||
                        !jsonRootObject[L"configName"]->isString() ||
                        jsonRootObject[L"configName"]->asString() != L"Model")
                    {
                        WARNING_LOG(wrongConfigurationFileStructureErrorMessage);
                        return false;
                    }

                    // MeshSrc
                    if (!jsonDocument->hasChild(L"meshSrc") ||
                        !jsonRootObject[L"meshSrc"]->isString())
                    {
                        WARNING_LOG(wrongConfigurationFileStructureErrorMessage);
                        return false;
                    }
                    meshFilePath = converterX.to_bytes(jsonRootObject[L"meshSrc"]->asString());

                    // MaterialSrc
                    if (!jsonDocument->hasChild(L"materialSrc") ||
                        !jsonRootObject[L"materialSrc"]->isString())
                    {
                        WARNING_LOG(wrongConfigurationFileStructureErrorMessage);
                        return false;
                    }
                    materialFilePath = converterX.to_bytes(jsonRootObject[L"materialSrc"]->asString());
                }
                catch (std::invalid_argument invalidArgument)
                {
                    WARNING_LOG("Could not open '" + inputPath + "'");
                    return false;
                }
                return true;
            }
        }
    }
}