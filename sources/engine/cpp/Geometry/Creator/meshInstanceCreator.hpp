#pragma once
#include <vector>
#include <memory>

#include "Geometry/meshInstance.hpp"
#include "Geometry/mesh.hpp"

namespace Engine
{
    namespace Geometry
    {
        class ObjectManager;
        namespace Creator
        {
            class MeshInstanceCreator
            {
                // Only ObjectManager can load and manipulate this data 
                friend class Geometry::ObjectManager;

            private:
                static bool create
                (
                    const std::vector<Mesh>& meshes,
                    const std::shared_ptr<Animation::Skeleton>& skeleton,
                    std::shared_ptr<MeshInstance>& meshInstance
                );

            };
        }
    }
}
