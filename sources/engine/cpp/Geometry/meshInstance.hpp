#pragma once
#include <glm/vec3.hpp>
#include <glad/glad.h>
#include <vector>
#include <string>
#include <memory>

#include "Private/meshNodeDescriptor.hpp"
#include "mesh.hpp"

namespace Engine::Animation
{
    class Skeleton;
}

namespace Engine::Geometry::Creator
{
    class MeshInstanceCreator;
}

namespace Engine::Geometry
{
    /*
    Contains VBOs, all nodes are packed into single VAO.
    Every nodes can be interpreted as other model (e.g. weapon and character - weapon doesn't have to be loaded immediately)
    */
    class MeshInstance
    {
        friend class Creator::MeshInstanceCreator;

    public:
        enum BufferObjectType
        {
            VERTEX_BO,
            TEXCOORD_BO,
            NORMAL_BO,
            BONE_ID_BO,
            BONE_WEIGHT_BO,
            INDICES_BO
        };

    protected:
        MeshInstance() = default;
        MeshInstance
        (
            const std::vector<Mesh>& meshes,
            const std::shared_ptr<Animation::Skeleton>& skeleton
        );

    public:
        ~MeshInstance();

        void bindVAO();

        // Calls render function for specified node
        // VAO, materials, program should be binded by caller
        void render(const GLuint& nodeId = 0);
        void renderInstanced(const GLuint& primCount, const GLuint& nodeId = 0);

        /* Getters */
        const std::shared_ptr<Animation::Skeleton>& getSkeleton() const;
        const glm::vec3& getBoundBox() const;
        const unsigned getNumNodes() const;

        // One node (root node) means that there is not bone hierarchy
        bool doesSkeletonContainBoneHierarchy() const;

    protected:
        // Initializes buffers into VRAM and copy skeleton data 
        void setupGeometry(const std::vector<Mesh>& meshes);

    private:
        // All data are packed into same VBOs (vertices, normals etc.), 
        //                  so need structure that descriptes every ModelNode
        std::vector<Private::MeshNodeDescriptor> m_meshNodeDescriptors;

        GLuint m_vbo;
        GLuint m_ibo;
        GLuint m_vao;

        // This property is copy from Mesh class (Mesh class contains only temporary data)
        // This property always exists
        std::shared_ptr<Animation::Skeleton> m_skeleton;

        // Bound box
        glm::vec3 m_boundBox = glm::vec3(0.0f, 0.0f, 0.0f);
    };
}
