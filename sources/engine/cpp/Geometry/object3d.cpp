#include <memory>

#include "Component/Object3D/skeletalAnimationComponent.hpp"

#include "Pipeline/globalUniformBlockBinding.hpp"
#include "Pipeline/programLibrary.hpp"
#include "Pipeline/shaderVariant.hpp"
#include "Pipeline/transform.hpp"

#include "Geometry/meshInstance.hpp"
#include "Geometry/canvas.hpp"

#include "Log/warningLog.hpp"
#include "Log/glStatus.hpp"
#include "Log/log.hpp"

#include "macros.hpp"
#include "model.hpp"

#include "object3d.hpp"

namespace Engine
{
    namespace Geometry
    {
        using namespace Component::Object3D;
        using namespace Pipeline;
        Object3D::Object3D(const std::vector<ComponentFlag>& componentFlags)
        {
            // Components
            bool skeletalAnimationEnabled = false;
            for (auto& componentFlag : componentFlags)
            {
                if (componentFlag == ComponentFlag::SkeletalAnimation)
                {
                    skeletalAnimationEnabled = true;
                }
            }

            if (skeletalAnimationEnabled)
            {
                insertSkeletalAnimationComponent();
            }

            // Initialization
            // Get or create a shared program
            initializeProgram();
            m_modelMatrix = std::make_unique<Pipeline::Transform>(m_modelMatrixUniformLocation);
        }

        Object3D::~Object3D()
        {
        }

        Object3D::Object3D(const Object3D& other)
        {
            *this = other;
        }

        Object3D& Object3D::operator=(const Object3D& other)
        {
            if (this != &other)
            {
                setModel(other.m_model);
                m_program = other.m_program;
                m_modelMatrixUniformLocation = other.m_modelMatrixUniformLocation;
                m_modelMatrix = std::make_unique<Pipeline::Transform>(m_modelMatrixUniformLocation);
                TODO(Copying components - diamond inheritance);
                //m_components = other.;
                m_meshBindingDuringRendering = other.m_meshBindingDuringRendering;
            }
            return *this;
        }

        void Object3D::setModel(const std::shared_ptr<Model>& model)
        {
            if (m_model.get() != model.get())
            {
                m_model = model;
                m_modelChanged.callSlots();
            }
        }

        void Object3D::initializeProgram()
        {
            if (m_program.get() != nullptr)
            {
                WARNING_LOG("Program was initialized");
                return;
            }

            // Creates shader variant from object components
            Pipeline::ShaderVariant shaderVariant;
            shaderVariant += Geometry::Canvas::getShaderData();
            for (auto& objectComponent : m_components)
            {
                shaderVariant += *objectComponent;
            }

            // Creates shaders
            auto& vertexShader = Pipeline::ProgramLibrary::instance().getOrCreateShaderVariant
            (
                "Engine/Shaders/object3d.vs",
                Pipeline::Shader::ShaderType::VertexShader,
                shaderVariant
            );

            auto& fragmentShader = Pipeline::ProgramLibrary::instance().getOrCreateShaderVariant
            (
                "Engine/Shaders/object3d.fs",
                Pipeline::Shader::ShaderType::FragmentShader,
                shaderVariant
            );

            // Creates or gets existed program
            bool newProgramObject;
            m_program = Pipeline::ProgramLibrary::instance().getOrCreateProgram
            (
                newProgramObject,
                vertexShader,
                std::nullopt,
                std::nullopt,
                fragmentShader
            );

            // Gets uniform names from components
            std::vector<std::string> uniformNames;
            for (auto& objectComponent : m_components)
            {
                auto uniformNamesOfComponent = objectComponent->getUniformNames();
                if (uniformNamesOfComponent.size() <= 0)
                    continue;
                uniformNames.insert
                (
                    uniformNames.cend(),
                    std::make_move_iterator(uniformNamesOfComponent.begin()),
                    std::make_move_iterator(uniformNamesOfComponent.end())
                );
            }

            // Initializes data of program (this is shared object, so it should be initialized only once)
            glUseProgram(**m_program);
            if (newProgramObject)
            {
                glUniform1i(glGetUniformLocation(**m_program, "albedoMap"), 0);
                glUniform1i(glGetUniformLocation(**m_program, "normalMap"), 1);
                glUniform1i(glGetUniformLocation(**m_program, "heightMap"), 2);
                glUniform1i(glGetUniformLocation(**m_program, "ambientOcclusionMap"), 3);
                glUniform1i(glGetUniformLocation(**m_program, "specularMap"), 4);
                glUniform1i(glGetUniformLocation(**m_program, "roughnessMap"), 5);

                // Binds uniform block which contains information about camera transform
                Pipeline::Program::bindUniformBlock
                (
                    **m_program,
                    "BlockTransform",
                    static_cast<GLuint>(GlobalUniformBlockBinding::BLOCK_TRANSFORM)
                );
            }

            // Gets common uniform locations
            m_modelMatrixUniformLocation = glGetUniformLocation
            (
                **m_program, 
                "modelMatrix"
            );

            // Gets uniform locations of components
            for (auto& uniformName : uniformNames)
            {
                if (uniformName.size() <= 0)
                {
                    WARNING_LOG("Cannot get uniform with name '" + uniformName + "'");
                    continue;
                }

                GLint uniformLocation = glGetUniformLocation
                (
                    **m_program,
                    uniformName.c_str()
                );
                if (uniformLocation == -1)
                {
                    WARNING_LOG("Uniform with name '" + uniformName + "' doesn't exist");
                    continue;
                }

                // Sets uniform location
                for (auto& objectComponent : m_components)
                {
                    auto uniformNamesOfComponent = objectComponent->getUniformNames();
                    if (std::find
                    (
                        uniformNamesOfComponent.cbegin(),
                        uniformNamesOfComponent.cend(),
                        uniformName
                    ) != uniformNamesOfComponent.cend())
                    {
                        objectComponent->setUniformLocation
                        (
                            uniformName,
                            uniformLocation
                        );
                    }
                }
            }

            CHECK_GL_ERROR();

            // Initializes data of program (this is shared object, so it should be initialized only once)
            if(newProgramObject)
            {
                LOG("New Object3D program has been created");
            }
        }

        void Object3D::update()
        {
            // Update action of component e.g.skeletal animation -
            // Uploads skeletal animation result to VRAM 
            // Skeleton can be shared, so Object3D cannot update animation result
            for (auto& component : m_components)
            {
                component->update();
            }
        }

        void Object3D::setMeshBindingDuringRendering(bool enabled)
        {
            m_meshBindingDuringRendering = enabled;
        }

        // Program should be enabled by parent
        void Object3D::render()
        {
            // Sets new value of world matrix (this value is changed after every render model - every model has diffrent worldMatrix)
            if (!m_model)
                return;
            if (!m_model->getMeshInstance())
                return;
            
            update();
            m_modelMatrix->bindMatrix();
            if (m_meshBindingDuringRendering)
                m_model->bindMesh();
            m_model->render();
        }

        /*void Object3D::RenderForShadow(const GLuint& worldMatrixLocation, bool bindMesh)
        {
            m_modelMatrix->BindMatrix(worldMatrixLocation);
            //m_model.RenderForShadow();
        }*/

        // Gets and Sets
        void Object3D::setTranslation(const glm::vec3& translation) 
        {
            m_modelMatrix->setTranslation(translation); 
            positionChanged();
        }

        void Object3D::setRotation(const glm::vec3& radianAngles)
        {
            m_modelMatrix->setRotation(radianAngles);
            rotationChanged();
        }

        void Object3D::setScale(const glm::vec3& scale) 
        {
            m_modelMatrix->setScale(scale); 
            scaleChanged();
        }

        void Object3D::lookAt(const glm::vec3& direction)
        {
            m_modelMatrix->lookAt(direction);
            rotationChanged();
        }

        void Object3D::move(const glm::vec3& vector)
        {
            Object3D::setTranslation(getTranslation() + vector);
        }

        void Object3D::rotate(const glm::vec3& radianAngles)
        {
            Object3D::setRotation(getRotation() + radianAngles);
        }

        void Object3D::scale(const glm::vec3& scale)
        {
            Object3D::setScale(getScale() + scale);
        }

        const glm::vec3& Object3D::getTranslation() const 
        {
            return m_modelMatrix->getTranslation(); 
        }

        const glm::vec3& Object3D::getRotation() const 
        {
            return m_modelMatrix->getRotation(); 
        }

        const glm::vec3& Object3D::getScale() const
        {
            return m_modelMatrix->getScale();
        }

        const glm::vec3 Object3D::getBoundBox() const
        {
            if (m_model.get() == nullptr
                || m_model->getMeshInstance().get() == nullptr)
            {
                return glm::vec3(0.0f, 0.0f, 0.0f);
            }
            return m_model->getMeshInstance()->getBoundBox();
        }

        void Object3D::insertSkeletalAnimationComponent()
        {
            if (!hasSkeletalAnimationComponent())
            {
                m_components.push_back(std::make_unique<SkeletalAnimationComponent>(*this));
            }
        }

        bool Object3D::hasSkeletalAnimationComponent()
        {
            for (const auto& component : m_components)
            {
                if (dynamic_cast<SkeletalAnimationComponent*>(component.get()) != nullptr)
                    return true;
            }
            return false;
        }

        const Object3D::Object3DComponentPtr& Object3D::getSkeletalAnimationComponent() const
        {
            for (const auto& component : m_components)
            {
                if (dynamic_cast<SkeletalAnimationComponent*>(component.get()) != nullptr)
                    return component;
            }
            return nullptr;
        }

        Pipeline::Program& Object3D::getProgram()
        {
            return *m_program;
        }
    }
}
