#include <string_view>
#include <algorithm>
#include <memory>

#include "Utils/sequenceArray.hpp"

#include "Log/glStatus.hpp"
#include "Log/log.hpp"

#include "canvas.hpp"

namespace Engine::Geometry
{
    Canvas::Canvas()
    {
    }

    Canvas::~Canvas()
    {
        clear();
    }

    bool Canvas::clear()
    {
        if (glIsFramebuffer(m_frameBuffer))
        {
            LOG("Deinitializing canvas...");
            glDeleteFramebuffers(1, &m_frameBuffer);
            glDeleteTextures(magic_enum::enum_count<MapType>(), m_maps.data());
            glDeleteTextures(1, &m_depthMap);
            return true;
        }
        return false;
    }

    void Canvas::bindForWriting()
    {
        glBindFramebuffer(GL_DRAW_FRAMEBUFFER, m_frameBuffer);
    }

    void Canvas::bindForReading()
    {
        for (GLuint i = 0; i < magic_enum::enum_count<MapType>(); ++i)
        {
            glActiveTexture(GL_TEXTURE0 + i);
            glBindTexture(GL_TEXTURE_2D, m_maps[i]);
        }
    }

    void Canvas::blitDepthBuffer(const GLuint targetFbo)
    {
        glBindFramebuffer(GL_READ_FRAMEBUFFER, m_frameBuffer);
        glBindFramebuffer(GL_DRAW_FRAMEBUFFER, targetFbo);
        glBlitFramebuffer
        (
            0,
            0,
            m_windowWidth,
            m_windowHeight,
            0,
            0,
            m_windowWidth,
            m_windowHeight,
            GL_DEPTH_BUFFER_BIT,
            GL_NEAREST
        );
    }

    void Canvas::initialize
    (
        const GLsizei windowWidth, 
        const GLsizei windowHeight
    )
    {
        if (clear())
            LOG("Reinitializing canvas...");
        else
            LOG("Initializing canvas...");

        m_windowWidth = windowWidth;
        m_windowHeight = windowHeight;

        glGenFramebuffers(1, &m_frameBuffer);
        glBindFramebuffer(GL_DRAW_FRAMEBUFFER, m_frameBuffer);

        glGenTextures(magic_enum::enum_count<MapType>(), m_maps.data());

        auto initializeOutputMap = [&]
        (
            MapType mapType,
            GLint internalFormat,
            GLenum format,
            GLenum type
            )
        {
            glBindTexture
            (
                GL_TEXTURE_2D,
                m_maps[static_cast<size_t>(mapType)]
            );
            glTexImage2D
            (
                GL_TEXTURE_2D,
                0,
                internalFormat,
                windowWidth,
                windowHeight,
                0,
                format,
                type,
                NULL
            );
            glTexParameteri
            (
                GL_TEXTURE_2D,
                GL_TEXTURE_MAG_FILTER,
                GL_NEAREST
            );
            glTexParameteri
            (
                GL_TEXTURE_2D,
                GL_TEXTURE_MIN_FILTER,
                GL_NEAREST
            );
            glFramebufferTexture2D
            (
                GL_FRAMEBUFFER,
                GL_COLOR_ATTACHMENT0 + static_cast<GLenum>(mapType),
                GL_TEXTURE_2D,
                m_maps[static_cast<size_t>(mapType)],
                0
            );
        };

        // shadingTypeMap
        initializeOutputMap
        (
            MapType::SHADING_TYPE_MAP,
            GL_R8UI,
            GL_RED_INTEGER,
            GL_UNSIGNED_BYTE
        );

        // colorShadowMap
        initializeOutputMap
        (
            MapType::COLOR_SHADOW_MAP,
            GL_RGBA8,
            GL_RGBA,
            GL_UNSIGNED_BYTE
        );

        // normalMap
        initializeOutputMap
        (
            MapType::NORMAL_MAP,
            GL_RGB16F,
            GL_RGB,
            GL_FLOAT
        );

        // positionMap
        initializeOutputMap
        (
            MapType::POSITION_MAP,
            GL_RGB16F,
            GL_RGB,
            GL_FLOAT
        );

        // aoSpecularRoughnessHeight
        initializeOutputMap
        (
            MapType::AO_SPECULAR_ROUGHNESS_HEIGHT_MAP,
            GL_RGBA8,
            GL_RGBA,
            GL_UNSIGNED_BYTE
        );

        // depthMap
        glGenTextures(1, &m_depthMap);
        glBindTexture(GL_TEXTURE_2D, m_depthMap);
        glTexImage2D
        (
            GL_TEXTURE_2D,
            0,
            GL_DEPTH_COMPONENT32,
            windowWidth,
            windowHeight,
            0,
            GL_DEPTH_COMPONENT,
            GL_UNSIGNED_INT,
            NULL
        );
        glFramebufferTexture2D
        (
            GL_FRAMEBUFFER,
            GL_DEPTH_ATTACHMENT,
            GL_TEXTURE_2D,
            m_depthMap,
            0
        );

        auto drawBuffers = Utils::SequenceNumber::makeArray<GLuint, magic_enum::enum_count<MapType>()>
            (
                [](auto i) -> GLuint
                {
                    return GL_COLOR_ATTACHMENT0 + static_cast<GLuint>(i);
                }
        );
        glDrawBuffers
        (
            magic_enum::enum_count<MapType>(),
            drawBuffers.data()
        );
        glReadBuffer(GL_NONE);

        CHECK_FRAMEBUFFER_ERROR();
        glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
    }

    void Canvas::initializeProgramAsReader(const GLuint& program)
    {
        glUseProgram(program);
        for (GLint mapTypeId = 0; mapTypeId < magic_enum::enum_count<MapType>(); ++mapTypeId)
        {
            // Validation
            auto mapTypeOpt = magic_enum::enum_cast<MapType>(mapTypeId);
            assert(mapTypeOpt.has_value());

            // Registers maps for reading mode
            static constexpr auto& mapTypeNames = magic_enum::enum_names<MapType>();

            // Transforms map type name
            auto mapTypeNameOfEnum = mapTypeNames[mapTypeId];
            std::string mapTypeName;

            for (unsigned mapTypeNameCharacterId = 0; mapTypeNameCharacterId < mapTypeNameOfEnum.size(); ++mapTypeNameCharacterId)
            {
                auto character = mapTypeNameOfEnum[mapTypeNameCharacterId];
                if (mapTypeNameOfEnum[mapTypeNameCharacterId] == '_')
                {
                    ++mapTypeNameCharacterId;
                    if (mapTypeNameCharacterId < mapTypeNameOfEnum.size())
                    {
                        character = mapTypeNameOfEnum[mapTypeNameCharacterId];
                        mapTypeName.push_back(std::toupper(character));
                    }
                    continue;
                }
                mapTypeName.push_back(std::tolower(character));
            }

            glUniform1i
            (
                glGetUniformLocation
                (
                    program,
                    mapTypeName.data()
                ),
                mapTypeId
            );
        }
    }

    const Pipeline::ShaderVariant& Canvas::getShaderData()
    {
        static std::unique_ptr<Pipeline::ShaderVariant> shaderVariant;

        if (shaderVariant.get() == nullptr)
        {
            shaderVariant = std::make_unique<Pipeline::ShaderVariant>();

            // Map type variables
            for (auto& mapTypeEntry : magic_enum::enum_entries<MapType>())
            {
                auto mapType = mapTypeEntry.first;
                auto mapTypeName = std::string(mapTypeEntry.second);

                // Generates shader variable name
                std::string shaderVariableName = mapTypeName + "_ID";

                // Pushes value
                shaderVariant->pushValue
                (
                    {
                        shaderVariableName,
                        std::to_string(static_cast<GLuint>(mapType))
                    }
                );
            }

            // Shading type variables
            for (auto& shadingTypeEntry : magic_enum::enum_entries<ShadingType>())
            {
                auto shadingType = shadingTypeEntry.first;
                auto shadingTypeName = std::string(shadingTypeEntry.second);

                // Generates shader variable name
                std::string shaderVariableName = shadingTypeName + "_SHADING_TYPE";

                // Pushes value
                shaderVariant->pushValue
                (
                    {
                        shaderVariableName,
                        std::to_string(static_cast<GLuint>(shadingType))
                    }
                );
            }
        }

        return *shaderVariant;
    }
}