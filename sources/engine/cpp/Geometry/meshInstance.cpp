#include <limits>

#include "Log/criticalLog.hpp"
#include "Log/warningLog.hpp"

#include "Animation/skeleton.hpp"
#include "vertex.hpp"
#include "mesh.hpp"

#include "meshInstance.hpp"

namespace Engine::Geometry
{
    MeshInstance::MeshInstance
    (
        const std::vector<Mesh>& meshes,
        const std::shared_ptr<Animation::Skeleton>& skeleton
    ) : m_skeleton(skeleton)
    {
        // Validation
        if (m_skeleton.get() == nullptr)
        {
            CRITICAL_LOG("Internal error", "Skeleton is nullptr");
        }
        setupGeometry(meshes);
    }

    MeshInstance::~MeshInstance()
    {
        if (glIsVertexArray(m_vao))
            glDeleteVertexArrays(1, &m_vao);

        if (glIsBuffer(m_vbo))
            glDeleteBuffers(1, &m_vbo);

        if (glIsBuffer(m_ibo))
            glDeleteBuffers(1, &m_ibo);
    }

    void MeshInstance::bindVAO()
    {
        glBindVertexArray(m_vao);
    }

    void MeshInstance::render(const GLuint& nodeId)
    {
        if (nodeId >= static_cast<GLuint>(m_meshNodeDescriptors.size()))
        {
            WARNING_LOG("Model doesn't contain node with id '" + std::to_string(nodeId) + "'");
            return;
        }
        glDrawElementsBaseVertex
        (
            GL_TRIANGLES,
            m_meshNodeDescriptors[nodeId].m_numIndices,
            GL_UNSIGNED_INT, 
            reinterpret_cast<const GLvoid*>(sizeof(GLuint) * m_meshNodeDescriptors[nodeId].m_indexOffset),
            m_meshNodeDescriptors[nodeId].m_vertexOffset
        );
    }

    void MeshInstance::renderInstanced(const GLuint& primCount, const GLuint& nodeId)
    {
        if (nodeId >= static_cast<GLuint>(m_meshNodeDescriptors.size()))
        {
            WARNING_LOG("Model doesn't contain node with id '" + std::to_string(nodeId) + "'");
            return;
        }
        glDrawElementsInstancedBaseVertex
        (
            GL_TRIANGLES,
            m_meshNodeDescriptors[nodeId].m_numIndices,
            GL_UNSIGNED_INT, 
            reinterpret_cast<const GLvoid*>(sizeof(GLuint) * m_meshNodeDescriptors[nodeId].m_indexOffset),
            primCount,
            m_meshNodeDescriptors[nodeId].m_vertexOffset
        );
    }

    void MeshInstance::setupGeometry(const std::vector<Mesh>& meshes)
    {
        if (meshes.size() == 0)
        {
            WARNING_LOG("Vector of meshes is empty");
            return;
        }

        m_meshNodeDescriptors.reserve(meshes.size());

        std::vector<Vertex> globalVertices;
        std::vector<GLuint> globalIndicies;

        for (auto& mesh : meshes)
        {
            auto& vertices = mesh.getVertices();
            auto indices = mesh.getIndices();

            m_meshNodeDescriptors.push_back
            (
                Private::MeshNodeDescriptor
                {
                    static_cast<GLuint>(globalVertices.size()),
                    static_cast<GLuint>(globalIndicies.size()),
                    static_cast<GLuint>(indices.size())
                }
            );

            globalVertices.insert
            (
                globalVertices.end(),
                std::make_move_iterator(vertices.begin()),
                std::make_move_iterator(vertices.end())
            );
            globalIndicies.insert
            (
                globalIndicies.end(),
                std::make_move_iterator(indices.begin()),
                std::make_move_iterator(indices.end())
            );
        }

        if (globalVertices.size() == 0 || globalIndicies.size() == 0)
        {
            WARNING_LOG("Vertex buffer is empty");
            return;
        }

        // Compute bound box
        glm::vec3 minBoundBox
        (
            std::numeric_limits<GLfloat>::max(),
            std::numeric_limits<GLfloat>::max(),
            std::numeric_limits<GLfloat>::max()
        );
        glm::vec3 maxBoundBox
        (
            std::numeric_limits<GLfloat>::lowest(),
            std::numeric_limits<GLfloat>::lowest(),
            std::numeric_limits<GLfloat>::lowest()
        );
        for (auto& vertex : globalVertices)
        {
            if (minBoundBox.x > vertex.m_position.x)
                minBoundBox.x = vertex.m_position.x;
            if (minBoundBox.y > vertex.m_position.y)
                minBoundBox.y = vertex.m_position.y;
            if (minBoundBox.z > vertex.m_position.z)
                minBoundBox.z = vertex.m_position.z;

            if (maxBoundBox.x < vertex.m_position.x)
                maxBoundBox.x = vertex.m_position.x;
            if (maxBoundBox.y < vertex.m_position.y)
                maxBoundBox.y = vertex.m_position.y;
            if (maxBoundBox.z < vertex.m_position.z)
                maxBoundBox.z = vertex.m_position.z;
        }
        m_boundBox = glm::vec3
        (
            maxBoundBox.x - minBoundBox.x,
            maxBoundBox.y - minBoundBox.y,
            maxBoundBox.z - minBoundBox.z
        );

        glGenVertexArrays(1, &m_vao);
        glBindVertexArray(m_vao);
        glGenBuffers(1, &m_vbo);
        glGenBuffers(1, &m_ibo);

        glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
        glBufferData(GL_ARRAY_BUFFER, sizeof(globalVertices[0]) * globalVertices.size(), &globalVertices[0], GL_STATIC_DRAW);

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ibo);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER,
            sizeof(globalIndicies[0]) * globalIndicies.size(),
            &globalIndicies[0],
            GL_STATIC_DRAW
        );

        GLint stride = sizeof(globalVertices[0]);
        GLuint offset = 0;
        glEnableVertexAttribArray(VERTEX_BO);
        glVertexAttribPointer
        (
            VERTEX_BO,
            3,
            GL_FLOAT,
            GL_FALSE,
            stride,
            reinterpret_cast<const GLvoid*>(offset)
        );
        offset += 3 * sizeof(GLfloat);

        glEnableVertexAttribArray(TEXCOORD_BO);
        glVertexAttribPointer
        (
            TEXCOORD_BO, 
            2, 
            GL_FLOAT, 
            GL_FALSE, 
            stride,
            reinterpret_cast<const GLvoid*>(offset)
        );
        offset += 2 * sizeof(GLfloat);

        glEnableVertexAttribArray(NORMAL_BO);
        glVertexAttribPointer
        (
            NORMAL_BO, 
            3, 
            GL_FLOAT, 
            GL_FALSE, 
            stride,
            reinterpret_cast<const GLvoid*>(offset)
        );
        offset += 3 * sizeof(GLfloat);

        // We don't know that bone data will be used or not, so we have to upload them anyway
        glEnableVertexAttribArray(BONE_ID_BO);
        glVertexAttribIPointer
        (
            BONE_ID_BO, 
            Vertex::NUM_BONES_PER_VERTEX, GL_UNSIGNED_INT, 
            stride,
            reinterpret_cast<const GLvoid*>(offset)
        );
        offset += Vertex::NUM_BONES_PER_VERTEX * sizeof(GLuint);

        glEnableVertexAttribArray(BONE_WEIGHT_BO);
        glVertexAttribPointer
        (
            BONE_WEIGHT_BO, Vertex::NUM_BONES_PER_VERTEX, 
            GL_FLOAT, 
            GL_FALSE, 
            stride,
            (const GLvoid*)(offset)
        );

        glBindVertexArray(0);
    }

    const std::shared_ptr<Animation::Skeleton>& MeshInstance::getSkeleton() const
    {
        return m_skeleton;
    }

    const glm::vec3& MeshInstance::getBoundBox() const
    {
        return m_boundBox;
    }

    const unsigned MeshInstance::getNumNodes() const
    {
        return m_meshNodeDescriptors.size();
    }

    bool MeshInstance::doesSkeletonContainBoneHierarchy() const
    {
        return m_skeleton->containsBoneHierarchy();
    }
}