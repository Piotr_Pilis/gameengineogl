#include <glm/vec3.hpp>
#include <vector>

#include "Importer/Object/TerrainObjectsJsonImporter.hpp"
#include "Log/warningLog.hpp"
#include "Map/terrain.hpp"

#include "Animation/Creator/animationCreator.hpp"
#include "Material/Creator/materialCreator.hpp"
#include "Creator/meshInstanceCreator.hpp"
#include "Creator/meshCreator.hpp"
#include "Creator/modelCreator.hpp"

#include "Material/materialInstance.hpp"
#include "Material/materialInfo.hpp"

#include "Animation/animation.hpp"
#include "Animation/skeleton.hpp"

#include "object3dInstanced.hpp"
#include "meshInstance.hpp"
#include "model.hpp"
#include "mesh.hpp"

#include "Entity/renderingManager.hpp"
#include "Entity/pawn.hpp"

#include "objectManager.hpp"

namespace Engine::Geometry
{
    using namespace Importer::Object;
    namespace
    {
        constexpr const char* MAP_OBJECTS_FILENAME = "objects.ini";
    }

    ObjectManager::~ObjectManager()
    {
    }

    void ObjectManager::initialize()
    {
    }

    bool ObjectManager::loadStaticObjectsFromMap(const TerrainPtr& terrain)
    {
        if (!terrain.get())
        {
            namespace Engine = Engine;
            WARNING_LOG("Active terrain is not set");
            return false;
        }

        auto mapObjectsConfigFilePath = terrain->getMapPath() + "/" + MAP_OBJECTS_FILENAME;

        TerrainObjectsJsonImporter terrainObjectsDataImporter;
        if (!terrainObjectsDataImporter.import(mapObjectsConfigFilePath))
        {
            WARNING_LOG("Cannot import data about terrain objects, reason: " +
                terrainObjectsDataImporter.getLastErrorMessage());
            return false;
        }
        auto& objectsData = terrainObjectsDataImporter.getObjects();
        if (!objectsData.size())
            return true;

        // Sorted objects data by object/ file name
        // <model name, data(position, rotation, scale)
        std::map<std::string, std::vector<ObjectsImporterAbstract::ObjectTransformationInfo>> sortedObjectsData;
        for (auto& objectData : objectsData)
        {
            if (!objectData.m_modelName.length())
            {
                WARNING_LOG("Object has no model name");
                continue;
            }
            sortedObjectsData[objectData.m_modelName].push_back(objectData.m_objectTransformation);
        }

        // Loads entities, models and meshes
        for (auto& objectData : sortedObjectsData)
        {
            /*if (objectData.second.size() >= 2)
            {
                //pushEntities(objectData.first, objectData.second);
            }
            else if(objectData.second.size() == 1)
            {*/
            pushEntity(objectData.first, objectData.second[0]);
            //}
        }

        return true;
    }

    bool ObjectManager::pushEntity(const std::string& modelFileName,
        const ObjectsImporterAbstract::ObjectTransformationInfo& transformationInfo)
    {
        try
        {
            // Creates entity
            Entity::RenderingManager::RenderableEntity entity = std::make_shared<Entity::Pawn>();
            Object3D* object3D = dynamic_cast<Object3D*>(entity.get());

            // Binds model to Object3D
            auto& model = getModel(modelFileName);
            if (model == nullptr)
            {
                namespace Engine = Engine;
                std::string errorMessage = "Model '" + modelFileName + "' cannot be loaded";
                WARNING_LOG(errorMessage);
                throw std::invalid_argument(errorMessage);
            }
            object3D->setModel(model);

            // Transformation
            object3D->setTranslation(transformationInfo.m_position);
            object3D->setRotation(transformationInfo.m_rotation);
            object3D->setScale(transformationInfo.m_scale);

            // Push to rendering queue
            object3D->setRenderingEnabled(true);
        }
        catch (std::invalid_argument invalidArgument)
        {
            return false;
        }
        return true;
    }

    /*bool ObjectManager::pushEntities(const std::string& src,
        const std::vector<Importer::Object::ObjectsImporterAbstract::ObjectTransformationInfo>& transformationInfo)
    {
        auto model = getModel(src);
        if (model == nullptr)
        {
            namespace Engine = Engine;
            CRITICAL_LOG("Model cannot be loaded");
            return;
        }

        m_instancedEntity.push_back(std::make_unique<Object3DInstanced>(
            *model->get(), positions));
    }*/

    const std::vector<ObjectManager::AnimationPtr>& ObjectManager::getOrCreateAnimations
    (
        const std::string& animationsFileName
    )
    {
        // Is already added?
        auto animationIterator = m_animations.find(animationsFileName);
        if (animationIterator != m_animations.end())
            return animationIterator->second;

        // Makes animations
        Animation::Creator::AnimationCreator::loadAnimations
        (
            animationsFileName,
            m_animations[animationsFileName]
        );
        return m_animations[animationsFileName];
    }

    const ObjectManager::MeshInstancePtr& ObjectManager::getMeshInstance
    (
        const std::string& src
    )
    {
        auto meshInstancesIterator = m_meshInstances.find(src);
        if (meshInstancesIterator != m_meshInstances.end())
            return meshInstancesIterator->second;
        else if (loadMeshInstance(src))
            return m_meshInstances[src];

        namespace Engine = Engine;
        WARNING_LOG("Failed to load mesh '" + src + "'!");
        return std::move(MeshInstancePtr(nullptr));
    }

    const ObjectManager::ModelPtr& ObjectManager::getModel
    (
        const std::string& name
    )
    {
        auto modelIterator = m_models.find(name);
        if (modelIterator != m_models.end())
            return modelIterator->second;
        else if (loadModel(name))
            return m_models[name];

        namespace Engine = Engine;
        WARNING_LOG("Failed to load model '" + name + "'!");
        return std::move(ModelPtr(nullptr));
    }

    const std::vector<ObjectManager::MaterialInstancePtr>& ObjectManager::getMaterialInstance
    (
        const std::string& src
    )
    {
        auto materialInstanceIterator = m_materialInstances.find(src);
        if (materialInstanceIterator != m_materialInstances.end())
            return materialInstanceIterator->second;
        else if (loadMaterialInstance(src))
            return m_materialInstances[src];

        namespace Engine = Engine;
        WARNING_LOG("Failed to load material instance '" + src + "'!");
        return std::move(std::vector<ObjectManager::MaterialInstancePtr>{});
    }

    bool ObjectManager::loadMeshInstance(const std::string& src)
    {
        std::vector<Mesh> meshes;
        if (!Creator::MeshCreator::loadMeshes
        (
            src,
            meshes,
            m_skeletons[src]
        ))
        {
            return false;
        }
        return Creator::MeshInstanceCreator::create
        (
            meshes,
            m_skeletons[src],
            m_meshInstances[src]
        );
    }

    bool ObjectManager::loadModel(const std::string& name)
    {
        // Loads model data
        std::string meshFilePath;
        std::string materialFilePath;
        if (!Creator::ModelCreator::loadModelData
        (
            name,
            meshFilePath,
            materialFilePath
        ))
        {
            return false;
        }
        if (!meshFilePath.length())
        {
            return false;
        }

        // Gets or creates mesh instance object
        auto& meshInstance = getMeshInstance(meshFilePath);
        if (meshInstance.get() == nullptr)
        {
            return false;
        }

        // Gets or creates material instance object
        auto& materialInstance = getMaterialInstance
        (
            materialFilePath
        );

        // Creates model object
        m_models[name] = std::shared_ptr<Model>
        (
            new Model
            (
                meshInstance,
                materialInstance
            )
        );
        return true;    
    }

    bool ObjectManager::loadMaterialInstance(const std::string& src)
    {
        std::vector<Material::MaterialInfo> materialInfos;
        if (!Material::Creator::MaterialCreator::loadMaterialData
        (
            src,
            materialInfos
        ))
        {
            return false;
        }

        std::vector<std::shared_ptr<Material::Material>> materials;
        if (!Material::Creator::MaterialCreator::create
        (
            materialInfos,
            materials
        ))
        {
            return false;
        }

        auto& materialInstances = m_materialInstances[src];
        for(const auto& material : materials)
        { 
            materialInstances.emplace_back
            (
                std::make_shared<Material::MaterialInstance>
                (
                    Material::MaterialInstance::MaterialType::FLAT_MATERIAL,
                    material
                )
            );
        }
        return true;
    }

    void ObjectManager::removeMesh(const std::string& src)
    {
        auto meshInstanceIterator = m_meshInstances.find(src);
        if (meshInstanceIterator != m_meshInstances.end())
            m_meshInstances.erase(meshInstanceIterator);
    }

    void ObjectManager::removeModel(const std::string& name)
    {
        auto modelIterator = m_models.find(name);
        if (modelIterator != m_models.end())
            m_models.erase(modelIterator);
    }

    void ObjectManager::removeMaterialInstance(const std::string& src)
    {
        auto materialInstanceIterator = m_materialInstances.find(src);
        if (materialInstanceIterator != m_materialInstances.end())
            m_materialInstances.erase(materialInstanceIterator);
    }
}