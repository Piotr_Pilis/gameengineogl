#pragma once
#include <glad/glad.h>
#include <vector>
#include <memory>
#include <string>

#include "vertex.hpp"
#include "macros.hpp"

struct aiMesh;
namespace Engine
{
    namespace Animation
    {
        class Skeleton;
    }

    namespace Geometry
    {
        namespace Creator
        {
            class MeshCreator;
        }

        TODO(Merging nodes)
        /*
        It's only container for raw data of mesh and skeleton
        You can create Mesh manually or load from file using ObjectManager
        */
        class Mesh
        {
            friend class Creator::MeshCreator;

        public:
            Mesh();
            ~Mesh();

            void setVertices(const std::vector<Vertex>& vertices) { m_vertices = vertices; }
            void setIndices(const std::vector<GLuint>& indices) { m_indices = indices; }

            const std::vector<Vertex>& getVertices() const { return m_vertices; }
            const std::vector<GLuint>& getIndices() const { return m_indices; }

        private:
            // Only Creator can use this method
            static bool loadBones
            (
                Mesh& mesh,
                const std::shared_ptr<Animation::Skeleton>& skeleton,
                const aiMesh& aiMesh
            );

        private:
            std::vector<Vertex> m_vertices;
            std::vector<GLuint> m_indices;

        };
    }
}
