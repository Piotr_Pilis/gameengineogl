#pragma once
#include <glad/glad.h>
#include <vector>
#include <memory>

#include "macros.hpp"

namespace Engine
{
    namespace Material
    {
        class MaterialInstance;
    }

    namespace Geometry
    {
        class ObjectManager;
        class MeshInstance;

        /*
        It's only container of meshInstance and material
        */
        class Model
        {
            friend class ObjectManager;

        private:
            Model() = delete;
            Model
            (
                const std::shared_ptr<MeshInstance>& meshInstance,
                const std::vector<std::shared_ptr<Material::MaterialInstance>>& materialInstances = {}
            );

        public:
            ~Model();

            // This method should be called before Render method
            // Make sure that m_meshInstance is not nullptr
            void bindMesh();

            // Prepares textures and calls render function for every modelNode
            // Should use glBindVertexArray(GetVAO()) before
            // Make sure that m_meshInstance is not nullptr
            void render();
            void renderInstanced(const GLuint& primCount);

            const std::shared_ptr<MeshInstance>& getMeshInstance() const { return m_meshInstance; }
            const std::vector<std::shared_ptr<Material::MaterialInstance>>& getMaterialInstances() const { return m_materialInstances; }

        private:
            void bindMaterial(const GLuint materialId);

        private:
            // This property should always exists
            std::shared_ptr<MeshInstance> m_meshInstance;
            std::vector<std::shared_ptr<Material::MaterialInstance>> m_materialInstances;

        };
    }
}
