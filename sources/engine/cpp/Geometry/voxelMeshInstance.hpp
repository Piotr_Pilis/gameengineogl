#pragma once
#include <glad/glad.h>
#include <memory>
#include "meshInstance.hpp"

namespace Engine
{
    namespace Geometry
    {
        class VoxelMeshInstance: 
            public MeshInstance
        {
        private:
            VoxelMeshInstance();
            ~VoxelMeshInstance();

        public:
            static VoxelMeshInstance& getInstance();

        private:
            void initialize();

        };
    }
}