#include <limits>
#include "vertex.hpp"

namespace Engine::Geometry
{
    /* BoneData */
    Vertex::BoneData::BoneData()
    {
        for (GLuint bonePerVertexDataId = 0; bonePerVertexDataId < NUM_BONES_PER_VERTEX; ++bonePerVertexDataId)
        {
            boneIds[bonePerVertexDataId] = 0;
            weights[bonePerVertexDataId] = 0.0f;
        }
    }

    bool Vertex::BoneData::addBoneData(const GLuint& boneId, const GLfloat& weight)
    {
        if (weight < 0.0f || weight > 1.0f)
        {
            return false;
        }

        for (GLuint bonePerVertexDataId = 0; bonePerVertexDataId < NUM_BONES_PER_VERTEX; ++bonePerVertexDataId)
        {
            // Is equal zero?
            if (weights[bonePerVertexDataId] < std::numeric_limits<GLfloat>::epsilon())
            {
                // So we have free slot for vertex bone data
                boneIds[bonePerVertexDataId] = boneId;
                weights[bonePerVertexDataId] = weight;
                return true;
            }
        }
        return false;
    }
}