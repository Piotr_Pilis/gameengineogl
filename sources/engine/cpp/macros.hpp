#pragma once
// Constants
#define PRINT_SHADER_SOURCES   false
#define PRINT_TODO_TASKS    false

// Utils
#define _STR(str) #str
#define STR(str) _STR(str)

#define __FILENAME__ (strrchr(__FILE__, '\\') ? strrchr(__FILE__, '\\') + 1 : __FILE__)
#if PRINT_TODO_TASKS
    #define TODO(msg) __pragma(message(__FILE__ "(" STR(__LINE__) "): TODO: "_STR(msg)))
#else
    #define TODO(msg)
#endif

#define INVALID_UNIFORM_LOCATION 0xffffffff