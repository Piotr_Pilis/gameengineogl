#pragma once

namespace Engine
{
    namespace Component
    {
        class AbstractComponent
        {
        public:
            AbstractComponent() = default;
            ~AbstractComponent() = default;

        };
    }
}
