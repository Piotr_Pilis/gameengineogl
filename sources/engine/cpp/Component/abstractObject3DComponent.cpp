#include "Geometry/object3d.hpp"
#include "abstractObject3DComponent.hpp"

namespace Engine
{
    namespace Component
    {
        AbstractObject3DComponent::AbstractObject3DComponent(Geometry::Object3D& parent) :
            m_parent(parent)
        {
        }
    }
}