#pragma once
#include <glad/glad.h>
#include <vector>
#include <string>
#include "Pipeline/shaderVariant.hpp"
#include "abstractComponent.hpp"

namespace Engine
{
    namespace Geometry
    {
        class Object3D;
    }

    namespace Component
    {
        /*
        Descriptes variant of class object e.g. (for now it's just a ShaderVariant)
        */
        class AbstractObject3DComponent : 
            public AbstractComponent,
            public Pipeline::ShaderVariant
        {
        public:
            AbstractObject3DComponent() = delete;
            AbstractObject3DComponent(Geometry::Object3D& parent);
            ~AbstractObject3DComponent() = default;

            virtual void update() = 0;
            virtual std::vector<std::string> getUniformNames() = 0;
            virtual void setUniformLocation
            (
                const std::string uniformName, 
                const GLint& uniformLocation
            ) = 0;

        private:
            Geometry::Object3D& m_parent;

        };
    }
}
