#pragma once
#include <glm/mat4x4.hpp>
#include <vector>
#include <memory>

#include "Event/connection.hpp"
#include "Component/abstractObject3DComponent.hpp"

namespace Engine
{
    namespace Geometry
    {
        class Object3D;
    }

    namespace Animation
    {
        class AnimationPlayer;
    }

    namespace Component
    {
        namespace Object3D
        {
            /*
            Uploads animation result to VRAM
            */
            class SkeletalAnimationComponent : 
                public AbstractObject3DComponent
            {
            public:
                SkeletalAnimationComponent() = delete;
                SkeletalAnimationComponent(Geometry::Object3D& parent);
                ~SkeletalAnimationComponent() = default;

                void update() override;
                std::vector<std::string> getUniformNames() override;
                void setUniformLocation
                (
                    const std::string uniformName,
                    const GLint& uniformLocation
                ) override;

                void setAnimationPlayer(const std::shared_ptr<Animation::AnimationPlayer>& animationPlayer);

            private:
                void uploadBoneTransformations
                (
                    const std::vector<glm::mat4x4>& boneTransformations
                );

            private:
                // Slots
                void onAnimationPlayerStateChanged();

            private:
                // Contains result of animations (be careful, it can be nullptr)
                std::shared_ptr<Animation::AnimationPlayer> m_animationPlayer;
                GLint m_boneTransformationsLocation;

                Event::Connection::Slot<> m_animationPlayerStateChangedSlot;
                bool m_wasRunning = true;

            };
        }
    }
}