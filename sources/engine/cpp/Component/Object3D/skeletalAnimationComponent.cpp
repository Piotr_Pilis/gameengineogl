#include <glm/gtc/type_ptr.hpp>
#include <algorithm>
#include <string>

#include "Animation/animationPlayer.hpp"
#include "Geometry/object3d.hpp"
#include "macros.hpp"

#include "skeletalAnimationComponent.hpp"

namespace Engine::Component::Object3D
{
    namespace
    {
        // Sections
        constexpr const char* const SHADER_SECTION_NAME = "SKELETAL_ANIMATION";

        // Values
        constexpr const char* const MAX_NUM_BONES_NAME = "MAX_NUM_BONES";
        constexpr const GLuint MAX_NUM_BONES_VALUE = 100;

        // Uniforms
        constexpr const char* const BONE_TRANSFORMATIONS_NAME = "boneTransformations";
    }

    SkeletalAnimationComponent::SkeletalAnimationComponent(Geometry::Object3D& parent) :
        AbstractObject3DComponent(parent)
    {
        m_sections =
        {
            std::string(SHADER_SECTION_NAME)
        };

        m_values =
        {
            { MAX_NUM_BONES_NAME, std::to_string(MAX_NUM_BONES_VALUE) }
        };
    }

    void SkeletalAnimationComponent::update()
    {
        TODO(Upload shared skeletal animation result only once);
        if (m_animationPlayer.get() == nullptr)
            return;
        if (m_animationPlayer->getState() != Animation::AnimationPlayer::State::PLAYING)
            return;

        auto boneTransformations = m_animationPlayer->getResult();
        uploadBoneTransformations(boneTransformations);
    }

    void SkeletalAnimationComponent::uploadBoneTransformations
    (
        const std::vector<glm::mat4x4>& boneTransformations
    )
    {
        if (boneTransformations.size() <= 0)
            return;
        glUniformMatrix4fv
        (
            m_boneTransformationsLocation,
            std::min
            (
                MAX_NUM_BONES_VALUE,
                static_cast<GLuint>(boneTransformations.size())
            ),
            GL_FALSE,
            glm::value_ptr(boneTransformations[0])
        );
    }

    std::vector<std::string> SkeletalAnimationComponent::getUniformNames()
    {
        return 
        {
            BONE_TRANSFORMATIONS_NAME
        };
    }

    void SkeletalAnimationComponent::setUniformLocation
    (
        const std::string uniformName,
        const GLint& uniformLocation
    )
    {
        if (uniformName == BONE_TRANSFORMATIONS_NAME)
        {
            m_boneTransformationsLocation = uniformLocation;
        }
    }

    void SkeletalAnimationComponent::setAnimationPlayer(const std::shared_ptr<Animation::AnimationPlayer>& animationPlayer)
    {
        // Disconnects signals
        if (m_animationPlayer.get() != nullptr)
        {
            m_animationPlayer->m_stateChanged.disconnect(m_animationPlayerStateChangedSlot);
        }

        // Sets new value
        m_animationPlayer = animationPlayer;

        // Connects signals
        m_animationPlayerStateChangedSlot = m_animationPlayer->m_stateChanged.connect
        (
            std::bind
            (
                &SkeletalAnimationComponent::onAnimationPlayerStateChanged,
                this
            )
        );

        // Upload default animation matrix values
        m_wasRunning = true;
        onAnimationPlayerStateChanged();
    }

    void SkeletalAnimationComponent::onAnimationPlayerStateChanged()
    {
        if (m_animationPlayer.get() == nullptr)
            return;
        if (m_animationPlayer->getState() == Animation::AnimationPlayer::State::STOPPED)
        {
            if (m_wasRunning)
            {
                // Uploads default animation matrices
                auto boneTransformations = m_animationPlayer->getResult();
                uploadBoneTransformations(boneTransformations);
                m_wasRunning = false;
            }
        }
        else if (m_animationPlayer->getState() == Animation::AnimationPlayer::State::PLAYING)
        {
            m_wasRunning = true;
        }
    }
}