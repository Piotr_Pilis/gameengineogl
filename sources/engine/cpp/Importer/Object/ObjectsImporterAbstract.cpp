#include "ObjectsImporterAbstract.hpp"

namespace Engine
{
    namespace Importer
    {
        namespace Object
        {
            const std::vector<ObjectsImporterAbstract::ObjectInfo>& ObjectsImporterAbstract::getObjects() const
            {
                return m_objects;
            }

            const std::string& ObjectsImporterAbstract::getLastErrorMessage() const
            {
                return m_lastErrorMessage;
            }
        }
    }
}