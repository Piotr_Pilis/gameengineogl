#pragma once
#include "ObjectsImporterAbstract.hpp"
#include "../../Data/json.h"

namespace Engine
{
    namespace Importer
    {
        namespace Object
        {
            class TerrainObjectsJsonImporter: 
                public ObjectsImporterAbstract
            {
            public:
                TerrainObjectsJsonImporter() = default;
                ~TerrainObjectsJsonImporter() = default;

                bool import(const std::string& inputPath) override;

            private:
                ObjectsImporterAbstract::ObjectInfo extractObjectInfoFromJson(
                    const JSONValue& objectJsonData);

            };
        }
    }
}
