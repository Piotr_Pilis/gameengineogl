#include <sstream>
#include <codecvt>
#include "../../FileSystem/ifstream.hpp"
#include "../../macros.hpp"

#include "TerrainObjectsJsonImporter.hpp"

namespace Engine
{
    namespace Importer
    {
        namespace Object
        {
            bool TerrainObjectsJsonImporter::import(const std::string& inputPath)
            {
                const std::string wrongConfigurationFileStructureErrorMessage = "Wrong structure of configuration file '" + inputPath + "'";
                m_objects.clear();
                try
                {
                    FileSystem::Ifstream fin(inputPath);
                    std::stringstream buffer;
                    buffer << fin.rdbuf();

                    auto jsonDocument = std::shared_ptr<JSONValue>(JSON::parse(buffer.str().c_str()));
                    auto jsonRootObject = jsonDocument->asObject();

                    // Header
                    if (!jsonDocument->hasChild(L"configName") ||
                        !jsonRootObject[L"configName"]->isString() ||
                        jsonRootObject[L"configName"]->asString() != L"Objects3DInWorld")
                    {
                        m_lastErrorMessage = wrongConfigurationFileStructureErrorMessage;
                        return false;
                    }

                    // Content
                    if (!jsonDocument->hasChild(L"objects"))
                    {
                        m_lastErrorMessage = wrongConfigurationFileStructureErrorMessage;
                        return false;
                    }
                    auto objectsJsonData = jsonRootObject[L"objects"];
                    if (!objectsJsonData->isArray())
                    {
                        m_lastErrorMessage = wrongConfigurationFileStructureErrorMessage;
                        return false;
                    }

                    // Array of informations about objects
                    auto& objectsJsonArray = objectsJsonData->asArray();
                    if (objectsJsonArray.size() != 0)
                    {
                        m_objects.reserve(objectsJsonArray.size());
                        for (auto& objectJsonData : objectsJsonArray)
                        {
                            // Object name property
                            ObjectsImporterAbstract::ObjectInfo objectInfo = extractObjectInfoFromJson(*objectJsonData);
                            if (objectInfo.m_modelName == "")
                            {
                                m_lastErrorMessage = wrongConfigurationFileStructureErrorMessage;
                                return false;
                            }
                            m_objects.push_back(objectInfo);
                        }
                    }
                }
                catch (std::invalid_argument invalidArgument)
                {
                    m_lastErrorMessage = "Could not open '" + inputPath + "'";
                    return false;
                }
                return true;
            }

            ObjectsImporterAbstract::ObjectInfo TerrainObjectsJsonImporter::extractObjectInfoFromJson(
                const JSONValue& objectJsonData)
            {
                using convert_typeX = std::codecvt_utf8<wchar_t>;
                std::wstring_convert<convert_typeX, wchar_t> converterX;

                ObjectsImporterAbstract::ObjectInfo objectInfo;
                if (!objectJsonData.isObject())
                {
                    return ObjectsImporterAbstract::ObjectInfo();
                }
                auto objectJsonObject = objectJsonData.asObject();

                // ModelName
                if (!objectJsonData.hasChild(L"modelName"))
                {
                    return ObjectsImporterAbstract::ObjectInfo();
                }
                auto objectFileNameJsonData = objectJsonObject[L"modelName"];
                if (!objectFileNameJsonData->isString())
                {
                    return ObjectsImporterAbstract::ObjectInfo();
                }
                objectInfo.m_modelName = converterX.to_bytes(objectFileNameJsonData->asString());

                // Position
                // Position X
                if (!objectJsonData.hasChild(L"x"))
                {
                    return ObjectsImporterAbstract::ObjectInfo();
                }
                auto objectXJsonData = objectJsonObject[L"x"];
                if (!objectXJsonData->isNumber())
                {
                    return ObjectsImporterAbstract::ObjectInfo();
                }
                objectInfo.m_objectTransformation.m_position.x = static_cast<GLfloat>(objectXJsonData->asNumber());

                // Position Y
                if (!objectJsonData.hasChild(L"y"))
                {
                    return ObjectsImporterAbstract::ObjectInfo();
                }
                auto objectYJsonData = objectJsonObject[L"y"];
                if (!objectYJsonData->isNumber())
                {
                    return ObjectsImporterAbstract::ObjectInfo();
                }
                objectInfo.m_objectTransformation.m_position.y = static_cast<GLfloat>(objectYJsonData->asNumber());

                // Position Z
                if (!objectJsonData.hasChild(L"z"))
                {
                    return ObjectsImporterAbstract::ObjectInfo();
                }
                auto objectZJsonData = objectJsonObject[L"z"];
                if (!objectZJsonData->isNumber())
                {
                    return ObjectsImporterAbstract::ObjectInfo();
                }
                objectInfo.m_objectTransformation.m_position.z = static_cast<GLfloat>(objectZJsonData->asNumber());

                // Rotation
                // Yaw
                if (!objectJsonData.hasChild(L"yaw"))
                {
                    return ObjectsImporterAbstract::ObjectInfo();
                }
                auto objectYawJsonData = objectJsonObject[L"yaw"];
                if (!objectYawJsonData->isNumber())
                {
                    return ObjectsImporterAbstract::ObjectInfo();
                }
                objectInfo.m_objectTransformation.m_rotation.x = static_cast<GLfloat>(objectYawJsonData->asNumber());

                // Pitch
                if (!objectJsonData.hasChild(L"pitch"))
                {
                    return ObjectsImporterAbstract::ObjectInfo();
                }
                auto objectPitchJsonData = objectJsonObject[L"pitch"];
                if (!objectPitchJsonData->isNumber())
                {
                    return ObjectsImporterAbstract::ObjectInfo();
                }
                objectInfo.m_objectTransformation.m_rotation.y = static_cast<GLfloat>(objectPitchJsonData->asNumber());

                // Roll
                if (!objectJsonData.hasChild(L"roll"))
                {
                    return ObjectsImporterAbstract::ObjectInfo();
                }
                auto objectRollJsonData = objectJsonObject[L"roll"];
                if (!objectRollJsonData->isNumber())
                {
                    return ObjectsImporterAbstract::ObjectInfo();
                }
                objectInfo.m_objectTransformation.m_rotation.z = static_cast<GLfloat>(objectRollJsonData->asNumber());

                // Scale
                // Scale X
                if (!objectJsonData.hasChild(L"scaleX"))
                {
                    return ObjectsImporterAbstract::ObjectInfo();
                }
                auto objectScaleXJsonData = objectJsonObject[L"scaleX"];
                if (!objectScaleXJsonData->isNumber())
                {
                    return ObjectsImporterAbstract::ObjectInfo();
                }
                objectInfo.m_objectTransformation.m_scale.x = static_cast<GLfloat>(objectScaleXJsonData->asNumber());

                // Scale Y
                if (!objectJsonData.hasChild(L"scaleY"))
                {
                    return ObjectsImporterAbstract::ObjectInfo();
                }
                auto objectScaleYJsonData = objectJsonObject[L"scaleY"];
                if (!objectScaleYJsonData->isNumber())
                {
                    return ObjectsImporterAbstract::ObjectInfo();
                }
                objectInfo.m_objectTransformation.m_scale.y = static_cast<GLfloat>(objectScaleYJsonData->asNumber());

                // Scale Z
                if (!objectJsonData.hasChild(L"scaleZ"))
                {
                    return ObjectsImporterAbstract::ObjectInfo();
                }
                auto objectScaleZJsonData = objectJsonObject[L"scaleZ"];
                if (!objectScaleZJsonData->isNumber())
                {
                    return ObjectsImporterAbstract::ObjectInfo();
                }
                objectInfo.m_objectTransformation.m_scale.z = static_cast<GLfloat>(objectScaleZJsonData->asNumber());

                return objectInfo;
            }
        }
    }
}