#pragma once
#include <glm/vec3.hpp>
#include <string>
#include <vector>

namespace Engine
{
    namespace Importer
    {
        namespace Object
        {
            /*
            Imports(restores!) information about object instances (mesh path, material path and their positions).
            */
            class ObjectsImporterAbstract
            {
            public:
                struct ObjectTransformationInfo
                {
                    glm::vec3 m_position;
                    glm::vec3 m_rotation;
                    glm::vec3 m_scale;
                };
                struct ObjectInfo
                {
                    std::string m_modelName = "";
                    ObjectTransformationInfo m_objectTransformation;
                };

            public:
                ObjectsImporterAbstract() = default;
                ~ObjectsImporterAbstract() = default;

                virtual bool import(const std::string& inputFileName) = 0;

                // Getters
                const std::vector<ObjectInfo>& getObjects() const;
                const std::string& getLastErrorMessage() const;

            protected:
                std::vector<ObjectInfo> m_objects;
                std::string m_lastErrorMessage;

            };
        }
    }
}
