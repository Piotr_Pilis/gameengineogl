#include <boost/algorithm/string/replace.hpp>
#include <boost/algorithm/string.hpp>
#include <magic_enum.hpp>

#include "error.hpp"

namespace Engine::Types
{
    Error::Error(Code code, const std::string& message) :
        m_code(code),
        m_message(message)
    {
    }

    Error::Code Error::getCode() const
    {
        return m_code;
    }

    std::string Error::getName() const
    {
        return toCodeName(m_code);
    }

    const std::string& Error::getMessage() const
    {
        return m_message;
    }

    bool Error::operator==(Code code)
    {
        return m_code == code;
    }

    std::string Error::toCodeName(Code code)
    {
        // Get name from enum value
        std::string codeName = std::string(magic_enum::enum_name(code));

        // Convert underscore to space
        boost::replace_all
        (
            codeName,
            "_",
            " "
        );

        // To lowercase
        boost::algorithm::to_lower(codeName);

        // Fiest letter to uppercase
        if (codeName.size() > 0)
        {
            codeName[0] = std::toupper(codeName[0]);
        }

        // Result
        return codeName;
    }
}