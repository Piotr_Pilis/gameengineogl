#pragma once
#include <string>

namespace Engine::Types
{
    class Error
    {
    public:
        enum class Code
        {
            UNDEFINED = 1,
            CORRUPTED_ASSETS
        };

    public:
        Error() = delete;
        Error(Code code, const std::string& message);

        Code getCode() const;
        std::string getName() const;
        const std::string& getMessage() const;

        bool operator==(Code code);

        static std::string toCodeName(Code code);

    private:
        Code m_code;
        std::string m_message;


    };
    using ErrorCode = Error::Code;
}