#pragma once
#include <type_traits>
#include <functional>
#include <stdexcept>
#include <variant>
#include <fmt/format.h>

#include "Log/criticalLog.hpp"
#include "Types/error.hpp"

namespace Engine::Types
{
    template<class T>
    class [[nodiscard]] Result
    {
    public:
        using Type = T;

    public:
        /* Exceptions */
        class BadAccess : public std::logic_error
        {
        public:
            explicit BadAccess (const std::string&& message) : std::logic_error(message)
            {}
        };

    public:
        Result(Type&& value) :
            m_data(std::move(value))
        {}

        Result(const Type& value) :
            m_data(value)
        {}

        Result(Error error) : 
            m_data(std::move(error))
        {}

        template<class... Args>
        Result(std::in_place_type_t<Type>, Args&& ...args) : 
            m_data(std::in_place_type<Type>, std::forward<Args>(args)...)
        {}

        // Implicit initialization
        template<class Arg, bool = requires{ !std::is_same_v<Arg, Type>; } >
        Result(const Arg& arg) :
            m_data(arg)
        {}

        Result(const Result&) = default;
        Result(Result&&) = default;
        Result& operator=(const Result&) = default;
        Result& operator=(Result&&) = default;
        ~Result() = default;

        bool isOk() const
        {
            return std::holds_alternative<Type>(m_data);
        }

        explicit operator bool() const
        {
            return isOk();
        }

        bool operator==(Error::Code code)
        {
            return !isOk() && error() == code;
        }

        const Type* operator->() const
        {
            checkAccess();
            return &std::get<Type>(m_data);
        }

        Type* operator->()
        {
            checkAccess();
            return &std::get<Type>(m_data);
        }

        const Type& operator*() const
        {
            checkAccess();
            return std::get<Type>(m_data);
        }

        Type& operator*()
        {
            checkAccess();
            return std::get<Type>(m_data);
        }

        const Error& error() const&
        {
            if (isOk())
            {
                throw BadAccess("Result holds value");
            }
            return std::get<Error>(m_data);
        }

        Error&& error()&&
        {
            if (isOk())
            {
                throw BadAccess("Result holds value");
            }
            return std::move(std::get<Error>(m_data));
        }

        const Type& value() const&

        {
            checkAccess();
            return std::get<Type>(m_data);
        }

        Type& value()&
        {
            checkAccess();
            return std::get<Type>(m_data);
        }

        Type&& value()&&
        {
            checkAccess();
            return std::move(std::get<Type>(m_data));
        }

        const Type& value(const Type& defaultValue) const&
        {
            return isOk() ? value() : defaultValue;
        }

        const Type& valueOr(const Type& defaultValue) const&
        {
            return isOk() ? value() : defaultValue;
        }

        // nodiscard will be ignored
        void ignore() {}

    private:
        void checkAccess() const
        {
            if (!isOk())
            {
                throw BadAccess("Result holds error");
            }
        }

    private:
        std::variant<Type, Error> m_data;
        
    };

    struct Ok {};
    using Status = Result<Ok>;
}

#define ENSURE(expression)                                          \
    if(auto result = (expression); !result)                         \
        return std::move(result).error();

#define UNWRAP(expression)                                          \
({                                                                  \
    auto result = (expression);                                     \
    if (!result) return std::move(result).error();                  \
    std::move(result.value());                                      \
})

// If error then break app running with critical error
#define ENSURE_CRITICAL(expression, errorMessage, ...)              \
    auto result = (expression);                                     \
    if(!result)                                                     \
    {                                                               \
        auto error = expression.error();                            \
        CRITICAL_LOG                                                \
        (                                                           \
            error.getName(),                                        \
            fmt::format(errorMessage, __VA_ARGS__)                  \
        );                                                          \
    }

#define UNWRAP_CRITICAL(expression, errorMessage, ...)              \
({                                                                  \
    auto result = (expression);                                     \
    if(!result)                                                     \
    {                                                               \
        auto error = expression.error();                            \
        CRITICAL_LOG                                                \
        (                                                           \
            error.getName(),                                        \
            fmt::format(errorMessage, __VA_ARGS__)                  \
        );                                                          \
    }                                                               \
    std::move(result.value());                                      \
})

// With error handler
#define ENSURE_HANDLE(expression, handleError)                      \
    if(auto result = (expression); !result)                         \
        return handleError(std::move(result).error());

#define UNWRAP_HANDLE(expression, handleError)                      \
({                                                                  \
    auto result = (expression);                                     \
    if (!result) return handleError(std::move(result).error());     \
    std::move(result.value());                                      \
})
