CMAKE_MINIMUM_REQUIRED(VERSION 3.18.3)
INCLUDE("${CMAKE_DIR}/Project.cmake")

## Libraries ##
# Boost
SET(Boost_USE_STATIC_LIBS ON)
SET(Boost_USE_MULTITHREADED ON)
SET(Boost_USE_STATIC_RUNTIME OFF)

FIND_PACKAGE(Boost CONFIG REQUIRED)
FIND_PACKAGE(glm CONFIG REQUIRED)
FIND_PACKAGE(assimp CONFIG REQUIRED)
FIND_PACKAGE(fmt CONFIG REQUIRED)
FIND_PACKAGE(magic_enum CONFIG REQUIRED)
FIND_PACKAGE(gli CONFIG REQUIRED)
FIND_PACKAGE(PhysFS CONFIG REQUIRED)

# Project
MAKE_LIBRARY_PROJECT(
    PROJECT_NAME
        engine
    ADDITIONAL_LINK_LIBRARIES
        Boost::headers
        glm::glm
        assimp::assimp
        fmt::fmt
        magic_enum::magic_enum
        gli
        physfs-static
    LIBRARIES
        opengl
)