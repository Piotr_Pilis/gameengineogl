import QtQuick 2.7
import QtQuick.Window 2.3
import "qrc:/qml/navigation"
import "qrc:/qml/workspace"

Rectangle
{
    id: windowWorkspace
    color: "#313131"
    z: -3

    Component.onCompleted:
    {
        mainWindow.SetMinimumWidth(Screen.width / 1.3);
        mainWindow.SetMinimumHeight(Screen.height / 1.3);
    }

    // ------------ Top Menu ------------
    TopMenu
    {
        id: topMenu
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        height: 60
    }

     // ------------ Middle Menu ------------
    MiddleMenu
    {
        id: middleMenu
        anchors.top: topMenu.bottom
        anchors.left: leftMenu.right
        anchors.right: rightMenu.left
        height: topMenu.height * 1.6
        z: 2
    }

    // ------------ Left Menu ------------
    LeftMenu
    {
        id: leftMenu
        anchors.top: topMenu.bottom
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        width: rightMenu.width * 0.85
        z: 3
    }

    // ------------ Right Menu ------------
    RightMenu
    {
        id: rightMenu
        anchors.top: topMenu.bottom
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        width: 230
        color: leftMenu.color
        z: 3
    }

    // ------------ Workspace ------------
    Workspace
    {
        id: workspace
        anchors.top: middleMenu.bottom
        anchors.bottom: bottomBar.top
        anchors.left: leftMenu.right
        anchors.right: rightMenu.left
        color: "transparent"
        z: 1
    }

    BottomBar
    {
        id: bottomBar
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        height: 30
        z: 3
    }
}