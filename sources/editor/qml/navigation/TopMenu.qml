import QtQuick 2.7
import QtQuick.Layouts 1.3
import "qrc:/qml/Control"

Rectangle
{
    id: topMenu
    gradient: Gradient {
        GradientStop { position: 1.0; color: "#21252b" }
        GradientStop { position: 0.0; color: "#282c34" }
    }

    Rectangle
    {
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        height: 1
        color: "#424242"
    }

    Rectangle
    {
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        height: 1
        color: "#454545"
    }

    RowLayout
    {
        id: buttonsLayout
        property int leftMargin: 7
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 5
        IconButtonWithText
        {
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            Layout.leftMargin: buttonsLayout.leftMargin
            width: height
            enabled: true
            iconSource: "qrc:/Images/newproject_button.png"
            title: "New Project"
        }

        IconButtonWithText
        {
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            Layout.leftMargin: buttonsLayout.leftMargin
            width: height
            enabled: true
            iconSource: "qrc:/Images/openproject_button.png"
            title: "Open Project"
        }

        IconButtonWithText
        {
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            Layout.leftMargin: buttonsLayout.leftMargin
            width: height
            enabled: true
            iconSource: "qrc:/Images/saveproject_button.png"
            title: "Save Project"
        }

        VerticalLine 
        {
            width: 30
        }

        IconButtonWithText
        {
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            width: height
            enabled: true
            iconSource: "qrc:/Images/gridmode_button.png"
            title: "Wireframe"

            onClicked:
            {
                sceneController.SetTerrainGridVisibility(true);
            }
        }

        IconButtonWithText
        {
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            Layout.leftMargin: buttonsLayout.leftMargin
            width: height
            enabled: true
            iconSource: "qrc:/Images/fillmode_button.png"
            title: "Solid"

            onClicked:
            {
                sceneController.SetTerrainGridVisibility(false);
            }
        }
    }
}