import QtQuick 2.7
import QtQuick.Layouts 1.3
import QtGraphicalEffects 1.0
import "qrc:/qml/control"

Rectangle
{
    id: leftMenu
    color: "#21252b"

    ColumnLayout
    {
        id: root
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right

        anchors.topMargin: 15
        anchors.leftMargin: 10
        anchors.rightMargin: 0
        
        NavbarHeader
        {
            id: cameraTransformHeader
            text: "Camera transformation"
            Layout.bottomMargin: 5
            topOffset: 20
        }

        RealInput
        {
            id: cameraPositionXInput
            width: parent.width * 0.45
            buttonsWidth: width * 0.2
            Layout.bottomMargin: 15

            numDecimalDigits: 1
            labelText: "X"
            inputText: "0.0"
        }

        RealInput
        {
            id: cameraPositionYInput
            width: parent.width * 0.45
            buttonsWidth: width * 0.2
            Layout.bottomMargin: cameraPositionXInput.Layout.bottomMargin

            numDecimalDigits: 1
            labelText: "Y"
            inputText: "0.0"
        }

        RealInput
        {
            id: cameraPositionZInput
            width: parent.width * 0.45
            buttonsWidth: width * 0.2

            numDecimalDigits: 1
            labelText: "Z"
            inputText: "0.0"
        }

        RealInput
        {
            id: cameraYawInput
            anchors.top: cameraPositionXInput.top
            anchors.right: parent.right
            anchors.rightMargin: parent.anchors.leftMargin
            width: parent.width * 0.45
            buttonsWidth: width * 0.2

            numDecimalDigits: 2
            step: 0.1
            labelText: "Yaw"
            inputText: "0.0"
        }

        RealInput
        {
            id: cameraTiltInput
            anchors.top: cameraPositionYInput.top
            anchors.right: parent.right
            anchors.rightMargin: parent.anchors.leftMargin
            width: parent.width * 0.45
            buttonsWidth: width * 0.2

            numDecimalDigits: 2
            step: 0.1
            labelText: "Tilt"
            inputText: "0.0"
        }

        StandardButton
        {
            id: setCurrentCameraTransformationButton
            anchors.top: cameraPositionZInput.bottom
            anchors.topMargin: cameraPositionZInput.height + 7
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.rightMargin: parent.anchors.leftMargin
            labelText: "get current transformation"

            onButtonClicked:
            {
                cameraPositionXInput.setValue(sceneController.GetCameraPostionX());
                cameraPositionYInput.setValue(sceneController.GetCameraPostionY());
                cameraPositionZInput.setValue(sceneController.GetCameraPostionZ());

                cameraYawInput.setValue(sceneController.GetCameraYaw());
                cameraTiltInput.setValue(sceneController.GetCameraTilt());
            }
        }

        StandardButton
        {
            id: applyCameraTransformationButton
            anchors.top: setCurrentCameraTransformationButton.bottom
            anchors.topMargin: 10
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.rightMargin: parent.anchors.leftMargin
            labelText: "apply"

            onButtonClicked:
            {
                sceneController.SetCameraPostion(
                    cameraPositionXInput.getValue(), 
                    cameraPositionYInput.getValue(), 
                    cameraPositionZInput.getValue()
                );

                sceneController.SetCameraRotation(
                    cameraYawInput.getValue(), 
                    cameraTiltInput.getValue()
                );
            }
        }

        /*NavbarHeader
        {
            id: somethingHeader
            Layout.topMargin: 30
            topOffset: cameraPosition.topOffset
        }*/
    }
}