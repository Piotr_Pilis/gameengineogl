import QtQuick 2.7
import QtQuick.Layouts 1.3
import "qrc:/qml/control"

Rectangle
{
    id: bottomBar
    property alias message: messageLabel.text
    color: "#21252b"
    Rectangle
    {
        id: topTopMargin
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: parent.top
        color: "#1f2228"
        height: 1
    }

    Rectangle
    {
        id: bottomTopMargin
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: topTopMargin.bottom
        color: "#3d434f"
        height: 1
    }

    Text 
    {
        id: messageLabel
        text: "Ready"
        anchors.left: parent.left
        anchors.verticalCenter: parent.verticalCenter
        anchors.leftMargin: 6
        font.family: "Lucida Sans Unicode"
        font.pointSize: 7
        color: "#85958e"
    }

    Text 
    {
        id: versionLabel
        text: "v0.0.2"
        anchors.right: parent.right
        anchors.verticalCenter: parent.verticalCenter
        anchors.rightMargin: 15
        font.family: "Lucida Sans Unicode"
        font.pointSize: 7
        color: "#85958e"
    }
}