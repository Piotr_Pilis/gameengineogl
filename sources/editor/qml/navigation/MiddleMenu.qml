import QtQuick 2.7
import QtQuick.Layouts 1.3
import QtGraphicalEffects 1.0
import "qrc:/qml/control"

Rectangle
{
    id: middleMenu
    color: "#333842"

    Rectangle
    {
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        height: 1
        color: "#303030"
    }

    DropShadow 
    {
        anchors.fill: parent
        horizontalOffset: 0
        verticalOffset: 2
        radius: 10.0
        samples: 16
        color: "#80101010"
        z: -1
    }

    RowLayout
    {
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        IconButton
        {
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            width: 123
            iconSource: "qrc:/Images/heightstage_button.png"
        }

        IconButton
        {
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            width: 123
            enabled: true
            iconSource: "qrc:/Images/objectstage_button.png"
        }

        IconButton
        {
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            width: 123
            enabled: true
            iconSource: "qrc:/Images/paintstage_button.png"
        }
    }
}