import QtQuick 2.7
import QtGraphicalEffects 1.0
import "qrc:/js/Math.js" as Math

Item
{
    id: root

    property real buttonsWidth: 30
    property alias maxValue: inputValidator.top
    property alias minValue: inputValidator.bottom
    property alias numDecimalDigits: inputValidator.decimals
    property real step: 1.0
    property alias inputText: input.text
    property alias labelText: label.text

    signal valueChanged(real value)

    function setValue(value)
    {
        if (value > input.validator.top || value < input.validator.bottom)
        {
            input.text = previousText
        }
        else
        {
            value = value.round(root.numDecimalDigits);
            input.text = String(value);
            root.valueChanged(value);
        }
    }

    function getValue()
    {
        return Number(input.text);
    } 

    width: 100
    height: 18

    Text
    {
        id: label
        anchors.left: parent.left
        anchors.leftMargin: 10
        text: ""
        font.pointSize: 9
        color: "#d5e0f4"
    }

    DropShadow 
    {
        anchors.fill: label
        horizontalOffset: 1
        verticalOffset: 0
        radius: 12.0
        samples: 16
        source: label
        color: "black"
    }

    Rectangle
    {
        id: backgroundButton
        anchors.top: label.bottom
        anchors.topMargin: 2
        width: parent.width - root.buttonsWidth
        height: parent.height
        radius: 10
        color: "#434969"

        DoubleValidator
        {
            id: inputValidator
            top: 999999999.0
            bottom: -999999999.0
            decimals: 2
        }

        TextInput
        {
            id: input
            property string previousText: ""

            clip: true
            enabled: root.enabled
            anchors.fill: parent
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.pixelSize: 10
            selectByMouse: true
            validator: inputValidator
            color: "white"
            text: "0.0"
            activeFocusOnTab: true

            onTextChanged:
            {
                var numericValue = root.getValue();
                if (numericValue > input.validator.top || numericValue < input.validator.bottom)
                {
                    input.text = previousText
                }
                if(!doesContainDecimalPoint())
                {
                    input.text += ".0";
                }
                previousText = input.text
                root.valueChanged(root.getValue());
            }  

            onEditingFinished:
            {
                if(!doesContainIntegerDigits())
                {
                    input.text = "0" + input.text;
                    previousText = input.text;
                }
            }

            function doesContainDecimalPoint()
            {
                return text.indexOf(".") >= 0; 
            }  
            
            function doesContainIntegerDigits()
            {
                return text.substr(0, text.indexOf('.')).length != 0;
            } 
        }
    }

    DropShadow 
    {
        anchors.fill: backgroundButton
        horizontalOffset: 0
        verticalOffset: 2
        radius: 10.0
        samples: 16
        color: "black"
        source: backgroundButton
    }

    Rectangle
    {
        id: upArrowButton
        anchors.right: parent.right
        anchors.top: backgroundButton.top
        height: parent.height * 0.5
        width: root.buttonsWidth + backgroundButton.width * 0.1
        color: "green"

        Image
        {
            id: upArrowIcon
            smooth: true
            fillMode: Image.PreserveAspectFit
            anchors.verticalCenter: parent.verticalCenter
            anchors.right: parent.right
            anchors.rightMargin: parent.width * 0.075
            height: parent.height / 1.3
            source: "qrc:/Images/uparrow_button.png"
        }

        MouseArea
        {
            id: upArrowMouseArea
            anchors.fill: parent
            cursorShape: "PointingHandCursor"
            hoverEnabled: true

            onClicked: 
            {
                root.setValue(root.getValue() + root.step);
            }

            onEntered: 
            {
                if(!parent.focus)
                    parent.color = "#2aa03c"
            }

            onExited: 
            {
                if(!parent.focus)
                    parent.color = "green"
            }
        }
    }

    Rectangle
    {
        id: downArrowButton
        anchors.right: parent.right
        anchors.top: upArrowButton.bottom
        height: parent.height * 0.5
        width: root.buttonsWidth + backgroundButton.width * 0.1
        color: "red"

        Image
        {
            id: downArrowIcon
            smooth: true
            fillMode: Image.PreserveAspectFit
            anchors.verticalCenter: parent.verticalCenter
            anchors.right: parent.right
            anchors.rightMargin: parent.width * 0.075
            height: parent.height / 1.3
            source: "qrc:/Images/downarrow_button.png"
        }

        MouseArea
        {
            id: downArrowMouseArea
            anchors.fill: parent
            cursorShape: "PointingHandCursor"
            hoverEnabled: true

            onClicked: 
            {
                root.setValue(root.getValue() - root.step);
            }

            onEntered: 
            {
                if(!parent.focus)
                    parent.color = "#ff7171"
            }

            onExited: 
            {
                if(!parent.focus)
                    parent.color = "red"
            }
        }
    }
}