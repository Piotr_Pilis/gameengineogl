import QtQuick 2.7
import QtGraphicalEffects 1.0

Item
{
    id: root
    property alias labelText: label.text

    signal buttonClicked()

    width: 200
    height: 18

    Rectangle
    {
        id: backgroundButton
        width: parent.width
        height: parent.height
        radius: 10
        color: "#434969"

        Text
        {
            id: label
            anchors.centerIn: parent
            text: ""
            font.pointSize: 9
            color: "#d5e0f4"
        }

        DropShadow 
        {
            anchors.fill: label
            horizontalOffset: 1
            verticalOffset: 0
            radius: 12.0
            samples: 16
            source: label
            color: "black"
        }

        MouseArea
        {
            id: mouseArea
            anchors.fill: parent
            cursorShape: "PointingHandCursor"
            hoverEnabled: true

            onClicked: 
            {
                root.buttonClicked();
            }

            onEntered: 
            {
                if(!parent.focus)
                    parent.color = "#636c9b"
            }

            onExited: 
            {
                if(!parent.focus)
                    parent.color = "#434969"
            }
        }
    }

    DropShadow 
    {
        anchors.fill: backgroundButton
        horizontalOffset: 0
        verticalOffset: 2
        radius: 10.0
        samples: 16
        color: "black"
        source: backgroundButton
    }
}