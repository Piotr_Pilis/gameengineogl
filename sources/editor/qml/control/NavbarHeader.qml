import QtQuick 2.7
import QtQuick.Shapes 1.0
import QtGraphicalEffects 1.0

Rectangle
{
    id: root
    property int topOffset: 0
    property alias text: title.text
    color: "transparent"
    anchors.left: parent.left
    anchors.right: parent.right
    height: 25

    Shape 
    {
        id: shape
        anchors.fill: parent
        ShapePath 
        {
            strokeWidth: 0
            fillColor: "#434969"
            startX: root.topOffset; startY: 0
            PathLine { x: shape.width; y: 0 }
            PathLine { x: shape.width; y: shape.height }
            PathLine { x: 0; y: shape.height  }
        }

        Text
        {
            id: title
            anchors.verticalCenter: parent.verticalCenter
            anchors.right: parent.right
            anchors.rightMargin: 15
            text: ""
            font.pointSize: 9
            color: "#d5e0f4"
        }

        DropShadow 
        {
            anchors.fill: title
            horizontalOffset: 1
            verticalOffset: 0
            radius: 12.0
            samples: 16
            source: title
            color: "black"
        }
    }

    Rectangle
    {
        anchors.top: shape.bottom
        anchors.right: shape.right
        anchors.left: shape.left
        height: 2
        color: "black"

        Rectangle
        {
            anchors.bottom: parent.bottom
            anchors.right: parent.right
            anchors.left: parent.left
            height: 1
            color: "#99ffffff"
        }
    }

    DropShadow 
    {
        anchors.fill: shape
        horizontalOffset: -1
        verticalOffset: 0
        radius: 7.0
        samples: 16
        source: shape
        color: "#566071"        
    }
}