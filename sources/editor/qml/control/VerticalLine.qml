import QtQuick 2.7
import QtQuick.Layouts 1.3

Item
{
    width: 100
    anchors.top: parent.top
    anchors.bottom: parent.bottom
    Rectangle
    {
        anchors.left: parent.left
        anchors.leftMargin: parent.width / 2
        anchors.bottom: parent.bottom
        anchors.bottomMargin: parent.height * 0.15
        height: parent.height * 0.7
        width: 2
        color: "#151515"
        Rectangle
        {
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            width: 1
            color: "#424242"
        }
    }
}