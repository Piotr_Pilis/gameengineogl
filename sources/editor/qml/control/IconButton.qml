import QtQuick 2.8
import QtQuick.Controls 2.1
import QtQuick.Controls.Styles 1.4
import QtGraphicalEffects 1.0

Rectangle
{
    id: root
    property alias iconSource: icon.source
    signal clicked();

    width: 30
    height: 30
    color: "transparent"

    Image
    {
        id: icon
        smooth: true
        fillMode: Image.PreserveAspectFit
        anchors.centerIn: parent
    }

    Colorize 
    {
        visible: !enabled
        anchors.fill: icon
        source: icon
        hue: 0.0
        saturation: 0.0
        lightness: -0.5
    }

    DropShadow 
    {
        id: iconShadow
        visible: false
        anchors.fill: icon
        horizontalOffset: 1
        verticalOffset: 1
        radius: 5.0
        samples: 17
        color: "#BBBBBB"
        source: icon
    }

    MouseArea{
        anchors.fill: parent
        hoverEnabled: true
        cursorShape: "PointingHandCursor"

        onEntered: 
        {
            iconShadow.visible = true;
        }

        onExited: 
        {
            iconShadow.visible = false;
        }

        onClicked: 
        {
            root.clicked();
        }
    }
}