import QtQuick 2.8
import QtQuick.Controls 2.1
import QtQuick.Controls.Styles 1.4
import QtGraphicalEffects 1.0

Rectangle
{
    id: root
    property alias title: title.text
    property alias fontSize: title.font.pointSize
    property alias iconSource: icon.source
    signal clicked();
    color: "transparent"

    Image
    {
        id: icon
        smooth: true
        fillMode: Image.PreserveAspectFit
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: title.top
    }

    Text 
    {
        id: title
        text: ""
        anchors.bottom: parent.bottom
        anchors.horizontalCenter: icon.horizontalCenter
        font.family: "Lucida Sans Unicode"
        font.pointSize: 7
        color: "#bcc6d8"
    }

    Colorize 
    {
        visible: !enabled
        anchors.fill: icon
        source: icon
        hue: 0.0
        saturation: 0.0
        lightness: -0.5
    }

    DropShadow 
    {
        id: iconShadow
        visible: false
        anchors.fill: icon
        horizontalOffset: 1
        verticalOffset: 1
        radius: 5.0
        samples: 17
        color: "#BBBBBB"
        source: icon
    }

    MouseArea{
        anchors.fill: parent
        hoverEnabled: true
        cursorShape: "PointingHandCursor"

        onEntered: 
        {
            iconShadow.visible = true;
        }

        onExited: 
        {
            iconShadow.visible = false;
        }

        onClicked: 
        {
            root.clicked();
        }
    }
}