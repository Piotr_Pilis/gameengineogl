import QtQuick 2.7
import Editor.Dialogs 1.0
import "qrc:/qml/control"

Rectangle
{
    id: workspace

    SceneView
    {
        id: sceneView0
        anchors.fill: parent
        mouseArea: sceneView0MouseArea

        MouseArea
        {
            id: sceneView0MouseArea
            anchors.fill: parent
            acceptedButtons: Qt.LeftButton | Qt.RightButton

            onClicked:
            {
                focus = true;
            }
        }
    }

    Text 
    {
        id: cameraTransformLabel
        text: "Camera position\nX: " + sceneView0.cameraPositionX.toFixed(2) + "\nY: " + sceneView0.cameraPositionY.toFixed(2) + "\nZ: " + sceneView0.cameraPositionZ.toFixed(2) + "\n\nCamera rotation\nYaw: " + sceneView0.cameraYaw + "\xB0\nTilt:  " + sceneView0.cameraTilt + "\xB0"
        anchors.left: sceneView0.left
        anchors.top: sceneView0.top
        anchors.leftMargin: 20
        anchors.topMargin: 10
        font.family: "Lucida Sans Unicode"
        font.pointSize: 8
        color: "#ffffff"
    }

    Rectangle
    {
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        width: 1
        color: "#FAFAFA"
    }

    Rectangle
    {
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        width: 1
        color: "#FAFAFA"
    }
} 