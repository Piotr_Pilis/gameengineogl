#include <QStyleOption>
#include <QHBoxLayout>
#include <QPushButton>
#include <QPaintEvent>
#include <QPainter>
#include <QMenuBar>
#include "titleBar.hpp"

namespace Editor
{
    namespace Dialog
    {
        TitleBar::TitleBar(QWidget* parent) :
            QWidget(parent),
            m_layout(new QHBoxLayout()),
            m_iconButton(new QPushButton(this)),
            m_menuBar(new QMenuBar(this)),
            m_minimizeButton(new QPushButton(this)),
            m_maximizeButton(new QPushButton(this)),
            m_restoreButton(new QPushButton(this)),
            m_closeButton(new QPushButton(this))
        {
            m_layout->setSpacing(0);
            m_layout->setContentsMargins(0, 0, 0, 0);

            this->setFixedHeight(30);
            this->setObjectName("windowTitleBar");
            this->setContentsMargins(0, 0, 0, 0);
            this->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
            this->setLayout(m_layout);

            // Icon Button
            m_iconButton->setObjectName("titleBarIcon");
            m_iconButton->setFocusPolicy(Qt::NoFocus);
            connect(
                m_iconButton, 
                &QPushButton::clicked,
                [this]() { emit iconButtonClicked(); }
            );

            // Menu Bar
            m_menuBar->setObjectName("menuBar");
            m_menuBar->setNativeMenuBar(true);
            createMenuBar();

            // Minimize Button
            m_minimizeButton->setFixedSize(36, 29);
            m_minimizeButton->setObjectName("minimizeButton");
            m_minimizeButton->setToolTip(tr("Minimize"));
            m_minimizeButton->setFocusPolicy(Qt::NoFocus);
            connect(
                m_minimizeButton,
                &QPushButton::clicked,
                [this]() { emit minimizeButtonClicked(); }
            );

            // Maximize Button
            m_maximizeButton->setFixedSize(36, 29);
            m_maximizeButton->setObjectName("maximizeButton");
            m_maximizeButton->setToolTip(tr("Maximize"));
            m_maximizeButton->setFocusPolicy(Qt::NoFocus);
            connect(
                m_maximizeButton,
                &QPushButton::clicked,
                [this]() { emit maximizeButtonClicked(); }
            );

            // Restore Button
            m_restoreButton->setFixedSize(36, 29);
            m_restoreButton->setObjectName("restoreButton");
            m_restoreButton->setToolTip(tr("Restore"));
            m_restoreButton->setFocusPolicy(Qt::NoFocus);
            m_restoreButton->setVisible(false);
            connect(
                m_restoreButton,
                &QPushButton::clicked,
                [this]() { emit restoreButtonClicked(); }
            );

            // Close Button
            m_closeButton->setFixedSize(36, 29);
            m_closeButton->setObjectName("closeButton");
            m_closeButton->setToolTip(tr("Close"));
            m_closeButton->setFocusPolicy(Qt::NoFocus);
            connect(
                m_closeButton,
                &QPushButton::clicked,
                [this]() { emit closeButtonClicked(); }
            );

            m_layout->addWidget(m_iconButton);
            m_layout->addWidget(m_menuBar);
            m_layout->addStretch(1);

            m_layout->addWidget(m_minimizeButton);
            m_layout->addWidget(m_maximizeButton);
            m_layout->addWidget(m_restoreButton);
            m_layout->addWidget(m_closeButton);

        }

        void TitleBar::createMenuBar()
        {
            auto dummyAction = new QAction(tr("&Dummy"));
            dummyAction->setObjectName("menu");
            dummyAction->setEnabled(false);

            auto fileMenu = new QMenu(tr("&File"), m_menuBar);
            fileMenu->setObjectName("menu");
            {
                fileMenu->addAction(tr("&New"))->setShortcut(QKeySequence(Qt::CTRL + Qt::Key_N));
                fileMenu->addAction(tr("&Open"))->setShortcut(QKeySequence(Qt::CTRL + Qt::Key_O));
                fileMenu->addMenu(tr("&Open recents"))->addAction(dummyAction);
                fileMenu->addSeparator();

                fileMenu->addAction(tr("&Save"))->setShortcut(QKeySequence(Qt::CTRL + Qt::Key_S));
                fileMenu->addAction(tr("&Save as..."));
                fileMenu->addSeparator();

                fileMenu->addAction(tr("&Exit"))->setShortcut(QKeySequence(Qt::ALT + Qt::Key_Q));
            }

            auto editMenu = new QMenu(tr("&Edit"), m_menuBar);
            editMenu->setObjectName("menu");
            {
                editMenu->addAction(tr("&Undo"))->setShortcut(QKeySequence(Qt::CTRL + Qt::Key_Z));
                editMenu->addAction(tr("&Redo"))->setShortcut(QKeySequence(Qt::CTRL + Qt::Key_Y));
                editMenu->addSeparator();

                editMenu->addAction(tr("&Delete"))->setShortcut(QKeySequence(Qt::Key_Delete));
            }

            auto aboutMenu = new QMenu(tr("&About"), m_menuBar);
            aboutMenu->setObjectName("menu");
            {
                aboutMenu->addAction(tr("&Version"));
            }

            m_menuBar->addMenu(fileMenu);
            m_menuBar->addMenu(editMenu);
            m_menuBar->addMenu(aboutMenu);
        }

        void TitleBar::setIcon(const QIcon& icon, const QSize& iconSize)
        {
            m_iconButton->setIcon(icon);
            m_iconButton->setIconSize(iconSize);
            m_iconButton->setBaseSize(iconSize);
        }

        void TitleBar::paintEvent(QPaintEvent* event)
        {
            QStyleOption styleOption;
            styleOption.initFrom(this);
            QPainter painter(this);
            style()->drawPrimitive(QStyle::PE_Widget, &styleOption, &painter, this);
            QWidget::paintEvent(event);
        }
    }
}