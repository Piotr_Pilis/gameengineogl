#pragma once
#include <QWidget>
#include <QIcon>
#include <QSize>

class QHBoxLayout;
class QPushButton;
class QPaintEvent;
class QMenuBar;

namespace Editor
{
    namespace Dialog
    {
        class TitleBar : public QWidget
        {
            Q_OBJECT;

        public:
            TitleBar(QWidget* parent = nullptr); 

            void setIcon(const QIcon& icon, const QSize& iconSize);

        signals:
            void iconButtonClicked();
            void minimizeButtonClicked();
            void maximizeButtonClicked();
            void restoreButtonClicked();
            void closeButtonClicked();

        private:
            void paintEvent(QPaintEvent* event) override;

            void createMenuBar();

        private:
            QHBoxLayout* m_layout;

            QPushButton* m_iconButton;
            QMenuBar* m_menuBar;
            QPushButton* m_minimizeButton;
            QPushButton* m_maximizeButton;
            QPushButton* m_restoreButton;
            QPushButton* m_closeButton;

            friend class MainWindow;
        };
    }
}