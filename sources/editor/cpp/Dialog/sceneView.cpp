#include <QtMath>
#include "../Controller/sceneRenderer.hpp"
#include "sceneView.hpp"

namespace Editor
{
    using namespace Controller;
    namespace Dialog
    {
        SceneView::SceneView(QQuickItem* parent) :
            QQuickFramebufferObject(parent),
            m_mouseAreaObject(nullptr)
        {    
        }

        SceneView::~SceneView()
        {
        }

        void SceneView::setMouseArea(QObject* object)
        {
            if (m_mouseAreaObject != nullptr)
            {
                m_mouseAreaObject->removeEventFilter(this);
            }
            m_mouseAreaObject = object;
            m_mouseAreaObject->installEventFilter(this);
        }

        QQuickFramebufferObject::Renderer* SceneView::createRenderer() const
        {
            return new SceneRenderer();
        }

        bool SceneView::eventFilter(QObject* object, QEvent* event)
        {
            if (event->type() == QEvent::MouseMove)
            {
                auto mouseEvent = dynamic_cast<QMouseEvent*>(event);
                if (mouseEvent != nullptr)
                    emit mouseMoved(mouseEvent);
            }
            else if (event->type() == QEvent::MouseButtonPress ||
                event->type() == QEvent::MouseButtonRelease)
            {
                auto mouseEvent = dynamic_cast<QMouseEvent*>(event);
                if (mouseEvent != nullptr)
                    emit mouseButtonClicked(mouseEvent);
            }
            return QObject::eventFilter(object, event);
        }

        void SceneView::cameraPositionChange(const qreal& x, const qreal& y, const qreal& z)
        {
            m_cameraPositionX = x;
            m_cameraPositionY = y;
            m_cameraPositionZ = z;
            emit cameraPositionChanged();
        }

        void SceneView::cameraRotationChange(const qreal& yaw, const qreal& tilt)
        {
            m_cameraYaw = yaw;
            m_cameraTilt = tilt;
            emit cameraRotationChanged();
        }

        qint32 SceneView::getCameraYaw() const
        { 
            return static_cast<qint32>(qAbs(m_cameraYaw * 180.0f / M_PI)) % 360;
        }

        qint32 SceneView::getCameraTilt() const
        { 
            return static_cast<qint32>(qAbs(m_cameraTilt * 180.0f / M_PI)) % 360;
        }

    }
}