#include <QWindowStateChangeEvent>
#include <QApplication>
#include <QVBoxLayout>
#include <QQuickWidget>
#include <QQmlContext>
#include <QQmlEngine>
#include <QSettings>
#include <QWidget>
#include <QEvent>

#include <Windows.h>
#include <windowsx.h>
#include <dwmapi.h>

#include "Controller/applicationController.hpp"
#include "mainWindow.hpp"

namespace 
{
    // we cannot just use WS_POPUP style
    // WS_THICKFRAME: without this the window cannot be resized and so aero snap, de-maximizing and minimizing won't work
    // WS_SYSMENU: enables the context menu with the move, close, maximize, minize... commands (shift + right-click on the task bar item)
    // WS_CAPTION: enables aero minimize animation/transition
    // WS_MAXIMIZEBOX, WS_MINIMIZEBOX: enable minimize/maximize
    enum class Style : DWORD 
    {
        windowed = WS_OVERLAPPEDWINDOW | WS_THICKFRAME | WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX | WS_MAXIMIZEBOX,
        aero_borderless = WS_POPUP | WS_THICKFRAME | WS_CAPTION | WS_SYSMENU | WS_MAXIMIZEBOX | WS_MINIMIZEBOX,
        basic_borderless = WS_POPUP | WS_THICKFRAME | WS_SYSMENU | WS_MAXIMIZEBOX | WS_MINIMIZEBOX
    };

    auto Maximized(HWND hwnd) -> bool 
    {
        WINDOWPLACEMENT placement;
        if (!::GetWindowPlacement(hwnd, &placement)) 
        {
            return false;
        }

        return placement.showCmd == SW_MAXIMIZE;
    }

    /* Adjust client rect to not spill over monitor edges when maximized.
    * rect(in/out): in: proposed window rect, out: calculated client rect
    * Does nothing if the window is not maximized.
    */
    auto AdjustMaximizedClientRect(HWND window, RECT& rect) -> void 
    {
        if (!Maximized(window)) 
        {
            return;
        }

        auto monitor = ::MonitorFromWindow(window, MONITOR_DEFAULTTONULL);
        if (!monitor) 
        {
            return;
        }

        MONITORINFO monitor_info{};
        monitor_info.cbSize = sizeof(monitor_info);
        if (!::GetMonitorInfoW(monitor, &monitor_info)) 
        {
            return;
        }

        // when maximized, make the client area fill just the monitor (without task bar) rect,
        // not the whole window rect which extends beyond the monitor.
        rect = monitor_info.rcWork;
    }

    auto CompositionEnabled() -> bool 
    {
        auto composition_enabled = FALSE;
        auto success = ::DwmIsCompositionEnabled(&composition_enabled) == S_OK;
        return composition_enabled && success;
    }

    auto SelectBorderlessStyle() -> Style 
    {
        return CompositionEnabled() ? Style::aero_borderless : Style::basic_borderless;
    }
}

namespace Editor
{
    namespace Dialog
    {
        MainWindow::MainWindow() :
            m_engine(new QQmlEngine(this)),
            m_iconButton(nullptr),
            m_titleBar(new TitleBar(this)),
            m_workspaceWidget(new QQuickWidget(m_engine, this))
        {
            initializeWindow();
            initializeEngine();

            QSettings settings(
                WINDOW_TITLE
            );
            settings.beginGroup("Window");
            {
                restoreGeometry(settings.value("Geometry").toByteArray());
                showNormal();
                if (settings.value("IsMaximized", isMaximized()).toBool())
                {
                    m_titleBar->m_maximizeButton->setVisible(false);
                    m_titleBar->m_restoreButton->setVisible(true);
                    showMaximized();
                }
            }
            settings.endGroup();
        }

        void MainWindow::initializeWindow()
        {
            setBorderless(true);

            auto layout = new QVBoxLayout();
            layout->setAlignment(Qt::AlignTop);
            layout->setSpacing(0);
            layout->setContentsMargins(0, 0, 0, 0);

            m_workspaceWidget->setResizeMode(
                QQuickWidget::ResizeMode::SizeRootObjectToView);
            m_workspaceWidget->setSizePolicy(
                QSizePolicy::Expanding, QSizePolicy::Expanding);
            m_workspaceWidget->setContentsMargins(0, 0, 0, 0);

            layout->addWidget(m_titleBar);
            layout->addWidget(m_workspaceWidget);

            m_iconButton = m_titleBar->m_iconButton;
            connect(
                m_titleBar,
                &TitleBar::maximizeButtonClicked,
                [this]() { emit maximize(); }
            );
            connect(
                m_titleBar,
                &TitleBar::minimizeButtonClicked,
                [this]() { emit minimize(); }
            );
            connect(
                m_titleBar,
                &TitleBar::restoreButtonClicked,
                [this]() { emit restore(); }
            );
            connect(
                m_titleBar,
                &TitleBar::closeButtonClicked,
                [this]() { emit close(); }
            );

            auto centralWidget = new QWidget(this);
            centralWidget->setObjectName("centralWidget");
            centralWidget->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
            centralWidget->setLayout(layout);
            centralWidget->setContentsMargins(0, 0, 0, 0);
            setCentralWidget(centralWidget);

            QSurfaceFormat format;
            format.setMajorVersion(4);
            format.setMinorVersion(6);
            format.setProfile(QSurfaceFormat::CompatibilityProfile);
            format.setDepthBufferSize(24);
            format.setStencilBufferSize(8);
            format.setSamples(4);
            m_workspaceWidget->setFormat(format);
        }

        void MainWindow::initializeEngine()
        {
            QQmlContext* context = m_engine->rootContext();
            context->setContextProperty("mainWindow", this);
        }

        MainWindow& MainWindow::instance()
        {
            static MainWindow instance;
            return instance;
        }

        void MainWindow::setWorkspaceSource(const QUrl& workspaceQmlSource)
        {
            m_workspaceWidget->setSource(workspaceQmlSource);
        }

        void MainWindow::setMinimumWidth(const qint32& width)
        {
            if (this->width() < width)
                this->resize(width, this->height());
            this->setMinimumWidth(width);
        }

        void MainWindow::setMinimumHeight(const qint32& height)
        {
            this->setMinimumHeight(height);
        }

        // Overrided winapi events
        void MainWindow::closeEvent(QCloseEvent* event)
        {
            QSettings settings(
                WINDOW_TITLE
            );
            
            settings.setValue("Window/Geometry", saveGeometry());
            settings.setValue("Window/IsMaximized", isMaximized());
        }

        void MainWindow::mouseMoveEvent(QMouseEvent* event)
        {
            emit mouseMoved(event);
        }

        void MainWindow::mousePressEvent(QMouseEvent* event)
        {
            emit mousePressed(event);
            if (event->button() == Qt::RightButton)
            {
                if (m_iconButton == nullptr)
                    return;
                if (m_iconButton->underMouse())
                {
                    auto hMenu = GetSystemMenu(reinterpret_cast<HWND>(winId()), FALSE);

                    if (hMenu)
                    {
                        MENUITEMINFO mii;
                        mii.cbSize = sizeof(MENUITEMINFO);
                        mii.fMask = MIIM_STATE;
                        mii.fType = 0;

                        // update the options
                        mii.fState = MF_ENABLED;
                        SetMenuItemInfo(hMenu, SC_RESTORE, FALSE, &mii);
                        SetMenuItemInfo(hMenu, SC_SIZE, FALSE, &mii);
                        SetMenuItemInfo(hMenu, SC_MOVE, FALSE, &mii);
                        SetMenuItemInfo(hMenu, SC_MAXIMIZE, FALSE, &mii);
                        SetMenuItemInfo(hMenu, SC_MINIMIZE, FALSE, &mii);

                        mii.fState = MF_GRAYED;

                        WINDOWPLACEMENT wp;
                        GetWindowPlacement(reinterpret_cast<HWND>(winId()), &wp);

                        switch (wp.showCmd)
                        {
                        case SW_SHOWMAXIMIZED:
                            SetMenuItemInfo(hMenu, SC_SIZE, FALSE, &mii);
                            SetMenuItemInfo(hMenu, SC_MOVE, FALSE, &mii);
                            SetMenuItemInfo(hMenu, SC_MAXIMIZE, FALSE, &mii);
                            SetMenuDefaultItem(hMenu, SC_CLOSE, FALSE);
                            break;
                        case SW_SHOWMINIMIZED:
                            SetMenuItemInfo(hMenu, SC_MINIMIZE, FALSE, &mii);
                            SetMenuDefaultItem(hMenu, SC_RESTORE, FALSE);
                            break;
                        case SW_SHOWNORMAL:
                            SetMenuItemInfo(hMenu, SC_RESTORE, FALSE, &mii);
                            SetMenuDefaultItem(hMenu, SC_CLOSE, FALSE);
                            break;
                        default:;
                        }

                        POINT p;
                        if (GetCursorPos(&p))
                        {
                            LPARAM cmd = TrackPopupMenu(hMenu, (TPM_RIGHTBUTTON | TPM_NONOTIFY | TPM_RETURNCMD),
                                p.x, p.y, NULL, reinterpret_cast<HWND>(winId()), Q_NULLPTR);

                            if (cmd) PostMessage(reinterpret_cast<HWND>(winId()), WM_SYSCOMMAND, cmd, 0);
                        }
                    }
                }
            }
        }

        void MainWindow::keyPressEvent(QKeyEvent* event)
        {
            emit keyPressed(event);
        }

        void MainWindow::keyReleaseEvent(QKeyEvent* event)
        {
            emit keyReleased(event);
        }

        bool MainWindow::nativeEvent
        (
            const QByteArray& eventType, 
            void* message, 
            qintptr* result
        )
        {
            Q_UNUSED(eventType);

            auto msg = static_cast<MSG*>(message);

            switch (msg->message)
            {
            case WM_NCCREATE: 
            {
                auto userdata = reinterpret_cast<CREATESTRUCTW*>(msg->lParam)->lpCreateParams;
                // store window instance pointer in window user data
                ::SetWindowLongPtrW(msg->hwnd, GWLP_USERDATA, reinterpret_cast<LONG_PTR>(userdata));
            }

            case WM_ERASEBKGND:
            {
                auto brush = CreateSolidBrush(RGB(48, 48, 48));
                SetClassLongPtr(msg->hwnd, GCLP_HBRBACKGROUND, reinterpret_cast<LONG_PTR>(brush));
            }

            case WM_NCCALCSIZE: 
            {
                if (msg->wParam == TRUE) 
                {
                    auto& params = *reinterpret_cast<NCCALCSIZE_PARAMS*>(msg->lParam);
                    AdjustMaximizedClientRect(msg->hwnd, params.rgrc[0]);
                }
                *result = 0;
                return true;
            }

            case WM_NCHITTEST: 
            {
                *result = 0;
                const LONG border_width = 8; //in pixels
                RECT winrect;
                GetWindowRect(reinterpret_cast<HWND>(winId()), &winrect);

                long x = GET_X_LPARAM(msg->lParam);
                long y = GET_Y_LPARAM(msg->lParam);

                auto resizeWidth = minimumWidth() != maximumWidth();
                auto resizeHeight = minimumHeight() != maximumHeight();

                if (resizeWidth)
                {
                    //left border
                    if (x >= winrect.left && x < winrect.left + border_width)
                    {
                        *result = HTLEFT;
                    }
                    //right border
                    if (x < winrect.right && x >= winrect.right - border_width)
                    {
                        *result = HTRIGHT;
                    }
                }
                if (resizeHeight)
                {
                    //bottom border
                    if (y < winrect.bottom && y >= winrect.bottom - border_width)
                    {
                        *result = HTBOTTOM;
                    }
                    //top border
                    if (y >= winrect.top && y < winrect.top + border_width)
                    {
                        *result = HTTOP;
                    }
                }
                if (resizeWidth && resizeHeight)
                {
                    //bottom left corner
                    if (x >= winrect.left && x < winrect.left + border_width &&
                        y < winrect.bottom && y >= winrect.bottom - border_width)
                    {
                        *result = HTBOTTOMLEFT;
                    }
                    //bottom right corner
                    if (x < winrect.right && x >= winrect.right - border_width &&
                        y < winrect.bottom && y >= winrect.bottom - border_width)
                    {
                        *result = HTBOTTOMRIGHT;
                    }
                    //top left corner
                    if (x >= winrect.left && x < winrect.left + border_width &&
                        y >= winrect.top && y < winrect.top + border_width)
                    {
                        *result = HTTOPLEFT;
                    }
                    //top right corner
                    if (x < winrect.right && x >= winrect.right - border_width &&
                        y >= winrect.top && y < winrect.top + border_width)
                    {
                        *result = HTTOPRIGHT;
                    }
                }

                if (*result != 0)
                    return true;

                auto action = QApplication::widgetAt(QCursor::pos());
                if (action == m_titleBar)
                {
                    *result = HTCAPTION;
                    return true;
                }
                break;
            }

            case WM_GETMINMAXINFO: {
                MINMAXINFO* mmi = reinterpret_cast<MINMAXINFO*>(msg->lParam);

                if (Maximized(msg->hwnd)) 
                {

                    RECT window_rect;

                    if (!GetWindowRect(msg->hwnd, &window_rect))
                        return false;

                    HMONITOR monitor = MonitorFromRect(&window_rect, MONITOR_DEFAULTTONULL);
                    if (!monitor)
                        return false;

                    MONITORINFO monitor_info = { 0 };
                    monitor_info.cbSize = sizeof(monitor_info);
                    GetMonitorInfo(monitor, &monitor_info);

                    RECT work_area = monitor_info.rcWork;
                    RECT monitor_rect = monitor_info.rcMonitor;

                    mmi->ptMaxPosition.x = abs(work_area.left - monitor_rect.left);
                    mmi->ptMaxPosition.y = abs(work_area.top - monitor_rect.top);

                    mmi->ptMaxSize.x = abs(work_area.right - work_area.left);
                    mmi->ptMaxSize.y = abs(work_area.bottom - work_area.top);
                    mmi->ptMaxTrackSize.x = mmi->ptMaxSize.x;
                    mmi->ptMaxTrackSize.y = mmi->ptMaxSize.y;

                    *result = 1;
                    return true;
                }
            }

            case WM_NCACTIVATE: {
                if (!CompositionEnabled()) 
                {
                    // Prevents window frame reappearing on window activation
                    // in "basic" theme, where no aero shadow is present.
                    *result = 1;
                    return true;
                }
                break;
            }

            case WM_SIZE: 
            {
                RECT winrect;
                GetClientRect(msg->hwnd, &winrect);

                WINDOWPLACEMENT wp;
                wp.length = sizeof(WINDOWPLACEMENT);
                GetWindowPlacement(msg->hwnd, &wp);
                if (this)
                {
                    if (wp.showCmd == SW_MAXIMIZE)
                    {
                        ::SetWindowPos(reinterpret_cast<HWND>(winId()), Q_NULLPTR, 0, 0, 0, 0, SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE);
                    }
                }
            }
            default:;
            }
            
            return QWidget::nativeEvent(eventType, message, result);
        }


        void MainWindow::changeEvent(QEvent* event)
        {
            if (event->type() == QEvent::WindowStateChange)
            {
                auto windowStateChangeEvent = static_cast<QWindowStateChangeEvent*>(event);

                if (!(windowStateChangeEvent->oldState() & Qt::WindowMaximized) && 
                    windowState() & Qt::WindowMaximized)
                {
                    m_titleBar->m_maximizeButton->setVisible(false);
                    m_titleBar->m_restoreButton->setVisible(true);
                    emit maximized();
                }
                else
                {
                    m_titleBar->m_maximizeButton->setVisible(true);
                    m_titleBar->m_restoreButton->setVisible(false);
                    emit restored();
                }
            }
            
            QWidget::changeEvent(event);
        }

        auto MainWindow::setBorderless(bool enabled) const -> void
        {
            auto new_style = (enabled) ? SelectBorderlessStyle() : Style::windowed;
            auto old_style = static_cast<Style>(::GetWindowLongPtrW(reinterpret_cast<HWND>(winId()), GWL_STYLE));

            if (new_style != old_style) 
            {
                ::SetWindowLongPtrW(reinterpret_cast<HWND>(winId()), GWL_STYLE, static_cast<LONG>(new_style));

                // redraw frame
                ::SetWindowPos(reinterpret_cast<HWND>(winId()), nullptr, 0, 0, 0, 0, SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE);
                ::ShowWindow(reinterpret_cast<HWND>(winId()), SW_SHOW);
            }
        }

        void MainWindow::maximize()
        {
            setWindowState(Qt::WindowMaximized);
        }

        void MainWindow::minimize()
        {
            setWindowState(Qt::WindowMinimized);
        }

        void MainWindow::restore()
        {
            setWindowState(Qt::WindowNoState);
        }

        bool MainWindow::close()
        {
            setCentralWidget(nullptr);
            return QWidget::close();
        }
    }
}