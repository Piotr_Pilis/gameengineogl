#pragma once
#include <QQuickWindow>
#include <QQuickFramebufferObject>
#include <QSize>
#include <memory>

namespace Editor
{
    namespace Controller
    {
        class SceneRenderer;
    }

    namespace Dialog
    {
        class SceneView : 
            public QQuickFramebufferObject
        {
            Q_OBJECT;
            Q_PROPERTY(QObject* mouseArea WRITE setMouseArea);

            Q_PROPERTY(qreal cameraPositionX READ getCameraPositionX NOTIFY cameraPositionChanged);
            Q_PROPERTY(qreal cameraPositionY READ getCameraPositionY NOTIFY cameraPositionChanged);
            Q_PROPERTY(qreal cameraPositionZ READ getCameraPositionZ NOTIFY cameraPositionChanged);

            Q_PROPERTY(qint32 cameraYaw READ getCameraYaw NOTIFY cameraRotationChanged);
            Q_PROPERTY(qint32 cameraTilt READ getCameraTilt NOTIFY cameraRotationChanged);

        public:
            explicit SceneView(QQuickItem* parent = nullptr);
            ~SceneView();

            QQuickFramebufferObject::Renderer *createRenderer() const override;
            void setMouseArea(QObject* object);

            Q_INVOKABLE qreal getCameraPositionX() const { return m_cameraPositionX; }
            Q_INVOKABLE qreal getCameraPositionY() const { return m_cameraPositionY; }
            Q_INVOKABLE qreal getCameraPositionZ() const { return m_cameraPositionZ; }

            Q_INVOKABLE qint32 getCameraYaw() const;
            Q_INVOKABLE qint32 getCameraTilt() const;

            bool eventFilter(QObject* object, QEvent* event) override;

        public slots:
            void cameraPositionChange(const qreal& x, const qreal& y, const qreal& z);
            void cameraRotationChange(const qreal& yaw, const qreal& tilt);

        signals:
            void mouseMoved(QMouseEvent* event);
            void mouseButtonClicked(QMouseEvent* event);

            void cameraPositionChanged();
            void cameraRotationChanged();

        private:
            QObject* m_mouseAreaObject;

            qreal m_cameraPositionX;
            qreal m_cameraPositionY;
            qreal m_cameraPositionZ;

            qreal m_cameraYaw;
            qreal m_cameraTilt;
        };
    }
}