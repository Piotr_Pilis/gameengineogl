#pragma once
#include <QMainWindow>
#include <QPushButton>
#include <QtQml>
#include <QUrl>

#include "titleBar.hpp"

class QQuickWidget;
class QQmlEngine;

namespace Editor
{
    namespace Dialog
    {
        class MainWindow : public QMainWindow
        {
            Q_OBJECT;

        private:
            MainWindow();

        public:
            static MainWindow& instance();
           
            void setWorkspaceSource(const QUrl& workspaceQmlSource);

            Q_INVOKABLE void setMinimumWidth(const qint32& width);
            Q_INVOKABLE void setMinimumHeight(const qint32& height);

            TitleBar& getTitleBar() { return *m_titleBar; }
            QQmlEngine* getEngine() { return m_engine; }
            

        public slots:
            void maximize();
            void minimize();
            void restore();
            bool close();

        signals:
            void maximized();
            void restored();

            void mouseMoved(QMouseEvent* event);
            void mousePressed(QMouseEvent* event);
            void keyPressed(QKeyEvent* event);
            void keyReleased(QKeyEvent *event);

        protected:
            void closeEvent(QCloseEvent* event) override;
            bool nativeEvent(const QByteArray& eventType, void* message, qintptr* result) override;
            void mouseMoveEvent(QMouseEvent* event) override;
            void mousePressEvent(QMouseEvent* event) override;
            void keyPressEvent(QKeyEvent* event) override;
            void keyReleaseEvent(QKeyEvent* event) override;
            void changeEvent(QEvent* event) override;

        private:
            void initializeWindow();
            void initializeEngine();
            auto setBorderless(bool enabled) const -> void;

        private:
            QQmlEngine* m_engine;
            TitleBar* m_titleBar;
            QQuickWidget* m_workspaceWidget;

            QPushButton* m_iconButton;

        };
    }
}