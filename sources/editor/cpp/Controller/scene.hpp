#pragma once
#include <QOpenGLFramebufferObject>
#include <QString>
#include <QObject>
#include <memory>

namespace Engine
{
    class Engine;
    namespace Map
    {
        class Terrain;
    }
}

namespace Editor
{
    namespace Controller
    {
        class SceneRenderer;
        class Scene:
            public QObject
        {
            Q_OBJECT;

        private:
            Scene() = default;
            ~Scene() = default;

        public:
            static Scene& instance();

            // Begin engine initialization
            static void initializeEngine();

            // Single tick
            void render(const quint32& defaultFramebufferObject);

            // Set up a terrain updater to camera controller,
            //  new canvas size by framebufferObject size
            void updateActiveRenderer(SceneRenderer* renderer);
            bool hasActiveRenderer() const;
            bool isRendererActive(SceneRenderer* renderer) const;

            Q_INVOKABLE void setCameraPostion(const qreal& x, const qreal& y, const qreal& z);
            Q_INVOKABLE void setCameraRotation(const qint32& yaw, const qint32& tilt);

            Q_INVOKABLE qreal getCameraPostionX() const;
            Q_INVOKABLE qreal getCameraPostionY() const;
            Q_INVOKABLE qreal getCameraPostionZ() const;

            Q_INVOKABLE qint32 getCameraYaw() const;
            Q_INVOKABLE qint32 getCameraTilt() const;

            Q_INVOKABLE void setTerrainGridVisibility(bool gridVisibility);

        private:
            // Or reload ...
            void loadTerrain(const QString& path);

        private:
            SceneRenderer* m_activeRenderer = nullptr;
            std::shared_ptr<Engine::Map::Terrain> m_terrain = nullptr;

        };
    }
}