#pragma once
#include <QQmlApplicationEngine>
#include <QScopedPointer>
#include <QApplication>
#include <QObject>

namespace Editor
{
    constexpr auto WINDOW_TITLE = "Game engine editor";

    namespace Dialog
    {
        class MainWindow;
    }

    namespace Controller
    {
        class ApplicationController : public QObject
        {
            Q_OBJECT;

        private:
            ApplicationController(QObject* parent = nullptr);

        public:
            ~ApplicationController();
            static ApplicationController& instance();

            void initialize(QApplication& application);

        private:
            QApplication* m_application;
            QScopedPointer<Editor::Dialog::MainWindow> m_mainWindow;

        };
    }
}