#include "engine.hpp"

#include <QOpenGLFramebufferObject>
#include <QOpenGLFunctions>
#include <QOpenGLContext>
#include <QQuickWindow>
#include <QtMath>

#include "world.hpp"
#include "Transform/cameraControllerAbstract.hpp"
#include "FileSystem/fileSystem.hpp"
#include "Log/warningLog.hpp"
#include "Log/criticalLog.hpp"
#include "Log/glStatus.hpp"
#include "Map/terrain.hpp"

#include "sceneRenderer.hpp"
#include "scene.hpp"

namespace Editor
{
    namespace Controller
    {
        Scene& Scene::instance()
        {
            static Scene instance;
            return instance;
        }

        void Scene::initializeEngine()
        {
            if (!gladLoadGL())
            {
                CRITICAL_LOG(
                    "Cannot initialize Window!", 
                    "Cannot initialize GLAD!"
                );
            }
            else
                LOG(
                    "GLAD initialized"
                );

            Engine::Log::checkCompatibility();

            // -> File system
            {
                Engine::FileSystem::initialize();
                Engine::FileSystem::permitSymbolicLinks(false);
                Engine::FileSystem::loadPath("data");
            }
            // <- File system
        }

        void Scene::updateActiveRenderer(SceneRenderer* renderer)
        {
            if (renderer == nullptr)
            {
                WARNING_LOG(
                    "Renderer cannot be nullptr"
                );
                return;
            }
            auto window = renderer->getWindow();
            if (window->width() <= 0 ||
                window->height() <= 0)
            {
                WARNING_LOG(
                    "Incorrect window size"
                );
                return;
            }

            QSize newViewportSize(
                window->width(),
                window->height()
            );

            // First initialization
            bool firstInitialization = m_activeRenderer == nullptr;
            bool shouldUpdateViewport = firstInitialization;

            // (...) Or new active renderer
            if (shouldUpdateViewport == false)
            {
                shouldUpdateViewport = 
                    m_activeRenderer != renderer;
            }
            
            m_activeRenderer = renderer;
            if (shouldUpdateViewport)
            {       
                if (firstInitialization)
                {
                    Engine::Engine::getInstance().initializeCanvas(window->width(), window->height());
                    loadTerrain("Map000");
                }
                else
                {
                    Engine::Engine::getInstance().reinitializeCanvas(window->width(), window->height());
                }

                Engine::Engine::getInstance().getWorld()->setActiveCameraController(renderer->getCameraController());
                CHECK_GL_ERROR();
            }
        }

        bool Scene::hasActiveRenderer() const
        { 
            return m_activeRenderer != nullptr; 
        }

        bool Scene::isRendererActive(SceneRenderer* renderer) const
        {
            return m_activeRenderer == renderer;
        }

        void Scene::render(const quint32& defaultFramebufferObject)
        {
            // Setup current render state
            auto& openGLFunctions = *QOpenGLContext::currentContext()->functions();
            openGLFunctions.glClearColor(0.19f, 0.19f, 0.19f, 1.f);
            Engine::Engine::getInstance().setTargetFrameBufferObject(defaultFramebufferObject);

            Engine::Engine::getInstance().tick();
        }

        void Scene::loadTerrain(const QString& path)
        {
            m_terrain = std::make_shared<Engine::Map::Terrain>(path.toStdString());
            Engine::Engine::getInstance().getWorld()->setActiveTerrain(m_terrain);
            setTerrainGridVisibility(true);
        }

        void Scene::setCameraPostion(const qreal& x, const qreal& y, const qreal& z)
        {
            if (m_activeRenderer == nullptr)
                return;
            return m_activeRenderer->getCameraController()->setCameraPosition(glm::vec3(x, y, z));
        }

        void Scene::setCameraRotation(const qint32& yaw, const qint32& tilt)
        {
            if (m_activeRenderer == nullptr)
                return;
            qreal yawInRadians = qAbs((yaw % 360) * M_PI / 180.0f);
            qreal tiltInRadians = qAbs((tilt % 360) * M_PI / 180.0f);
            return m_activeRenderer->getCameraController()->setCameraRotation(yawInRadians, tiltInRadians);
        }

        qreal Scene::getCameraPostionX() const
        {
            if (m_activeRenderer == nullptr)
                return 0.0;
            return m_activeRenderer->getCameraController()->getCameraPosition().x;
        }

        qreal Scene::getCameraPostionY() const
        {
            if (m_activeRenderer == nullptr)
                return 0.0;
            return m_activeRenderer->getCameraController()->getCameraPosition().y;
        }

        qreal Scene::getCameraPostionZ() const
        {
            if (m_activeRenderer == nullptr)
                return 0.0;
            return m_activeRenderer->getCameraController()->getCameraPosition().z;
        }

        qint32 Scene::getCameraYaw() const
        {
            if (m_activeRenderer == nullptr)
                return 0;
            return static_cast<qint32>(qAbs(
                m_activeRenderer->getCameraController()->getCameraRotation().x * 180.0f / M_PI
            )) % 360;
        }

        qint32 Scene::getCameraTilt() const
        {
            if (m_activeRenderer == nullptr)
                return 0;
            return static_cast<qint32>(qAbs(
                m_activeRenderer->getCameraController()->getCameraRotation().y * 180.0f / M_PI
            )) % 360;
        }

        void Scene::setTerrainGridVisibility(bool gridVisibility)
        {
            if(m_terrain != nullptr)
            {
                //Engine::Engine::getInstance().setTerrainGridVisibility(gridVisibility);
            }
        }
    }
}