#pragma once
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <QOpenGLFunctions>
#include <QQuickFramebufferObject>
#include <QQuickWindow>
#include <QObject>
#include <QSize>
#include <memory>

namespace Engine
{
    namespace Transform
    {
        class CameraControllerAbstract;
    }
}

namespace Editor
{
    namespace Controller
    {
        class SceneRenderer : 
            public QObject, 
            public QQuickFramebufferObject::Renderer
        {
            Q_OBJECT;

        public:
            SceneRenderer();
            ~SceneRenderer();

            void synchronize(QQuickFramebufferObject *item) override;
            void render() override;
            QOpenGLFramebufferObject* createFramebufferObject(
                const QSize &size
            ) override;

            void setWindow(QQuickWindow* window);
            QQuickWindow* getWindow() const { return m_window; }

            std::shared_ptr<Engine::Transform::CameraControllerAbstract>& 
                getCameraController() { return m_cameraController; }
            const GLuint getFramebufferObjectHandle() const;

        private slots:
            // By Window Events
            void keyPressEvent(QKeyEvent* event);
            void keyReleaseEvent(QKeyEvent* event);

            // By View Events
            void mouseMoveEvent(QMouseEvent* event);
            void mousePressEvent(QMouseEvent* event);

        signals:
            void cameraPositionChanged(const qreal& x, const qreal& y, const qreal& z);
            void cameraRotationChanged(const qreal& yaw, const qreal& tilt);

        private:
            /* Set first active renderer or 
            **  emit signal that window state is changed */
            void updateRendererStateIfNeeded();
            void setupCameraControllerKeys();

            void cameraPositionChange(const glm::vec3& cameraPosition);
            void cameraRotationChange(const glm::vec3& cameraRotation);

        private:
            QQuickWindow* m_window;
            std::shared_ptr<Engine::Transform::CameraControllerAbstract>
                m_cameraController;

        };
    }
}