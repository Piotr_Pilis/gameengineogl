#include "engine.hpp"

#include <boost/signals2/signal.hpp>
#include <boost/bind/bind.hpp>

#include <QOpenGLFramebufferObjectFormat>
#include <QOpenGLFunctions>

#include <glm/gtc/matrix_transform.hpp>

#include "Transform/basicCameraController.hpp"
#include "Definition/buttonEventState.hpp"
#include "FileSystem/fileSystem.hpp"
#include "Log/warningLog.hpp"
#include "Log/criticalLog.hpp"

#include "Dialog/mainWindow.hpp"
#include "Dialog/sceneView.hpp"
#include "sceneRenderer.hpp"
#include "scene.hpp"

namespace Editor
{
    namespace Controller
    {
        SceneRenderer::SceneRenderer() :  
            m_window(nullptr)
        { 
            connect
            (
                &Dialog::MainWindow::instance(), 
                &Dialog::MainWindow::keyPressed,
                this, 
                &SceneRenderer::keyPressEvent
            );
            connect
            (
                &Dialog::MainWindow::instance(), 
                &Dialog::MainWindow::keyReleased,
                this, 
                &SceneRenderer::keyReleaseEvent
            );
        }

        SceneRenderer::~SceneRenderer()
        {
        }

        void SceneRenderer::synchronize(QQuickFramebufferObject* item)
        {
            auto sceneView = dynamic_cast<Dialog::SceneView*>(item);
            if (sceneView == nullptr)
            {
                CRITICAL_LOG(
                    "Cannot initialize scene view!", 
                    "Item is not a SceneView type"
                );
                return;
            }
            connect(sceneView, &Dialog::SceneView::mouseMoved,
                this, &SceneRenderer::mouseMoveEvent);
            connect(sceneView, &Dialog::SceneView::mouseButtonClicked,
                this, &SceneRenderer::mousePressEvent);

            connect(this, &SceneRenderer::cameraPositionChanged,
                sceneView, &Dialog::SceneView::cameraPositionChange);
            connect(this, &SceneRenderer::cameraRotationChanged,
                sceneView, &Dialog::SceneView::cameraRotationChange);

            setWindow(item->window());
        }

        QOpenGLFramebufferObject* SceneRenderer::createFramebufferObject(
            const QSize &size)
        {
            QOpenGLFramebufferObjectFormat format;
            format.setAttachment(QOpenGLFramebufferObject::CombinedDepthStencil);
            format.setSamples(0);
            return new QOpenGLFramebufferObject(size, format);
        }


        void SceneRenderer::setWindow(QQuickWindow* window)
        {
            if (m_window == nullptr)
            {
                m_cameraController = Engine::Transform::BasicCameraController::make
                (
                    window->width(), 
                    window->height()
                );

                auto perspectiveMatrix = glm::perspectiveFov(
                    0.785f,
                    static_cast<GLfloat>(window->width()),
                    static_cast<GLfloat>(window->height()),
                    0.1f, 6000.0f
                );
                perspectiveMatrix[1][1] = -perspectiveMatrix[1][1];
                m_cameraController->getCamera()->setProjectionMatrix(perspectiveMatrix);

                Engine::Transform::BasicCameraController* cameraControllerAsBasic =
                    static_cast<Engine::Transform::BasicCameraController*>(
                        m_cameraController.get());
                cameraControllerAsBasic->setCameraPosition(glm::vec3(0.0f, 0.0f, 40.0f));
                cameraControllerAsBasic->setCameraRotation(-0.7f, 1.5f);
                setupCameraControllerKeys();

                m_cameraController->cameraPositionChanged.connect
                (
                    decltype(m_cameraController->cameraPositionChanged)::slot_type
                    (
                        &SceneRenderer::cameraPositionChange,
                        this,
                        boost::placeholders::_1
                    )
                );
                m_cameraController->cameraRotationChanged.connect
                (
                    decltype(m_cameraController->cameraRotationChanged)::slot_type
                    (
                        &SceneRenderer::cameraRotationChange,
                        this,
                        boost::placeholders::_1
                    )
                );
            }
            m_window = window;
            updateRendererStateIfNeeded();
        }

        void SceneRenderer::render()
        {
            //m_window->openglContext()->makeCurrent(m_window);
            auto& openGLFunctions = *QOpenGLContext::currentContext()->functions();
            openGLFunctions.glViewport(0, 0,
                m_window->width() * m_window->devicePixelRatio(),
                m_window->height() * m_window->devicePixelRatio()
            );
            Scene::instance().render(getFramebufferObjectHandle());
            update();
        }

        void SceneRenderer::setupCameraControllerKeys()
        {
            if (m_cameraController == nullptr)
                return;

            Engine::Transform::BasicCameraController* cameraController =
                static_cast<Engine::Transform::BasicCameraController*>(
                    m_cameraController.get());

            cameraController->setCameraRotationButton(Qt::MouseButton::RightButton);

            cameraController->setCameraMovingButtonBack(Qt::Key::Key_S);
            cameraController->setCameraMovingButtonForward(Qt::Key::Key_W);
            cameraController->setCameraMovingButtonLeft(Qt::Key::Key_A);
            cameraController->setCameraMovingButtonRight(Qt::Key::Key_D);

            cameraController->setCameraMovingButtonTop(Qt::Key::Key_R);
            cameraController->setCameraMovingButtonBottom(Qt::Key::Key_F);
        }

        void SceneRenderer::updateRendererStateIfNeeded()
        {
            // Set first active renderer or 
            //  emit signal that window state is changed
            if (!Scene::instance().hasActiveRenderer() ||
                Scene::instance().isRendererActive(this))
            {
                if (m_window == nullptr)
                {
                    WARNING_LOG(
                        "Window is not assigned to sceneRenderer"
                    );
                    return;
                }
                Scene::instance().updateActiveRenderer(this);
            }
        }

        const GLuint SceneRenderer::getFramebufferObjectHandle() const
        {
            return this->framebufferObject()->handle();
        }

        void SceneRenderer::mouseMoveEvent(QMouseEvent* event)
        {
            m_cameraController->onMousePositionChanged(event->x(), event->y());
        }

        void SceneRenderer::mousePressEvent(QMouseEvent* event)
        { 
            auto actionType = Engine::Definition::ButtonEventState::PRESSED;
            if (event->type() == QEvent::MouseButtonRelease)
                actionType = Engine::Definition::ButtonEventState::RELEASED;
            else if (event->type() != QEvent::MouseButtonPress)
                return;
            m_cameraController->onMouseButtonClicked(event->button(), actionType);
        }

        void SceneRenderer::keyPressEvent(QKeyEvent* event)
        {
            m_cameraController->onKeyClicked(event->key(), 
                Engine::Definition::ButtonEventState::PRESSED);
        }

        void SceneRenderer::keyReleaseEvent(QKeyEvent* event)
        {
            m_cameraController->onKeyClicked(event->key(),
                Engine::Definition::ButtonEventState::RELEASED);
        }

        void SceneRenderer::cameraPositionChange(const glm::vec3& cameraPosition)
        {
            emit cameraPositionChanged(cameraPosition.x, cameraPosition.y, cameraPosition.z);
        }
          
        void SceneRenderer::cameraRotationChange(const glm::vec3& cameraRotation)
        {
            emit cameraRotationChanged(cameraRotation.x, cameraRotation.y);
        }
    }
}