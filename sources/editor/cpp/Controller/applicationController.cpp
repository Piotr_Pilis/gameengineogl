#include <QtQml>
#include <QFile>

#include "../Dialog/mainWindow.hpp"
#include "../Dialog/sceneView.hpp"
#include "scene.hpp"
#include "Log/warningLog.hpp"

#include "applicationController.hpp"

namespace Editor
{
    namespace Controller
    {
        ApplicationController::ApplicationController(QObject* parent) :
            QObject(parent),
            m_application(nullptr)
        {
        }

        ApplicationController::~ApplicationController()
        {
        }

        ApplicationController& ApplicationController::instance()
        {
            static ApplicationController instance;
            return instance;
        }

        void ApplicationController::initialize(QApplication& application)
        {
            if (m_application != nullptr)
            {
                WARNING_LOG("ApplicationController is initialized!");
                return;
            } 
            m_application = &application;           
            Editor::Dialog::MainWindow& mainWindow = Editor::Dialog::MainWindow::instance();

            //Controller::Scene::initializeEngine();

            // Window Style
            QFile file(":/resources/styles/darkStyle.qss");
            file.open(QFile::ReadOnly);
            m_application->setStyleSheet(file.readAll());

            mainWindow.getEngine()->rootContext()->setContextProperty(
                "sceneController", &Controller::Scene::instance()
            );

            qmlRegisterType<Dialog::SceneView>("Editor.Dialogs", 1, 0, "SceneView");

            // Window Sources
            mainWindow.setWorkspaceSource(QUrl(QLatin1String("qrc:/qml/MainWindowWorkspace.qml")));
            auto& titleBar = mainWindow.getTitleBar();
            titleBar.setIcon(QIcon(QPixmap(":/resources/images/icon.ico")), QSize(40, 40));
            mainWindow.setWindowTitle(WINDOW_TITLE);
        }
    }
}