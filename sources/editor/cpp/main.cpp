#include <QApplication>
#include "Controller/applicationController.hpp"

qint32 main(qint32 argc, char* argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QApplication::setAttribute(Qt::AA_UseHighDpiPixmaps);

    QApplication application(argc, argv);
    Editor::Controller::ApplicationController& applicationController =
        Editor::Controller::ApplicationController::instance();
    applicationController.initialize(application);
    return application.exec();
}