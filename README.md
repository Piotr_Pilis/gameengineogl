=======================================================================================

# How to setup project

- Download glfw sources from: http://www.glfw.org/
- Download CmakeGui for Windows and install it
- Run CmakeGui and create Visual Project of glfw (x64)
- Compile GLFW - choose build for AllBuild
- Install GLFW - choose build for Install

Do the same steps for GLI, PhysFS, GLM and Assimp libraries.

- Set system variable 'GLFW3X64_DIR' as path to GLFW(x64) directory:

    1.  Go to Explorer.exe
2.  Open properties of My Computer - PPM -> Properties
3.  Click the Advanced system settings link
4.  Choose Environment Variables
5.  In the New System Variable click New
6.  Set name as: GLFW3X64_DIR, value as: path to GLFW

- Set system variable 'GLIX64_DIR' as path to GLI(x64) directory as above
- Set system variable 'PHYSFSX64_DIR' as path to PhysFS(x64) directory as above
- Set system variable 'GLMX64_DIR' as path to GLM(x64) directory as above
- Set system variable 'ASSIMPX64_DIR' as path to Assimp(x64) directory as above
- Set system variable 'CORRADEX64_DIR' as path to Corrade(x64) directory as above
	Requires: Interconnect, Utility and TestSuite
- Set system variable 'QTDIR' as path to Qt(x64) directory as above
	* Requires: Quick, Widgets, Gui and Core
	* Tested on Qt 5.10.1

- Call script/generate*.bat

**Script parameters**

- GENERATE_EDITOR_SOLUTION(bool, default: true) e.t true
- APPLICATION_NAMES(list, default: *) e.g. app1;app2;app3

**Warning!**

- Libraries of Assimp with release and debug mode will be overwrited during compilation.
- System cannot contains global variable to these libraries like ASSIMP_DIR!

=======================================================================================

# Editor v0.0.1
![](https://bytebucket.org/Piotr_Pilis/gameengineogl/raw/6c1972cb522d72e5b3cca9dac254d43f77531d88/wiki/images/editor001.png)
