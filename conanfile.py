from conan import ConanFile
from conan.tools.cmake import CMakeDeps

class GameEngineOGLConan(ConanFile):
    name = "gameengineogl"
    version = "1.0"
    settings = "os", "compiler", "build_type", "arch"
    requires = [
        "boost/1.76.0",
        "glm/1.0.1",
        "assimp/5.2.2",
        "glfw/3.3.6",
        "magic_enum/0.7.3",
        "fmt/10.2.1",
        "gli/cci.20210515",
        "physfs/3.2.0"
    ]
    generators = "CMakeDeps",
    required_conan_version = ">=2.0.0"
    
    def layout(self):
        self.folders.build = "build" 
        self.folders.generators = "build/build"
