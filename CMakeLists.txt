CMAKE_MINIMUM_REQUIRED(VERSION 3.31.3)
INCLUDE("cmake/common/Solution.cmake")

# TODO: Filtering targets to be generated are currently disabled.
#         In future i want to this target to be root and main apps included this as target

# Solution definition
DEFINE_ROOT_SOLUTION(
    SOLUTION_NAME
        "GameEngineOGL"
    EXTERNAL_PROJECTS_DIR
        "${CMAKE_CURRENT_SOURCE_DIR}/external"
    USE_CONAN_MANAGER 
        TRUE
    CXX_STANDARD
        20
)

### Variables
SET(GANE_ENGINE_CMAKE_DIR "${CMAKE_CURRENT_SOURCE_DIR}/cmake")
SET(ENGINE_SOURCE_DIR "${SOURCE_DIR}/engine")

### Projects
# OpenGL
FILTER_SUBDRIECTORY(DIRECTORY "${SOURCE_DIR}/opengl"
    ACCEPTED_FILTERS
        All
)

# Engine
FILTER_SUBDRIECTORY(DIRECTORY "${SOURCE_DIR}/engine"
    ACCEPTED_FILTERS
        All
)

# Editor
FILTER_SUBDRIECTORY(DIRECTORY "${SOURCE_DIR}/editor"
    ACCEPTED_FILTERS
        All
)

# Unit tests
# TODO

# Samples
# Get subdir list
MACRO(SUBDIRLIST result curdir)
    FILE(GLOB children RELATIVE ${curdir} ${curdir}/*)
    SET(dirlist "")
    FOREACH(child ${children})
    IF(IS_DIRECTORY ${curdir}/${child})
        LIST(APPEND dirlist ${child})
    ENDIF()
    ENDFOREACH()
    SET(${result} ${dirlist})
ENDMACRO()

SET(SAMPLES_DIR ${SOURCE_DIR}/applications)
SUBDIRLIST(ALL_APPLICATION_NAMES ${SAMPLES_DIR})
SET(APPLICATION_NAMES ${ALL_APPLICATION_NAMES})

FOREACH(APPLICATION_NAME IN LISTS APPLICATION_NAMES) 
    FILTER_SUBDRIECTORY(DIRECTORY "${SAMPLES_DIR}/${APPLICATION_NAME}"
        ACCEPTED_FILTERS
            All
    )
ENDFOREACH()

# Makes root solution based on previously defined settings
MAKE_ROOT_SOLUTION()