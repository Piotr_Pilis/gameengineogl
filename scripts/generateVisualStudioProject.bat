@echo off
color 07

set ERROROLEVEL=
set ERROR_COUNT=0

setlocal EnableDelayedExpansion
set "INPUT_ARGUMENTS=%*"

set BUILD_SCRIPT_PATH=%~dp0
set PROJECTS_ROOTDIR=%BUILD_SCRIPT_PATH%\..
set BIN_ROOTDIR=%PROJECTS_ROOTDIR%\bin
set BUILD_ROOTDIR=%PROJECTS_ROOTDIR%\build

:: ---------------------------------------------------------------- Microsoft Visual Studio 2017 Build Search

set VSWHERE="%PROJECTS_ROOTDIR%\thirdParty\vswhere.exe"
if not exist %VSWHERE% (
	  call:ErrorMessage "%VSWHERE% doesn't exist"
)

for /f "usebackq tokens=*" %%i in (`%VSWHERE% -version 17.0 -products * -requires Microsoft.Component.MSBuild -property installationPath`) do (
	set VSINSTALLPATH="%%i"
)

if [%VSINSTALLPATH%] == [] (
	  call::ErrorMessage "Visual Studio has not been found!"
)

for /f "usebackq tokens=*" %%i in (`%VSWHERE% -version 17.0 -requires Microsoft.Component.MSBuild -find MSBuild\**\Bin\MSBuild.exe`) do (
  set VSMSBUILDPATH="%%i"
)

if [%VSMSBUILDPATH%] == [] (
	  call::ErrorMessage "MSBuild has not been found!"
)

if exist "%VSMSBUILDPATH%" (
	set IDE_TYPE=Visual Studio 17 2022
)

pushd .
call %VSINSTALLPATH%\VC\Auxiliary\Build\vcvars64.bat
if "%ERRORLEVEL%" neq "0" (
	call::ErrorMessage "Failed to set Visual Studio environment variables!"
)
popd

:: ---------------------------------------------------------------- Clearing project
echo.
echo Clearing project...
if exist "%BIN_ROOTDIR%" rmdir /s /q "%BIN_ROOTDIR%" & echo %BIN_ROOTDIR% has been removed

:: ---------------------------------------------------------------- Generating with CMake
echo.
echo Generating with CMake

if not exist "%BUILD_ROOTDIR%" md "%BUILD_ROOTDIR%"

pushd "%BUILD_ROOTDIR%"

:: TODO: Release build
cmake -DCMAKE_GENERATOR_PLATFORM=x64 -DCMAKE_BUILD_TYPE=Debug -T ClangCL -G "%IDE_TYPE%" -DCMAKE_EXPORT_COMPILE_COMMANDS=ON "%PROJECTS_ROOTDIR%"

if "%ERRORLEVEL%" neq "0" (
    call:ErrorMessage "CMake generation failed!"
)

:: ---------------------------------------------------------------- Success message
:Finish
echo.
color 02
echo.
echo -------------------------------------- Build succeeded --------------------------------------
echo.
pause
exit /B 0

:: ---------------------------------------------------------------- Error message
:ErrorMessage
color 0C
echo.
echo.
echo -------------------------------------- Error --------------------------------------
echo.
echo ERROR      : %~1
echo ----------------------------------------------------------------------------------
pause
exit /B 1

:: ----------------------------------------------------------------
:end
