@ECHO OFF
SET PROJECTS_ROOTDIR=..
SET BIN_ROOTDIR=%PROJECTS_ROOTDIR%\bin
SET BUILD_ROOTDIR=%PROJECTS_ROOTDIR%\build
SET CONSOLE=%PROJECTS_ROOTDIR%\thirdParty\apps\ansicon\ansicon.exe

:: ---------------------------------------------------------------- Initialization
echo Running unit tests...
IF EXIST %BIN_ROOTDIR%\openWorld\Debug\unitTests.exe (
	SET UNIT_TEST=%BIN_ROOTDIR%\openWorld\Debug\unitTests.exe
) ELSE IF EXIST %BIN_ROOTDIR%\openWorld\Release\unitTests.exe (
	SET UNIT_TEST=%BIN_ROOTDIR%\openWorld\Release\unitTests.exe
) ELSE (
	ECHO File unitTest.exe doesn't exist! First You have to build it!
	PAUSE
	GOTO :EOF
)

CALL %CONSOLE% %UNIT_TEST%
PAUSE