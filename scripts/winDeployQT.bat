@echo off
set ERROROLEVEL=
set ERROR_COUNT=0

setlocal EnableDelayedExpansion

set CONFIG=Debug

SET BUILD_SCRIPT_PATH=%~dp0
set PROJECTS_ROOTDIR=%BUILD_SCRIPT_PATH%\..\..
set BIN_ROOTDIR=%PROJECTS_ROOTDIR%\bin
set BUILD_ROOTDIR=%PROJECTS_ROOTDIR%\build

set EDITOR_SOLUTION_DIR=%PROJECTS_ROOTDIR%\src\editor

:: ---------------------------------------------------------------- Initialization
echo Initializing...
echo.
echo Your conifguration
echo ===============================================================

:: ------------------- Qt
IF "[%QTDIR%]" == "[]" (
	call::ErrorMessage "QTDIR not defined!"
)
echo "QTDIR: %QTDIR%"

echo ===============================================================
echo.
echo.

:: ---------------------------------------------------------------- Deploy

set QML_DIR=%EDITOR_SOLUTION_DIR%\qml

set WINDEPLOYQT_SCRIPT=%QTDIR%\bin\windeployqt.exe
if not exist "%WINDEPLOYQT_SCRIPT%" (
	  call:ErrorMessage "%WINDEPLOYQT_SCRIPT% doesn't exist"	  
)

set EDITOR_EXE=%BIN_ROOTDIR%\editor.exe
if not exist "%EDITOR_EXE%" (
	  call:ErrorMessage "%EDITOR_EXE% doesn't exist"	  
)

"%WINDEPLOYQT_SCRIPT%" "%EDITOR_EXE%" --qmldir "%QML_DIR%"

:: ---------------------------------------------------------------- Success message
:Finish
echo.
color 02
echo.
echo -------------------------------------- Request succeeded --------------------------------------
echo.
pause
exit /B 0

:: ---------------------------------------------------------------- Error message
:ErrorMessage
color 0C
echo.
echo.
echo -------------------------------------- Error --------------------------------------
echo.
echo ERROR      : %~1
echo ----------------------------------------------------------------------------------
pause
exit /B 1

:: ----------------------------------------------------------------
:end