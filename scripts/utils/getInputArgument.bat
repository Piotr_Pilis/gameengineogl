@echo off

if not defined INPUT_ARGUMENTS (
	set "%~2="
	exit /B
)

call set "tmpvar=%%INPUT_ARGUMENTS:*%1=%%"
if "%tmpvar%"=="%INPUT_ARGUMENTS%" (set "%~2=") else (
for /f "tokens=1 delims= " %%a in ("%tmpvar%") do set tmpvar=%%a
set "%~2=!tmpvar:~1!"
)
exit /B